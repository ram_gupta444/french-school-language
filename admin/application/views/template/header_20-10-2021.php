<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>FSL - Admin</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="https://via.placeholder.com/50" type="image/x-icon" />
	<link href="<?PHP echo base_url();?>assets/css/jquery.noty.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>assets/css/noty_theme_default.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>assets/css/select2.css" rel="stylesheet">
	<!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> -->
	<!-- Fonts and icons -->
	<script src="<?php echo base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			//google: {"families":["Lato:300,400,700,900"]},
			custom: {
				"families": [
					"Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
					"simple-line-icons"
				],
				urls: ['<?php echo base_url();?>assets/css/fonts.min.css']
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	
	<script src="<?php echo base_url();?>assets/js/core/jquery-2.1.4.min.js"></script>
	<!-- <script src="<?php echo
        base_url();?>assets/js/core/jquery.3.2.1.min.js"></script> -->
	<script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
	<!-- <script src="<?php echo
        base_url();?>assets/js/jquery.validate.min.js"></script> -->
	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-itara.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/atlantis.css">
	<script src="<?PHP echo base_url();?>assets/js/jquery.noty.js"></script>
	
	<script type="text/javascript" src="<?PHP echo base_url();?>assets/js/jquery-ui.min.js"></script>
	
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.min.css" />
	<script src="<?PHP echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?PHP echo base_url();?>assets/js/bootstrap-timepicker.js"></script>
	<!--timepicker css-->
	<link type="text/css" rel="stylesheet" href="<?PHP echo base_url();?>assets/css/bootstrap-timepicker.css" />
	<!---ckeditor cdn------>
	<script src="https://cdn.ckeditor.com/4.16.0/standard-all/ckeditor.js"></script>
	
</head>
<style>
.nav-logo a img {
    width: 60px;
    height: 60px;
}
</style>
<body>
	<div class="wrapper">
		<!---------------------------------------------------------------------------------------------------->
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header nav-logo" data-background-color="blue2">
				<!-- <a href="index.html" class="logo"> <img
                    src="https://via.placeholder.com/100x35" alt="navbar brand"
                    class="navbar-brand"> </a> -->
				<a href="#/">
					<img src="<?php echo base_url();?>assets/images/logo.svg" alt="navbar brand"
						class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
					data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more">
					<i class="icon-options-vertical"></i>
				</button>
				<!-- <div class="nav-toggle"> <button class="btn btn-toggle toggle-sidebar"> <i
                    class="icon-menu"></i> </button> </div> -->
			</div>
			<!-- End Logo Header -->
			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
				<div class="container-fluid">
					<div class="collapse" id="search-nav">
						<form class="navbar-left navbar-form nav-search mr-md-3">
							<div class="input-group main-search">
								<div class="input-group-prepend">
									<button type="submit" class="btn btn-search pr-1">
										<i class="fa fa-search search-icon"></i>
									</button>
								</div>
								<input type="text" placeholder="Search ..." class="form-control">
							</div>
						</form>
					</div>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button"
								aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"
								aria-expanded="false">
								<div class="avatar-sm">
									<img src="<?php echo base_url();?>assets/images/profile-icon.svg" alt="..."
										class="avatar-img rounded-circle profile-icon">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<a class="dropdown-item" href="<?php echo base_url();?>profile/addEdit">My
											Profile</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="<?PHP echo base_url();?>home/logout">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
		<!--------------------------------------------->
		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<ul class="nav nav-primary">
						<li class="nav-item active">
							<a href="<?php echo base_url();?>bookings" class="itara-btn-black">Bookings</a>
						</li>
						
						<li class="nav-item">
							<a data-toggle="collapse" href="#classes" class="collapsed" aria-expanded="false">
								<img src="<?php echo base_url();?>assets/images/classes.png" alt=""
									class="sidebar-menu-icon">
								<p>All Classes</p>
								<span class="caret"></span>
							</a>
							<!-- for website page active menu code by ram  -->
							<?php $classes  = array('class_detail','class_schedule');?>
							<div class="collapse <?= (in_array($this->uri->segment(1),$classes)?"show":" ")?> " id="classes">
								<ul class="nav nav-collapse">
									<li class="<?= ($this->uri->segment(1) == "class_detail")?"active":"";?>">
										<a href="<?php echo base_url();?>class_detail">
											<span class="sub-item">Class Details</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "class_schedule")?"active":"";?>">
										<a href="<?php echo base_url();?>class_schedule">
											<span class="sub-item">Class Schedule</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						
						<li class="nav-item <?= ($this->uri->segment(1) == "event")?"active":"";?>">
							<a href="<?php echo base_url();?>event">
								<img src="<?php echo base_url();?>assets/images/event.png" alt=""
									class="sidebar-menu-icon">
								<p>All Immersive Events</p>
							</a>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#webpages" class="collapsed" aria-expanded="false">
								<img src="<?php echo base_url();?>assets/images/web-pages.svg" alt=""
									class="sidebar-menu-icon">
								<p>Web Pages</p>
								<span class="caret"></span>
							</a>
							<!-- for website page active menu code by shiv on 19-4-2021  -->
							<?php $website_pages  = array('home','private_tuition','about_us','contact','gallery_image','adult_group_study','adult_program','children_group_study','children_program');?>
							<div class="collapse <?= (in_array($this->uri->segment(1),$website_pages)?"show":" ")?> " id="webpages">
								<ul class="nav nav-collapse">
									<li class="<?= ($this->uri->segment(1) == "home")?"active":"";?>">
										<a href="<?php echo base_url();?>home">
											<span class="sub-item">Homepage</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "private_tuition")?"active":"";?>">
										<a href="<?php echo base_url();?>private_tuition">
											<span class="sub-item">Private tution</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "about_us")?"active":"";?>">
										<a href="<?php echo base_url();?>about_us">
											<span class="sub-item">About us</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "contact")?"active":"";?>">
										<a href="<?php echo base_url();?>contact">
											<span class="sub-item">Contact us</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "gallery_image")?"active":"";?>">
										<a href="<?php echo base_url();?>gallery_image">
											<span class="sub-item">Gallery Master</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "adult_group_study")?"active":"";?>">
										<a href="<?php echo base_url();?>adult_group_study">
											<span class="sub-item">group learning</span>
										</a>
									</li>
									<li class="<?= ($this->uri->segment(1) == "adult_program")?"active":"";?>">
										<a href="<?php echo base_url();?>adult_program">
											<span class="sub-item">Emmersive Events</span>
										</a>
									</li>
									<!-- <li class="<?= ($this->uri->segment(1) == "children_group_study")?"active":"";?>">
										<a href="<?php echo base_url();?>children_group_study">
											<span class="sub-item">Children group study</span>
										</a>
									</li> -->
									<!-- <li class="<?= ($this->uri->segment(1) == "children_program")?"active":"";?>">
										<a href="<?php echo base_url();?>children_program">
											<span class="sub-item">Childeren immersive progam</span>
										</a>
									</li> -->
								</ul>
							</div>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "testimonial")?"active":"";?>">
							<a href="<?php echo base_url();?>testimonial">
								<img src="<?php echo base_url();?>assets/images/customers.svg" alt=""
									class="sidebar-menu-icon">
								<p>Testimonial</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "front_user")?"active":"";?>">
							<a href="<?php echo base_url();?>front_user">
								<img src="<?php echo base_url();?>assets/images/profile.svg" alt=""
									class="sidebar-menu-icon">
								<p>Registered User</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "users")?"active":"";?>">
							<a href="<?php echo base_url();?>users">
								<img src="<?php echo base_url();?>assets/images/profile.svg" alt=""
									class="sidebar-menu-icon">
								<p>Admin User</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "profile")?"active":"";?>">
							<a href="<?php echo base_url();?>profile/addEdit">
								<img src="<?php echo base_url();?>assets/images/profile.svg" alt=""
									class="sidebar-menu-icon">
								<p>Profile</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "messages")?"active":"";?>">
							<a href="<?php echo base_url();?>messages">
								<img src="<?php echo base_url();?>assets/images/message.svg" alt=""
									class="sidebar-menu-icon">
								<p>Messages list</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "counts")?"active":"";?>">
							<a href="<?php echo base_url();?>counts">
								<img src="<?php echo base_url();?>assets/images/number.svg" alt=""
									class="sidebar-menu-icon">
								<p>Counts</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "our_program")?"active":"";?>">
							<a href="<?php echo base_url();?>our_program">
								<img src="<?php echo base_url();?>assets/images/number.svg" alt=""
									class="sidebar-menu-icon">
								<p>Our Program</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "testimonial_video")?"active":"";?>">
							<a href="<?php echo base_url();?>testimonial_video">
								<img src="<?php echo base_url();?>assets/images/number.svg" alt=""
									class="sidebar-menu-icon">
								<p>Testimonial video</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "email_template")?"active":"";?>">
							<a href="<?php echo base_url();?>email_template">
								<img src="<?php echo base_url();?>assets/images/number.svg" alt=""
									class="sidebar-menu-icon">
								<p>Email Templates</p>
							</a>
						</li>
						<li class="nav-item <?= ($this->uri->segment(1) == "language_master")?"active":"";?>">
							<a href="<?php echo base_url();?>language_master">
								<img src="<?php echo base_url();?>assets/images/number.svg" alt=""
									class="sidebar-menu-icon">
								<p>Language Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?PHP echo base_url();?>home/logout" ">
                                    <img
                                        src=" <?php echo base_url();?>assets/images/logout.svg" alt=""
								class="sidebar-menu-icon">
								<p>Logout</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->