</div>
<!--   Core JS Files   -->
	<!-- <script src="<?php echo base_url();?>assets/js/core/jquery.3.2.1.min.js"></script> -->
	
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>

	<script src="<?php echo base_url();?>assets/js/core/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="<?php echo base_url();?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="<?php echo base_url();?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>

	<!-- Moment JS -->
	<script src="<?php echo base_url();?>assets/js/plugin/moment/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="<?php echo base_url();?>assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="<?php echo base_url();?>assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="<?php echo base_url();?>assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="<?php echo base_url();?>assets/js/plugin/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.rowReordering.js"></script>
	<!-- <script src="//mpryvkin.github.io/jquery-datatables-row-reordering/1.2.3/jquery.dataTables.rowReordering.js"></script> -->

	<!-- Bootstrap Notify -->
	<script src="<?php echo base_url();?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- Bootstrap Toggle -->
	<script src="<?php echo base_url();?>assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="<?php echo base_url();?>assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Google Maps Plugin -->
	<script src="<?php echo base_url();?>assets/js/plugin/gmaps/gmaps.js"></script>

	<!-- Dropzone -->
	<script src="<?php echo base_url();?>assets/js/plugin/dropzone/dropzone.min.js"></script>

	<!-- Fullcalendar -->
	<script src="<?php echo base_url();?>assets/js/plugin/fullcalendar/fullcalendar.min.js"></script>

	<!-- DateTimePicker -->
	<script src="<?php echo base_url();?>assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>

	<!-- Bootstrap Tagsinput -->
	<script src="<?php echo base_url();?>assets/js/plugin/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

	<!-- Bootstrap Wizard -->
	<script src="<?php echo base_url();?>assets/js/plugin/bootstrap-wizard/bootstrapwizard.js"></script>

	<!-- jQuery Validation -->
	<!-- <script src="<?php echo base_url();?>assets/js/plugin/jquery.validate/jquery.validate.min.js"></script> -->

	<!-- Summernote -->
	<script src="<?php echo base_url();?>assets/js/plugin/summernote/summernote-bs4.min.js"></script>

	<!-- Select2 -->
	<script src="<?php echo base_url();?>assets/js/plugin/select2/select2.full.min.js"></script>

	<!-- Sweet Alert -->
	<script src="<?php echo base_url();?>assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Owl Carousel -->
	<script src="<?php echo base_url();?>assets/js/plugin/owl-carousel/owl.carousel.min.js"></script>

	<!-- Magnific Popup -->
	<script src="<?php echo base_url();?>assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Atlantis JS -->
	<script src="<?php echo base_url();?>assets/js/atlantis.min.js"></script>
	
	<script>
		$('.basic-datatables').DataTable({
			"order": [[ 0, "desc" ]],
			});
			$('[data-toggle="tooltip"]').tooltip({
   container: 'body'
});

function displayMsg(type,msg)
{
	$.noty({
		text:msg,
		layout:"topRight",
		type:type
	});
}
function setTabIndex(){
	var tabindex = 1;
	$('input,select,textarea,.icon-plus,.icon-minus,button,a').each(function() {
		if (this.type != "hidden") {
			var $input = $(this);
			$input.attr("tabindex", tabindex);
			tabindex++;
		}
	});
}
$(function(){
	setTabIndex();
	$(".select2").each(function(){
		$(this).select2({
			placeholder: "Select",
			allowClear: true
		});
		$("#s2id_"+$(this).attr("id")).removeClass("searchInput");
	});
	$(".dataTables_filter input.hasDatepicker").change( function () {				
		oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
	});			
	window.scrollTo(0,0);
});
$(document).ready(function() {
		$('#OpenImgUpload').click(function() {
			$('#imgupload').trigger('click');
		});		
	})
	/*Preview profile pic*/
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#profile-pic-avatar')
					.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	</script>
	
</body>
</html>