<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Messages extends CI_Controller {

function __construct(){
	parent::__construct();
	$this->load->model('messagesmodel','',TRUE);
	$this->load->model('common_model/common_model','common',TRUE);
	
}
function index(){
	if(!empty($_SESSION["fls_admin"])){
		//print_r($result);exit;
		$result['messages_data'] = $this->messagesmodel->getMessages();
		// print_r($result['users_data']);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}else{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
}

}

?>
