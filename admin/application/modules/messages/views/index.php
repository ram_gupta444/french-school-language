<div class="main-panel">
    <div class="container">
        <div class="page-inner">
            <div class="page-header page-header-btn">
                <div class="page-header-title">
                    <!-- <h4 class="page-title"><a href="#/"><img src="assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Users</h4> -->
                    <h4 class="page-title">Footer Form Email IDs</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#"><i class="flaticon-home"></i></a>
                        </li>
                        <li class="separator"><i class="flaticon-right-arrow"></i></li>
                        <li class="nav-item">
                        <li><a href="<?php echo base_url(); ?>messages">Messages</a></li>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>Email ID</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <th>#</th>
                                <th>Message</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                if (!empty($messages_data)) {
                                    foreach ($messages_data as $key => $value) { ?>
                                        <tr>
                                            <td><?= $value['messages_id'] ?></td>
                                            <td><?= $value['message'] ?></td>
                                        </tr>
                                <?php }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	
<script>

	document.title = "Messages";
</script>