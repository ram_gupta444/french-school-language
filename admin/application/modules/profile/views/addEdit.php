<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header">
				<h4 class="page-title"><a href="#/"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Profile</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Profile</a>
					</li>
				</ul>
			</div>
			<div class="row">
				<div class="col-12">
					<ul class="nav nav-itara-pills" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="password-tab" data-toggle="pill" href="#password" role="tab" aria-controls="password" aria-selected="false">Password</a>
						</li>
					</ul>
					<div class="tab-content mt-2 mb-3" id="pills-tabContent">
						<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-8 col-lg-6">
									<form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
										<div class="section-single last mt-4">
											<h3>Personal Info</h3>
											<div class="form-group">
												<div class="profile-pic-wrapper mb-2">
													<img id="profile-pic-avatar" src="<?= (!empty($profile_details[0]['profile_photo']) ? FRONT_URL."/images/profile_image/".$profile_details[0]['profile_photo']  : base_url("assets/images/profile-placeholder.svg")) ?>"/>
													<input type="file" id="imgupload" name="imgupload" accept=".jpg,.png,.jpeg,.svg,.gif" onchange="readURL(this);" style="display:none" />	
													<a href="#/" class="profile-upload-btn" id="OpenImgUpload"><img src="<?php echo base_url();?>assets/images/profile-add.svg" /></a>
													<a href="javasript:void(0)" class="btn btn-primary btn-xs" onclick="delete_profile();">Delete</a>
												</div>																						
											</div>
											<input type="hidden" id="hidden_user_id" name="hidden_user_id" value="<?php if(!empty($_SESSION["fls_admin"][0]->user_id)){echo $_SESSION["fls_admin"][0]->user_id;}?>" />
											<div class="form-group">
												<label for="">Full Name</label>
												<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Enter Full Name" value="<?= (!empty($profile_details[0]['full_name'])) ? $profile_details[0]['full_name'] : '' ?>">
											</div>
											<div class="form-group">
												<label for="">Gender</label>
												<select name="gender" id="gender" class="form-control">
												<option value="">Select gender</option>
												<?php 
													if(isset($genders) && !empty($genders)){
														foreach($genders as $cdrow){
															$sel='';
															if(!empty($profile_details[0]['gender'])){
																$sel = ($cdrow == $profile_details[0]['gender'])? 'selected="selected"' : '';
															} ?>
													<option value="<?php echo $cdrow;?>" <?php echo $sel; ?>><?php echo $cdrow;?></option>
												<?php }}?>
												</select>
											</div>
											<div class="form-group">
												<label for="">Date of Birth</label>
												<input type="text" class="form-control required datepicker" id="dob" name="dob" placeholder="dd/mm/yyyy" value="<?php if(!empty($profile_details[0]['dob'])){echo date('d-m-Y', strtotime($profile_details[0]['dob']));}?>">
											</div>
											<div class="form-group">
												<label for="">Email</label>
												<input type="email" class="form-control" id="email_id" name="email_id" placeholder="eg: johndoe@xyz.com" value="<?= (!empty($profile_details[0]['email_id'])) ? $profile_details[0]['email_id'] : '' ?>">
											</div>
											<div class="form-group">
												<label for="">Phone</label>
												<input type="text" class="form-control" id="phone" name="phone" placeholder="eg: +91- 123-456-7890" value="<?= (!empty($profile_details[0]['phone'])) ? $profile_details[0]['phone'] : '' ?>">
											</div>
											<div class="form-group">
												<a href="#" class="btn btn-light-itara">Discard</a>
												<button type="submit" class="btn btn-dark-itara">Save</a>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-8 col-lg-6">
									<form class="form-horizontal" id="form-password" method="POST" enctype="multipart/form-data">
										<div class="section-single last mt-4">
											<h3>Password</h3>
											<div class="form-group">
												<label for="">Current Password</label>
												<input type="password" class="form-control" id="current_password" name="current_password" placeholder="Enter Current Password">
											</div>
											<div class="form-group">
												<label for="">New Password</label>
												<input type="password" class="form-control" id="new_password" name="new_password" placeholder="Enter New Password">
											</div>
											<div class="form-group">
												<label for="">Confirm Password</label>
												<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Re-enter New Password">
											</div>
											<div class="form-group">
												<a href="#" class="btn btn-light-itara">Discard</a>
												<button type="submit" class="btn btn-dark-itara">Save</a>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var vRules = {
	full_name:{required:true},
	gender:{required:true},
	dob:{required:true},
	// email_id:{required:true},
	email_id:{required:true, email:true,remote: {
							url:"<?php echo base_url($this->router->fetch_module().'/dataEmailExist');?>" ,
							type: "post",
							data: {email_id: function() {return $("#email_id").val();},
							user_id: function() {return $("#hidden_user_id").val();
								},
						  }}},
	phone:{required:true},
};
var vMessages = {
	full_name:{required:"Please enter name."},
	gender:{required:"Please select gender."},
	dob:{required:"Please enter dob."},
	// email_id:{required:"Please enter email."},
	email_id:{required:"Please Enter the Email Id",email:"Please enter correct email id",remote:"Email ID already exist.."},
	phone:{required:"Please enter phone number."},
};
$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>profile/submitUserForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			// dataType: 'json',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>profile/addEdit";
					},2000);
				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
		});
	}
});
var vRules = {
	current_password:{required:true},
	new_password:{required:true},
	confirm_password:{required:true},
};
var vMessages = {
	current_password:{required:"Please enter current password."},
	new_password:{required:"Please enter new password."},
	confirm_password:{required:"Please enter confirm password."},
};
$("#form-password").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>profile/submitPasswordForm";
		$("#form-password").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>profile/addEdit";
					},2000);
				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
		});
	}
});
function delete_profile(){
	$.ajax({
		url:"<?php echo base_url();?>profile/profilePicDelete",
		dataType: 'json',
		type: "post",
		// data:{status:1},
		// cache: false,
		success: function (response) {
			// console.log(response.msg);
			if(response.success == "1")
			{
				displayMsg("success",response.msg);
				setTimeout(function(){
						window.location = "<?php echo base_url();?>profile/addEdit";
					},2000);
			}
			else
			{	
				displayMsg("error",response.msg);
				return false;
			}
		}
	});
}
$(".datepicker").datepicker({
	format: 'dd-mm-yyyy',
	todayHighlight: true,
});
</script>