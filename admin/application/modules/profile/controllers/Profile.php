<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Profile extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('profilemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$result =array();
			// echo "<pre>"; print_r($_SESSION["fls_admin"][0]->user_id);exit;
			$result['profile_details'] = $this->profilemodel->getFormdata($_SESSION["fls_admin"][0]->user_id);
			// echo "<pre>"; print_r($result);exit;
			$result['genders'] = $this->profilemodel->enum_select("tbl_users","gender");
			// echo "<pre>"; print_r($result['gender']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
    }
	function submitUserForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['full_name'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter full name'));
				exit;
			}
			if(!isset($_POST['gender'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select gender'));
				exit;
			}
			if(!isset($_POST['email_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter email'));
				exit;
			}
			if(!isset($_POST['phone'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter phone number'));
				exit;
			}
			$data = array();
			$data['full_name'] = $_POST['full_name'];
			$data['gender'] = $_POST['gender'];
			$data['email_id'] = $_POST['email_id'];
			$data['phone'] = $_POST['phone'];
			$data['dob'] = date('Y-m-d', strtotime($_POST['dob']));
			$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
			$result = $this->profilemodel->updateRecord('tbl_users', $data,'user_id',$_SESSION["fls_admin"][0]->user_id);
			$path = DOC_ROOT_FRONT."/images/profile_image/";
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
			if(!empty($_FILES['imgupload']))
			{
				$_FILES['image_name']['name'] =  'p'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['imgupload']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['imgupload']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['imgupload']['error'];
				$_FILES['image_name']['type'] =  $_FILES['imgupload']['type'];
				$_FILES['image_name']['size'] =  $_FILES['imgupload']['size'];
				/* File Type Change multiple into single */
				$config = array();
				$config['upload_path'] = $path;
				$config['max_size'] = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload');
				$this->upload->initialize($config);
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$subdata['profile_photo'] = $file_data['upload_data']['file_name'];
				}
				$this->profilemodel->updateRecord('tbl_users', $subdata,'user_id',$_SESSION["fls_admin"][0]->user_id);
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function submitPasswordForm()
	{
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['current_password'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter current password'));
				exit;
			}
			if(!isset($_POST['new_password'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter new password'));
				exit;
			}
			if(!isset($_POST['confirm_password'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter confirm password'));
				exit;
			}
			$condition = "user_id ='".$_SESSION["fls_admin"][0]->user_id."' ";
			$result = $this->common->getData("tbl_users",'*',$condition);
			if($result[0]['password'] != MD5($_POST['current_password'])){
				echo json_encode(array("success"=>"0",'msg'=>'current password does not match'));
				exit;
			}
			if($_POST['new_password'] != $_POST['confirm_password']){
				echo json_encode(array("success"=>"0",'msg'=>'New password and confirm password does not match'));
				exit;
			}
			if(!empty($result)){
				$data = array();
				$data['password'] = MD5($_POST['new_password']);
				$result = $this->profilemodel->updateRecord('tbl_users', $data,'user_id',$_SESSION["fls_admin"][0]->user_id);
				echo json_encode(array("success"=>"1",'msg'=>'Password updated successfully!'));
				exit;
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
				exit;
			}	
		}
	}
	function profilePicDelete(){
		$condition = "user_id ='".$_SESSION["fls_admin"][0]->user_id."' ";
		$result = $this->common->getData("tbl_users",'*',$condition);
		if(!empty($result[0]['profile_photo'])){
			$path = DOC_ROOT_FRONT."/images/profile_image/".$result[0]['profile_photo'];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data['profile_photo'] = '';
			$result = $this->profilemodel->updateRecord('tbl_users', $data,'user_id',$_SESSION["fls_admin"][0]->user_id);
			echo json_encode(array("success"=>"1",'msg'=>'Profile Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }
	function dataEmailExist(){
		// print_r($_POST);
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			// echo "hello";
			// exit;
		   $condition = array("email_id"=>$_POST['email_id']);
		   if(!empty($_POST['user_id']) &&  isset($_POST['user_id'])){
			   $condition['user_id <>'] = $_POST['user_id'];
		   }
		//    print_r($condition);
		//    exit();
		   $result=$this->profilemodel->getDatas("tbl_users","*",$condition);
		   if($result > 0){
			   echo  json_encode(FALSE);
		   }else{
			   echo  json_encode(TRUE);
		   }
		}
	  }
}