<?PHP
class Profilemodel extends CI_Model
{
    function getFormdata($ID){
        $this -> db -> select('u.*');
        $this -> db -> from('tbl_users as u');
        $this -> db -> where('u.user_id ', $ID);
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
          return $query->result_array();
        }
        else{
          return false;
        }
     }
     function enum_select($table, $field){
      $query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
      $row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
      $regex = "/'(.*?)'/";
      preg_match_all( $regex , $row, $enum_array );
      $enum_fields = $enum_array[1];
      return( $enum_fields );
    }
    function updateRecord($tableName, $data, $column, $value)
    {
      $this->db->where("$column", $value);
      $this->db->update($tableName, $data);
      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else {
        return true;
      }
    } 
    function getDatas($table,$select = "*", $condition = '', $order_by = "",$order = ""){
      $this->db->select($select);
      $this->db->from($table);
      if(!empty($condition)){ 
        $this->db->where($condition);
      }	
      if(!empty($order_by) && !empty($order)){ 
        $this->db->order_by($order_by, $order);
      }
        $query = $this->db->get();
        // echo "<pre>";
        // print_r($query);
        // echo $this->db->last_query();
      // exit();
      if ($query->num_rows() > 0) {
        return $query->result_array();
      } else {
        return false;
      }
    }
}
?>
