<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Testimonial_video extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('testimonial_videomodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['testimonials_details'] = $this->common->getData("tbl_testimonial_video",'*',$condition);
		$condition = "status = 'Active'  ";
		$result['language_dropdown'] = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 

		
		// $result['stored_langauges'] = $this->common->getData("tbl_testimonial_video","languages"," testimonial_id = ".$testimonial_id."  ");

		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!empty($_POST['edit_exist'])){
				if(!empty($_POST['title'])){
					foreach($_POST['title_existing'] as $fkey=>$fval){
						if(!empty($_POST['title_existing_id'][$fkey])){
							$count_data = array();
							$count_data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
							$count_data['updated_on'] = date("Y-m-d H:i:s");
							$count_data['title'] = $_POST['title'][$fkey];
							$count_data['url'] = (!empty($_POST['url'][$fkey])) ? $_POST['url'][$fkey] : '';
							$count_data['languages'] = (!empty($_POST['languages'][$fkey])) ? implode(',',$_POST['languages'][$fkey]) : '';
							
							$result = $this->testimonial_videomodel->updateRecord('tbl_testimonial_video', $count_data,'testimonial_video_id',$_POST['title_existing_id'][$fkey]);
						}else{
							$new_data = array();
							$new_data['title'] = (!empty($_POST['title'][$fkey])) ? $_POST['title'][$fkey] : '';
							$new_data['url'] = (!empty($_POST['url'][$fkey])) ? $_POST['url'][$fkey] : '';
							$new_data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
							$new_data['created_on'] = date("Y-m-d H:i:s");
							$new_data['languages'] = (!empty($_POST['languages'][$fkey])) ? implode(',',$_POST['languages'][$fkey]) : '';
							$result = $this->testimonial_videomodel->insertData("tbl_testimonial_video",$new_data,"1");
							}
						}
					}
				
				}else{
					if(!empty($_POST['title']))
					{
						foreach ($_POST['title'] as $key => $value) {
							$data['title'] = $value;
							$data['url'] = $_POST['url'][$key];
							$data['created_on'] = date("Y-m-d H:i:s");
							$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;	
							$data['languages'] = (!empty($_POST['languages'][$key])) ? implode(',',$_POST['languages'][$key]) : '';
							$result = $this->testimonial_videomodel->insertData('tbl_testimonial_video',$data,'1');
						}
					}
				}

			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	function deleteCount(){
		$condition = "testimonial_video_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_testimonial_video",'*',$condition);
		if(!empty($result)){
			$result = $this->testimonial_videomodel->delrecord('tbl_testimonial_video', 'testimonial_video_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Row Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}

?>
