<div class="main-panel">
    <div class="container">
        <form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
            <div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h4 class="page-title"><a href="#/"><img
                                    src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt=""
                                    class="back-icon-title"></a> Testimonial videos</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <a href="#" class="btn btn-light-itara">Discard</a>
                        <button type="submit" class="btn btn-dark-itara">Save</a>
                    </div>
                </div>
                <input type="hidden" id="edit_exist" name="edit_exist" value="<?= (!empty($testimonials_details[0]['testimonial_video_id'])) ? $testimonials_details[0]['testimonial_video_id'] : '' ?>">
                <hr>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-10">
                        <div class="section-single">
                            <?php if(empty($testimonials_details)){?>
                                <h3>Testimonial videos</h3>
                                    <table class="table  product_location1 table-bordered table-striped disable_ testimonial-video-table" id="admore_inner_table_image_0">
                                        <tbody >
                                            <tr>
                                            <th>Language</th>
                                            <th>Label</th>
                                            <th>Youtube URL</th>
                                            <th>Action</th>
                                            </tr>
                                          
                                            <tr tr_inner_count_image="0" class="active">
                                                <td>
                                                <!-- <p style="margin:5px 0;">Language</p> -->
                                                <select class="form-control form-control select2" id="languages" name="languages[]" multiple>
                                                <?php if(!empty($language_dropdown)){ ?>
                                                    <option value="">Select language</option>
                                                            <?php 
                                                            foreach($language_dropdown as $languagekey=>$val){
                                                                $sel='';
                                                                $sel= (in_array($val['language_master_id'],explode(',',$value['languages']))?'selected':'');
                                                            ?>
                                                            <option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>
                                                        <?php } 
                                                            }
                                                        ?>
                                                </select>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="title_existing[0]" value="" id="title_existing_0">

                                                    <input type="hidden" name="title_existing_id[0]" value="" id="title_existing_id_0">
													<!-- <p style="margin:5px 0;">Label</p> -->
                                                    <input type="text" class="form-control required" name="title[0]" id="title0" placeholder="Enter title">
                                                </td>
                                                <td>
                                                    <!-- <p style="margin:5px 0;">Youtube URL</p> -->
                                                    <input type="text" class="form-control required" name="url[0]" value="" id="url_0" placeholder="Enter youtube url">
                                                </td>
                                                <td>
                                                    <button class="btn btn-xs add_more_inner_image" panel-count_image="0" type="button" style="padding:5px;"><i class="material-icons" style="font-size:15px;" ><img src="<?php echo base_url();?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            
                                <?php }else{ ?>
                                    <h3>Testimonial videos</h3>

                                    <table class="table  product_location1 table-bordered table-striped disable_ testimonial-video-table" id="admore_inner_table_image_0">
                                        <tbody >
                                        
                                        <?php foreach ($testimonials_details as $key => $value) { ?>
                                            <tr tr_inner_count_image="<?= $key?>" class="active">
                                            <td>
                                                <p style="margin:5px 0;">Language</p>
                                                <select class="form-control form-control select2" id="languages_<?= $key?>" name="languages[<?= $key?>][]" multiple>
                                                <?php if(!empty($language_dropdown)){ ?>
                                                    <option value="">Select language</option>
                                                            <?php 
                                                            foreach($language_dropdown as $languagekey=>$val){
                                                                $sel='';
                                                                $sel= (in_array($val['language_master_id'],explode(',',$value['languages']))?'selected':'');
                                                            ?>
                                                            <option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>
                                                        <?php } 
                                                            }
                                                        ?>
                                                </select>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="title_existing[<?= $key ?>]" value="<?= (!empty($value['title']) ? $value['title'] : '' )?>" id="title_existing_<?= $key ?>">

                                                    <input type="hidden" name="title_existing_id[<?= $key ?>]" value="<?= (!empty($value['testimonial_video_id']) ? $value['testimonial_video_id'] : '' )?>" id="title_existing_id_<?= $key ?>">
													                          <p style="margin:5px 0;">Person Name</p>	
                                                    <input type="text" class="form-control <?= (!empty($value['title'])) ? '' :'required'?>" name="title[<?= $key ?>]" id="title<?= $key ?>" value="<?=(!empty($value['title'])) ? $value['title'] : '' ?>">
                                                </td>
                                                <td>
                                                    <p style="margin:5px 0;">Youtube URL</p>
                                                    <input type="text" class="form-control <?= (!empty($value['url'])) ? '' :'required'?>" name="url[<?= $key ?>]" value="<?=(!empty($value['url'])) ? $value['url'] : '' ?>" id="url_<?= $key ?>" >
                                                </td>
                                                <td>
                                                <?php if($key =='0'){?>
                                                <button class="btn btn-xs  add_more_inner_image" panel-count_image="<?php echo $key;?>" type="button" style="padding:5px;"><i class="material-icons" style="font-size:15px;" ><img src="<?php echo base_url();?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                                                <?php }else{?>
                                                    
                                                    <?php if($key != '0'){ ?>
                                                    <a href="#/" class="btn btn-secondary btn-xs" onclick="deleteCount(<?= (!empty($value['testimonial_video_id'])) ? $value['testimonial_video_id'] : '' ?>);">Delete</a>
                                                    <?php } ?>
                                            <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                            
                                        </tbody>
                                    </table>
                                <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$(document).on("click",".add_more_inner_image",function(){
	var xi = $(this).attr("panel-count_image");
	var txi = $("#admore_inner_table_image_"+xi+" tbody tr:last-child").attr("tr_inner_count_image");
	txi++;
	if(isNaN(txi) || txi == "undefined"){
		txi = 0;
	}
	// if(txi ==4){
	// 	alert("Only 4 Product Variant Images Allowed");
	// 	return false;
	// };
	var inner_tr_image = '<tr tr_inner_count_image="'+txi+'" class="active">'+
                            '<td>'+
                            '<p style="margin:5px 0;">Language</p>'+
                            '<select class="form-control form-control select2" id="languages_'+txi+'" name="languages['+txi+'][]" multiple>'+
                            <?php if(!empty($language_dropdown)){ ?>
                                '<option value="">Select language</option>'+
                                        <?php 
                                        foreach($language_dropdown as $languagekey=>$val){
                                            $sel='';
                                            $sel= (in_array($val['language_master_id'],explode(',',$value['languages']))?'selected':'');
                                        ?>
                                        '<option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>'+
                                    <?php } 
                                        }
                                    ?>
                            '</select>'+
                            '</td>'+
							'<td>'+
                                '<input type="hidden" name="title_existing['+txi+']" value="" id="title_existing_['+txi+']">'+

								'<input type="hidden" name="title_existing_id['+txi+']" value="" id="title_existing_id_['+txi+']">'+
								'<p style="margin:5px 0;">Person Name</p>'+
								'<input type="text" class="form-control required" name="title['+txi+']" id="title'+txi+'" placeholder="Enter title">'+
							'</td>'+
							'<td>'+								'<p style="margin:5px 0;">Youtube URL</p>'	+					
								'<input type="text" class="form-control required" name="url['+txi+']" value="" id="url_['+txi+']" placeholder="Enter youtube url">'+
							'</td>'+
							'<td>'+
								'<button title="Remove" class=" btn btn-xs remove inner_remove_image" type="button">'+
									'<i class="material-icons"><img src="<?= base_url()?>assets/images/minus-outline.svg" style="width:30px"></i>'+
								'</button>'+
							'</td>'+
						'</tr>';
	$("#admore_inner_table_image_"+xi + " tbody").append(inner_tr_image);
    $("select").select2();
});

$(document).on("click",'.inner_remove_image', function(){
	var tr_inner_count_image=$(this).closest('tr').attr('tr_inner_count_image');
			if(tr_inner_count_image!= 0)
			{
				$(this).closest('tr').remove();
			}else{
				swal({title: "Alert!",text: "<p class='font-bold col-red'>Atleast one option is required..</p>",timer: 2000,showConfirmButton: false,html: true});
			}
});


$("#form-validate").validate({
    // rules: vRules,
    // messages: vMessages,
    submitHandler: function (form) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var act = "<?php echo base_url();?>testimonial_video/submitForm";
        $("#form-validate").ajaxSubmit({
            url: act,
            type: 'post',
            cache: false,
            clearForm: false,
            success: function (response) {
                var res = eval('(' + response + ')');
                if (res['success'] == "1") {
                    displayMsg("success", res['msg']);
                    setTimeout(function () {
                        window.location = "<?php echo base_url();?>testimonial_video/index";
                    }, 2000);
                } else {
                    displayMsg("error", res['msg']);
                    return false;
                }
            },
        });
    }
});

function deleteCount(id){
var r=confirm("Are you sure wants to delete");
    if (r==true){
        $.ajax({
            url:"<?php echo base_url();?>testimonial_video/deleteCount",
            dataType: 'json',
            type: "post",
            data:{id:id},
            success: function (response) {
                if(response.success == "1")
                {
                    displayMsg("success",response.msg);
                    setTimeout(function(){
                            window.location = "<?php echo base_url();?>testimonial_video/index";
                        },2000);
                }
                else
                {	
                    displayMsg("error",response.msg);
                    return false;
                }
            }
        });
    }
}
</script>