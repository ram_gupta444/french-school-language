<div class="main-panel">
    <div class="container">
        <form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
            <div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h4 class="page-title"><a href="#/"><img
                                    src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt=""
                                    class="back-icon-title"></a> Immersive Events</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#/">Website Pages</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Immersive Events</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <a href="#" class="btn btn-light-itara">Discard</a>
                        <button type="submit" class="btn btn-dark-itara">Save</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="section-single">
                            <input type="hidden" id="hidden_adult_program_id" name="hidden_adult_program_id" value="<?= (!empty($adult_program_details[0]['adult_program_id'])) ? $adult_program_details[0]['adult_program_id'] : '' ?>">
                           
                            <div class="section-single">
                                <h3>Section 1</h3>
                                <div class="form-group">
                                    <label for="homebannerheading">Heading </label>
                                    <textarea class="form-control editors" id="section1_heading" name="section1_heading" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['section1_heading'])) ? $adult_program_details[0]['section1_heading'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Description</label>
                                    <textarea class="form-control editors" id="section1_description" name="section1_description" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['section1_description'])) ? $adult_program_details[0]['section1_description'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Dynamic data title</label>
                                    <textarea class="form-control editors" id="dynamic_data_title" name="dynamic_data_title" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['dynamic_data_title'])) ? $adult_program_details[0]['dynamic_data_title'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single">
                                <h3>Section 2</h3>
                                <div class="form-group">
                                    <label for="homebannerheading">Heading</label>
                                    <textarea class="form-control editors" id="section2_heading" name="section2_heading" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['section2_heading'])) ? $adult_program_details[0]['section2_heading'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Sub Heading</label>
                                    <textarea class="form-control editors" id="section2_sub_heading" name="section2_sub_heading" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['section2_sub_heading'])) ? $adult_program_details[0]['section2_sub_heading'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single last">
                                <h3>Meta Tags</h3>
                                <div class="form-group">
                                    <label for="defaultSelect">Meta Title</label>
                                    <textarea class="form-control" id="meta_title" name="meta_title" rows="2" placeholder=""><?= (!empty($adult_program_details[0]['meta_title'])) ? $adult_program_details[0]['meta_title'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="defaultSelect">Meta Description</label>
                                    <textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder=""><?= (!empty($adult_program_details[0]['meta_description'])) ? $adult_program_details[0]['meta_description'] : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
<script>

    var vRules = {
        section1_heading: {
            required: true
        },
        meta_title: {
            required: true
        },
    };
    var vMessages = {
        section1_heading: {
            required: "Please enter section 1 heading."
        },
        meta_title: {
            required: "Please enter meta title."
        },
    };
    $("#form-validate").validate({
        rules: vRules,
        messages: vMessages,
        submitHandler: function (form) {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var act = "<?php echo base_url();?>adult_program/submitForm";
            $("#form-validate").ajaxSubmit({
                url: act,
                type: 'post',
                cache: false,
                clearForm: false,
                success: function (response) {
                    var res = eval('(' + response + ')');
                    if (res['success'] == "1") {
                        displayMsg("success", res['msg']);
                        setTimeout(function () {
                            window.location = "<?php echo base_url();?>adult_program/index";
                        }, 2000);
                    } else {
                        displayMsg("error", res['msg']);
                        return false;
                    }
                },
            });
        }
    });

    CKEDITOR.replaceClass = 'editors';
    /*Preview Images*/
    var loadFile = function (event, id) {
        var image = document.getElementById('homepreview-' + id);
        image.src = URL.createObjectURL(event.target.files[0]);
    };
    /*Preview Images*/
</script>