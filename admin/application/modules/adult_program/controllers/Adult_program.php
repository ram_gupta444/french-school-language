<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Adult_program extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('adult_programmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['adult_program_details'] = $this->common->getData("tbl_adult_program",'*',$condition);
		
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['section1_heading'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter section 1 heading'));
				exit;
			}
			$data = array();
			$data['section1_heading'] = (!empty($_POST['section1_heading'])) ? $_POST['section1_heading'] : '';
			$data['section1_description'] = (!empty($_POST['section1_description'])) ? $_POST['section1_description'] : '';
			$data['dynamic_data_title'] = (!empty($_POST['dynamic_data_title'])) ? $_POST['dynamic_data_title'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_sub_heading'] = (!empty($_POST['section2_sub_heading'])) ? $_POST['section2_sub_heading'] : '';
			$data['meta_title'] = (!empty($_POST['meta_title'])) ? $_POST['meta_title'] : '';
			$data['meta_description'] = (!empty($_POST['meta_description'])) ? $_POST['meta_description'] : '';
			// echo "<pre>";print_r($data);exit;
			if(!empty($_POST['hidden_adult_program_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->adult_programmodel->updateRecord('tbl_adult_program', $data,'adult_program_id',$_POST['hidden_adult_program_id']);
				
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->adult_programmodel->insertData('tbl_adult_program', $data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

}

?>
