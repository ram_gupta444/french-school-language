<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Private_tuition extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('private_tuitionmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		// $result['private_tuition_details'] = $this->common->getData("tbl_private_tuition",'*',$condition);
		$result['private_tuition_list'] = $this->private_tuitionmodel->getFormdata();
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$private_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$private_id = $url_prams['id'];
				
				$result['stored_langauge'] = $this->common->getData("tbl_private_tuition","language"," private_tuition_id = ".$private_id."  ");
			}
			// echo $private_id;
			$result['language_dropdown'] = $this->private_tuitionmodel->getDropdown("tbl_language_master","language_master_id,language_name");
			$result['private_tuition_details'] = $this->common->getData("tbl_private_tuition",'*',"private_tuition_id = ".$private_id." ");


			// echo "<pre>";print_r($result['stored_langauge']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['heading'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter heading'));
				exit;
			}
			$condition = "language = '".$_POST['language']."' ";
			if(isset($_POST['hidden_private_tuition_id']) && (!empty($_POST['hidden_private_tuition_id'])))
			{
				$condition .= " &&  private_tuition_id !='".$_POST['hidden_private_tuition_id']."' ";
			}
			$check_language = $this->common->getdata("tbl_private_tuition",'language',$condition);
			if(!empty($check_language)){
				echo json_encode(array("success"=>"0",'msg'=>'Language Already Present!'));
				exit;
			}
			$data = array();
			$data['language'] = (!empty($_POST['language'])) ? $_POST['language'] : '';
			$data['heading'] = (!empty($_POST['heading'])) ? $_POST['heading'] : '';
			$data['description'] = (!empty($_POST['description'])) ? $_POST['description'] : '';
			$data['button_text'] = (!empty($_POST['button_text'])) ? $_POST['button_text'] : '';
			$data['button_link'] = (!empty($_POST['button_link'])) ? $_POST['button_link'] : '';
			$data['section1_heading'] = (!empty($_POST['section1_heading'])) ? $_POST['section1_heading'] : '';
			$data['section1_sub_heading'] = (!empty($_POST['section1_sub_heading'])) ? $_POST['section1_sub_heading'] : '';
			$data['section1_description'] = (!empty($_POST['section1_description'])) ? $_POST['section1_description'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_sub_heading'] = (!empty($_POST['section2_sub_heading'])) ? $_POST['section2_sub_heading'] : '';
			$data['section2_description'] = (!empty($_POST['section2_description'])) ? $_POST['section2_description'] : '';
			$data['meta_title'] = (!empty($_POST['meta_title'])) ? $_POST['meta_title'] : '';
			$data['meta_description'] = (!empty($_POST['meta_description'])) ? $_POST['meta_description'] : '';
			
			if(!empty($_FILES['section1_image'])){
				$_FILES['image_name']['name'] =  'P'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['section1_image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['section1_image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['section1_image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['section1_image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['section1_image']['size'];
				/* File Type Change multiple into single */
				
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['section1_image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_section1_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/private_tuition/'.$_POST['hidden_section1_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/private_tuition/'. $_POST['hidden_section1_image_name']);
						}
					}
				}
			}
			if(!empty($_FILES['section2_image'])){
				$_FILES['image_name']['name'] =  'P'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['section2_image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['section2_image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['section2_image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['section2_image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['section2_image']['size'];
				/* File Type Change multiple into single */
				
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['section2_image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_section2_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/private_tuition/'.$_POST['hidden_section2_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/private_tuition/'. $_POST['hidden_section2_image_name']);
						}
					}
				}
			}
			
			// echo "<pre>";print_r($data);exit;
			if(!empty($_POST['hidden_private_tuition_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->private_tuitionmodel->updateRecord('tbl_private_tuition', $data,'private_tuition_id',$_POST['hidden_private_tuition_id']);
				
				
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->private_tuitionmodel->insertData('tbl_private_tuition', $data,'1');
				
			}

			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/private_tuition/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }

	function deleteImage(){
		$condition = "private_tuition_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_private_tuition",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/private_tuition/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->private_tuitionmodel->updateRecord('tbl_private_tuition', $data,'private_tuition_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}

}

?>
