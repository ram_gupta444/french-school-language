<?PHP
class Private_tuitionmodel extends CI_Model
{

     function insertData($tbl_name,$data_array,$sendid = NULL)
     {
       $this->db->insert($tbl_name,$data_array);
       //print_r($this->db->last_query());
         //exit;
       $result_id = $this->db->insert_id();
       
       if($sendid == 1)
       {
         return $result_id;
       }
     }
     function updateRecord($tableName, $data, $column, $value)
     {
       $this->db->where("$column", $value);
       $this->db->update($tableName, $data);
       if ($this->db->affected_rows() > 0) {
         return true;
       }
       else {
         return true;
       }
     }
     function delrecord($tbl_name,$tbl_id,$record_id)
     {
       $this->db->where($tbl_id, $record_id);
         $this->db->delete($tbl_name);
       if($this->db->affected_rows() >= 1)
       {
         return true;
         }
         else
         {
         return false;
         }
     }
     function getDropdown($tbl_name,$tble_flieds){
      $this -> db -> select($tble_flieds);
      $this -> db -> from($tbl_name);
      $query = $this -> db -> get();
      if($query -> num_rows() >= 1)
      {
        return $query->result_array();
      }
      else
      {
        return false;
      }
   }
   function getFormdata(){
    $this -> db -> select('pt.*,l.language_name');
    $this -> db -> from('tbl_private_tuition as pt');
    $this->db->join('tbl_language_master as l','l.language_master_id = pt.language','left');
    $query = $this -> db -> get();
    if($query -> num_rows() >= 1){
      return $query->result_array();
    }
    else{
      return false;
    }
 }
}