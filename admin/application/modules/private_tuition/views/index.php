<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="#/"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Private Tuitions List</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Private Tuitions List</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>private_tuition/addEdit" class="btn btn-dark-itara">Add New Private Tuition</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>#</th>
									<th>Heading</th>
									<th>Description</th>
									<th>Language</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>#</th>
									<th>Heading</th>
									<th>Description</th>
									<th>Language</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($private_tuition_list)) {
                                    foreach ($private_tuition_list as $key => $value) { ?>
								<tr>
									<td><?= $value['private_tuition_id'] ?></td>
									<td><?= $value['section1_heading'] ?></td>
									<td><?= $value['section1_description'] ?></td>
									<td><?= $value['language_name'] ?></td>
									<td>
										<div class="actions">
											<a href="<?= base_url('private_tuition/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['private_tuition_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                           
										</div>
									</td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
