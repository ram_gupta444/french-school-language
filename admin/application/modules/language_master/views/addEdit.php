<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>language_master"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Add Language</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Language</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#"> Add Language</a>
							</li>						
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url();?>language_master"><button class="btn" type="button">Cancel</button></a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						
						<input type="hidden" id="language_master_id" name="language_master_id" value="<?php if(!empty($language_details[0]->language_master_id)){echo $language_details[0]->language_master_id;}?>">

						<input type="hidden" id="hidden_language_logo_name" name="hidden_language_logo_name" value="<?= (!empty($language_details[0]->language_logo)) ? $language_details[0]->language_logo : '' ?>">

						<input type="hidden" id="hidden_image_name" name="hidden_image_name" value="<?= (!empty($language_details[0]->image)) ? $language_details[0]->image : '' ?>">

						<div class="form-group">
							<label for="homebannerimg">Language Logo</label>
							<p class="preview-img-wrapper">
								<img id="homepreview-1" src="<?= (!empty($language_details[0]->language_logo) ? FRONT_URL."/images/language_logo/".$language_details[0]->language_logo  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
							</p>
						
							<p>
								<input type="file" accept="image/*" name="language_logo" id="language_logo" onchange="loadFile(event,'1')" style="display: none;">
							</p>
							<a href="#/" class="btn btn-primary btn-xs">
								<label for="language_logo">Upload</label>
							</a>
							<?php if(!empty($language_details[0]->language_logo)){ ?>
							<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image_logo('<?= (!empty($language_details[0]->language_master_id)) ? $language_details[0]->language_master_id : '' ?>','language_logo');">Delete</a>
							<?php } ?>
						</div>
							
						<div class="form-group">
							<label for="testimonialname">Language Name*</label>
							<input class="form-control" id="language_name" name="language_name" placeholder="" value="<?= (!empty($language_details[0]->language_name)) ? $language_details[0]->language_name :" "?>">
						</div>
						<div class="form-group">
							<?php 
							if(!empty($language_details[0]->language_master_id)){
								$check = "";
							}else{
								$check = "checked";
							}
							
							?>
							<p><strong>Status</strong></p>	
							<input type="checkbox" name="status" id="status" <?= (!empty($language_details) && $language_details[0]->status == 'Active') ?"checked":"$check"?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
						</div>		
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
					
						<div class="section-single">
							<div class="form-group">
								<label for="homebannerimg">Banner Image </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-2" src="<?= (!empty($language_details[0]->image) ? FRONT_URL."/images/language_home/".$language_details[0]->image  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="image" id="image" onchange="loadFile(event,'2')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="image">Upload</label>
								</a>
								<?php if(!empty($language_details[0]->image)){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($language_details[0]->language_master_id)) ? $language_details[0]->language_master_id : '' ?>','image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="heading" name="heading" rows="5" placeholder=""><?= (!empty($language_details[0]->heading)) ? $language_details[0]->heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Description</label>
								<textarea class="form-control editors" id="description" name="description" rows="5" placeholder=""><?= (!empty($language_details[0]->description)) ? $language_details[0]->description : '' ?></textarea>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Text</label>
										<input type="text" class="form-control" id="button_text" name="button_text" placeholder="Enter Link Text" value="<?= (!empty($language_details[0]->button_text)) ? $language_details[0]->button_text : '' ?>">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Link</label>
										<input type="text" class="form-control" id="button_link" name="button_link" placeholder="Enter Link URL" value="<?= (!empty($language_details[0]->button_link)) ? $language_details[0]->button_link : '' ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2</h3>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="section2_heading" name="section2_heading" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_heading)) ? $language_details[0]->section2_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Sub Heading</label>
								<textarea class="form-control editors" id="section2_sub_heading" name="section2_sub_heading" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_sub_heading)) ? $language_details[0]->section2_sub_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Description </label>
								<textarea class="form-control editors" id="section2_description" name="section2_description" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_description)) ? $language_details[0]->section2_description : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2 Block 1</h3>
							<div class="form-group">
								<label for="section1text">Section 2 Heading 1 </label>
								<textarea class="form-control editors" id="section2_heading1" name="section2_heading1" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_heading1)) ? $language_details[0]->section2_heading1 : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 2 Description 1 </label>
								<textarea class="form-control editors" id="section2_description1" name="section2_description1" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_description1)) ? $language_details[0]->section2_description1 : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2 Block 2</h3>
							<div class="form-group">
								<label for="section1text">Section 2 Heading 2 </label>
								<textarea class="form-control editors" id="section2_heading2" name="section2_heading2" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_heading2)) ? $language_details[0]->section2_heading2 : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 2 Description 2 </label>
								<textarea class="form-control editors" id="section2_description2" name="section2_description2" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_description2)) ? $language_details[0]->section2_description2 : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2 Block 3</h3>
							<div class="form-group">
								<label for="section1text">Section 2 Heading 3 </label>
								<textarea class="form-control editors" id="section2_heading3" name="section2_heading3" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_heading3)) ? $language_details[0]->section2_heading3 : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 2 Description 3 </label>
								<textarea class="form-control editors" id="section2_description3" name="section2_description3" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_description3)) ? $language_details[0]->section2_description3 : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2 Block 4</h3>
							<div class="form-group">
								<label for="section1text">Section 2 Heading 4 </label>
								<textarea class="form-control editors" id="section2_heading4" name="section2_heading4" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_heading4)) ? $language_details[0]->section2_heading4 : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 2 Description 4 </label>
								<textarea class="form-control editors" id="section2_description4" name="section2_description4" rows="2" placeholder=""><?= (!empty($language_details[0]->section2_description4)) ? $language_details[0]->section2_description4 : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3</h3>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="section3_heading" name="section3_heading" rows="2" placeholder=""><?= (!empty($language_details[0]->section3_heading)) ? $language_details[0]->section3_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Sub Heading</label>
								<textarea class="form-control editors" id="section3_sub_heading" name="section3_sub_heading" rows="2" placeholder=""><?= (!empty($language_details[0]->section3_sub_heading)) ? $language_details[0]->section3_sub_heading : '' ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	var vRules = {
		language_name:{required:true},
		heading:{required:true},
		description:{required:true},
	};
	var vMessages = {
		language_name:{required:"Please enter language name."},
		heading:{required:"Please enter heading."},
		description:{required:"Please enter description."},
	};
	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
			}
			var act = "<?php echo base_url();?>language_master/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>language_master/index";
						},2000);
					}
					else
					{	
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>language_master/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>language_master/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}

function delete_image_logo(id,language_logo){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>language_master/deleteImageLogo",
			dataType: 'json',
			type: "post",
			data:{id:id,language_logo:language_logo},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>language_master/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}

CKEDITOR.replaceClass = 'editors';


/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
document.title = "Add Edit Language master";
</script>
