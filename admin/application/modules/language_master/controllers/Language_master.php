<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Language_master extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('language_mastermodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
    function index()
    {
		$result = array();
		$condition = "1=1";
		$result['language_data'] = $this->common->getData("tbl_language_master",'*',$condition);
        $this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$language_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$language_id = $url_prams['id'];
			}
			//echo $language_id;
			$result['language_details'] = $this->language_mastermodel->getFormdata($language_id);
			// echo "<pre>";print_r($result['menu_details']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['language_name'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter language name'));
				exit;
			}
			if(empty(trim($_POST['hidden_language_logo_name']))){
				if(empty($_FILES['language_logo']) && !isset($_FILES['language_logo'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select language logo'));
					exit;
				}
			}
			if(empty(trim($_POST['hidden_image_name']))){
				if(empty($_FILES['image']) && !isset($_FILES['image'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select banner image'));
					exit;
				}
			}
			if(!empty($_POST['language_name'])){
				$condition = "language_name LIKE '%".$_POST['language_name']."%' ";
				if(!empty($_POST['language_master_id'])){
					$condition .= " AND  language_master_id != ".$_POST['language_master_id']." ";
				}			
				$check_name = $this->common->getData("tbl_language_master",'*',$condition);
				
				if(!empty($check_name[0]['language_master_id'])){
					echo json_encode(array("success"=>0, 'msg'=>'Language Name Already Present!'));
					exit;
				}
			}
			
			$data = array();
			if(!empty($_POST['status']) && isset($_POST['status'])){
				$data['status'] = 'Active';
			}else{
				$data['status'] = 'Inactive';
			}
			$data['language_name'] = (!empty($_POST['language_name'])) ? $_POST['language_name'] : '';
			$data['slug'] = (!empty($_POST['language_name'])) ? preg_replace('/[^A-Za-z0-9\-]/', '',str_replace(' ', '-', strtolower(trim($_POST['language_name'])))) : '';
			$data['heading'] = (!empty($_POST['heading'])) ? $_POST['heading'] : '';
			$data['description'] = (!empty($_POST['description'])) ? $_POST['description'] : '';
			$data['button_text'] = (!empty($_POST['button_text'])) ? $_POST['button_text'] : '';
			$data['button_link'] = (!empty($_POST['button_link'])) ? $_POST['button_link'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_sub_heading'] = (!empty($_POST['section2_sub_heading'])) ? $_POST['section2_sub_heading'] : '';
			$data['section2_description'] = (!empty($_POST['section2_description'])) ? $_POST['section2_description'] : '';
			$data['section2_heading1'] = (!empty($_POST['section2_heading1'])) ? $_POST['section2_heading1'] : '';
			$data['section2_description1'] = (!empty($_POST['section2_description1'])) ? $_POST['section2_description1'] : '';
			$data['section2_heading2'] = (!empty($_POST['section2_heading2'])) ? $_POST['section2_heading2'] : '';
			$data['section2_description2'] = (!empty($_POST['section2_description2'])) ? $_POST['section2_description2'] : '';
			$data['section2_heading3'] = (!empty($_POST['section2_heading3'])) ? $_POST['section2_heading3'] : '';
			$data['section2_description3'] = (!empty($_POST['section2_description3'])) ? $_POST['section2_description3'] : '';
			$data['section2_heading4'] = (!empty($_POST['section2_heading4'])) ? $_POST['section2_heading4'] : '';
			$data['section2_description4'] = (!empty($_POST['section2_description4'])) ? $_POST['section2_description4'] : '';
			$data['section3_heading'] = (!empty($_POST['section3_heading'])) ? $_POST['section3_heading'] : '';
			$data['section3_sub_heading'] = (!empty($_POST['section3_sub_heading'])) ? $_POST['section3_sub_heading'] : '';

			if(!empty($_FILES['language_logo'])){
				$_FILES['language_logo_name']['name'] =  'l'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['language_logo']['name'];
				$_FILES['language_logo_name']['tmp_name'] =  $_FILES['language_logo']['tmp_name'];
				$_FILES['language_logo_name']['error'] =  $_FILES['language_logo']['error'];
				$_FILES['language_logo_name']['type'] =  $_FILES['language_logo']['type'];
				$_FILES['language_logo_name']['size'] =  $_FILES['language_logo']['size'];
				/* File Type Change multiple into single */
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_logo_options());
				if(!$this->upload->do_upload('language_logo_name')){
					$image_error_log = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error_log['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['language_logo'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_language_logo_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/language_logo/'.$_POST['hidden_language_logo_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/language_logo/'. $_POST['hidden_language_logo_name']);
						}
					}
				}
			}

			if(!empty($_FILES['image'])){
				$_FILES['image_name']['name'] =  'l'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['image']['size'];
				/* File Type Change multiple into single */
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/language_home/'.$_POST['hidden_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/language_home/'. $_POST['hidden_image_name']);
						}
					}
				}
			}
			if(!empty($_POST['language_master_id']))
			{
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				//update 
				$result = $this->language_mastermodel->updateRecord('tbl_language_master', $data,'language_master_id',$_POST['language_master_id']);
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->language_mastermodel->insertData('tbl_language_master',$data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}
		else {
			return false;
		}
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/language_home/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	function set_upload_logo_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/language_logo/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	function deleteImage(){
		$condition = "language_master_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_language_master",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/language_home/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->language_mastermodel->updateRecord('tbl_language_master', $data,'language_master_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
	function deleteImageLogo(){
		$condition = "language_master_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_language_master",'*',$condition);
		if(!empty($result[0][$_POST['language_logo']])){
			$path = DOC_ROOT_FRONT."/images/language_logo/".$result[0][$_POST['language_logo']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['language_logo']] = '';
			$result = $this->language_mastermodel->updateRecord('tbl_language_master', $data,'language_master_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}