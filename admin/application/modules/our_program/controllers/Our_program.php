<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Our_program extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('our_programmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['our_details'] = $this->common->getData("tbl_our_program",'*',$condition);
		if(!empty($result['our_details'])){
			foreach ($result['our_details'] as $key => $value) {
				$result['languages'][] = $this->our_programmodel->getCustomList($value['languages']);
				// $languages[] = $value['languages'];
			}
		}
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$our_program_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$our_program_id = $url_prams['id'];

				$result['stored_langauges'] = $this->common->getData("tbl_our_program","languages"," our_program_id = ".$our_program_id."  ");
			}
			$condition = "status = 'Active'  ";
			$result['language_dropdown'] = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 
			// echo $our_program_id;
			$result['our_program_details'] = $this->our_programmodel->getFormdata($our_program_id);
			// echo "<pre>";print_r($result['testimonial_details']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['heading'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter heading'));
				exit;
			}
			// echo "<pre>";print_r($_POST);exit;
			$data = array();
			$data['heading'] = (!empty($_POST['heading'])) ? $_POST['heading'] : '';
			$data['description'] = (!empty($_POST['description'])) ? $_POST['description'] : '';
			$data['button_text1'] = (!empty($_POST['button_text1'])) ? $_POST['button_text1'] : '';
			$data['button_link1'] = (!empty($_POST['button_link1'])) ? $_POST['button_link1'] : '';
			$data['button_text2'] = (!empty($_POST['button_text2'])) ? $_POST['button_text2'] : '';
			$data['button_link2'] = (!empty($_POST['button_link2'])) ? $_POST['button_link2'] : '';
			$data['meta_title'] = (!empty($_POST['meta_title'])) ? $_POST['meta_title'] : '';

			if(!empty($_POST['show_on_homepage']) && isset($_POST['show_on_homepage'])){
				$data['show_on_homepage'] = 1;
			}else{
				$data['show_on_homepage'] = 0;
			}
			$data['languages'] = (!empty($_POST['languages'])) ? implode(',',$_POST['languages']) : '';
			$data['meta_description'] = (!empty($_POST['meta_description'])) ? $_POST['meta_description'] : '';
			if(!empty($_FILES['image'])){
				$_FILES['image_name']['name'] =  'P'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['image']['size'];
				/* File Type Change multiple into single */
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['image'] = $file_data['upload_data']['file_name'];
					if(!empty($_POST['hidden_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/our_program/'.$_POST['hidden_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/our_program/'. $_POST['hidden_image_name']);
						}
					}
				}
			}
			if(!empty($_POST['hidden_our_program_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->our_programmodel->updateRecord('tbl_our_program', $data,'our_program_id',$_POST['hidden_our_program_id']);
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->our_programmodel->insertData('tbl_our_program', $data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/our_program/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		return $config;
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }
	function deleteImage(){
		$condition = "our_program_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_our_program",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/our_program/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->our_programmodel->updateRecord('tbl_our_program', $data,'our_program_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}

	function deleteour_program(){

		$Result = $this->our_programmodel->delrecord('tbl_our_program', 'our_program_id ',$_POST['id']);
		if($Result)
		{
			echo json_encode(array("success" => "1", "msg" => "deleted successfully.", "body" => NULL));
			exit;
		}else{
			echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
			exit;
		}	
	}
}
?>
