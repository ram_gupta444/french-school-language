<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="<?php echo base_url(); ?>testimonial"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Language Programs</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Language Programs</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>our_program/addEdit" class="btn btn-dark-itara">Add Program</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Language</th>
									<th>Created On</th>
									<th>Status</th>	
									<th>Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Language</th>
									<th>Created On</th>
									<th>Status</th>	
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($our_details)) {
                                    foreach ($our_details as $key => $value) { ?>
								<tr>
									<td><?= $value['our_program_id'] ?></td>
									<td><?= $value['heading'] ?></td>
									<td>
									<?php 
									if(!empty($languages)){
										if(!empty($languages[$key])){
											foreach ($languages[$key] as $lkey => $languagevalue) { ?>
												<?= $languagevalue['language_name']."<br>"; ?>
											<?php } 
										}
									}?>
									</td>
									<td><?= date("d M, y", strtotime($value['created_on'])) ?></td>
									<td><span class="<?= ($value['status']== 'Active') ?"active":"inactive"?>-label"><?= $value['status'] ?></span></span></td>
									<td>
										<div class="actions">
											<a href="<?= base_url('our_program/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['our_program_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                            <a href="javasript:void(0)" title="Delete"><img src="<?php echo base_url();?>assets/images/delete.svg" alt="Delete" class="delete_our_program" data-id="<?php echo $value['our_program_id'];?>"></a>
                                            </td>
										</div>
									</td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(".delete_our_program").click(function(e) {
	var id=$(this).attr("data-id");
	var r=confirm("Are you sure to wants to delete");
    	if (r==true){
			$.ajax({
				url: "<?php echo base_url();?>our_program/deleteour_program",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>our_program/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
    	}
})
		
</script>