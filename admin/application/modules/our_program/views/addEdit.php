<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="#/"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Our Program</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#/">Website Pages</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#"> Our Program</a>
							</li>
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url(); ?>our_program/index" class="btn btn-light-itara">Discard</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
					<input type="hidden" id="hidden_our_program_id" name="hidden_our_program_id" value="<?= (!empty($our_program_details[0]['our_program_id'])) ? $our_program_details[0]['our_program_id'] : '' ?>">

					<input type="hidden" id="hidden_image_name" name="hidden_image_name" value="<?= (!empty($our_program_details[0]['image'])) ? $our_program_details[0]['image'] : '' ?>">

						<div class="section-single"> 
							<div class="form-group">
								<label for="homebannerheading">Select language</label>
								<select class="form-control form-control select2" id="languages" name="languages[]" multiple>
								<option value="">Select language</option>
								<?php if(!empty($language_dropdown)){ ?>
									<option value="">Select language</option>
											<?php 
											foreach($language_dropdown as $key=>$val){
												$sel='';
												$sel= (in_array($val['language_master_id'],explode(',',$stored_langauges[0]['languages']))?'selected':'');
											?>
											<option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>
										<?php } 
											}
										?>
								</select>
							</div><br>
							<div class="form-group">
								<p><strong>Show on home page</strong></p>	
								<input type="checkbox" name="show_on_homepage" id="show_on_homepage" <?php echo (!empty($our_program_details) &&  $our_program_details[0]['show_on_homepage'] == '1' ? "checked": " ")?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">		
							</div>
						</div> 
						<div class="section-single">
							<div class="form-group">
								<label for="homebannerimg">Image </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-1" src="<?= (!empty($our_program_details[0]['image']) ? FRONT_URL."/images/our_program/".$our_program_details[0]['image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="image" id="image" onchange="loadFile(event,'1')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="image">Upload</label>
								</a>
								<?php if(!empty($our_program_details[0]['image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($our_program_details[0]['our_program_id'])) ? $our_program_details[0]['our_program_id'] : '' ?>','image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="">Heading * </label>
								<textarea class="form-control editors" id="heading" name="heading" rows="2" placeholder=""><?= (!empty($our_program_details[0]['heading'])) ? $our_program_details[0]['heading'] : '' ?></textarea>
							</div>
                            <div class="form-group">
								<label for="">Description * </label>
								<textarea class="form-control editors" id="description" name="description" rows="2" placeholder=""><?= (!empty($our_program_details[0]['description'])) ? $our_program_details[0]['description'] : '' ?></textarea>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Text 1</label>
										<input type="text" class="form-control" id="button_text1" name="button_text1" placeholder="Enter Link Text" value="<?= (!empty($our_program_details[0]['button_text1'])) ? $our_program_details[0]['button_text1'] : '' ?>">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Link 1</label>
										<input type="text" class="form-control" id="button_link1" name="button_link1" placeholder="Enter Link URL" value="<?= (!empty($our_program_details[0]['button_link1'])) ? $our_program_details[0]['button_link1'] : '' ?>">
									</div>
								</div>
							</div>
                            <div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Text 2</label>
										<input type="text" class="form-control" id="button_text2" name="button_text2" placeholder="Enter Link Text" value="<?= (!empty($our_program_details[0]['button_text2'])) ? $our_program_details[0]['button_text2'] : '' ?>">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Link 2</label>
										<input type="text" class="form-control" id="button_link2" name="button_link2" placeholder="Enter Link URL" value="<?= (!empty($our_program_details[0]['button_link2'])) ? $our_program_details[0]['button_link2'] : '' ?>">
									</div>
								</div>
							</div>
						</div>
						
						<div class="section-single last">
							<h3>Meta Tags</h3>
							<div class="form-group">
								<label for="defaultSelect">Meta Title</label>
								<textarea class="form-control editors" id="meta_title" name="meta_title" rows="2" placeholder=""><?= (!empty($our_program_details[0]['meta_title'])) ? $our_program_details[0]['meta_title'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="defaultSelect">Meta Description</label>
								<textarea class="form-control editors" id="meta_description" name="meta_description" rows="5" placeholder=""><?= (!empty($our_program_details[0]['meta_description'])) ? $our_program_details[0]['meta_description'] : '' ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	
var vRules = {
	heading:{required:true},
};
var vMessages = {
	heading:{required:"Please enter heading."},
};
$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
		}
		var act = "<?php echo base_url();?>our_program/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>our_program/index";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
		});
	}
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>our_program/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>our_program/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}

CKEDITOR.replaceClass = 'editors';


/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
</script>
