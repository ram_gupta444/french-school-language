<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>users"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Add User</h4>
						<ul class="breadcrumbs">
							<li class="nav-home"><a href="#"><i class="flaticon-home"></i></a></li>
							<li class="separator"><i class="flaticon-right-arrow"></i></li>
							<li class="nav-item"><a href="#">User</a></li>
							<li class="separator"><i class="flaticon-right-arrow"></i></li>
							<!-- <li class="nav-item"><a href="<?php echo base_url(); ?>users/addEdit"> Add User</a></li>						 -->
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url();?>users"><button class="btn" type="button">Cancel</button></a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
					<input type="hidden" id="user_id" name="user_id" value="<?php if(!empty($users[0]->user_id)){echo $users[0]->user_id;}?>" />
						<div class="control-group form-group">
							<label class="control-label" for="selectError">Role</label>
							<div class="controls">
								<select id="role_id" name="role_id" class="input-xlarge form-control">
									<option value="">Select</option>
									<?php 
										if(isset($roles) && !empty($roles)){
										foreach($roles as $cdrow){
											//if($cdrow->role_id !=2){
											$sel = ($cdrow->role_id == $users[0]->role_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->role_id;?>" <?php echo $sel; ?>><?php echo $cdrow->role_name;?></option>
										<?php //}
										}}?>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
										<label class="control-label" for="focusedInput">User Name*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="user_name" name="user_name" type="text" value="<?php if(!empty($users[0]->full_name)){echo $users[0]->full_name;}?>">
										</div>
									</div>
									<!-- <div class="control-group form-group">
										<label class="control-label" for="focusedInput">Last Name*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="last_name" name="last_name" type="text" value="<?php if(!empty($users[0]->last_name)){echo $users[0]->last_name;}?>">
										</div>
									</div> -->
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput">Email ID*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="email_id" name="email_id" type="text" value="<?php if(!empty($users[0]->email_id)){echo $users[0]->email_id;}?>">
										</div>
									</div>
									<?php 
									$change_pwd_display = "block";
									if(!empty($users[0]->user_id)){
										$change_pwd_display = "none";
										?>
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput"><input class="" id="change_pwd" name="change_pwd" type="checkbox"> Change Password</label>
									</div>
									<?php } ?>
						
									<div class="cls_change_pwd" style="display:<?php echo $change_pwd_display; ?>" id="change_password_div">
										<div class="control-group form-group change_pwd">
											<label class="control-label" for="focusedInput">Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="password" name="password" type="password"  autocomplete="off">
											</div>
										</div>
										
										<div class="control-group form-group">
											<label class="control-label" for="focusedInput">Confirm Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="confirm_password" name="confirm_password" type="password">
											</div>
										</div>
									</div>
									<div class="control-group form-group">
										<label class="control-label" for="status">Status*</label>
										<div class="controls">
											<select name="status" id="status" class="form-control">
												<option value="Active" <?php if(!empty($users[0]->status) && $users[0]->status == "Active"){?> selected <?php }?>>Active</option>
												<option value="In-active" <?php if(!empty($users[0]->status) && $users[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
											</select>
										</div>
									</div>
                        
                    </div>	
                   
				</div>			
                </div>
            </div>
         </div>
        </form>
	</div>
</div>

<script>
$("#change_pwd").on("change", function(){
	$("#password").val("");
	$("#confirm_password").val("");
	
	if(this.checked) {
		var returnVal = confirm("Are you sure?");
		$(this).prop("checked", returnVal);
		if(returnVal){
			$('.cls_change_pwd').show();  
		}
	}else{
		$(this).prop("checked", false);
		$('.cls_change_pwd').hide();  
	}   
});

var vRules = {
	user_name:{required:true},
	// last_name:{required:true},
	email_id:{required:true, email:true},
	<?php if(empty($users[0]->user_id)){ ?>
	password:{required:true},
	confirm_password:{required:true,equalTo:password},
	<?php }else{ ?>
	password:{required:"#change_pwd:checked"},
	confirm_password:{required:"#change_pwd:checked",equalTo:password},
	<?php } ?>
	role_id:{required:true}
};
var vMessages = {
	user_name:{required:"Please enter first name"},
	// last_name:{required:"Please enter last name"},
	email_id:{required:"Please enter user name"},
	password:{required:"Please enter password"},
	confirm_password:{required:"Please enter confirm password",equalTo:"Password does not match."},
	role_id:{required:"Please select role."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>users/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>users";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "Add Edit Permission";

</script>

