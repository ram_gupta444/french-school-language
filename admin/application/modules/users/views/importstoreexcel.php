<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/> 

<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
		<div>
			<h1>Import Stores</h1>            
		</div>
		<div>
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
				<li><a href="<?php echo base_url();?>storelocator">Store Locator</a></li>
				<li><a href="#">Import Stores</a></li>
			</ul>
		</div>
	</div>
	<div class="card">    
		<div class="page-title-border">
			<div class="col-sm-12 col-md-12 left-button-top">
				<!--<a href="<?php echo FRONT_URL;?>/images/stars_store_locator.xlsx" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Download Template</a>
				-->
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="card-body">             
		<div class="box-content">
			<div class="col-sm-8 col-md-4">
				<!--<div class="box-header form-group" data-original-title>
					<h2><i class="halflings-icon edit"></i><span class="break"></span>Import Product</h2>
				</div> -->
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<div class="control-group form-group">
						<div class="control-group col-sm-12">
							<label class="control-label" for="import_store_excel">Import</label>
							<div class="controls">
								<input id="import_store_excel" name="import_store_excel" type="file" class="form-control" />
							</div>
						</div>
					</div>	
					<div class="form-actions form-group col-sm-12">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>storelocator" class="btn btn-primary">Cancel</a>
					</div>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
		</div>
		<div class="clearfix"></div>
	</div>        
</div><!-- end: Content -->
			
<script>
							
var vRules = {
	import_store_excel:{required:true}
};

var vMessages = {
	import_store_excel:{required:"Please select file."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>users/importStoreExcel";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
			},
			success: function (response) 
			{
				$(".btn-primary").show();
				if(response.success)
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>users";
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
});

</script>

