<div class="main-panel">
    <div class="container">
        <div class="page-inner">
            <div class="page-header page-header-btn">
                <div class="page-header-title">
                    <!-- <h4 class="page-title"><a href="#/"><img src="assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Users</h4> -->
                    <h4 class="page-title">Users</h4>
                    <ul class="breadcrumbs">
                        <li class="nav-home">
                            <a href="#"><i class="flaticon-home"></i></a>
                        </li>
                        <li class="separator"><i class="flaticon-right-arrow"></i></li>
                        <li class="nav-item">
                        <li><a href="<?php echo base_url(); ?>users">Users</a></li>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                <th>User Id</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Status</th>
                                <!-- <th>Actions</th> -->
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <th>User Id</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Status</th>
                                <!-- <th>Actions</th> -->
                                </tr>
                            </tfoot>
                            <tbody>

                                <?php
                                if (!empty($users_data)) {
                                    foreach ($users_data as $key => $value) { ?>
                                        <tr>
                                            <td><?= $value['user_id'] ?></td>
                                            <td><?= $value['email_id'] ?></td>
                                            <td><?= $value['full_name'] ?></td>
                                            <td><?= $value['role_name'] ?></td>
                                            <td><?= $value['status'] ?></td>
                                            <!-- <td><a href="<?= base_url('users/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['user_id']), '+/', '-_'), '=').'')?>" title=""> <img src="assets/images/edit.svg" alt="Edit"></a>
                                            </td> -->
                    </div>
                                        </tr>
                                <?php }
                                } ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>

			
<script>
	function deleteData(id)
	{
    	var r=confirm("Are you sure you want to delete this record?");
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delRecord/"+id,
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	document.title = "Users";
</script>