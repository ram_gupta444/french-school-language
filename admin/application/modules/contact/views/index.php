<div class="main-panel">
    <div class="container">
        <form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
            <div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h4 class="page-title"><a href="#/"><img
                                    src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt=""
                                    class="back-icon-title"></a> Contact</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#/">Website Pages</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <a href="#" class="btn btn-light-itara">Discard</a>
                        <button type="submit" class="btn btn-dark-itara">Save</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="section-single">
                            <input type="hidden" id="hidden_contact_id" name="hidden_contact_id" value="<?= (!empty($contact_details[0]['contact_id'])) ? $contact_details[0]['contact_id'] : '' ?>">
                            <input type="hidden" id="hidden_section1_image_name" name="hidden_section1_image_name" value="<?= (!empty($contact_details[0]['section1_image'])) ? $contact_details[0]['section1_image'] : '' ?>">

                            <input type="hidden" id="hidden_section2_image_name" name="hidden_section2_image_name" value="<?= (!empty($contact_details[0]['section2_image'])) ? $contact_details[0]['section2_image'] : '' ?>">

                            <input type="hidden" id="hidden_section3_image_name" name="hidden_section3_image_name" value="<?= (!empty($contact_details[0]['section3_image'])) ? $contact_details[0]['section3_image'] : '' ?>">

                            <div class="section-single">
                                <div class="form-group">
                                    <label for="homebannerheading">Heading </label>
                                    <textarea class="form-control editors" id="heading" name="heading" rows="2"
                                        placeholder=""><?= (!empty($contact_details[0]['heading'])) ? $contact_details[0]['heading'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Description</label>
                                    <textarea class="form-control editors" id="description" name="description" rows="2" placeholder=""><?= (!empty($contact_details[0]['description'])) ? $contact_details[0]['description'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single">
                                <h3>Section 1</h3>
                                <div class="form-group">
                                    <label for="homebannerimg">Image </label>
                                    <p class="preview-img-wrapper">
                                        <img id="homepreview-1"
                                            src="<?= (!empty($contact_details[0]['section1_image']) ? FRONT_URL."/images/contact_us/".$contact_details[0]['section1_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>" />
                                    </p>
                                    <p>
                                        <input type="file" accept="image/*" name="section1_image" id="section1_image"
                                            onchange="loadFile(event,1)" style="display: none;">
                                    </p>
                                    <a href="#/" class="btn btn-primary btn-xs">
                                        <label for="section1_image">Upload</label>
                                    </a>
                                    <?php if(!empty($contact_details[0]['section1_image'])){ ?>
                                    <a href="#/" class="btn btn-secondary btn-xs"
                                        onclick="delete_image('<?= (!empty($contact_details[0]['contact_id'])) ? $contact_details[0]['contact_id'] : '' ?>','section1_image');">Delete</a>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Phone Number</label>
                                    <textarea class="form-control editors" id="section1_phone" name="section1_phone"
                                        rows="2"
                                        placeholder=""><?= (!empty($contact_details[0]['section1_phone'])) ? $contact_details[0]['section1_phone'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single">
                                <h3>Section 2</h3>
                                <div class="form-group">
                                    <label for="homebannerimg">Image </label>
                                    <p class="preview-img-wrapper">
                                        <img id="homepreview-2"
                                            src="<?= (!empty($contact_details[0]['section2_image']) ? FRONT_URL."/images/contact_us/".$contact_details[0]['section2_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>" />
                                    </p>
                                    <p>
                                        <input type="file" accept="image/*" name="section2_image" id="section2_image"
                                            onchange="loadFile(event,2)" style="display: none;">
                                    </p>
                                    <a href="#/" class="btn btn-primary btn-xs">
                                        <label for="section2_image">Upload</label>
                                    </a>
                                    <?php if(!empty($contact_details[0]['section2_image'])){ ?>
                                    <a href="#/" class="btn btn-secondary btn-xs"
                                        onclick="delete_image('<?= (!empty($contact_details[0]['contact_id'])) ? $contact_details[0]['contact_id'] : '' ?>','section2_image');">Delete</a>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Email ID</label>
                                    <textarea class="form-control editors" id="section2_email" name="section2_email"
                                        rows="2" placeholder=""><?= (!empty($contact_details[0]['section2_email'])) ? $contact_details[0]['section2_email'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single">
                                <h3>Section 3</h3>
                                <div class="form-group">
                                    <label for="homebannerimg">Image </label>
                                    <p class="preview-img-wrapper">
                                        <img id="homepreview-3"
                                            src="<?= (!empty($contact_details[0]['section3_image']) ? FRONT_URL."/images/contact_us/".$contact_details[0]['section3_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>" />
                                    </p>
                                    <p>
                                        <input type="file" accept="image/*" name="section3_image" id="section3_image"
                                            onchange="loadFile(event,3)" style="display: none;">
                                    </p>
                                    <a href="#/" class="btn btn-primary btn-xs">
                                        <label for="section3_image">Upload</label>
                                    </a>
                                    <?php if(!empty($contact_details[0]['section3_image'])){ ?>
                                    <a href="#/" class="btn btn-secondary btn-xs"
                                        onclick="delete_image('<?= (!empty($contact_details[0]['contact_id'])) ? $contact_details[0]['contact_id'] : '' ?>','section3_image');">Delete</a>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label for="homebannerheading">Description</label>
                                    <textarea class="form-control editors" id="section3_description"
                                        name="section3_description" rows="2"
                                        placeholder=""><?= (!empty($contact_details[0]['section3_description'])) ? $contact_details[0]['section3_description'] : '' ?></textarea>
                                </div>
                            </div>
                            <div class="section-single last">
                                <h3>Meta Tags</h3>
                                <div class="form-group">
                                    <label for="defaultSelect">Meta Title</label>
                                    <textarea class="form-control" id="meta_title" name="meta_title" rows="2" placeholder=""><?= (!empty($contact_details[0]['meta_title'])) ? $contact_details[0]['meta_title'] : '' ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="defaultSelect">Meta Description</label>
                                    <textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder=""><?= (!empty($contact_details[0]['meta_description'])) ? $contact_details[0]['meta_description'] : '' ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
</div>
<script>
    var vRules = {
        heading: {
            required: true
        },
        meta_title: {
            required: true
        },
    };
    var vMessages = {
        heading: {
            required: "Please enter heading."
        },
        meta_title: {
            required: "Please enter meta title."
        },
    };
    $("#form-validate").validate({
        rules: vRules,
        messages: vMessages,
        submitHandler: function (form) {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var act = "<?php echo base_url();?>contact/submitForm";
            $("#form-validate").ajaxSubmit({
                url: act,
                type: 'post',
                cache: false,
                clearForm: false,
                success: function (response) {
                    var res = eval('(' + response + ')');
                    if (res['success'] == "1") {
                        displayMsg("success", res['msg']);
                        setTimeout(function () {
                            window.location = "<?php echo base_url();?>contact/index";
                        }, 2000);
                    } else {
                        displayMsg("error", res['msg']);
                        return false;
                    }
                },
            });
        }
    });

    function delete_image(id, image) {
        var r = confirm("Are you sure wants to delete");
        if (r == true) {
            $.ajax({
                url: "<?php echo base_url();?>contact/deleteImage",
                dataType: 'json',
                type: "post",
                data: {
                    id: id,
                    image: image
                },
                success: function (response) {
                    if (response.success == "1") {
                        displayMsg("success", response.msg);
                        setTimeout(function () {
                            window.location = "<?php echo base_url();?>contact/index";
                        }, 2000);
                    } else {
                        displayMsg("error", response.msg);
                        return false;
                    }
                }
            });
        }
    }
    CKEDITOR.replaceClass = 'editors';
    /*Preview Images*/
    var loadFile = function (event, id) {
        var image = document.getElementById('homepreview-' + id);
        image.src = URL.createObjectURL(event.target.files[0]);
    };
    /*Preview Images*/
</script>