<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Front_user extends CI_Controller {

function __construct(){
	parent::__construct();
	$this->load->model('front_usermodel','',TRUE);
	$this->load->model('common_model/common_model','common',TRUE);
	// if(!$this->privilegeduser->hasPrivilege("UserList")){
	// 	redirect('home');
	// }
}
 
function index(){
	if(!empty($_SESSION["fls_admin"])){
		//print_r($result);exit;
		$result['users_data'] = $this->front_usermodel->getUser();
		// print_r($result['users_data']);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}else{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
}
 

 function addEdit($id=NULL){
	if(!empty($_SESSION["fls_admin"])){
     
		//print_r($_GET);
		$user_id = "";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
		}
		
		//echo $user_id;
		$result['roles'] = $this->front_usermodel->getDropdown("roles","role_id,role_name");
		$result['users'] = $this->front_usermodel->getFormdata($user_id);
		
		// echo "<pre>";print_r($result['users']);exit;
		
		
		$this->load->view('template/header.php');
		//$this->load->view('homebanner/addEdit',$resulte);
		$this->load->view('users/addEdit',$result);
		$this->load->view('template/footer.php');
	}else{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
}
 
function submitForm(){
		/*print_r($_POST);
		exit;*/
		
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{
		
		$condition = "email_id= '".$_POST['email_id']."'";
		if(isset($_POST['user_id']) && $_POST['user_id'] > 0)
		{
			$condition .= " &&  user_id !='".$_POST['user_id']."'  ";
		}
		
		$check_name = $this->front_usermodel->checkRecord($_POST,$condition);
		if(!empty($check_name[0]->user_id)){
			echo json_encode(array("success"=>"0",'msg'=>'User Already Present!'));
			exit;
		}
		//exit;
		
		if(!empty($_POST['user_id'])){
			//update
			/*echo "in update";
			print_r($_POST);
			exit;*/
			$data = array();
			$data['role_id'] = $_POST['role_id'];
			$data['full_name'] = $_POST['user_name'];
			$data['email_id'] = $_POST['email_id'];
			if(!empty($_POST['password'])){
				$data['password'] = md5($_POST['password']);
			}
			
			$data['status'] = $_POST['status'];
			
			$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
			
			$result = $this->front_usermodel->updateUserId($data,$_POST['user_id']);
				
			if(!empty($result)){
				echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully'));
				exit;
			}else{
				echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
				exit;
			}
			
			
		}else{
			//add
			/*echo "in add";
			print_r($_POST);
			exit;*/
			
			$data = array();
			//$data['is_active'] = $_POST['is_active'];
			$data['role_id'] = $_POST['role_id'];
			$data['full_name'] = $_POST['user_name'];
			$data['email_id'] = $_POST['email_id'];
			if(!empty($_POST['password'])){
				$data['password'] = md5($_POST['password']);
			}
			
			$data['status'] = $_POST['status'];
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
			$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
			
			$result = $this->front_usermodel->insertData('tbl_users',$data,'1');
			
			if(!empty($result)){

				$message = "";
					$message  = '<html><body>';		
					$message .= '<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;width:100%;">
										<tbody>
											<tr>
												<td align="center" valign="top">
													<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;border:1px solid #ccc;" width="100%">
														<tbody>
															<tr>
																<td align="center" valign="top">
																	<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;" width="100%">
																		<tbody>
																			<tr>
																				<td style="height:100px;text-align:left;text-align:center;border-bottom:2px solid #0075bc">Itara CRM</td>
																			</tr>
																			<tr>
																				<td style="padding:20px;">
																					<p style="margin-bottom:10px;">
																						Hello '.$_POST['user_name'].',</p>
																					<p style="margin-bottom:10px;">
																						You have successfully registered as a User on Itara CRM.</p>
																					<p style="margin-bottom:10px;">
																						Your Details are below</p>
																					<p style="margin-bottom:10px;">
																						Link: http://webshowcase-india.com/demo/itara_cms/</p>
																					<p style="margin-bottom:10px;">
																						Email Id: '.$_POST['email_id'].'</p>
																					<p style="margin-bottom:10px;">
																						Password: '.$_POST['password'].'<br />
																						<br />
																					</p>
																					<p style="margin-bottom:2px;">
																						Regards,</p>
																					<p style="margin:2px 0 2px 0">
																						Support Team</p>
																					<p style="margin:2px 0 2px 0">
																						Itara CRM</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
';
					$message .=  '</body></html>';
					
					$this->email->clear();
					$this->email->from("info@attoinfotech.in"); // change it to yours
					$subject = "";
					$subject = "Itara CRM - User Credentials";
					$this->email->to($_POST['email_id']); // change it to yours
					$this->email->subject($subject);
					$this->email->message($message);
					$checkemail = $this->email->send();
					$this->email->clear(TRUE);
					
				echo json_encode(array("success"=>"1",'msg'=>'Record Added Successfully.'));
				exit;
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
				exit;
			}
			
		}
		 
		
	}else{
		return false;
	}
}


//For Delete

function delRecord()
 {
	$id=$_POST['id'];
	$status=$_POST['status'];
	
	$appdResult = $this->front_usermodel->delrecord("tbl_users","user_id",$id,$status);
	
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>
