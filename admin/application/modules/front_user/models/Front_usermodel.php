<?PHP
class Front_usermodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 	
	 }
	 
	function getUser(){
		$this -> db -> select('i.*,r.role_name');
		$this -> db -> from('tbl_users as i');
		$this -> db -> join('roles as r', 'i.role_id  = r.role_id', 'left');
		$this -> db ->where_not_in("i.role_id",1);
		$query = $this -> db -> get();
			if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	function getDropdown($tbl_name,$tble_flieds){
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	   $query = $this -> db -> get();
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	function getFormdata($ID){
	   $this -> db -> select('u.*');
	   $this -> db -> from('tbl_users as u');
	   $this -> db -> where('u.user_id', $ID);
	   $query = $this -> db -> get();
	   //print_r($this->db->last_query());
	   //exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	//Update customer by id
	 function updateUserId($datar,$eid)
	 {
		 $this -> db -> where('user_id', $eid);
		 $this -> db -> update('tbl_users',$datar);
		 
		 if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		 else
			{
			  return true;
			} 
	 }
	 
	// function delrecord($tbl_name,$tbl_id,$record_id,$data)
	// {
	// 	$this -> db -> where($tbl_id, $record_id);
	// 	$this -> db -> update($tbl_name,$data);
		 
	// 	if ($this->db->affected_rows() > 0){
	// 		return true;
	// 	}
	// 	else{
	// 		return true;
	// 	} 
	// }
	function delrecord($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function checkRecord($POST,$condition){
		//print_r($POST);
		//exit;
	   $this -> db -> select('r.user_id');
	   $this -> db -> from('tbl_users as r');
	   $this->db->where("($condition)");
	   $query = $this -> db -> get();
		//print_r($this->db->last_query());
	   //exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
}
?>