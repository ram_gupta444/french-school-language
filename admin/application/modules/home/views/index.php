<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="#/"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Homepage</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#/">Website Pages</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Homepage</a>
							</li>
						</ul>
					</div>
					<div>
						<a href="#" class="btn btn-light-itara">Discard</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
					<input type="hidden" id="hidden_home_id" name="hidden_home_id" value="<?= (!empty($home_details[0]['home_id'])) ? $home_details[0]['home_id'] : '' ?>">

					<input type="hidden" id="hidden_banner_image_name" name="hidden_banner_image_name" value="<?= (!empty($home_details[0]['banner_image'])) ? $home_details[0]['banner_image'] : '' ?>">

						<div class="section-single">
							<h3>Section 1</h3>
							
							<div class="form-group">
								<label for="homebannerimg">Banner Image </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-1" src="<?= (!empty($home_details[0]['banner_image']) ? FRONT_URL."/images/home/".$home_details[0]['banner_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="banner_image" id="banner_image" onchange="loadFile(event,'1')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="banner_image">Upload</label>
								</a>
								<?php if(!empty($home_details[0]['banner_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($home_details[0]['home_id'])) ? $home_details[0]['home_id'] : '' ?>','banner_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Banner Heading * </label>
								<textarea class="form-control editors" id="banner_heading" name="banner_heading" rows="2" placeholder=""><?= (!empty($home_details[0]['banner_heading'])) ? $home_details[0]['banner_heading'] : '' ?></textarea>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Text</label>
										<input type="text" class="form-control" id="button_text" name="button_text" placeholder="Enter Link Text" value="<?= (!empty($home_details[0]['button_text'])) ? $home_details[0]['button_text'] : '' ?>">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Button Link</label>
										<input type="text" class="form-control" id="button_link" name="button_link" placeholder="Enter Link URL" value="<?= (!empty($home_details[0]['button_link'])) ? $home_details[0]['button_link'] : '' ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2</h3>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="section2_heading" name="section2_heading" rows="5" placeholder=""><?= (!empty($home_details[0]['section2_heading'])) ? $home_details[0]['section2_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Description</label>
								<textarea class="form-control editors" id="section2_description" name="section2_description" rows="5" placeholder=""><?= (!empty($home_details[0]['section2_description'])) ? $home_details[0]['section2_description'] : '' ?></textarea>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Know More Button Text</label>
										<input type="text" class="form-control" id="knowMorebutton_text" name="knowMorebutton_text" placeholder="Enter Link Text" value="<?= (!empty($home_details[0]['knowMorebutton_text'])) ? $home_details[0]['knowMorebutton_text'] : '' ?>">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-6">
									<div class="form-group">
										<label for="discountcode">Know More Button Link</label>
										<input type="text" class="form-control" id="knowMorebutton_link" name="knowMorebutton_link" placeholder="Enter Link URL" value="<?= (!empty($home_details[0]['knowMorebutton_link'])) ? $home_details[0]['knowMorebutton_link'] : '' ?>">
									</div>
								</div>
							</div>
							
						</div>
						
						<div class="section-single">
							<h3>Section 3</h3>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="section3_heading" name="section3_heading" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_heading'])) ? $home_details[0]['section3_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Sub Heading</label>
								<textarea class="form-control editors" id="section3_sub_heading" name="section3_sub_heading" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_sub_heading'])) ? $home_details[0]['section3_sub_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Description </label>
								<textarea class="form-control editors" id="section3_description" name="section3_description" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_description'])) ? $home_details[0]['section3_description'] : '' ?></textarea>
							</div>

							
						</div>
						<div class="section-single">
							<h3>Section 3 Block 1</h3>
							<div class="form-group">
								<label for="section1text">Section 3 Heading 1 </label>
								<textarea class="form-control editors" id="section3_heading1" name="section3_heading1" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_heading1'])) ? $home_details[0]['section3_heading1'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 3 Description 1 </label>
								<textarea class="form-control editors" id="section3_description1" name="section3_description1" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_description1'])) ? $home_details[0]['section3_description1'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3 Block 2</h3>
							<div class="form-group">
								<label for="section1text">Section 3 Heading 2 </label>
								<textarea class="form-control editors" id="section3_heading2" name="section3_heading2" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_heading2'])) ? $home_details[0]['section3_heading2'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 3 Description 2 </label>
								<textarea class="form-control editors" id="section3_description2" name="section3_description2" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_description2'])) ? $home_details[0]['section3_description2'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3 Block 3</h3>
							<div class="form-group">
								<label for="section1text">Section 3 Heading 3 </label>
								<textarea class="form-control editors" id="section3_heading3" name="section3_heading3" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_heading3'])) ? $home_details[0]['section3_heading3'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 3 Description 3 </label>
								<textarea class="form-control editors" id="section3_description3" name="section3_description3" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_description3'])) ? $home_details[0]['section3_description3'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3 Block 4</h3>
							<div class="form-group">
								<label for="section1text">Section 3 Heading 4 </label>
								<textarea class="form-control editors" id="section3_heading4" name="section3_heading4" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_heading4'])) ? $home_details[0]['section3_heading4'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Section 3 Description 4 </label>
								<textarea class="form-control editors" id="section3_description4" name="section3_description4" rows="2" placeholder=""><?= (!empty($home_details[0]['section3_description4'])) ? $home_details[0]['section3_description4'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 4</h3>
							<div class="form-group">
								<label for="section1text">Heading</label>
								<textarea class="form-control editors" id="section4_heading" name="section4_heading" rows="2" placeholder=""><?= (!empty($home_details[0]['section4_heading'])) ? $home_details[0]['section4_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="section1text">Sub Heading</label>
								<textarea class="form-control editors" id="section4_sub_heading" name="section4_sub_heading" rows="2" placeholder=""><?= (!empty($home_details[0]['section4_sub_heading'])) ? $home_details[0]['section4_sub_heading'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single last">
							<h3>Meta Tags</h3>
							<div class="form-group">
								<label for="defaultSelect">Meta Title</label>
								<textarea class="form-control editors" id="meta_title" name="meta_title" rows="2" placeholder=""><?= (!empty($home_details[0]['meta_title'])) ? $home_details[0]['meta_title'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="defaultSelect">Meta Description</label>
								<textarea class="form-control editors" id="meta_description" name="meta_description" rows="5" placeholder=""><?= (!empty($home_details[0]['meta_description'])) ? $home_details[0]['meta_description'] : '' ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	
var vRules = {
	banner_heading:{required:true},
};
var vMessages = {
	banner_heading:{required:"Please enter banner heading."},
};
$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
		}
		var act = "<?php echo base_url();?>home/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home/index";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
		});
	}
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>home/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>home/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}

CKEDITOR.replaceClass = 'editors';


/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
</script>
