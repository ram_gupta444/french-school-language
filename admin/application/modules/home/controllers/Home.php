<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('homemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['home_details'] = $this->common->getData("tbl_home",'*',$condition);
		
		$this->load->view('template/header.php');
		$this->load->view('home/index',$result);
		$this->load->view('template/footer.php');
	}
	
	function submitForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['banner_heading'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter banner heading'));
				exit;
			}
			// echo "<pre>";print_r($_POST);exit;
			$data = array();
			$data['banner_heading'] = (!empty($_POST['banner_heading'])) ? $_POST['banner_heading'] : '';
			$data['button_text'] = (!empty($_POST['button_text'])) ? $_POST['button_text'] : '';
			$data['button_link'] = (!empty($_POST['button_link'])) ? $_POST['button_link'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_description'] = (!empty($_POST['section2_description'])) ? $_POST['section2_description'] : '';
			$data['knowMorebutton_text'] = (!empty($_POST['knowMorebutton_text'])) ? $_POST['knowMorebutton_text'] : '';
			$data['knowMorebutton_link'] = (!empty($_POST['knowMorebutton_link'])) ? $_POST['knowMorebutton_link'] : '';
			$data['section3_heading'] = (!empty($_POST['section3_heading'])) ? $_POST['section3_heading'] : '';
			$data['section3_sub_heading'] = (!empty($_POST['section3_sub_heading'])) ? $_POST['section3_sub_heading'] : '';
			$data['section3_description'] = (!empty($_POST['section3_description'])) ? $_POST['section3_description'] : '';
			$data['section3_heading1'] = (!empty($_POST['section3_heading1'])) ? $_POST['section3_heading1'] : '';
			$data['section3_description1'] = (!empty($_POST['section3_description1'])) ? $_POST['section3_description1'] : '';
			$data['section3_heading2'] = (!empty($_POST['section3_heading2'])) ? $_POST['section3_heading2'] : '';
			$data['section3_description2'] = (!empty($_POST['section3_description2'])) ? $_POST['section3_description2'] : '';
			$data['section3_heading3'] = (!empty($_POST['section3_heading3'])) ? $_POST['section3_heading3'] : '';
			$data['section3_description3'] = (!empty($_POST['section3_description3'])) ? $_POST['section3_description3'] : '';
			$data['section3_heading4'] = (!empty($_POST['section3_heading4'])) ? $_POST['section3_heading4'] : '';
			$data['section3_description4'] = (!empty($_POST['section3_description4'])) ? $_POST['section3_description4'] : '';
			$data['section4_heading'] = (!empty($_POST['section4_heading'])) ? $_POST['section4_heading'] : '';
			$data['section4_sub_heading'] = (!empty($_POST['section4_sub_heading'])) ? $_POST['section4_sub_heading'] : '';
			$data['meta_title'] = (!empty($_POST['meta_title'])) ? $_POST['meta_title'] : '';
			$data['meta_description'] = (!empty($_POST['meta_description'])) ? $_POST['meta_description'] : '';
			
			if(!empty($_FILES['banner_image'])){
				$_FILES['image_name']['name'] =  'P'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['banner_image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['banner_image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['banner_image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['banner_image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['banner_image']['size'];
				/* File Type Change multiple into single */
				
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['banner_image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_banner_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/home/'.$_POST['hidden_banner_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/home/'. $_POST['hidden_banner_image_name']);
						}
					}
				}
			}

			if(!empty($_POST['hidden_home_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->homemodel->updateRecord('tbl_home', $data,'home_id',$_POST['hidden_home_id']);
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->homemodel->insertData('tbl_home', $data,'1');
			}
			
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/home/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }

	function deleteImage(){
		$condition = "home_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_home",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/home/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->homemodel->updateRecord('tbl_home', $data,'home_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}

	function logout()
	{
		session_destroy();
		redirect('login', 'refresh');
	}

}

?>
