<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="#/"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Group Learning List</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Group Learning List</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>adult_group_study/addEdit" class="btn btn-dark-itara">Add New group</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>#</th>
									<th>Heading</th>
									<th>Description</th>
									<th>Language</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>#</th>
									<th>Heading</th>
									<th>Description</th>
									<th>Language</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($adult_group_study_list)) {
                                    foreach ($adult_group_study_list as $key => $value) { ?>
								<tr>
									<td><?= $value['adult_group_study_id'] ?></td>
									<td><?= $value['section1_heading'] ?></td>
									<td><?= $value['section1_description'] ?></td>
									<td><?= $value['language_name'] ?></td>
									<td>
										<div class="actions">
											<a href="<?= base_url('adult_group_study/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['adult_group_study_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                           
										</div>
									</td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(".delete_event").click(function(e) {
	var id=$(this).attr("data-id");
	var r=confirm("Are you sure to wants to delete");
    	if (r==true){
			$.ajax({
				url: "<?php echo base_url();?>event/deleteevent",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>event/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
    	}
})
</script>