<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Adult_group_study extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('adult_group_studymodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		// $result['adult_group_study_list'] = $this->common->getData("tbl_adult_group_study",'*',$condition);
		$result['adult_group_study_list'] = $this->adult_group_studymodel->getFormdata();
		
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$group_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$group_id = $url_prams['id'];
				
				$result['stored_langauge'] = $this->common->getData("tbl_adult_group_study","language"," adult_group_study_id = ".$group_id."  ");
			}
			// echo $group_id;
			$result['language_dropdown'] = $this->adult_group_studymodel->getDropdown("tbl_language_master","language_master_id,language_name");
			$result['adult_group_study_details'] = $this->common->getData("tbl_adult_group_study",'*',"adult_group_study_id = ".$group_id." ");


			// echo "<pre>";print_r($result['stored_langauge']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['section1_heading'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter section 1 heading'));
				exit;
			}
			$condition = "language = '".$_POST['language']."' ";
			if(isset($_POST['hidden_adult_group_study_id']) && (!empty($_POST['hidden_adult_group_study_id'])))
			{
				$condition .= " &&  adult_group_study_id !='".$_POST['hidden_adult_group_study_id']."' ";
			}
			$check_language = $this->common->getdata("tbl_adult_group_study",'language',$condition);
			if(!empty($check_language)){
				echo json_encode(array("success"=>"0",'msg'=>'Language Already Present!'));
				exit;
			}

			$data = array();
			$data['language'] = (!empty($_POST['language'])) ? $_POST['language'] : '';
			$data['section1_heading'] = (!empty($_POST['section1_heading'])) ? $_POST['section1_heading'] : '';
			$data['section1_description'] = (!empty($_POST['section1_description'])) ? $_POST['section1_description'] : '';
			$data['dynamic_data_title'] = (!empty($_POST['dynamic_data_title'])) ? $_POST['dynamic_data_title'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_sub_heading'] = (!empty($_POST['section2_sub_heading'])) ? $_POST['section2_sub_heading'] : '';
			$data['meta_title'] = (!empty($_POST['meta_title'])) ? $_POST['meta_title'] : '';
			$data['meta_description'] = (!empty($_POST['meta_description'])) ? $_POST['meta_description'] : '';
			// echo "<pre>";print_r($data);exit;
			if(!empty($_POST['hidden_adult_group_study_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->adult_group_studymodel->updateRecord('tbl_adult_group_study', $data,'adult_group_study_id',$_POST['hidden_adult_group_study_id']);
				
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->adult_group_studymodel->insertData('tbl_adult_group_study', $data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

}

?>
