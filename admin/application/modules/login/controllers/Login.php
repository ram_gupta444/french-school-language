<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require "PHPMailerAutoload.php";
class Login extends MX_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('loginmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if($this->session->userdata('fls_admin'))
			redirect(base_url().'home/index');  
	}

	public function index(){
		$this->load->view('template/login_header.php');
		$this->load->view('index.php');
		$this->load->view('template/login_footer.php');
	}
	function loginvalidate(){
        // echo "<pre>";
        // print_r($_POST);exit;
        $result = $this->loginmodel->logincheck($_POST['username'],md5($_POST['password']));
        if($result){
			if(!empty($_POST['remember_me']) && $_POST['remember_me'] == "on"){
				$details = array("username"=>$_POST['username'],"password"=>$_POST['password']);
				setcookie("fls_admin_remember_me",serialize($details));		
			}else{
				setcookie("fls_admin_remember_me","");
				unset($_COOKIE["fls_admin_remember_me"]);
			}
			$_SESSION["fls_admin"] = $result;
			// $this->getByUsername($_SESSION["fls_admin"][0]->user_id);	
			echo json_encode(array("success"=>true, "msg"=>'You are successfully logged in.'));
			exit;	
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Wrong user name or password.'));
			exit;
		}
	}
	function forgot_password()
	{
		// $result = $this->loginmodel->logincheck($_POST['username'],md5($_POST['password']));
		$result = $this->loginmodel->getDataCustom("tbl_users", "*","status = 'Active' AND email_id ='".$_POST['forgot_email_id']."' ");
		// echo "<pre>";print_r($result[0]['user_id']);exit;
        if($result){
			$code = md5(rand());
			$data_array =array();
			$data_array['verification_code'] = $code;
			$result_update = $this->loginmodel->updateRecord('tbl_users', $data_array,'user_id',$result[0]['user_id']);
			$link = base_url().'login/reset_password/'.$code;
			// smtp mail code start----------
			// if($result_update){
			// 	$toemail = $_POST['forgot_email_id'];
			// 	$subject = 'Reset password';
			// 	$body = "To reset your Itara account password click the link <a href='$link'>Here</a>."; 
			// 	$mail = new PHPMailer;
			// 	$mail->isSMTP();
			// 	$mail->Host = 'smtp.gmail.com';
			// 	$mail->SMTPAuth = true;
			// 	$mail->Username = 'ramshankargupta444@gmail.com';
			// 	$mail->Password = 'xuatpvmdirjwggvb';
			// 	// ouprpbabhoujegyo
			// 	$mail->SMTPSecure = 'tls';
			// 	$mail->Port = 587;
			// 	$mail->addaddress($toemail);
			// 	$mail->isHTML(true); // Set email format to HTML
			// 	$mail->Subject = $subject;
			// 	$mail->Body    = $body;
			// 	if (!$mail->send()) {
			// 		echo 'Message could not be sent.';
			// 		echo 'Mailer Error: ' . $mail->ErrorInfo;
			// 		exit;
			// 	}
			// }
			// smtp mail code end----------
			if($result_update){
				$this->email->from(FROM_EMAIL, 'Reset password');
				$this->email->to($_POST['forgot_email_id']);
				$this->email->subject('Reset password');
				$content = "To reset your Itara account password click the link <a href='$link'>Here</a>.";
				$this->email->message($content);
				$this->email->send();
			}
			echo json_encode(array("success"=>true, "msg"=>'A link to reset password is sent to your Mail.'));
			exit;	
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Please enter a valid Email!'));
			exit;
		}
	}

	// function getByUsername($user_id=null){
	// 	$user_id=1;
	// 	$result = array();
	// 	$user_privilage = array();
	// 	$condition = "user_id = '".$user_id."' ";
		
	// 	$main_table = array("tbl_users as tu", array("tu.role_id"));
	// 	$join_tables =  array();
	// 	$join_tables = array(
	// 			array("", "role_perm as  rp", "tu.role_id = rp.role_id", array()),
	// 			array("", "permissions as  p", "p.perm_id = rp.perm_id", array("p.perm_desc")),
	// 			);
	// 	$rs = $this->common->JoinFetch($main_table, $join_tables, $condition); 
	// 	$result = $this->common->MySqlFetchRow($rs, "array");
	// 	foreach ($result as $key => $value) {
	// 		$user_privilage[$value['perm_desc']] = true;
	// 	}
	// 			$_SESSION["fls_admin"]["user_privilage"] = $user_privilage; 

	// }

	function reset_password()
	{
		$code = $this->uri->segment(3);
		$result = $this->loginmodel->getDataCustom("tbl_users", "user_id","status = 'Active' AND verification_code ='".$code."' ");
		// print_r($result[0]['user_id']);exit;
		$user_id['id'] = $result[0]['user_id'];
		$this->load->view('template/login_header.php');
		$this->load->view('reset_password.php',$user_id);
		$this->load->view('template/login_footer.php');
	}
	function change_password()
	{
		$result = $this->loginmodel->getDataCustom("tbl_users", "*","status = 'Active' AND user_id ='".$_POST['hidden_user_id']."' ");
		if($result){
			if(!isset($_POST['password'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter new password.'));
				exit;
			}
			if(!isset($_POST['cpassword'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter confirm new password.'));
				exit;
			}
			if($_POST['password'] != $_POST['cpassword']){
				echo json_encode(array("success"=>"0",'msg'=>'Passwords do not match!.'));
				exit;
			}
			$data_array =array();
			$data_array['verification_code'] = '';
			$data_array['password'] = MD5($_POST['password']);
			$result_update = $this->loginmodel->updateRecord('tbl_users', $data_array,'user_id',$result[0]['user_id']);
			if($result_update){
				echo json_encode(array("success"=>true, "msg"=>'Password changed successfully!'));
				exit;
			}else{
				echo json_encode(array("success"=>false, "msg"=>'Something went wrong, Please try again later!'));
				exit;
			}
		}else{
			echo json_encode(array("success"=>false, "msg"=>'User not found, Please try again later!'));
			exit;
		}
	}
	
}
?>