
<html>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
			<div class="mb-3 text-center">
				<img src="<?php echo base_url(); ?>assets/images/itara-logo-login.svg" alt="">
				<h3 class="text-center">Reset Password</h3>
			</div>
            <?php if($id){ ?>
			<div class="login-form">
            <form class="login-form" id="form-validate" method="post">
				<input type="hidden" id="hidden_user_id" name="hidden_user_id" value="<?php echo (!empty($id) ? $id : '' ) ?>">
				<div class="form-group">
					<label for="password" class="placeholder"><b>New Password</b></label>
					<div class="position-relative">
						<input id="password" name="password" type="password" class="form-control" required>
					</div>
                    <label for="cpassword" class="placeholder"><b>Confirm New Password</b></label>
					<div class="position-relative">
						<input id="cpassword" name="cpassword" type="password" class="form-control" required>
					</div>
				</div>
				<div class="form-group form-action-d-flex mb-3">
					<button type="submit" class="btn btn-primary btn-rounded btn-login">Change Password<i class="fa fa-sign-in fa-lg"></i></button>
				</div>
                <div id="show_msg" style="font-size: 12px;"></div>
			</form>
				
			</div>
          <?php  }else{?>
            <div class="heading_s1 text-center">
                <h2>Link Expired!</h2>
                <h3 style="font-weight:normal;">Please try again if you have not recovered your password!</h3>
            </div>
          <?php }?>
		</div>
	</div>
</body>
</html>
<script>
var vRules = {
	password:{required:true},
    cpassword:{required:true},
};
var vMessages = {
	password:{required:"Please enter new password."},
	cpassword:{required:"Please enter confirm password."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/change_password";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$("#show_msg").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);

				}
				else
				{	
					$("#show_msg").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});


var vRules = {
	email:{required:true},
};
var vMessages = {
	email:{required:"Please enter email id."},
};
$("#fp_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/forgot_password";
		$("#fp_form").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$("#show_msg_forgot").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);

				}
				else
				{	
					$("#show_msg_forgot").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});
</script>