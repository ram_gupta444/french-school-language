<html>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
			<div class="mb-3 text-center">
				<img src="<?php echo base_url(); ?>assets/images/logo.svg" style="margin-top: -40px;" alt="">
				<!-- <h3 class="text-center">Sign In To Admin</h3> -->
			</div>
			<div class="login-form">
            <form class="login-form" id="form-validate" method="post">
				<div class="form-group">
					<label for="username" class="placeholder"><b>Username</b></label>
					<input id="username" name="username" type="text" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="password" class="placeholder"><b>Password</b></label>
					<a href="#"  data-target="#exampleModal" data-toggle="modal" class="link float-right">Forgot Password ?</a>
					<div class="position-relative">
						<input id="password" name="password" type="password" class="form-control" required>
						<div class="show-password">
							<i class="icon-eye"></i>
						</div>
					</div>
				</div>
				<div class="form-group form-action-d-flex mb-3">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="rememberme">
						<label class="custom-control-label m-0" for="rememberme">Remember Me</label>
					</div>
					<button type="submit" class="btn btn-primary btn-rounded btn-login">Sign In <i class="fa fa-sign-in fa-lg"></i></button>
				</div>
                <div id="show_msg" style="font-size: 12px;"></div>
                </form>
			</div>
		</div>
		<div class="container container-signup animated fadeIn">
			<h3 class="text-center">Sign Up</h3>
			<div class="login-form">
				<div class="form-group">
					<label for="fullname" class="placeholder"><b>Fullname</b></label>
					<input  id="fullname" name="fullname" type="text" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="email" class="placeholder"><b>Email</b></label>
					<input  id="email" name="email" type="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="passwordsignin" class="placeholder"><b>Password</b></label>
					<div class="position-relative">
						<input  id="passwordsignin" name="passwordsignin" type="password" class="form-control" required>
						<div class="show-password">
							<i class="icon-eye"></i>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="confirmpassword" class="placeholder"><b>Confirm Password</b></label>
					<div class="position-relative">
						<input  id="confirmpassword" name="confirmpassword" type="password" class="form-control" required>
						<div class="show-password">
							<i class="icon-eye"></i>
						</div>
					</div>
				</div>
				<div class="row form-sub m-0">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="agree" id="agree">
						<label class="custom-control-label" for="agree">I Agree the terms and conditions.</label>
					</div>
				</div>
				<div class="row form-action">
					<div class="col-md-6">
						<a href="#" id="show-signin" class="btn btn-danger btn-link w-100 fw-bold">Cancel</a>
					</div>
					<div class="col-md-6">
						<a href="#" class="btn btn-primary w-100 fw-bold">Sign Up</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form id="fp_form" method="post">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Reset Password</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="show_msg_forgot" style="font-size: 12px;padding-left: 20px;"></div>
			<div class="modal-body">
			<input type="text" required class="form-control" name="forgot_email_id" placeholder="Enter Email">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">submit</button>
			</div>
			</div>
		</form>
	</div>
	</div>
</body>
</html>
<script>

var vRules = {
	username:{required:true},
	password:{required:true}
};

var vMessages = {
	username:{required:"Please enter username."},
	password:{required:"Please enter password."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/loginvalidate";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$("#show_msg").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);
				}
				else
				{	
					$("#show_msg").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});

var vRules = {
	email:{required:true},
};
var vMessages = {
	email:{required:"Please enter email id."},
};

$("#fp_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/forgot_password";
		$("#fp_form").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$("#show_msg_forgot").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);
				}
				else
				{	
					$("#show_msg_forgot").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});
</script>