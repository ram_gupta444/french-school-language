<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>testimonial"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>Testimonial</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>testimonial">Testimonials</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>testimonial/addEdit"> New Testimonial</a>
							</li>						
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url(); ?>testimonial" class="btn btn-light-itara">Cancel</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<input type="hidden" id="hidden_testimonial_id" name="hidden_testimonial_id" value="<?php if(!empty($testimonial_details[0]->testimonial_id)){echo $testimonial_details[0]->testimonial_id;}?>"> 
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="section-single"> 
							<div class="form-group">
								<label for="homebannerheading">Select language</label>
								<select class="form-control form-control select2" id="languages" name="languages[]" multiple>
								<option value="">Select language</option>
								<?php if(!empty($language_dropdown)){ ?>
									<option value="">Select language</option>
											<?php 
											foreach($language_dropdown as $key=>$val){
												$sel='';
												$sel= (in_array($val['language_master_id'],explode(',',$stored_langauges[0]['languages']))?'selected':'');
											?>
											<option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>
										<?php } 
											}
										?>
								</select>
							</div><br>
							<div class="form-group">
								<p><strong>Show on home page</strong></p>	
								<input type="checkbox" name="show_on_homepage" id="show_on_homepage" <?php echo (!empty($testimonial_details) &&  $testimonial_details[0]->show_on_homepage == '1' ? "checked": " ")?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">		
							</div>
						</div> 
						<div class="section-single">						
							<div class="form-group">
								<label for="testimonialname">Name</label>
								<input type="text"  class="form-control" id="testimonialname" name="testimonialname" placeholder="Enter Full Name" value="<?php if(!empty($testimonial_details[0]->testimonialname)){echo $testimonial_details[0]->testimonialname;}?>">
							</div>
							<div class="form-group">
								<label for="faqquestion">Testimonial</label>
								<textarea class="form-control" id="shortdesc" name="shortdesc" rows="5" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."><?php if(!empty($testimonial_details[0]->testimonial_description)){echo $testimonial_details[0]->testimonial_description;}?></textarea>
							</div>
							<div class="form-group">
								<p><strong>Status</strong></p>	
								<input type="checkbox" name="status" id="status" <?php echo (!empty($testimonial_details) &&  $testimonial_details[0]->status == 'Active' ? "checked": " ")?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">		
							</div>
						</div>	
					</div>
					
					
					<!-- <div class="col-12 col-sm-12 col-md-4">
						<div class="section-sidebar section-bordered">
							<h4>Published Details</h4>
							<p class="mb-1"><strong>Status:</strong> Published</p>			
							<p><strong>Published On:</strong> 12 August 2020</p>						
							<div class="row">
								<div class="col-4">
									<p><strong>Active</strong></p>								
								</div>
								<div class="col-8">
									<div class="item-control">
										<input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
									</div>
								</div>
							</div>
							<div class="sidebar-actions">
								<a href="#/" class="action-icon"><img src="<?php echo base_url(); ?>assets/images/duplicate.svg" alt=""> Duplicate</a>
								<a href="#/" class="action-icon"><img src="<?php echo base_url(); ?>assets/images/delete.svg" alt=""> Delete</a>
							</div>
						</div>					
					</div> -->
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){

	var vRules = {
		testimonialname:{required:true},
		shortdesc:{required:true}
	};
	var vMessages = {
		testimonialname:{required:"Please enter testimonial name."},
		shortdesc:{required:"Please enter description."}
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>testimonial/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>testimonial/index";
						},2000);

					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
});

document.title = "Add Edit Testimonial";

</script>