<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="<?php echo base_url(); ?>testimonial"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Testimonials</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Testimonials</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>testimonial/addEdit" class="btn btn-dark-itara">Add Testimonial</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Language</th>
									<th>Created On</th>
									<th>Status</th>	
									<th>Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Language</th>
									<th>Created On</th>
									<th>Status</th>	
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($testimonial_details)) {
                                    foreach ($testimonial_details as $key => $value) { ?>
								<tr>
									<td><?= $value['testimonial_id'] ?></td>
									<td><?= $value['testimonialname'] ?></td>
									<td>
									<?php 
									if(!empty($languages)){
										if(!empty($languages[$key])){
											foreach ($languages[$key] as $lkey => $languagevalue) { ?>
												<?= $languagevalue['language_name']."<br>"; ?>
											<?php } 
										}
									}?>
									</td>
									<td><?= date("d M, y", strtotime($value['created_on'])) ?></td>
									<td><span class="<?= ($value['status']== 'Active') ?"active":"inactive"?>-label"><?= $value['status'] ?></span></span></td>
									<td>
										<div class="actions">
											<a href="<?= base_url('testimonial/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['testimonial_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                            <a href="javasript:void(0)" title="Delete"><img src="<?php echo base_url();?>assets/images/delete.svg" alt="Delete" class="delete_testimonial" data-id="<?php echo $value['testimonial_id'];?>"></a>
                                            </td>
										</div>
									</td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(".delete_testimonial").click(function(e) {
	var id=$(this).attr("data-id");
	var r=confirm("Are you sure to wants to delete");
    	if (r==true){
			$.ajax({
				url: "<?php echo base_url();?>testimonial/deletetestimonial",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>testimonial/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
    	}
})
		
</script>