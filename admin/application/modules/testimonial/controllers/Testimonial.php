<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Testimonial extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('testimonialmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['testimonial_details'] = $this->common->getData("tbl_testimonial",'*',$condition);
		if(!empty($result['testimonial_details'])){
			foreach ($result['testimonial_details'] as $key => $value) {
				$result['languages'][] = $this->testimonialmodel->getCustomList($value['languages']);
				// $languages[] = $value['languages'];
			}
		}
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$testimonial_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$testimonial_id = $url_prams['id'];
				$result['stored_langauges'] = $this->common->getData("tbl_testimonial","languages"," testimonial_id = ".$testimonial_id."  ");
			}
			// echo $testimonial_id;
			$condition = "status = 'Active'  ";
			$result['language_dropdown'] = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 
			$result['testimonial_details'] = $this->testimonialmodel->getFormdata($testimonial_id);
			// echo "<pre>";print_r($result['testimonial_details']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['testimonialname'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter testimonial name'));
				exit;
			}
			$data = array();
			if(!empty($_POST['status']) && isset($_POST['status'])){
				$data['status'] = 'Active';
			}else{
				$data['status'] = 'Inactive';
			}
			if(!empty($_POST['show_on_homepage']) && isset($_POST['show_on_homepage'])){
				$data['show_on_homepage'] = 1;
			}else{
				$data['show_on_homepage'] = 0;
			}
			$data['testimonialname'] = $_POST['testimonialname'];
			$data['testimonial_description'] = $_POST['shortdesc'];
			$data['languages'] = (!empty($_POST['languages'])) ? implode(',',$_POST['languages']) : '';
			if(!empty($_POST['hidden_testimonial_id']))
			{
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				//update 
				$result = $this->testimonialmodel->updateRecord('tbl_testimonial', $data,'testimonial_id ',$_POST['hidden_testimonial_id']);
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->testimonialmodel->insertData('tbl_testimonial',$data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}
		else {
			return false;
		}
	}
	function deletetestimonial(){
		$Result = $this->testimonialmodel->delrecord('tbl_testimonial', 'testimonial_id ',$_POST['id']);
		if($Result)
		{
			echo json_encode(array("success" => "1", "msg" => "deleted successfully.", "body" => NULL));
			exit;
		}else{
			echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
			exit;
		}	
	}
}