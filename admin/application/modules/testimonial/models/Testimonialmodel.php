<?PHP

class Testimonialmodel extends CI_Model

{



    function getFormdata($ID){

        $this -> db -> select('t.*');

        $this -> db -> from('tbl_testimonial as t');

        $this -> db -> where('t.testimonial_id ', $ID);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1){

          return $query->result();

        }

        else{

          return false;

        }

     }

     function insertData($tbl_name,$data_array,$sendid = NULL)

     {

       $this->db->insert($tbl_name,$data_array);

       //print_r($this->db->last_query());

         //exit;

       $result_id = $this->db->insert_id();

       

       if($sendid == 1)

       {

         return $result_id;

       }

     }

     function updateRecord($tableName, $data, $column, $value)

     {

       $this->db->where("$column", $value);

       $this->db->update($tableName, $data);

       if ($this->db->affected_rows() > 0) {

         return true;

       }

       else {

         return true;

       }

     }

     function delrecord($tbl_name,$tbl_id,$record_id)

     {

       $this->db->where($tbl_id, $record_id);

       $this->db->delete($tbl_name);

       if($this->db->affected_rows() >= 1)

       {

         return true;

         }

         else

         {

         return false;

         }

     }
     function getCustomList($languages){
      $this -> db -> select('language_master_id,language_name');
      $this -> db -> from('tbl_language_master');
      // $this -> db -> join("tbl_language_master as l","l.language_master_id IN explode(',',c.languages)");
      $this -> db -> where_in('language_master_id ', explode(',',$languages));
      $query = $this -> db -> get();
      // print_r($this->db->last_query());exit;
      if($query -> num_rows() >= 1){
        return $query->result_array();
      }
      else{
        return false;
      }
   }

}