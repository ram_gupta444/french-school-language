<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Email_template extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('email_templatemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
    function index()
    {
		$result = array();
		$condition = "1=1 ";
		$result['email_data'] = $this->common->getData("tbl_email_template",'*',$condition);
        $this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
	}
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$email_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$email_id = $url_prams['id'];
			}
			//echo $email_id;
			$result['email_details'] = $this->email_templatemodel->getFormdata($email_id);
			// echo "<pre>";print_r($result['menu_details']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['email_title'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select email title'));
				exit;
			}
			$data = array();
			// if(!empty($_POST['status']) && isset($_POST['status'])){
			// 	$data['status'] = 'Active';
			// }else{
			// 	$data['status'] = 'Inactive';
			// }
			$data['email_title'] = (!empty($_POST['email_title'])) ? $_POST['email_title'] : '';
			$data['email_subject'] = (!empty($_POST['email_subject'])) ? $_POST['email_subject'] : '';
			$data['eamil_body'] = (!empty($_POST['eamil_body'])) ? $_POST['eamil_body'] : '';
			if(!empty($_POST['email_template_id']))
			{
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				//update 
				$result = $this->email_templatemodel->updateRecord('tbl_email_template', $data,'email_template_id',$_POST['email_template_id']);
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->email_templatemodel->insertData('tbl_email_template',$data,'1');
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}
		else {
			return false;
		}
	}
}