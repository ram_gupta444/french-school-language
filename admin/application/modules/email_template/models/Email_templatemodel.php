<?PHP
class Email_templatemodel extends CI_Model
{
    function getFormdata($ID){
        $this -> db -> select('e.*');
        $this -> db -> from('tbl_email_template as e');
        $this -> db -> where('e.email_template_id', $ID);
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
          return $query->result();
        }
        else{
          return false;
        }
     }
     function insertData($tbl_name,$data_array,$sendid = NULL)
     {
       $this->db->insert($tbl_name,$data_array);
       //print_r($this->db->last_query());
         //exit;
       $result_id = $this->db->insert_id();
       if($sendid == 1)
       {
         return $result_id;
       }
     }
     function updateRecord($tableName, $data, $column, $value)
     {
       $this->db->where("$column", $value);
       $this->db->update($tableName, $data);
       if ($this->db->affected_rows() > 0) {
         return true;
       }
       else {
         return true;
       }
     }
     function getData($table,$fields,$condition){
      $this -> db -> select($fields);
      $this -> db -> from($table);
      $this->db->where($condition);
      $query = $this -> db -> get();
      if($query -> num_rows() >= 1){
        return $query->result_array();
      }else{
        return false;
      }
    }
}