<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>email_template"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Add Email</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Email</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#"> Add Email</a>
							</li>						
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url();?>email_template"><button class="btn" type="button">Cancel</button></a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="section-single last">	
						<input type="hidden" id="email_template_id" name="email_template_id" value="<?php if(!empty($email_details[0]->email_template_id)){echo $email_details[0]->email_template_id;}?>">
						</div>	
						<div class="form-group">
							<label for="testimonialname">Title*</label>
							<textarea class="form-control editors" id="email_title" name="email_title" rows="2" placeholder=""><?= (!empty($email_details[0]->email_title)) ? $email_details[0]->email_title : '' ?></textarea>
						</div>
						<div class="form-group">
							<label for="homebannerheading">Subject</label>
							<textarea class="form-control editors" id="email_subject" name="email_subject" rows="2" placeholder=""><?= (!empty($email_details[0]->email_subject)) ? $email_details[0]->email_subject : '' ?></textarea>
						</div>
						<div class="form-group">
							<label for="homebannerheading">Email Body</label>
							<textarea class="form-control editors" id="eamil_body" name="eamil_body" rows="2" placeholder=""><?= (!empty($email_details[0]->eamil_body)) ? $email_details[0]->eamil_body : '' ?></textarea>
						</div>
						<!-- <div class="form-group">
							<p><strong>Status</strong></p>	
							<input type="checkbox" name="status" id="status" <?= (!empty($email_details) && $email_details[0]->status == 'Active') ?"checked":" "?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
						</div>		 -->
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	var vRules = {
		email_title:{required:true},
	};
	var vMessages = {
		email_title:{required:"Please enter email title."},
	};
	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
			}
			var act = "<?php echo base_url();?>email_template/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>email_template/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
});
CKEDITOR.replaceClass = 'editors';
document.title = "Add Edit email template";
</script>
