<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
			<div class="page-header-title">
				<h4 class="page-title"><a href="<?php echo base_url(); ?>email_template"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>Email Template</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Settings</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Email Template</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>email_template/addEdit" class="btn btn-dark-itara">Add Email Template</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Created On</th>
									<th>Updated On</th>
									<!-- <th>Status</th>								 -->
									<th>Actions</th>									
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Created On</th>
									<th>Updated On</th>
									<!-- <th>Status</th>								 -->
									<th>Actions</th>	
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($email_data)) {
                                    foreach ($email_data as $key => $value) { ?>
								<tr>
									<td><?= $value['email_template_id'] ?></td>
									<td><?= $value['email_title'] ?></td>
									<td><?= date("d M, y", strtotime($value['created_on'])) ?></td>
									<td><?= date("d M, y", strtotime($value['updated_on'])) ?></td>
									<!-- <td><span class="<?= ($value['status']== 'Active') ?"active":"inactive"?>-label"><?= $value['status'] ?></span></span></td> -->
									<td>
										<div class="actions">
											<a href="<?= base_url('email_template/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['email_template_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                            </td>
										</div>
									</td>
								</tr>
								<?php }
                                } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>