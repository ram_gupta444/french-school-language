<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Counts extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('countsmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['counts_details'] = $this->common->getData("tbl_counts",'*',$condition);
		
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!empty($_POST['edit_exist'])){
				if(!empty($_POST['title'])){
					foreach($_POST['counts_existing'] as $fkey=>$fval){
						if(!empty($_POST['title_existing_id'][$fkey])){
							$count_data = array();
							$count_data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
							$count_data['updated_on'] = date("Y-m-d H:i:s");
							$count_data['title'] = $_POST['title'][$fkey];
							$count_data['numbers'] = (!empty($_POST['numbers'][$fkey])) ? $_POST['numbers'][$fkey] : '';
							
							$result = $this->countsmodel->updateRecord('tbl_counts', $count_data,'counts_id',$_POST['title_existing_id'][$fkey]);
						}else{
							$new_data = array();
							$new_data['title'] = (!empty($_POST['title'][$fkey])) ? $_POST['title'][$fkey] : '';
							$new_data['numbers'] = (!empty($_POST['numbers'][$fkey])) ? $_POST['numbers'][$fkey] : '';
							$new_data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
							$new_data['created_on'] = date("Y-m-d H:i:s");
							$result = $this->countsmodel->insertData("tbl_counts",$new_data,"1");
							}
						}
					}
				
				}else{
					if(!empty($_POST['title']))
					{
						foreach ($_POST['title'] as $key => $value) {
							$data['title'] = $value;
							$data['numbers'] = $_POST['numbers'][$key];
							$data['created_on'] = date("Y-m-d H:i:s");
							$data['created_by'] = $_SESSION["fls_admin"][0]->user_id;	
							
							$result = $this->countsmodel->insertData('tbl_counts',$data,'1');
						}
					}
				}

			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	function deleteCount(){
		$condition = "counts_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_counts",'*',$condition);
		if(!empty($result)){
			$result = $this->countsmodel->delrecord('tbl_counts', 'counts_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Row Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}

?>
