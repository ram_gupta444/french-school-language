<div class="main-panel">
    <div class="container">
        <form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
            <div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h4 class="page-title"><a href="#/"><img
                                    src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt=""
                                    class="back-icon-title"></a> Homepage Counter</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="#">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <a href="#" class="btn btn-light-itara">Discard</a>
                        <button type="submit" class="btn btn-dark-itara">Save</a>
                    </div>
                </div>
                <input type="hidden" id="edit_exist" name="edit_exist" value="<?= (!empty($counts_details[0]['counts_id'])) ? $counts_details[0]['counts_id'] : '' ?>">
                <hr>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="section-single">
                            <?php if(empty($counts_details)){?>
                                <!-- <h3>Counts Name</h3> -->
                                    <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_image_0">
                                        <tbody >
                                            <tr tr_inner_count_image="0" class="active">
                                                <td>
                                                    <input type="hidden" name="counts_existing[0]" value="" id="counts_existing_0">

                                                    <input type="hidden" name="title_existing_id[0]" value="" id="title_existing_id_0">
                                                    <p style="margin:5px 0;">Label</p>
                                                    <input type="text" class="form-control required" name="title[0]" id="title0" placeholder="Enter title">
                                                </td>
                                                <td>
                                                  <p style="margin:5px 0;">Value</p>
                                                    <input type="text" class="form-control required" name="numbers[0]" value="" id="numbers_0" placeholder="Enter number">
                                                </td>
                                                <td>
                                                    <button class="btn p5 bg-transparent add_more_inner_image" panel-count_image="0" type="button"><i class="material-icons" style="font-size:15px;" ><img src="<?php echo base_url();?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            
                                <?php }else{ ?>
                                    <!-- <h3>Counts Name</h3> -->

                                    <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_image_0">
                                        <tbody >
                                        <?php foreach ($counts_details as $key => $value) { ?>
                                            <tr tr_inner_count_image="<?= $key?>" class="active">
                                                <td>
                                                    <input type="hidden" name="counts_existing[<?= $key ?>]" value="<?= (!empty($value['title']) ? $value['title'] : '' )?>" id="counts_existing_<?= $key ?>">

                                                    <input type="hidden" name="title_existing_id[<?= $key ?>]" value="<?= (!empty($value['counts_id']) ? $value['counts_id'] : '' )?>" id="title_existing_id_<?= $key ?>">
                                                    <p style="margin:5px 0;">Label</p>        
                                                    <input type="text" class="form-control <?= (!empty($value['title'])) ? '' :'required'?>" name="title[<?= $key ?>]" id="title<?= $key ?>" value="<?=(!empty($value['title'])) ? $value['title'] : '' ?>">
                                                </td>
                                                <td>
                                                <p style="margin:5px 0;">Value</p>
                                                    <input type="text" class="form-control <?= (!empty($value['numbers'])) ? '' :'required'?>" name="numbers[<?= $key ?>]" value="<?=(!empty($value['numbers'])) ? $value['numbers'] : '' ?>" id="numbers_<?= $key ?>" >
                                                </td>
                                                <td>
                                                <?php if($key =='0'){?>
                                                <button class="btn p5 btn-transparent add_more_inner_image" panel-count_image="<?php echo $key;?>" type="button"><i class="material-icons" style="font-size:15px;" ><img src="<?php echo base_url();?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                                                <?php }else{?>
                                                    <!-- <button title="Remove" class="btn-danger btn btn-sm remove inner_remove_image" type="button">
                                                    <i class="material-icons"><img src="<?= base_url()?>assets/images/minus-outline.svg"></i>
                                                    </button> -->
                                                    <?php if($key != '0'){ ?>
                                                    <a href="#/" class="btn btn-secondary btn-xs" onclick="deleteCount(<?= (!empty($value['counts_id'])) ? $value['counts_id'] : '' ?>);">Delete</a>
                                                    <?php } ?>
                                            <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                            
                                        </tbody>
                                    </table>
                                <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$(document).on("click",".add_more_inner_image",function(){
	var xi = $(this).attr("panel-count_image");
	var txi = $("#admore_inner_table_image_"+xi+" tbody tr:last-child").attr("tr_inner_count_image");
	txi++;
	if(isNaN(txi) || txi == "undefined"){
		txi = 0;
	}
	// if(txi ==4){
	// 	alert("Only 4 Product Variant Images Allowed");
	// 	return false;
	// };
	var inner_tr_image = '<tr tr_inner_count_image="'+txi+'" class="active">'+
							'<td>'+
                                '<input type="hidden" name="counts_existing['+txi+']" value="" id="counts_existing_['+txi+']">'+

								'<input type="hidden" name="title_existing_id['+txi+']" value="" id="title_existing_id_['+txi+']">'+

								'<input type="text" class="form-control required" name="title['+txi+']" id="title'+txi+'" placeholder="Enter title">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control required" name="numbers['+txi+']" value="" id="numbers_['+txi+']" placeholder="Enter number">'+
							'</td>'+
							'<td>'+
								'<button title="Remove" class="btn p5 bg-transparent remove inner_remove_image" type="button">'+
									'<i class="material-icons"><img src="<?= base_url()?>assets/images/minus-outline.svg" style="width:30px"></i>'+
								'</button>'+
							'</td>'+
						'</tr>';
	$("#admore_inner_table_image_"+xi + " tbody").append(inner_tr_image);
});

$(document).on("click",'.inner_remove_image', function(){
	var tr_inner_count_image=$(this).closest('tr').attr('tr_inner_count_image');
			if(tr_inner_count_image!= 0)
			{
				$(this).closest('tr').remove();
			}else{
				swal({title: "Alert!",text: "<p class='font-bold col-red'>Atleast one option is required..</p>",timer: 2000,showConfirmButton: false,html: true});
			}
});


$("#form-validate").validate({
    // rules: vRules,
    // messages: vMessages,
    submitHandler: function (form) {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var act = "<?php echo base_url();?>counts/submitForm";
        $("#form-validate").ajaxSubmit({
            url: act,
            type: 'post',
            cache: false,
            clearForm: false,
            success: function (response) {
                var res = eval('(' + response + ')');
                if (res['success'] == "1") {
                    displayMsg("success", res['msg']);
                    setTimeout(function () {
                        window.location = "<?php echo base_url();?>counts/index";
                    }, 2000);
                } else {
                    displayMsg("error", res['msg']);
                    return false;
                }
            },
        });
    }
});

function deleteCount(id){
var r=confirm("Are you sure wants to delete");
    if (r==true){
        $.ajax({
            url:"<?php echo base_url();?>counts/deleteCount",
            dataType: 'json',
            type: "post",
            data:{id:id},
            success: function (response) {
                if(response.success == "1")
                {
                    displayMsg("success",response.msg);
                    setTimeout(function(){
                            window.location = "<?php echo base_url();?>counts/index";
                        },2000);
                }
                else
                {	
                    displayMsg("error",response.msg);
                    return false;
                }
            }
        });
    } 
}
</script>