<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>terms_conditions"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>Terms & Conditions</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url(); ?>terms_conditions">Terms & Conditions</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
						</ul>
					</div>
					<div>
						<a href="<?php echo base_url(); ?>terms_conditions" class="btn btn-light-itara">Cancel</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<input type="hidden" id="terms_conditions_id" name="terms_conditions_id" value="<?= (!empty($terms_conditions_data[0]['terms_conditions_id'])?$terms_conditions_data[0]['terms_conditions_id']:"") ?>"> 
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="section-single"> 
							<div class="form-group">
								<label for="terms_conditions">Terms & Conditions</label>
								<textarea class="form-control editors" id="terms_conditions" name="terms_conditions" rows="5" placeholder="Add an terms_conditions" required ><?= (!empty($terms_conditions_data[0]['terms_conditions'])?$terms_conditions_data[0]['terms_conditions']:"") ?></textarea>	
							</div> 
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>

$(document).ready(function(){
var vRules = {
	terms_conditions:{required:true},
    };
    var vMessages = {
		terms_conditions:{required:"Please enter heading."},
    };
    $("#form-validate").validate({
        rules: vRules,
        messages: vMessages,
        submitHandler: function (form) {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            var act = "<?php echo base_url();?>terms_conditions/submitForm";
            $("#form-validate").ajaxSubmit({
                url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
                success: function (response) {
                    var res = eval('(' + response + ')');
                    if (res['success'] == "1") {
                        displayMsg("success", res['msg']);
                        setTimeout(function () {
                            window.location = "<?php echo base_url();?>terms_conditions/index";
                        }, 2000);
                    } else {
                        displayMsg("error", res['msg']);
                        return false;
                    }
                },
            });
        }
    });
});

	CKEDITOR.replaceClass = 'editors';
	document.title="Add/Edit terms Conditions";
</script>