<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Event extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('eventmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['events_details'] = $this->common->getData("tbl_events",'*',$condition);
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$event_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$event_id = $url_prams['id'];
				$result['stored_langauges'] = $this->eventmodel->getdata("tbl_events","languages"," events_id = ".$event_id."  ");

			}
			$result['language_dropdown'] = $this->eventmodel->getDropdown("tbl_language_master","language_master_id,language_name");
			// echo $event_id;
			// echo "<pre>";print_r($result['stored_langauges']);exit;
			$result['event_details'] = $this->eventmodel->getFormdata($event_id);

			$result['stored_similar_events'] = $this->eventmodel->getdata("tbl_similar_events","event_id"," events_id = ".$event_id."  ");
			if($result['stored_similar_events']){
				foreach ($result['stored_similar_events'] as $key => $value) {
					$result['stored_similar_events_id'][] = $value['event_id'];
				}
			}
			
			$result['similar_events'] = $this->eventmodel->getDropdowncustom("tbl_events","events_id,headline",$event_id);
			// echo "<pre>";print_r($result['stored_similar_events_id']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(empty($_POST['languages'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select language'));
				exit;
			}
			if(!isset($_POST['headline'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter headline'));
				exit;
			}
			$data = array();
			$data_array2 =array();
			$data['category'] = (!empty($_POST['category'])) ? $_POST['category'] : '';
			$data['event_type'] = (!empty($_POST['event_type'])) ? $_POST['event_type'] : '';
			$data['event_timeline'] = (!empty($_POST['event_timeline'])) ? $_POST['event_timeline'] : '';
			$data['location'] = (!empty($_POST['location'])) ? $_POST['location'] : '';
			$data['event_fees'] = (!empty($_POST['event_fees'])) ? $_POST['event_fees'] : '';
			$data['day_and_date'] = (!empty($_POST['day_and_date'])) ? date('Y-m-d', strtotime($_POST['day_and_date'])) : '';
			$data['time'] = (!empty($_POST['time'])) ? $_POST['time'] : '';
			$data['no_of_seet'] = (!empty($_POST['no_of_seet'])) ? $_POST['no_of_seet'] : '';
			$data['headline'] = (!empty($_POST['headline'])) ? $_POST['headline'] : '';
			$data['intro_text'] = (!empty($_POST['intro_text'])) ? $_POST['intro_text'] : '';
			$data['languages'] = (!empty($_POST['languages'])) ? implode(',',$_POST['languages']) : '';
			if(!empty($_POST['status']) && isset($_POST['status'])){
				$data['status'] = 'Active';
			}else{
				$data['status'] = 'Inactive';
			}
			if(!empty($_FILES['banner_image'])){
				$_FILES['image_name']['name'] =  'E'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['banner_image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['banner_image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['banner_image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['banner_image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['banner_image']['size'];
				/* File Type Change multiple into single */
				
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['banner_image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_banner_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/event/'.$_POST['hidden_banner_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/event/'. $_POST['hidden_banner_image_name']);
						}
					}
				}
			}

			if(!empty($_POST['hidden_event_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->eventmodel->updateRecord('tbl_events', $data,'events_id',$_POST['hidden_event_id']);
				
				if(!empty($_POST['event_drop_down']) && isset($_POST['event_drop_down'])){
					$this->eventmodel->delrecord('tbl_similar_events', 'events_id',$_POST['hidden_event_id']);
					foreach ($_POST['event_drop_down'] as $key => $value) {
						$data_array2['events_id'] = $_POST['hidden_event_id'];
						$data_array2['event_id'] = $value;
						$data_array2['created_on'] = date("Y-m-d H:i:s");
						$data_array2['created_by'] = $_SESSION["fls_admin"][0]->user_id;
						
						$similar_events = $this->eventmodel->insertData('tbl_similar_events', $data_array2, '1');
					}
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->eventmodel->insertData('tbl_events', $data,'1');
				if(!empty($_POST['event_drop_down']) && isset($_POST['event_drop_down'])){
					foreach ($_POST['event_drop_down'] as $key => $value) {
						$data_array2['events_id'] = $result;
						$data_array2['event_id'] = $value;
						$data_array2['created_on'] = date("Y-m-d H:i:s");
						$data_array2['created_by'] = $_SESSION["fls_admin"][0]->user_id;
						$similar_events = $this->eventmodel->insertData('tbl_similar_events', $data_array2, '1');
					}
				}
			}
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/event/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }
	function deleteImage(){
		$condition = "events_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_events",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/event/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->eventmodel->updateRecord('tbl_events', $data,'events_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}