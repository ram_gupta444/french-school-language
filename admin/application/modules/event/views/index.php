<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="#"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Event Details</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Event Details</a>
					</li>
				</ul>
				</div>
				<div>
					<a href="<?php echo base_url(); ?>event/addEdit" class="btn btn-dark-itara">Add New Event</a>
				</div>				
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>#</th>
									<th>Category</th>
									<th>Event Name</th>
									<th>Type</th>
									<th>Date & Day</th>	
									<th>Time</th>
									<th>Fees</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>#</th>
									<th>Category</th>
									<th>Event Name</th>
									<th>Type</th>
									<th>Date & Day</th>	
									<th>Time</th>
									<th>Fees</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($events_details)) {
                                    foreach ($events_details as $key => $value) { ?>
								<tr>
									<td><?= $value['events_id'] ?></td>
									<td><?= $value['category'] ?></td>
									<td><?= $value['headline'] ?></td>
									<td><?= $value['event_type'] ?></td>
									<td><?= date("d M, y", strtotime($value['day_and_date'])) ?></td>
									<td><?= $value['time'] ?></td>
									<td>S$ <?= $value['event_fees'] ?></td>
									<td><span class="<?= ($value['status']== 'Active') ?"active":"inactive"?>-label"><?= $value['status'] ?></span></span></td>
									<td>
										<div class="actions">
											<a href="<?= base_url('event/addEdit?text='.rtrim(strtr(base64_encode("id=".$value['events_id']), '+/', '-_'), '=').'')?>" title=""> <img src="<?php echo base_url();?>assets/images/edit.svg" alt="Edit"></a>
                                            <!-- <a href="javasript:void(0)" title="Delete"><img src="<?php echo base_url();?>assets/images/delete.svg" alt="Delete" class="delete_event" data-id="<?php echo $value['events_id'];?>"></a> -->
                                            </td>
										</div>
									</td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(".delete_event").click(function(e) {
	var id=$(this).attr("data-id");
	var r=confirm("Are you sure to wants to delete");
    	if (r==true){
			$.ajax({
				url: "<?php echo base_url();?>event/deleteevent",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>event/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
    	}
})
</script>