<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Class_detail extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('class_detailmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['classes_details'] = $this->common->getData("tbl_classes",'*',$condition);
		// $result['classes_details'] = $this->class_detailmodel->getCustomList();
		// echo "<pre>";print_r($result['classes_details']);exit;
		if(!empty($result['classes_details'])){
			foreach ($result['classes_details'] as $key => $value) {
				$result['languages'][] = $this->class_detailmodel->getCustomList($value['languages']);
				// $languages[] = $value['languages'];
			}
		}
		// echo "<pre>";print_r($result);exit;
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }
	function addEdit($id=NULL){
		if(!empty($_SESSION["fls_admin"])){
			$class_id = "";
			if(!empty($_GET['text']) && isset($_GET['text'])){
				$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
				parse_str($varr,$url_prams);
				$class_id = $url_prams['id'];
				
				$result['stored_langauges'] = $this->class_detailmodel->getdata("tbl_classes","languages"," classes_id = ".$class_id."  ");
			}
			// echo $class_id;
			$result['language_dropdown'] = $this->class_detailmodel->getDropdown("tbl_language_master","language_master_id,language_name");
			$result['class_details'] = $this->class_detailmodel->getFormdata($class_id);

			$result['stored_similar_classes'] = $this->class_detailmodel->getdata("tbl_similar_classes","class_id"," classes_id = ".$class_id."  ");
			if($result['stored_similar_classes']){
				foreach ($result['stored_similar_classes'] as $key => $value) {
					$result['stored_similar_class_id'][] = $value['class_id'];
				}
			}
			
			$result['similar_class'] = $this->class_detailmodel->getDropdowncustom("tbl_classes","classes_id,title",$class_id);

			$result['levels'] = $this->class_detailmodel->getDropdown("tbl_level","level_id,level_name");
			// echo "<pre>";print_r($result['stored_similar_events_id']);exit;
			$this->load->view('template/header.php');
			$this->load->view('addEdit',$result);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(empty($_POST['languages'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select language'));
				exit;
			}
			if(!isset($_POST['title'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter title'));
				exit;
			}
			$data = array();
			$data_array2 =array();
			$data['category'] = (!empty($_POST['category'])) ? $_POST['category'] : '';
			$data['class_type'] = (!empty($_POST['class_type'])) ? $_POST['class_type'] : '';
			$data['location'] = (!empty($_POST['location'])) ? $_POST['location'] : '';
			$data['class_fees'] = (!empty($_POST['class_fees'])) ? $_POST['class_fees'] : '';
			$data['level'] = (!empty($_POST['level'])) ? $_POST['level'] : '';
			$data['no_of_hours'] = (!empty($_POST['no_of_hours'])) ? $_POST['no_of_hours'] : '';
			$data['duration_in_week'] = (!empty($_POST['duration_in_week'])) ? $_POST['duration_in_week'] : '';
			$data['no_of_seet'] = (!empty($_POST['no_of_seet'])) ? $_POST['no_of_seet'] : '';
			$data['total_hours'] = (!empty($_POST['total_hours'])) ? $_POST['total_hours'] : '';
			$data['title'] = (!empty($_POST['title'])) ? $_POST['title'] : '';
			$data['headline'] = (!empty($_POST['headline'])) ? $_POST['headline'] : '';
			$data['intro_text'] = (!empty($_POST['intro_text'])) ? $_POST['intro_text'] : '';
			$data['section2_heading'] = (!empty($_POST['section2_heading'])) ? $_POST['section2_heading'] : '';
			$data['section2_paragraph'] = (!empty($_POST['section2_paragraph'])) ? $_POST['section2_paragraph'] : '';
			$data['section3_heading'] = (!empty($_POST['section3_heading'])) ? $_POST['section3_heading'] : '';
			$data['section3_paragraph'] = (!empty($_POST['section3_paragraph'])) ? $_POST['section3_paragraph'] : '';
			$data['section4_heading'] = (!empty($_POST['section4_heading'])) ? $_POST['section4_heading'] : '';
			$data['section4_paragraph'] = (!empty($_POST['section4_paragraph'])) ? $_POST['section4_paragraph'] : '';
			$data['languages'] = (!empty($_POST['languages'])) ? implode(',',$_POST['languages']) : '';
			if(!empty($_POST['status']) && isset($_POST['status'])){
				$data['status'] = 'Active';
			}else{
				$data['status'] = 'Inactive';
			}
			
			if(!empty($_FILES['banner_image'])){
				$_FILES['image_name']['name'] =  'C'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['banner_image']['name'];
				$_FILES['image_name']['tmp_name'] =  $_FILES['banner_image']['tmp_name'];
				$_FILES['image_name']['error'] =  $_FILES['banner_image']['error'];
				$_FILES['image_name']['type'] =  $_FILES['banner_image']['type'];
				$_FILES['image_name']['size'] =  $_FILES['banner_image']['size'];
				/* File Type Change multiple into single */
				
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				
				if(!$this->upload->do_upload('image_name')){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
					exit;
				}else{
					$file_data = array('upload_data' => $this->upload->data());
					$data['banner_image'] = $file_data['upload_data']['file_name'];

					if(!empty($_POST['hidden_banner_image_name'])){
						if (file_exists(DOC_ROOT_FRONT . '/images/class/'.$_POST['hidden_banner_image_name'])) {
							unlink(DOC_ROOT_FRONT . '/images/class/'. $_POST['hidden_banner_image_name']);
						}
					}
				}
			}

			if(!empty($_POST['hidden_classes_id'])){
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->class_detailmodel->updateRecord('tbl_classes', $data,'classes_id',$_POST['hidden_classes_id']);
				
				if(!empty($_POST['class_drop_down']) && isset($_POST['class_drop_down'])){
					$this->class_detailmodel->delrecord('tbl_similar_classes', 'classes_id',$_POST['hidden_classes_id']);
					foreach ($_POST['class_drop_down'] as $key => $value) {
						$data_array2['classes_id'] = $_POST['hidden_classes_id'];
						$data_array2['class_id'] = $value;
						$data_array2['created_on'] = date("Y-m-d H:i:s");
						$data_array2['created_by'] = $_SESSION["fls_admin"][0]->user_id;
						
						$similar_events = $this->class_detailmodel->insertData('tbl_similar_classes', $data_array2, '1');
					}
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
				$result = $this->class_detailmodel->insertData('tbl_classes', $data,'1');
				if(!empty($_POST['class_drop_down']) && isset($_POST['class_drop_down'])){
					foreach ($_POST['class_drop_down'] as $key => $value) {
						$data_array2['classes_id'] = $result;
						$data_array2['class_id'] = $value;
						$data_array2['created_on'] = date("Y-m-d H:i:s");
						$data_array2['created_by'] = $_SESSION["fls_admin"][0]->user_id;
						$similar_events = $this->class_detailmodel->insertData('tbl_similar_classes', $data_array2, '1');
					}
				}
			}
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/class/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = '*';
		$config['max_size']      = '1000000';
		$config['overwrite']     = FALSE;
		
		return $config;
	}
	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }
	function deleteImage(){
		$condition = "classes_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_classes",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/class/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$data = array();
			$data[$_POST['image']] = '';
			$result = $this->class_detailmodel->updateRecord('tbl_classes', $data,'classes_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}