<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>class_detail"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>New Class Details</h4>
					
					</div>
					<div>
						<a href="<?php echo base_url(); ?>class_detail" class="btn btn-light-itara">Cancel</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<input type="hidden" id="hidden_classes_id" name="hidden_classes_id" value="<?php if(!empty($class_details[0]->classes_id)){echo $class_details[0]->classes_id;}?>"> 

				<input type="hidden" id="hidden_banner_image_name" name="hidden_banner_image_name" value="<?php if(!empty($event_details[0]->banner_image)){echo $event_details[0]->banner_image;}?>"> 
				<div class="row">
					<div class="col-12 col-sm-12 col-md-9">
						<div class="section-single last">	
							<div class="form-group">
								<label for="homebannerheading">Select language</label>
								<select class="form-control form-control select2" id="languages" name="languages[]" multiple>
								<option value="">Select language</option>
								<?php if(!empty($language_dropdown)){ ?>
									<option value="">Select language</option>
											<?php 
											foreach($language_dropdown as $key=>$val){
												$sel='';
												$sel= (in_array($val['language_master_id'],explode(',',$stored_langauges[0]['languages']))?'selected':'');
											?>
											<option value="<?php echo $val['language_master_id'];?>" <?= $sel?> ><?php echo $val['language_name'];?></option>
										<?php } 
											}
										?>
								</select>
							</div><br>				
							
								<div class="row">
									<div class="col-12 col-sm-4">
                    <div class="form-group">
										<label>Category</label><br/>
										<input type="radio" name="category" id="category" value="adult" <?php echo (!empty($class_details) &&  $class_details[0]->category == 'Adult' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="category">Adult</label>&nbsp;&nbsp;

										<input type="radio" name="category" id="category" value="children" <?php echo (!empty($class_details) &&  $class_details[0]->category == 'Children' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="category">Children</label><br/>
                    <label for="category" generated="true" class="error"></label>
                    </div>
									</div>
									<div class="col-12 col-sm-5">
                    <div class="form-group">
                      <label>Location</label><br>
											<input type="text" name="location" id="location" value="<?php if(!empty($class_details[0]->location)){echo $class_details[0]->location;}?>" placeholder="Enter location" class="form-control">
                    </div>
										
									</div>
									<div class="col-12 col-sm-3">
										<div class="form-group">
												<label>Fees (S$)</label><br/>
											<input type="text" name="class_fees" id="class_fees" value="<?php if(!empty($class_details[0]->class_fees)){echo $class_details[0]->class_fees;}?>" placeholder="S$350" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
										</div>
									</div>
								</div>
						
							
								<div class="row">
									<div class="col-12 col-sm-3">
                    <div class="form-group">
										<label >Level</label><br/>
										<select id="level" name="level" class="form-control">
										<?php if(!empty($levels)){ ?>
											<option value="" disabled>Select Level</option>
													<?php 
													foreach($levels as $key=>$val){
														$sel='';
														if(!empty($class_details[0]->level)){
															$sel= (in_array($val['level_id'],$class_details[0]->level )?'selected':'');
														}
													?>
													<option value="<?php echo $val['level_id'];?>" <?= $sel?> ><?php echo $val['level_name'];?></option>
												<?php } 
													}
												?>
										</select>
                    </div>
									</div>
									<div class="col-12 col-sm-2" >
                    <div class="form-group">
										<label>Hours per class</label><br/>
										<input type="text" name="no_of_hours" id="no_of_hours" value="<?php if(!empty($class_details[0]->no_of_hours)){echo $class_details[0]->no_of_hours;}?>" placeholder="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                    </div>
									</div>
									<div class="col-12 col-sm-2">
                    <div class="form-group">
										<label>Duration in Weeks</label>
										<input type="text" name="duration_in_week" id="duration_in_week" value="<?php if(!empty($class_details[0]->duration_in_week)){echo $class_details[0]->duration_in_week;}?>" placeholder="4" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                    </div>
									</div>
									<div class="col-12 col-sm-2">
                    <div class="form-group">
										<label>No. of Seats</label><br/>
										<input type="text" name="no_of_seet" id="no_of_seet" value="<?php if(!empty($class_details[0]->no_of_seet)){echo $class_details[0]->no_of_seet;}?>" placeholder="25" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                  </div>
									</div>
									<div class="col-12 col-sm-2">
                    <div class="form-group">          
                      <label>Total Hours</label>
                      <input type="text" name="total_hours" id="total_hours" value="<?php if(!empty($class_details[0]->total_hours)){echo $class_details[0]->total_hours;}?>" placeholder="25" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" class="form-control">
                    </div>         
									</div>
								</div>
							
							
							<div class="form-group">
								<div class="row">
									<div class="col-12 col-sm-2">
										<p><strong>Status</strong></p>	
										<input type="checkbox" name="status" id="status" <?php echo (!empty($class_details) &&  $class_details[0]->status == 'Active' ? "checked": " ")?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">		
									</div>
									<div class="col-12 col-sm-4">
										<label>Type</label><br>
										<input type="radio" name="class_type" id="class_type" value="Online" <?php echo (!empty($class_details) &&  $class_details[0]->class_type == 'Online' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="class type">Online</label>&nbsp;&nbsp;

										<input type="radio" name="class_type" id="class_type" value="In Person" <?php echo (!empty($class_details) &&  $class_details[0]->class_type == 'In Person' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="class type">In Person</label><br>
									</div>
								</div>
							</div>
						</div>	
						<hr style="border-width: 2px;border-color: black;">
						<div class="section-single">
							<h3>Section 1</h3>
							
							<div class="form-group">
								<label for="homebannerimg">Image for banner </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-1" src="<?= (!empty($class_details[0]->banner_image) ? FRONT_URL."/images/class/".$class_details[0]->banner_image  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="banner_image" id="banner_image" onchange="loadFile(event,'1')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="banner_image">Upload</label>
								</a>
								<?php if(!empty($class_details[0]->banner_image)){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($class_details[0]->classes_id)) ? $class_details[0]->classes_id : '' ?>','banner_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Course Code* </label>
								<input type="text" class="form-control editors" id="title" name="title" rows="2" placeholder="" value="<?= (!empty($class_details[0]->title)) ? $class_details[0]->title : '' ?>"> 
							</div>
							<div class="form-group">
								<label for="homebannerheading">Course Name</label>
								<textarea class="form-control editors" id="headline" name="headline" rows="2" placeholder=""><?= (!empty($class_details[0]->headline)) ? $class_details[0]->headline : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Intro text for class</label>
								<textarea class="form-control editors" id="intro_text" name="intro_text" rows="2" placeholder=""><?= (!empty($class_details[0]->intro_text)) ? $class_details[0]->intro_text : '' ?></textarea>
							</div>
						</div>

						<div class="section-single">
							<h3>Section 2</h3>
							
							<div class="form-group">
								<label for="homebannerheading"> Learn to title</label>
								<textarea class="form-control editors" id="section2_heading" name="section2_heading" rows="2" placeholder=""><?= (!empty($class_details[0]->section2_heading)) ? $class_details[0]->section2_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading"> Learn to description</label>
								<textarea class="form-control editors" id="section2_paragraph" name="section2_paragraph" rows="2" placeholder=""><?= (!empty($class_details[0]->section2_paragraph)) ? $class_details[0]->section2_paragraph : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3</h3>
							<div class="form-group">
								<label for="homebannerheading">Exam and Certificates title</label>
								<textarea class="form-control editors" id="section3_heading" name="section3_heading" rows="2" placeholder=""><?= (!empty($class_details[0]->section3_heading)) ? $class_details[0]->section3_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Exam and Certificates description</label>
								<textarea class="form-control editors" id="section3_paragraph" name="section3_paragraph" rows="2" placeholder=""><?= (!empty($class_details[0]->section3_paragraph)) ? $class_details[0]->section3_paragraph : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 4</h4>
							<div class="form-group">
								<label for="homebannerheading">Free Placement Test title</label>
								<textarea class="form-control editors" id="section4_heading" name="section4_heading" rows="2" placeholder=""><?= (!empty($class_details[0]->section4_heading)) ? $class_details[0]->section4_heading : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Free Placement Test description</label>
								<textarea class="form-control editors" id="section4_paragraph" name="section4_paragraph" rows="2" placeholder=""><?= (!empty($class_details[0]->section4_paragraph)) ? $class_details[0]->section4_paragraph : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Similar Class</h3>
							<div class="form-group">
								<label for="homebannerheading">Select Class</label>
								<select class="form-control form-control select2" id="class_drop_down" name="class_drop_down[]" multiple>
								<option value="">Select Class</option>
								<?php if(!empty($similar_class)){ ?>
									<option value="">Select Class</option>
											<?php 
											foreach($similar_class as $key=>$val){
												$sel='';
												$sel= (in_array($val['classes_id'],$stored_similar_class_id)?'selected':'');
											?>
											<option value="<?php echo $val['classes_id'];?>" <?= $sel?> ><?php echo $val['title'];?></option>
										<?php } 
											}
										?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	var vRules = {
		title:{required:true},
		category:{required:true},
	};
	var vMessages = {
		title:{required:"Please enter Course Code."},
		category:{required:"Please select category."},
	};
	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
			}
			var act = "<?php echo base_url();?>class_detail/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>class_detail/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>class_detail/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>class_detail/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}
document.title = "Add Edit class detail";

$(".datepicker").datepicker({
	format: 'dd-mm-yyyy',
	todayHighlight: true,
});

$('#time').timepicker({
// 12 or 24 hour
twelvehour: true,
// defaultTime: '08:00 AM'
});

CKEDITOR.replaceClass = 'editors';
/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
</script>