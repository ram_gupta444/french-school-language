<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"></a> Bookings</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Bookings</a>
					</li>
				</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-hover table-bordered basic-datatables" cellpadding="0" cellspacing="0">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Event/Class name</th>
									<th>Type</th>
									<th>Email</th>	
									<th>Contact Number</th>
									<th>Payment Status</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Event/Class name</th>
									<th>Type</th>
									<th>Email</th>	
									<th>Contact Number</th>
									<th>Payment Status</th>
								</tr>
							</tfoot>
							<tbody>
							<?php
                                if (!empty($bookings_details)) {
                                    foreach ($bookings_details as $key => $value) { ?>
								<tr>
									<td><?= $value['registration_id'] ?></td>
									<td><?= $value['first_name'].' '.$value['last_name']?></td>
									<?php if($value['type'] == 'Event'){ ?>
										<td><?= $value['event_name'] ?></td>
									<?php }else{ ?>
										<td><?= $value['class_name'] ?></td>
									<?php } ?>
									
									<td><?= $value['type'] ?></td>
									<td><?= $value['email_id'] ?></td>
									<td><?= $value['phone_no'] ?></td>
									<td><?= $value['payment_status'] ?></td>
								</tr>
								<?php }
                                } ?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
