<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Bookings extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('bookingsmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['bookings_details'] = $this->bookingsmodel->getCustomData("tbl_registration",'*',$condition);
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }

}