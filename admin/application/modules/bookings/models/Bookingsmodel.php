<?PHP
class Bookingsmodel extends CI_Model
{
  function getCustomData(){
    $this -> db -> select('r.registration_id,r.first_name,r.last_name,r.email_id,r.phone_no,r.type,r.event_id,r.schedule_id,r.payment_status,e.events_id,e.headline as event_name,sc.schedule_id,sc.class_drop_down,c.classes_id,c.headline as class_name');
    $this -> db -> from('tbl_registration as r');
    $this->db->join('tbl_events as e','r.event_id = e.events_id','left');
    $this->db->join('tbl_schedule as sc','r.schedule_id = sc.schedule_id','left');
    $this->db->join('tbl_classes as c','sc.class_drop_down = c.classes_id','left');
    $query = $this -> db -> get();
    // echo $this->db->last_query();exit;
    if($query -> num_rows() >= 1){
      return $query->result_array();
    }
    else{
      return false;
    }
 }

}