<?PHP
class Class_schedulemodel extends CI_Model
{
    function getFormdata($ID){
        $this -> db -> select('e.*');
        $this -> db -> from('tbl_events as e');
        $this -> db -> where('e.events_id ', $ID);
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
          return $query->result();
        }
        else{
          return false;
        }
     }
     function insertData($tbl_name,$data_array,$sendid = NULL)
     {
       $this->db->insert($tbl_name,$data_array);
       //print_r($this->db->last_query());
         //exit;
       $result_id = $this->db->insert_id();
       if($sendid == 1)
       {
         return $result_id;
       }
     }
     function updateRecord($tableName, $data, $column, $value)
     {
       $this->db->where("$column", $value);
       $this->db->update($tableName, $data);
       if ($this->db->affected_rows() > 0) {
         return true;
       }
       else {
         return true;
       }
     }
     function delrecord($tbl_name,$tbl_id,$record_id)
     {
       $this->db->where($tbl_id, $record_id);
       $this->db->delete($tbl_name);
       if($this->db->affected_rows() >= 1)
       {
         return true;
         }
         else
         {
         return false;
         }
     }
     function getDropdown($tbl_name,$tble_flieds){
      $this -> db -> select($tble_flieds);
      $this -> db -> from($tbl_name);
      $query = $this -> db -> get();
      if($query -> num_rows() >= 1)
      {
        return $query->result_array();
      }
      else
      {
        return false;
      }
   }
   function getData($table,$fields,$condition){
    $this -> db -> select($fields);
    $this -> db -> from($table);
    $this->db->where($condition);
    $query = $this -> db -> get();
    if($query -> num_rows() >= 1){
      return $query->result_array();
    }else{
      return false;
    }
  }
}