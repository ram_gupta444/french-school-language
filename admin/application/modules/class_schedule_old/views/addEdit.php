<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="<?php echo base_url(); ?>event"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>New Event</h4>
					
					</div>
					<div>
						<a href="<?php echo base_url(); ?>event" class="btn btn-light-itara">Cancel</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<input type="hidden" id="hidden_event_id" name="hidden_event_id" value="<?php if(!empty($event_details[0]->events_id)){echo $event_details[0]->events_id;}?>"> 
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="section-single last">						
							<div class="form-group">
								<div class="row">
									<div class="col-12 col-sm-4">
										<label>Category</label><br>
										<input type="radio" name="category" id="category" value="adult" <?php echo (!empty($event_details) &&  $event_details[0]->category == 'Adult' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="category">Adult</label>&nbsp;&nbsp;

										<input type="radio" name="category" id="category" value="children" <?php echo (!empty($event_details) &&  $event_details[0]->category == 'Children' ? "checked": " ")?>>&nbsp;&nbsp;
										<label for="category">Children</label><br>
									</div>
									<div class="col-12 col-sm-5">
										<label>Location</label><br>
										<input type="text" name="location" id="location" value="<?php if(!empty($event_details[0]->location)){echo $event_details[0]->location;}?>" placeholder="Bandra,Mumbai" >
									</div>
									<div class="col-12 col-sm-3">
										<label>Event Fees</label><br>
										<span>$</span><input type="text" name="event_fees" id="event_fees" value="<?php if(!empty($event_details[0]->event_fees)){echo $event_details[0]->event_fees;}?>" placeholder="$350" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-12 col-sm-4">
										<label>Day and Date</label><br>
											<input type="text" class="form-control datepicker" id="day_and_date" name="day_and_date" placeholder="Date" value="<?php echo (!empty($event_details[0]->day_and_date) && $event_details[0]->day_and_date != '0000-00-00' ) ? date('d-m-Y', strtotime($event_details[0]->day_and_date)) : '';?>">
									</div>
									<div class="col-12 col-sm-5" style="padding-right: 90px;">
										<label>Time</label><br>
											<input type="text" class="form-control" name="time" id="time" placeholder="Start Time" value="<?php if(!empty($event_details[0]->time)){echo $event_details[0]->time; }?>">
									</div>
									<div class="col-12 col-sm-3">
										<label>No. of Seats</label>
										<input type="text" name="no_of_seet" id="no_of_seet" value="<?php if(!empty($event_details[0]->no_of_seet)){echo $event_details[0]->no_of_seet;}?>" placeholder="25" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
									</div>
								</div>
							</div>
							<div class="form-group">
								<p><strong>Status</strong></p>	
								<input type="checkbox" name="status" id="status" <?php echo (!empty($event_details) &&  $event_details[0]->status == 'Active' ? "checked": " ")?> data-toggle="toggle" data-onstyle="primary" data-style="btn-round">		
							</div>
						</div>	
						<hr style="border-width: 2px;border-color: black;">
						<div class="section-single">
							<h3>Section 1</h3>
							
							<div class="form-group">
								<label for="homebannerimg">Image for banner </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-1" src="<?= (!empty($event_details[0]->banner_image) ? FRONT_URL."/images/event/".$event_details[0]->banner_image  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="banner_image" id="banner_image" onchange="loadFile(event,'1')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="banner_image">Upload</label>
								</a>
								<?php if(!empty($event_details[0]->banner_image)){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($event_details[0]->events_id)) ? $event_details[0]->events_id : '' ?>','banner_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Headline* </label>
								<textarea class="form-control editors" id="headline" name="headline" rows="2" placeholder=""><?= (!empty($event_details[0]->headline)) ? $event_details[0]->headline : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Intro text for class</label>
								<textarea class="form-control editors" id="intro_text" name="intro_text" rows="2" placeholder=""><?= (!empty($event_details[0]->intro_text)) ? $event_details[0]->intro_text : '' ?></textarea>
							</div>
							
						</div>
						<div class="section-single">
							<h3>Similar Events</h3>
							<div class="form-group">
								<label for="homebannerheading">Select an event</label>
								<select class="form-control form-control select2" id="event_drop_down" name="event_drop_down[]" multiple>
								<option value="">Select Event</option>
								<?php if(!empty($similar_events)){ ?>
									<option value="">Select Event</option>
											<?php 
											foreach($similar_events as $key=>$val){
												$sel='';
												$sel= (in_array($val['events_id'],$stored_similar_events_id)?'selected':'');
											?>
											<option value="<?php echo $val['events_id'];?>" <?= $sel?> ><?php echo $val['headline'];?></option>
										<?php } 
											}
										?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
$(document).ready(function(){
	var vRules = {
		headline:{required:true},
		category:{required:true},
	};
	var vMessages = {
		headline:{required:"Please enter headline."},
		category:{required:"Please select category."},
	};
	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
			}
			var act = "<?php echo base_url();?>event/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							window.location = "<?php echo base_url();?>event/index";
						},2000);
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>event/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>event/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}
document.title = "Add Edit event";

$(".datepicker").datepicker({
	format: 'dd-mm-yyyy',
	todayHighlight: true,
});

$('#time').timepicker({
// 12 or 24 hour
twelvehour: true,
// defaultTime: '08:00 AM'
});

CKEDITOR.replaceClass = 'editors';
/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
</script>