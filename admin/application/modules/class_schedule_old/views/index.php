<head>
<title>Jquery Fullcalandar Integration with PHP and Mysql</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
</head>
<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="#"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Class Schedule</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Class Schedule</a>
					</li>
				</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="container">
					<div id="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	var calendar = $('#calendar').fullCalendar({
	
		editable:true,
		header:{
		left:'prev,next today',
		center:'title',
		right:'month,agendaWeek,agendaDay'
		},
		events: '<?php echo base_url();?>class_schedule/load',
		selectable:true,
		selectHelper:true,
	
		select: function(start, end, allDay)
		{
			var title = prompt("Enter Event Title");
			// $('#myModal').modal('show');
			// $('#new-event').modal('show');
			if(title)
			{
				var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
				var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
				$.ajax({
				url:"<?php echo base_url();?>class_schedule/insert",
				type:"POST",
				data:{title:title, start:start, end:end},
				success:function()
				{
					calendar.fullCalendar('refetchEvents');
					alert("Added Successfully");
				}
				})
			}
		},
		editable:true,
		eventResize:function(event)
		{
			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
			var title = event.title;
			var id = event.id;
			$.ajax({
			url:"<?php echo base_url();?>class_schedule/update",
			type:"POST",
			data:{title:title, start:start, end:end, id:id},
			success:function(){
			calendar.fullCalendar('refetchEvents');
			alert('Class schedule Update');
			}
			})
		},

		eventDrop:function(event)
		{
			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
			var title = event.title;
			var id = event.id;
			$.ajax({
			url:"<?php echo base_url();?>class_schedule/update",
			type:"POST",
			data:{title:title, start:start, end:end, id:id},
			success:function()
			{
			calendar.fullCalendar('refetchEvents');
			alert("Event Updated");
			}
			});
		},

		eventClick:function(event)
		{
		if(confirm("Are you sure you want to remove it?"))
		{
		var id = event.id;
		$.ajax({
		url:"<?php echo base_url();?>class_schedule/delete",
		type:"POST",
		data:{id:id},
		success:function()
		{
			calendar.fullCalendar('refetchEvents');
			alert("Schedule Removed");
		}
		})
		}
		},

	});
});

</script>

     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
		  <select name="class_schedule" id="class_schedule">
			  <option value="">select class</option>
			  <option value="class1"> class1</option>
			  <option value="class2"> class2</option>
		  </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!--* Modal init *-->
<div class="modal fade" id="new-event" tabindex="-1" role="dialog" aria-labelledby="new-event-label" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <!-- Modal body -->
         <div class="modal-body">
            <form class="new-event--form">
               <div class="form-group">
                  <label class="form-control-label">Event title</label>
                  <input type="text" class="form-control form-control-alternative new-event--title" placeholder="Event Title">
               </div>
               <div class="form-group mb-0">
                  <label class="form-control-label d-block mb-3">Status color</label>
                  <div class="btn-group btn-group-toggle btn-group-colors event-tag" data-toggle="buttons">
                     <label class="btn bg-info active"><input type="radio" name="event-tag" value="bg-info" autocomplete="off" checked></label>
                     <label class="btn bg-warning"><input type="radio" name="event-tag" value="bg-warning" autocomplete="off"></label>
                     <label class="btn bg-danger"><input type="radio" name="event-tag" value="bg-danger" autocomplete="off"></label>
                     <label class="btn bg-success"><input type="radio" name="event-tag" value="bg-success" autocomplete="off"></label>
                     <label class="btn bg-default"><input type="radio" name="event-tag" value="bg-default" autocomplete="off"></label>
                     <label class="btn bg-primary"><input type="radio" name="event-tag" value="bg-primary" autocomplete="off"></label>
                  </div>
               </div>
               <input type="hidden" class="new-event--start" />
               <input type="hidden" class="new-event--end" />
            </form>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="submit" class="btn btn-primary new-event--add">Add event</button>
            <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>