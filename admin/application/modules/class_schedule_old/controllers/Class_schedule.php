<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Class_schedule extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('class_schedulemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }
	function load()
	{
		$condition = "1=1 ";
		$class_schedule = $this->class_schedulemodel->getData("tbl_schedule",'*',$condition);
		if(!empty($class_schedule)){
			foreach ($class_schedule as $key => $value) {
				$data[] = array(
					'id' =>     $value['schedule_id'],
					'title' =>     $value['schedule_id'],
					'start' =>     $value['start_date_time'],
					'end' =>     $value['end_date_time'],
				);
			}
		}
		echo json_encode($data);
	}
	function insert(){
		$data =array();
		$data['classes_id'] = $_POST['title'];
		$data['start_date_time'] = $_POST['start'];
		$data['end_date_time'] = $_POST['end'];
		$similar_events = $this->class_schedulemodel->insertData('tbl_schedule', $data, '1');
	}
	function update()
	{
		if($_POST['id']){
			$data =array();
			$data['title'] = $_POST['title'];
			$data['start_date_time'] = $_POST['start'];
			$data['end_date_time'] = $_POST['end'];
			$result = $this->class_schedulemodel->updateRecord('tbl_schedule', $data,'schedule_id',$_POST['id']);
		}
	}
	function delete(){
		if($_POST['id']){
			$Result = $this->class_schedulemodel->delrecord('tbl_schedule', 'schedule_id ',$_POST['id']);
		}
	}
}