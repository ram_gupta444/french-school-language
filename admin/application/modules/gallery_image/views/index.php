<div class="main-panel">
  <div class="container">
    <form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
      <div class="page-inner">
        <div class="page-header page-header-btn">
          <div class="page-header-title">
            <h4 class="page-title"><a href="#/"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a>Footer Gallery Images</h4>
            <ul class="breadcrumbs">
              <li class="nav-home">
                <a href="#">
                  <i class="flaticon-home"></i>
                </a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#/">Website Pages</a>
              </li>
              <li class="separator">
                <i class="flaticon-right-arrow"></i>
              </li>
              <li class="nav-item">
                <a href="#">Gallery Image</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="#" class="btn btn-light-itara">Discard</a>
            <button type="submit" class="btn btn-dark-itara">Save</a>
          </div>
        </div>
        <input type="hidden" id="edit_exist" name="edit_exist" value="<?= (!empty($gallery_image_details[0]['gallery_image_id'])) ? $gallery_image_details[0]['gallery_image_id'] : '' ?>"">
                <hr>
                <div class=" row">
        <div class="col-12">
          <div class="section-single">
            <?php if (empty($gallery_image_details)) { ?>
              <h3>Upload up to 4 images</h3>
              <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_image_0">
                <tbody>
                  <tr tr_inner_count_image="0" class="active">
                    <td>

                    </td>
                    <td>
                      <input type="hidden" name="images_existing[0]" value="" id="images_existing_0">

                      <input type="hidden" name="images_existing_id[0]" value="" id="images_existing_id_0">

                      <input type="file" class="form-control required" name="file_[0]" id="file_0">
                    </td>
                    <td>
                      <input type="text" class="form-control required" name="image_alt[0]" value="" id="image_alt_0" placeholder="Enter image name">
                    </td>
                    <td>
                      <button class="btn add_more_inner_image bg-transparent p5" panel-count_image="0" type="button"><i class="material-icons" style="font-size:15px;"><img src="<?php echo base_url(); ?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>

            <?php } else { ?>
              <h3>Upload up to 4 images</h3>
              <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_image_0">
                <tbody>
                  <?php foreach ($gallery_image_details as $key => $value) { ?>
                    <tr tr_inner_count_image="<?= $key ?>" class="active">
                      <td>
                        <?php if (!empty($value['image_name'])) { ?>
                          <img src="<?= (!empty($value['image_name']) ? FRONT_URL . "/images/gallery_images/" . $value['image_name'] : '') ?>" alt="" width="100" height="80" class="mt5">&nbsp
                          <?php if ($key != '0') { ?>
                            <a href="#/" class="btn btn-secondary btn-xs mt5 mb5" onclick="deleteAboutImage(<?= (!empty($value['gallery_image_id'])) ? $value['gallery_image_id'] : '' ?>,`image_name`,`gallery_images`);">Delete</a>
                          <?php } ?>
                        <?php } ?>
                      </td>
                      <td>
                        <input type="hidden" name="images_existing[<?= $key ?>]" value="<?= (!empty($value['image_name']) ? $value['image_name'] : '') ?>" id="images_existing_<?= $key ?>">

                        <input type="hidden" name="images_existing_id[<?= $key ?>]" value="<?= (!empty($value['gallery_image_id']) ? $value['gallery_image_id'] : '') ?>" id="images_existing_id_<?= $key ?>">
                        <p style="margin:5px 0;">Upload Image</p>    
                        <input type="file" class="form-control <?= (!empty($value['image_name'])) ? '' : 'required' ?>" name="file_[<?= $key ?>]" id="file_<?= $key ?>">
                      </td>
                      <td>
                        <p style="margin:5px 0;">Image description</p>
                        <input type="text" class="form-control <?= (!empty($value['image_alt'])) ? '' : 'required' ?>" name="image_alt[<?= $key ?>]" value="<?= (!empty($value['image_alt'])) ? $value['image_alt'] : '' ?>" id="image_alt_<?= $key ?>">
                      </td>
                      <td>
                        <?php if ($key == '0') { ?>
                          <button class="btn add_more_inner_image bg-transparent p5" panel-count_image="<?php echo $key; ?>" type="button"><i class="material-icons"><img src="<?php echo base_url(); ?>assets/images/plus-outline.svg" alt="" class="add-more-img"></i></button>
                        <?php } else { ?>
                          <button title="Remove" class=" btn  remove inner_remove_image p5 bg-transparent" type="button" >
                            <i class="material-icons"><img src="<?= base_url() ?>assets/images/minus-outline.svg" style="width:30px"></i>
                          </button>
                        <?php } ?>
                      </td>
                    </tr>
                  <?php } ?>

                </tbody>
              </table>
            <?php } ?>
          </div>
        </div>
      </div>
  </div>
  </form>
</div>
</div>

<script>
  $(document).on("click", ".add_more_inner_image", function() {
    var xi = $(this).attr("panel-count_image");
    var txi = $("#admore_inner_table_image_" + xi + " tbody tr:last-child").attr("tr_inner_count_image");
    txi++;
    if (isNaN(txi) || txi == "undefined") {
      txi = 0;
    }
    // if(txi ==4){
    // 	alert("Only 4 Product Variant Images Allowed");
    // 	return false;
    // };
    var inner_tr_image = '<tr tr_inner_count_image="' + txi + '" class="active">' +
      '<td>' +

      '</td>' +
      '<td>' +
      '<input type="hidden" name="images_existing[' + txi + ']" value="" id="images_existing_[' + txi + ']">' +

      '<input type="hidden" name="images_existing_id[' + txi + ']" value="" id="images_existing_id_[' + txi + ']">' +

      '<input type="file" class="form-control required" name="file_[' + txi + ']" id="file_' + txi + '">' +
      '</td>' +
      '<td>' +
      '<input type="text" class="form-control required" name="image_alt[' + txi + ']" value="" id="image_alt_[' + txi + ']" placeholder="Enter image name">' +
      '</td>' +
      '<td>' +
      '<button title="Remove" class=" btn p5 bg-transparent remove inner_remove_image" type="button">' +
      '<i class="material-icons"><img src="<?= base_url() ?>assets/images/minus-outline.svg" style="width:30px"></i>' +
      '</button>' +
      '</td>' +
      '</tr>';
    $("#admore_inner_table_image_" + xi + " tbody").append(inner_tr_image);
  });

  $(document).on("click", '.inner_remove_image', function() {
    var tr_inner_count_image = $(this).closest('tr').attr('tr_inner_count_image');
    if (tr_inner_count_image != 0) {
      $(this).closest('tr').remove();
    } else {
      swal({
        title: "Alert!",
        text: "<p class='font-bold col-red'>Atleast one option is required..</p>",
        timer: 2000,
        showConfirmButton: false,
        html: true
      });
    }
  });


  $("#form-validate").validate({
    // rules: vRules,
    // messages: vMessages,
    submitHandler: function(form) {
      for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
      }
      var act = "<?php echo base_url(); ?>gallery_image/submitForm";
      $("#form-validate").ajaxSubmit({
        url: act,
        type: 'post',
        cache: false,
        clearForm: false,
        success: function(response) {
          var res = eval('(' + response + ')');
          if (res['success'] == "1") {
            displayMsg("success", res['msg']);
            setTimeout(function() {
              window.location = "<?php echo base_url(); ?>gallery_image/index";
            }, 2000);
          } else {
            displayMsg("error", res['msg']);
            return false;
          }
        },
      });
    }
  });

  function deleteAboutImage(id, image, folder_name) { 
    var r = confirm("Are you sure wants to delete");
    if (r == true) {
      $.ajax({
        url: "<?php echo base_url(); ?>gallery_image/deleteAboutImage",
        dataType: 'json',
        type: "post",
        data: {
          id: id,
          image: image,
          folder_name: folder_name
        },
        success: function(response) {
          if (response.success == "1") {
            displayMsg("success", response.msg);
            setTimeout(function() {
              window.location = "<?php echo base_url(); ?>gallery_image/index";
            }, 2000);
          } else {
            displayMsg("error", response.msg);
            return false;
          }
        }
      });
    }
  }
  CKEDITOR.replaceClass = 'editors';
  /*Preview Images*/
  var loadFile = function(event, id) {
    var image = document.getElementById('homepreview-' + id);
    image.src = URL.createObjectURL(event.target.files[0]);
  };
  /*Preview Images*/
</script>