<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Gallery_image extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('gallery_imagemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
 
	function index()
	{
		$result = array();
		$condition = "1=1 ";
		$result['gallery_image_details'] = $this->common->getData("tbl_gallery_image",'*',$condition);
		
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	
	function submitForm(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			// echo "<pre>";print_r($data);exit;
			if(!empty($_POST['edit_exist'])){
				// if(!empty($_FILES['file_']) && count($_FILES['file_']['name']) > 0){
					foreach($_POST['images_existing'] as $fkey=>$fval){
						if(!empty($_POST['images_existing'][$fkey]) && empty($_FILES['file_']['name'][$fkey])){
							$image_data = array();
							$image_data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
							$image_data['updated_on'] = date("Y-m-d H:i:s");
							$image_data['image_name'] = $_POST['images_existing'][$fkey];
							$image_data['image_alt'] = (!empty($_POST['image_alt'][$fkey])) ? $_POST['image_alt'][$fkey] : '';
							
							$result = $this->gallery_imagemodel->updateRecord('tbl_gallery_image', $image_data,'gallery_image_id',$_POST['images_existing_id'][$fkey]);
						}else{
							$path = DOC_ROOT_FRONT.'/images/gallery_images/';
							if(!empty($_POST['images_existing'][$fkey])){
								$img_path = $path."/".$_POST['images_existing'][$fkey];
								if(is_file($img_path)){
									unlink($img_path);
								}
							}
							$image_data = array();
							$image_data['updated_by'] = $_SESSION["fls_admin"][0]->user_id;
							$image_data['updated_on'] = date("Y-m-d H:i:s");
							$image_data['image_alt'] = (!empty($_POST['image_alt'][$fkey])) ? $_POST['image_alt'][$fkey] : '';
							
							$_FILES['product_images_temp']['name'] = 'A'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['file_']['name'][$fkey];
							$_FILES['product_images_temp']['type'] = $_FILES['file_']['type'][$fkey];
							$_FILES['product_images_temp']['tmp_name'] = $_FILES['file_']['tmp_name'][$fkey];
							$_FILES['product_images_temp']['size'] = $_FILES['file_']['size'][$fkey];
							$config = array();
							$config['upload_path'] = $path;
							$config['max_size'] = '1000000';
							// $config['allowed_types'] = 'pdf';
							$config['allowed_types'] = '*';
							$this->load->library('upload');
							$this->upload->initialize($config);
							if(!$this->upload->do_upload('product_images_temp')){
								$image_error = array('error' => $this->upload->display_errors());
								echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
								exit;
							}else{
								$file_data = array('upload_data' => $this->upload->data());
								$image_data['image_name'] = $file_data['upload_data']['file_name'];
								
								if(!empty($_POST['images_existing_id'][$fkey])){
									$result = $this->gallery_imagemodel->updateRecord('tbl_gallery_image', $image_data,'gallery_image_id',$_POST['images_existing_id'][$fkey]);

								}else{
									$image_data['created_by'] = $_SESSION["fls_admin"][0]->user_id;
									$image_data['created_on'] = date("Y-m-d H:i:s");
									$this->common->insertData("tbl_gallery_image",$image_data,"1");
								}
							}
						}
					}
				// }
			}else{
				if(!empty($_FILES['file_']['name']))
				{
					$path = DOC_ROOT_FRONT."/images/gallery_images/";
					if (!file_exists($path)) {
						mkdir($path, 0777, true);
					}
					foreach ($_FILES['file_']['name'] as $imagekey => $image_value) {
						$subdata['created_on'] = date("Y-m-d H:i:s");
						$subdata['created_by'] = $_SESSION["fls_admin"][0]->user_id;	
						/* File Type Change multiple into single */
						$_FILES['image_name']['name'] =  'A'.$this->getRandomCert(10, md5(date("F j, Y, g:i a"))).$_FILES['file_']['name'][$imagekey];
						$_FILES['image_name']['tmp_name'] =  $_FILES['file_']['tmp_name'][$imagekey];
						$_FILES['image_name']['error'] =  $_FILES['file_']['error'][$imagekey];
						$_FILES['image_name']['type'] =  $_FILES['file_']['type'][$imagekey];
						$_FILES['image_name']['size'] =  $_FILES['file_']['size'][$imagekey];
						/* File Type Change multiple into single */
						$config = array();
						$config['upload_path'] = $path;
						$config['max_size'] = '1000000';
						// $config['allowed_types'] = 'pdf';
						$config['allowed_types'] = '*';
						$this->load->library('upload');
						$this->upload->initialize($config);
						
						if(!$this->upload->do_upload('image_name')){
							$image_error = array('error' => $this->upload->display_errors());
							echo json_encode(array("success"=>false,"msg"=>$image_error['error']));
							exit;
						}else{
							$file_data = array('upload_data' => $this->upload->data());
							$subdata['image_name'] = $file_data['upload_data']['file_name'];
							$subdata['image_alt'] = (!empty($_POST['image_alt'][$imagekey])) ? $_POST['image_alt'][$imagekey] : '';
						}
						$result = $this->gallery_imagemodel->insertData('tbl_gallery_image',$subdata,'1');
					}
				}
			}

			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	private function getRandomCert($length = 6, $seed = 'abcdef0123456789') {
        for ($str = '', $i = 0; $i < $length; ++$i)
            $str .= $seed{rand(0, strlen($seed) - 1)};
        return $str;
    }

	function deleteAboutImage(){
		$condition = "gallery_image_id ='".$_POST['id']."' ";
		$result = $this->common->getData("tbl_gallery_image",'*',$condition);
		if(!empty($result[0][$_POST['image']])){
			$path = DOC_ROOT_FRONT."/images/".$_POST['folder_name']."/".$result[0][$_POST['image']];
			unlink($path);
		}
		if(!empty($result)){
			$result = $this->gallery_imagemodel->delrecord('tbl_gallery_image', 'gallery_image_id',$_POST['id']);
			echo json_encode(array("success"=>"1",'msg'=>'Image Deleted successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'something went wrong.!'));
			exit;
		}	
	}
}

?>
