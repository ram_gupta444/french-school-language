<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.min.css" />
	<script src="<?PHP echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?PHP echo base_url();?>assets/js/bootstrap-timepicker.js"></script>
	<!--timepicker css-->
	<link type="text/css" rel="stylesheet" href="<?PHP echo base_url();?>assets/css/bootstrap-timepicker.css" />	<div class="modal-header">	<h4>Set a schedule </h4><button type="button" class="close" data-dismiss="modal" style="position: absolute;    right: 25px;">&times;</button>	</div>
	<div class="modal-body" style="margin-inline: 50px;">
    <!--<h3>Set a schedule <button type="button" class="close" data-dismiss="modal">&times;</button></h3><br>-->
    <div class="row">
        <input type="hidden" name="hidden_id" id="hidden_id" value="<?= (!empty($selected_model_detail[0]['schedule_id']) ? $selected_model_detail[0]['schedule_id'] : '');?>">
        <div class="col-xs-12" >			<div class="form-group">			<label for="">Select Class</label>		
            <select name="class" id="class" class="form-control" style="margin-right: 306px;">
                <?php if(!empty($model)){ ?>
                    <option value="">Select Course</option>
                            <?php 
                            // echo "<pre>";print_r($model);exit;
                            foreach($model as $key=>$val){
                            ?>
                            <option value="<?php echo $val['classes_id'];?>" <?php echo(!empty($selected_model[0]['classes_id']) && $selected_model[0]['classes_id'] == $val['classes_id'])?"selected":"";?>><?php echo $val['title'];?></option>
                        <?php } 
                            }
                        ?>
            </select>			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" style="margin-inline: -16px;marign0;">		<div class="form-group">
        <label for="title">Duration</label>
            <select name="duration" id="duration" class="form-control">
                <option value="" disabled>Select Duration</option>
                <option value="1" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 1) ? 'selected' : ''; ?> >1 week</option>
                <option value="2" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 2) ? 'selected' : ''; ?>>2 weeks</option>
                <option value="3" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 3) ? 'selected' : ''; ?>>3 weeks</option>
                <option value="4" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 4) ? 'selected' : ''; ?>>4 weeks</option>
                <option value="5" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 5) ? 'selected' : ''; ?>>5 weeks</option>
                <option value="6" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 6) ? 'selected' : ''; ?>>6 weeks</option>
                <option value="7" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 7) ? 'selected' : ''; ?>>7 weeks</option>
                <option value="8" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 8) ? 'selected' : ''; ?>>8 weeks</option>
                <option value="9" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 9) ? 'selected' : ''; ?>>9 weeks</option>
                <option value="10" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 10) ? 'selected' : ''; ?>>10 weeks</option>
                <option value="11" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 11) ? 'selected' : ''; ?>>11 weeks</option>
                <option value="12" <?= (!empty($selected_model_detail[0]['duration']) && $selected_model_detail[0]['duration'] == 12) ? 'selected' : ''; ?>>12 weeks</option>
            </select>		</div>
        </div>
        <div class="col-md-6" style="margin-inline: 15px;">		<div class="form-group">
        <label for="title">Class Label (for scheduling)</label>
            <input type="text" name="title" id="title" class="form-control" value="<?= (!empty($selected_model_detail[0]['title']) ? $selected_model_detail[0]['title'] : '') ?>">		</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        <label class="col-xs-4" for="title">Repeat on &nbsp;&nbsp;</label>
        <input type="checkbox" value="Monday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Monday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> M
        <input type="checkbox" value="Tuesday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Tuesday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> T
        <input type="checkbox" value="Wednesday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Wednesday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> w
        <input type="checkbox" value="Thursday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Thursday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> T
        <input type="checkbox" value="Friday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Friday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> F
        <input type="checkbox" value="Saturday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Saturday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> S
        <input type="checkbox" value="Sunday" name="repeat_on" <?php echo (!empty($selected_model_detail[0]['repeat_on']) && (in_array('Sunday',explode(',',$selected_model_detail[0]['repeat_on']))) ? 'checked' : ''); ?>> S
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" style="padding-left:0;">			<div class="form-group">				<label for="title">Class Start Date</label>
				<input type="text" name="starts_date" id="starts_date" class="form-control" style="margin-inline: -16px;margin:0" placeholder="Start Date" value="<?php echo (!empty($selected_model_detail[0]['starts_date']) && $selected_model_detail[0]['starts_date'] != '0000-00-00' ) ? date('d-m-Y', strtotime($selected_model_detail[0]['starts_date'])) : '';?>"/>			</div>
        </div>
        <div class="col-md-6" style="padding-left:0;">			<div class="form-group">				<label for="title">Class End Date</label>
            <input type="text" name="ends_date" id="ends_date" class="form-control"  style="margin-inline: -9px;margin:0;" placeholder="End Date" value="<?php echo (!empty($selected_model_detail[0]['ends_date']) && $selected_model_detail[0]['ends_date'] != '0000-00-00' ) ? date('d-m-Y', strtotime($selected_model_detail[0]['ends_date'])) : '';?>"/>			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6" style="padding-left:0;">			<div class="form-group">				<label for="title">Class Start Time</label>
            <input type="text" name="start_time" id="start_time" class="form-control"  style="margin-inline: -16px;margin:0" placeholder="Start Time" vaue="<?php if(!empty($selected_model_detail[0]['start_time'])){echo $selected_model_detail[0]['start_time']; }?>"/>			</div>
        </div>
        <div class="col-md-6" style="padding-left:0;">			<div class="form-group">				<label for="title">Class End Time</label>
            <input type="text" name="end_time" id="end_time" class="form-control"  style="margin-inline: -9px;margin:0" placeholder="End Time" value="<?php if(!empty($selected_model_detail[0]['end_time'])){echo $selected_model_detail[0]['end_time']; }?>"/>			</div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id="save-event">Save</button>
</div>
<script>

$("#starts_date,#ends_date").datepicker({
	format: 'dd-mm-yyyy',
	todayHighlight: true,
});
$("#start_time,#end_time").timepicker({
// 12 or 24 hour
twelvehour: true,
// defaultTime: '08:00 AM'
});

$('#save-event').on('click', function() {
		// var title = $('#title').val();
		var class_drop = $('#class').val();
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		var starts_date = $('#starts_date').val();
		var ends_date = $('#ends_date').val();
		var title = $('#title').val();
		var duration = $('#duration').val();
		var hidden_id = $('#hidden_id').val();
		// alert(class_drop);
		if (class_drop) {
			var eventData = {
				// title: title,
				// start: $('#starts-at').val(),
				// end: $('#ends-at').val(),
				// class_drop: $('#class').val(),
				// class_selected = [],
				// $("input:checkbox[name='class']:checked").each(function(){    
				// 	class_selected.push($(this).val()),    		
				// }),
				// start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss"),
				// end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss"),
			};
			
			var repeat_on = [];
				$("input:checkbox[name='repeat_on']:checked").each(function(){    
					repeat_on.push($(this).val());    		
				});
		}
		$('#calendar').fullCalendar('unselect');
		// Clear modal inputs
		$('.modal').find('input').val('');
		// hide modal
		$('.modal').modal('hide');
		$.ajax({
			url:"<?php echo base_url();?>class_schedule/insert",
			type:"POST",
			data:{repeat_on:repeat_on ,class_drop:class_drop,start_time:start_time,starts_date:starts_date,ends_date:ends_date,end_time:end_time,title:title,duration:duration,hidden_id:hidden_id},
			// success:function()
			// {
				
			// 	$('#calendar').fullCalendar('refetchEvents');
			// 	alert("Added Successfully");
			// }
			success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
                        // $('#calendar').fullCalendar('refetchEvents');
						alert(res['msg']);
                        location.reload();
					}
					else
					{	
                        // $('#calendar').fullCalendar('refetchEvents');
                        location.reload();
						alert(res['msg']);
						return false;
					}
				}
			})
	});
</script>