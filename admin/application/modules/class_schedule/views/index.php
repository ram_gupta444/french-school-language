<head>
<title>Jquery Fullcalandar Integration with PHP and Mysql</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>


</head>
<div class="main-panel">
	<div class="container">
		<div class="page-inner">
			<div class="page-header page-header-btn">
				<div class="page-header-title">
				<h4 class="page-title"><a href="#"><img src="<?php echo base_url();?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> Class Schedule</h4>
				<ul class="breadcrumbs">
					<li class="nav-home">
						<a href="#">
							<i class="flaticon-home"></i>
						</a>
					</li>
					<li class="separator">
						<i class="flaticon-right-arrow"></i>
					</li>
					<li class="nav-item">
						<a href="#">Class Schedule</a>
					</li>
				</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="container">
					<div id="calendar"></div>
					<div id='datepicker'></div>
					</div>
					<div class="modal fade" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div id="model_body"></div>

							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<script>
// $(document).ready(function() {
// 	var calendar = $('#calendar').fullCalendar({
	
// 		editable:true,
// 		header:{
// 		left:'prev,next today',
// 		center:'title',
// 		right:'month,agendaWeek,agendaDay'
// 		},
// 		events: '<?php echo base_url();?>class_schedule/load',
// 		selectable:true,
// 		selectHelper:true,
	
// 		select: function(start, end, allDay)
// 		{
// 			var title = prompt("Enter Event Title");
// 			$('.modal').modal('show');
// 			if(title)
// 			{
// 				var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
// 				var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
// 				$.ajax({
// 				url:"<?php echo base_url();?>class_schedule/insert",
// 				type:"POST",
// 				data:{title:title, start:start, end:end},
// 				success:function()
// 				{
// 					calendar.fullCalendar('refetchEvents');
// 					alert("Added Successfully");
// 				}
// 				})
// 			}
// 		},
// 		editable:true,
// 		eventResize:function(event)
// 		{
// 			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
// 			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
// 			var title = event.title;
// 			var id = event.id;
// 			$.ajax({
// 			url:"<?php echo base_url();?>class_schedule/update",
// 			type:"POST",
// 			data:{title:title, start:start, end:end, id:id},
// 			success:function(){
// 			calendar.fullCalendar('refetchEvents');
// 			alert('Class schedule Update');
// 			}
// 			})
// 		},

// 		eventDrop:function(event)
// 		{
// 			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
// 			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
// 			var title = event.title;
// 			var id = event.id;
// 			$.ajax({
// 			url:"<?php echo base_url();?>class_schedule/update",
// 			type:"POST",
// 			data:{title:title, start:start, end:end, id:id},
// 			success:function()
// 			{
// 			calendar.fullCalendar('refetchEvents');
// 			alert("Event Updated");
// 			}
// 			});
// 		},

// 		eventClick:function(event)
// 		{
// 		if(confirm("Are you sure you want to remove it?"))
// 		{
// 		var id = event.id;
// 		$.ajax({
// 		url:"<?php echo base_url();?>class_schedule/delete",
// 		type:"POST",
// 		data:{id:id},
// 		success:function()
// 		{
// 			calendar.fullCalendar('refetchEvents');
// 			alert("Schedule Removed");
// 		}
// 		})
// 		}
// 		},

// 	});
// });


$(document).ready(function() {
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events: '<?php echo base_url();?>class_schedule/load',
		// navLinks: true, // can click day/week names to navigate views
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay) {
			$.ajax({
				url:"<?php echo base_url();?>class_schedule/model_open",
				dataType: "json",
				type:"POST",
				success: function(response){
				if(response.success){
					$("#model_body").html(response.modelHtml);
					$('.modal').modal('show');
					}
				}
			})

		},
		editable: true,

		eventClick: function(event, element) {
			var id = (event.id);
			$.ajax({
				url:"<?php echo base_url();?>class_schedule/model_open",
				dataType: "json",
				type:"POST",
				data:{id:id},
				success: function(response){
				if(response.success){
					$("#model_body").html(response.modelHtml);
					$('.modal').modal('show');
					}
				}
			})
		},

		// eventClick:function(event)
		// {
		// if(confirm("Are you sure you want to remove it?"))
		// {
		// var id = event.id;
		// $.ajax({
		// url:"<?php echo base_url();?>class_schedule/delete",
		// type:"POST",
		// data:{id:id},
		// success:function()
		// {
		// 	calendar.fullCalendar('refetchEvents');
		// 	alert("Schedule Removed");
		// }
		// })
		// }
		// },

		
		eventLimit: true, // allow "more" link when too many events
		eventResize:function(event)
		{
			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
			var title = event.title;
			var id = event.id;
			$.ajax({
			url:"<?php echo base_url();?>class_schedule/update",
			type:"POST",
			data:{title:title, start:start, end:end, id:id},
			success:function(){
			$('#calendar').fullCalendar('refetchEvents');
			alert('Class schedule Update');
			}
			})
		},

		eventDrop:function(event)
		{
			var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
			var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
			var title = event.title;
			var id = event.id;
			$.ajax({
			url:"<?php echo base_url();?>class_schedule/update",
			type:"POST",
			data:{title:title, start:start, end:end, id:id},
			success:function()
			{
			$('#calendar').fullCalendar('refetchEvents');
			alert("Event Updated");
			}
			});
		},

	});
});


</script>
