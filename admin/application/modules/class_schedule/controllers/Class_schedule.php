<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Class_schedule extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('class_schedulemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}
	function index()
	{
		$result = array();
		
		$this->load->view('template/header.php');
		$this->load->view('index.php',$result);
		$this->load->view('template/footer.php');
    }
	function load()
	{
		$condition = "1=1 ";
		$class_schedule = $this->class_schedulemodel->getData_custom();
		$drop_down = $this->class_schedulemodel->getData("tbl_classes",'classes_id,category',$condition);
		// echo "<pre>";print_r($drop_down);exit;
		if(!empty($class_schedule)){
			foreach ($class_schedule as $key => $value) {
				$data[] = array(
					'id' =>     $value['schedule_id'],
					'title' =>     $value['title'],
					// 'start' =>     $value['starts_date'],
					// 'end' =>     $value['ends_date'],
					'start' =>     $value['start_date_time'],
					'end' =>     $value['end_date_time'],
				);
			}
		}
		// echo "<pre>";print_r($data);exit;
		echo json_encode($data);
	}
	function model_open(){
		// echo "<pre>";print_r($_POST);exit;
		//for edit 
		if(!empty($_POST['id'])){
			$condition = "classes_id = '".$_POST['id']."' ";
			$drop_down['selected_model_detail'] = $this->class_schedulemodel->getData("tbl_schedule",'*',"schedule_id = '".$_POST['id']."'");
			
			if($drop_down['selected_model_detail']){
				$drop_down['selected_model'] = $this->class_schedulemodel->getData("tbl_classes",'classes_id,category',"classes_id = '".$drop_down['selected_model_detail'][0]['class_drop_down']."'");
			}
			// echo $this->db->last_query();
			// echo "<pre>";print_r($drop_down['selected_model']);exit;
		}
		//for add classes schedule
		
		$condition = "1=1 ";
		$drop_down['model'] = $this->class_schedulemodel->getData("tbl_classes",'classes_id,category,title',$condition);
		$modelHtml = '';
		// echo "<pre>";print_r($drop_down);exit;
		if($drop_down['model']){
			$modelHtml = $this->load->view('model',$drop_down,true);
		}
		// echo "<pre>";print_r($modelHtml);exit;
		if($modelHtml){
			echo json_encode(array("success"=>true,"modelHtml"=>$modelHtml));
		}else{
			echo json_encode(array("success"=>false,"modelHtml"=>$modelHtml));
		}
	}
	function insert(){
		// echo "<pre>";print_r($_POST);exit;
		$data =array();
		// $data['classes_id'] = $_POST['title'];
			$starts_date = $_POST['starts_date'];
			$start_time = $_POST['start_time'];
			$ends_date = $_POST['ends_date'];
			$end_time = $_POST['end_time'];
			$data['start_date_time'] = (!empty($starts_date)) ? date('Y-m-d H:i:s', strtotime("$starts_date $start_time")) : '';
			$data['end_date_time'] = (!empty($ends_date)) ? date('Y-m-d H:i:s', strtotime("$ends_date $end_time")) : '';
			$data['title'] = $_POST['title'];
			$data['class_drop_down'] = $_POST['class_drop'];
			$data['duration'] = $_POST['duration'];
			$data['repeat_on'] = (!empty($_POST['repeat_on']) ? implode(',',$_POST['repeat_on']) : '');
			$data['starts_date'] = (!empty($_POST['starts_date'])) ? date('Y-m-d', strtotime($_POST['starts_date'])) : '';
			$data['start_time'] = $_POST['start_time'];
			$data['ends_date'] = (!empty($_POST['ends_date'])) ? date('Y-m-d', strtotime($_POST['ends_date'])) : '';
			$data['end_time'] = $_POST['end_time'];
		if($_POST['hidden_id']){
			$similar_events = $this->class_schedulemodel->updateRecord('tbl_schedule', $data,'schedule_id ',$_POST['hidden_id']);
		}else{
			$similar_events = $this->class_schedulemodel->insertData('tbl_schedule', $data, '1');
		}	
		if (!empty($similar_events)) {
			echo json_encode(array(
				'success' => '1',
				'msg' => 'Record Added/Updated Successfully.'
			));
			exit;
		}else{
			echo json_encode(array(
				'success' => '0',
				'msg' => 'Problem in data update.'
			));
			exit;
		}
	}
	function update()
	{
		// echo "<pre>";print_r($_POST);exit;

		if($_POST['id']){
			$start_date_time = $_POST['start'];
			$tmp_start = explode(' ',$start_date_time);
			$start_date = $tmp_start[0];
			$start_time = $tmp_start[1];

			$end_date_time = $_POST['end'];
			$tmp_end = explode(' ',$end_date_time);
			$end_date = $tmp_end[0];
			$end_time = $tmp_end[1];
		
			$data =array();
			$data['title'] = $_POST['title'];
			$data['start_date_time'] = $_POST['start'];
			$data['end_date_time'] = $_POST['end'];

			$data['starts_date'] = $start_date;
			$data['start_time'] = date("h.i A", strtotime($start_time));

			$data['ends_date'] = $end_date;
			$data['end_time'] = date("h.i A", strtotime($end_time));

			$result = $this->class_schedulemodel->updateRecord('tbl_schedule', $data,'schedule_id',$_POST['id']);
		}
	}
	function delete(){
		if($_POST['id']){
			$Result = $this->class_schedulemodel->delrecord('tbl_schedule', 'schedule_id ',$_POST['id']);
		}
	}
}