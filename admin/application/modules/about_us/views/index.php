<div class="main-panel">
	<div class="container">
		<form class="form-horizontal" id="form-validate" method="POST" enctype="multipart/form-data">
			<div class="page-inner">
				<div class="page-header page-header-btn">
					<div class="page-header-title">
						<h4 class="page-title"><a href="#/"><img src="<?php echo base_url(); ?>assets/images/arrow-outline-left.svg" alt="" class="back-icon-title"></a> About Us</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#/">Website Pages</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">About Us</a>
							</li>
						</ul>
					</div>
					<div>
						<a href="#" class="btn btn-light-itara">Discard</a>
						<button type="submit" class="btn btn-dark-itara">Save</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
					<input type="hidden" id="hidden_about_us_id" name="hidden_about_us_id" value="<?= (!empty($about_details[0]['about_us_id'])) ? $about_details[0]['about_us_id'] : '' ?>">

					<input type="hidden" id="hidden_section1_image_name" name="hidden_section1_image_name" value="<?= (!empty($about_details[0]['section1_image'])) ? $about_details[0]['section1_image'] : '' ?>">

					<input type="hidden" id="hidden_section2_image_name" name="hidden_section2_image_name" value="<?= (!empty($about_details[0]['section2_image'])) ? $about_details[0]['section2_image'] : '' ?>">

					<input type="hidden" id="hidden_section3_image_name" name="hidden_section3_image_name" value="<?= (!empty($about_details[0]['section3_image'])) ? $about_details[0]['section3_image'] : '' ?>">

					<input type="hidden" id="hidden_section4_image_name" name="hidden_section4_image_name" value="<?= (!empty($about_details[0]['section4_image'])) ? $about_details[0]['section4_image'] : '' ?>">

					<input type="hidden" id="hidden_section6_image_name" name="hidden_section6_image_name" value="<?= (!empty($about_details[0]['section6_image'])) ? $about_details[0]['section6_image'] : '' ?>">

				
						<div class="section-single">
							<div class="form-group">
								<label for="homebannerheading">Page title * </label>
								<textarea class="form-control editors" id="heading" name="heading" rows="2" placeholder=""><?= (!empty($about_details[0]['heading'])) ? $about_details[0]['heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Page introduction * </label>
								<textarea class="form-control editors" id="description" name="description" rows="2" placeholder=""><?= (!empty($about_details[0]['description'])) ? $about_details[0]['description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 1 - About us introduction</h3>
							<div class="form-group">
								<label for="homebannerimg">Image </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-1" src="<?= (!empty($about_details[0]['section1_image']) ? FRONT_URL."/images/about_us/".$about_details[0]['section1_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="section1_image" id="section1_image" onchange="loadFile(event,'1')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="section1_image">Upload</label>
								</a>
								<?php if(!empty($about_details[0]['section1_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($about_details[0]['home_id'])) ? $about_details[0]['home_id'] : '' ?>','section1_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Title</label>
								<textarea class="form-control editors" id="section1_heading" name="section1_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section1_heading'])) ? $about_details[0]['section1_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Description</label>
								<textarea class="form-control editors" id="section1_description" name="section1_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section1_description'])) ? $about_details[0]['section1_description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 2 - Our vision</h3>
							<div class="form-group">
								<label for="homebannerimg">Icon </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-2" src="<?= (!empty($about_details[0]['section2_image']) ? FRONT_URL."/images/about_us/".$about_details[0]['section2_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="section2_image" id="section2_image" onchange="loadFile(event,'2')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="section2_image">Upload</label>
								</a>
								<?php if(!empty($about_details[0]['section2_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($about_details[0]['home_id'])) ? $about_details[0]['home_id'] : '' ?>','section2_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Title</label>
								<textarea class="form-control editors" id="section2_heading" name="section2_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section2_heading'])) ? $about_details[0]['section2_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Description</label>
								<textarea class="form-control editors" id="section2_description" name="section2_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section2_description'])) ? $about_details[0]['section2_description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 3 - Our Misson</h3>
							<div class="form-group">
								<label for="homebannerimg">Icon </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-3" src="<?= (!empty($about_details[0]['section3_image']) ? FRONT_URL."/images/about_us/".$about_details[0]['section3_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="section3_image" id="section3_image" onchange="loadFile(event,'3')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="section3_image">Upload</label>
								</a>
								<?php if(!empty($about_details[0]['section3_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($about_details[0]['home_id'])) ? $about_details[0]['home_id'] : '' ?>','section3_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Title</label>
								<textarea class="form-control editors" id="section3_heading" name="section3_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section3_heading'])) ? $about_details[0]['section3_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Description</label>
								<textarea class="form-control editors" id="section3_description" name="section3_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section3_description'])) ? $about_details[0]['section3_description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 4 - Core values</h3>
							<div class="form-group">
								<label for="homebannerimg">Icon </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-4" src="<?= (!empty($about_details[0]['section4_image']) ? FRONT_URL."/images/about_us/".$about_details[0]['section4_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="section4_image" id="section4_image" onchange="loadFile(event,'4')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="section4_image">Upload</label>
								</a>
								<?php if(!empty($about_details[0]['section4_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($about_details[0]['home_id'])) ? $about_details[0]['home_id'] : '' ?>','section4_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Title</label>
								<textarea class="form-control editors" id="section4_heading" name="section4_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section4_heading'])) ? $about_details[0]['section4_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Introduction</label>
								<textarea class="form-control editors" id="section4_description" name="section4_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section4_description'])) ? $about_details[0]['section4_description'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Description</label>
								<textarea class="form-control editors" id="section4_sub_description" name="section4_sub_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section4_sub_description'])) ? $about_details[0]['section4_sub_description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 5  - Testimonial</h3>
							<div class="form-group">
								<label for="homebannerheading">Testimonial text</label>
								<textarea class="form-control editors" id="section5_heading" name="section5_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section5_heading'])) ? $about_details[0]['section5_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Testimmonial name</label>
								<textarea class="form-control editors" id="section5_description" name="section5_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section5_description'])) ? $about_details[0]['section5_description'] : '' ?></textarea>
							</div>
						</div>
						<div class="section-single">
							<h3>Section 6 - Why Choose Us</h3>
							<div class="form-group">
								<label for="homebannerheading">Title above banner</label>
								<textarea class="form-control editors" id="section6_trans_heading" name="section6_trans_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section6_trans_heading'])) ? $about_details[0]['section6_trans_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerimg">Banner </label>
								<p class="preview-img-wrapper">
									<img id="homepreview-6" src="<?= (!empty($about_details[0]['section6_image']) ? FRONT_URL."/images/about_us/".$about_details[0]['section6_image']  : 'https://via.placeholder.com/200x100.png?text=1920px+x+1080px') ?>"/>
								</p>
							
								<p>
									<input type="file" accept="image/*" name="section6_image" id="section6_image" onchange="loadFile(event,'6')" style="display: none;">
								</p>
								<a href="#/" class="btn btn-primary btn-xs">
									<label for="section6_image">Upload</label>
								</a>
								<?php if(!empty($about_details[0]['section6_image'])){ ?>
								<a href="#/" class="btn btn-secondary btn-xs" onclick="delete_image('<?= (!empty($about_details[0]['home_id'])) ? $about_details[0]['home_id'] : '' ?>','section6_image');">Delete</a>
								<?php } ?>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Title</label>
								<textarea class="form-control editors" id="section6_heading" name="section6_heading" rows="2" placeholder=""><?= (!empty($about_details[0]['section6_heading'])) ? $about_details[0]['section6_heading'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="homebannerheading">Description</label>
								<textarea class="form-control editors" id="section6_description" name="section6_description" rows="2" placeholder=""><?= (!empty($about_details[0]['section6_description'])) ? $about_details[0]['section6_description'] : '' ?></textarea>
							</div>
						</div>
						
						<div class="section-single last">
							<h3>Meta Tags</h3>
							<div class="form-group">
								<label for="defaultSelect">Meta Title</label>
								<textarea class="form-control editors" id="meta_title" name="meta_title" rows="2" placeholder=""><?= (!empty($about_details[0]['meta_title'])) ? $about_details[0]['meta_title'] : '' ?></textarea>
							</div>
							<div class="form-group">
								<label for="defaultSelect">Meta Description</label>
								<textarea class="form-control editors" id="meta_description" name="meta_description" rows="5" placeholder=""><?= (!empty($about_details[0]['meta_description'])) ? $about_details[0]['meta_description'] : '' ?></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).on("click",".add_more_inner_image",function(){
	var xi = $(this).attr("panel-count_image");
	var txi = $("#admore_inner_table_image_"+xi+" tbody tr:last-child").attr("tr_inner_count_image");
	txi++;
	if(isNaN(txi) || txi == "undefined"){
		txi = 0;
	}
	if(txi ==4){
		alert("Only 4 Product Variant Images Allowed");
		return false;
	};
	var inner_tr_image = '<tr tr_inner_count_image="'+txi+'" class="active">'+
							'<td>'+
								
							'</td>'+
							'<td>'+
								'<input type="hidden" name="images_existing['+txi+']" value="" id="images_existing_['+txi+']">'+

								'<input type="hidden" name="images_existing_id['+txi+']" value="" id="images_existing_id_['+txi+']">'+

								'<input type="file" class="form-control required" name="file_['+txi+']" id="file_'+txi+'">'+
							'</td>'+
							'<td>'+
								'<input type="text" class="form-control required" name="image_alt['+txi+']" value="" id="image_alt_['+txi+']" placeholder="Enter image name">'+
							'</td>'+
							'<td>'+
								'<button title="Remove" class="btn-danger btn btn-sm remove inner_remove_image" type="button">'+
									'<i class="material-icons"><img src="<?= base_url()?>assets/images/minus-outline.svg"></i>'+
								'</button>'+
							'</td>'+
						'</tr>';
	$("#admore_inner_table_image_"+xi + " tbody").append(inner_tr_image);
});

$(document).on("click",'.inner_remove_image', function(){
	var tr_inner_count_image=$(this).closest('tr').attr('tr_inner_count_image');
			if(tr_inner_count_image!= 0)
			{
				$(this).closest('tr').remove();
			}else{
				swal({title: "Alert!",text: "<p class='font-bold col-red'>Atleast one option is required..</p>",timer: 2000,showConfirmButton: false,html: true});
			}
});


var vRules = {
	banner_heading:{required:true},
};
var vMessages = {
	banner_heading:{required:"Please enter banner heading."},
};
$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		for ( instance in CKEDITOR.instances ) {
			CKEDITOR.instances[instance].updateElement();
		}
		var act = "<?php echo base_url();?>about_us/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>about_us/index";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
		});
	}
});

function delete_image(id,image){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>about_us/deleteImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>about_us/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}

function deleteAboutImage(id,image,folder_name){
	var r=confirm("Are you sure wants to delete");
	if (r==true){
		$.ajax({
			url:"<?php echo base_url();?>about_us/deleteAboutImage",
			dataType: 'json',
			type: "post",
			data:{id:id,image:image,folder_name:folder_name},
			success: function (response) {
				if(response.success == "1")
				{
					displayMsg("success",response.msg);
					setTimeout(function(){
							window.location = "<?php echo base_url();?>about_us/index";
						},2000);
				}
				else
				{	
					displayMsg("error",response.msg);
					return false;
				}
			}
		});
	}
}
CKEDITOR.replaceClass = 'editors';
/*Preview Images*/
var loadFile = function(event,id) {
	var image = document.getElementById('homepreview-'+id);		
	image.src = URL.createObjectURL(event.target.files[0]);		
};
/*Preview Images*/
</script>
