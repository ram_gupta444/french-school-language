$(document).ready(function () {
  /*Toggle contact popup*/
  const $menu = $('.nav-account');
  $(document).mouseup(e => {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
      && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu.children('.my-account-popup').removeClass('is-active');
    }
  });
  $('.account-link').on('click', () => {
    $menu.children('.my-account-popup').toggleClass('is-active');
  });
  const $menu2 = $('.nav-lang');
  $(document).mouseup(e => {
    if (!$menu2.is(e.target) // if the target of the click isn't the container...
      && $menu2.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu2.children('.lang-popup').removeClass('is-active');
    }
  });
  $('.lang-link').on('click', () => {
    $menu2.children('.lang-popup').toggleClass('is-active');
  });
  /*Toggle contact popup*/
  /*Toggle between hamburger and close icon*/
  $('.navbar-toggler').click(function () {
    $(this).toggleClass('open');
  })
  /*Toggle between hamburger and close icon*/
  /*My account popup*/
  $('.my-account-mobile a').click(function () {
    $('.popup-overlay,.my-account-popup-mobile').fadeIn();
  })
  $('.popup-overlay').click(function () {
    $(this).fadeOut();
    $('.tls-popup-contact,.my-account-popup-mobile,.language-popup-mobile').fadeOut()
  })
  /*My account popup*/
  /*Language popup*/
  $('.language-mobile a').click(function () {
    $('.popup-overlay,.language-popup-mobile').fadeIn();
  })
  /*Language popup*/
  /*Filter toggle*/
  $('.btn-filter').click(function () {
    $('.filter-content-wrapper').toggle();
  });
  /*Homepage programs equal height*/
  if ($(window).width() > 767) {
    /* var maxHeight = 0;
    $(".hp-single div.txtm").each(function () {
      if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
    });
    $(".hp-single div.txtm").outerHeight(maxHeight); */

    var maxHeight2 = 0;
    $(".hp-single h3").each(function () {
      if ($(this).outerHeight() > maxHeight2) { maxHeight2 = $(this).outerHeight(); }
    });
    $(".hp-single h3").outerHeight(maxHeight2);
  }
  /*Homepage programs equal height*/
  /*Testimonial carousel*/

  $('.testimonial-slider').owlCarousel({
    animateOut: 'animate__fadeOutLeft',
    animateIn: 'animate__fadeInRight',
    items: 1,
    margin: 70,
    stagePadding: 0,
    loop: true,
    nav: true,
    navText: ["<img src='http://fls.webshowcase-india.com/assets/images/arrow-left-outline-blue.svg'>", "<img src='http://fls.webshowcase-india.com/assets/images/arrow-right-filled-blue.svg'>"],
    dots: false,
  });

  /*Testimonial carousel*/
  /*Private tuitions read more*/
  $('.ptl-read-more-link').click(function () {
    $(this).siblings('.ptl-content').toggleClass('read-more');
    if ($(this).siblings('.ptl-content').hasClass('read-more')) {
      $(this).text('Read Less');
      $(this).addClass('read-less-icon')
    } else {
      $(this).text('Read More');
      $(this).removeClass('read-less-icon')
    }
  })
  /*Private tuitions read more*/
  $('.gsd-count').niceSelect();
  $('.nav-language-select').niceSelect();
  $('.filter-select').niceSelect();
  $('.select-contact').niceSelect();
  /*Floating labels*/
  var formFields = $('.form-group');

  formFields.each(function () {
    var field = $(this);
    var input = field.find('input');
    var textarea = field.find('textarea');
    var label = field.find('label');

    function checkInput() {
      var valueLength = input.val().length;

      if (valueLength > 0) {
        label.addClass('freeze')
      } else {
        label.removeClass('freeze')
      }
    }
    function checkTextarea() {
      var valueLength2 = textarea.val().length;

      if (valueLength2 > 0) {
        label.addClass('freeze')
      } else {
        label.removeClass('freeze')
      }
    }

    input.change(function () {
      checkInput()
    })
    textarea.change(function () {
      checkTextarea()
    })
  });
  /*Floating labels*/
  /*Login toggle*/
  $('.fp-link').click(function () {
    $('.login-form,.register-form').hide();
    $('.forgot-password-form').show();

  })
  $('.login-link').click(function () {
    $('.forgot-password-form,.register-form').hide();
    $('.login-form').show();
  })
  $('.create-account-link').click(function () {
    $('.login-form,.forgot-password-form').hide();
    $('.register-form').show();
  })
  /*Login toggle*/
  /*Tabs toggle*/
  // $('.tabs-list li a').on('click', function () {   
  //   $('.tabs-list li a').removeClass('active');
  //   $(this).addClass('active');
  //   var tab_value = $(this).attr('data-tab');
  //   $('.tabs-single').hide();
  //   $('.tabs-single.' + tab_value).show();  
  // })
  /*Tabs toggle*/

  /*Checkout registration*/
  // $('.rs-title a.remove-reg').on('click',function(){
  //   $(this).parent('.rs-title').parent('.reg-single').remove()
  // })

  $('.arrow-reg').on('click', function () {
    $(this).parent('.rs-title').siblings('.reg-form').toggle();
    $(this).toggleClass('rotate-icon')
  });
  /*Checkout registration*/
  AOS.init({
    delay: 100,
    duration: 500,
  });
  $('.showpassword').click(function () {
    if ($(this).siblings('.password').attr('type') == 'password') {
      $(this).siblings('.password').attr('type', 'text');
      /* $(this).children('img').attr("src","images/hide.svg"); */
    } else {
      $(this).siblings('.password').attr('type', 'password');
      /* $(this).children('img').attr("src","images/show.svg"); */
    }
  });
  $('.accept-cookie,.reject-cookie').click(function () {
    $('.cookie-container').fadeOut()
  });
  if ($(window).width() < 768) {
    $('.video-testimonial-wrapper').owlCarousel({
      animateOut: 'animate__fadeOutLeft',
      animateIn: 'animate__fadeInRight',
      items: 1,
      margin: 70,
      stagePadding: 0,
      loop: true,
      nav: true,
      navText: ["<img src='http://fls.webshowcase-india.com/assets/images/arrow-left-outline-blue.svg'>", "<img src='http://fls.webshowcase-india.com/assets/images/arrow-right-filled-blue.svg'>"],
      dots: false,
    });
  }
  /*Footer language list in two columns*/
  if ($('.footer-adult-fls .footer-links-list li').length > 6) {
    $('.footer-adult-fls .footer-links-list').addClass('two-cols');
  };
  /*Smooth scroll to div*/
  var cl_list = $('#classes-list').length;
  if (cl_list) {
    $('html, body').animate({
      scrollTop: $('#classes-list').offset().top
    }, 800);
  }
  /*Smooth scroll to div*/

});
