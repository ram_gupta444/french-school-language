<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class All_group_study extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('all_group_studymodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		if(empty($_SESSION['language_id'])){
			redirect(base_url());
		}
		$_SESSION['last_stage'] = "all_group_study";
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$language_id = $_SESSION['language_id'];
		
		$result = array();
		$condition = "language = '".$_SESSION['language_id']."' ";
		$result['adult_group_study'] = $this->common->getData("tbl_adult_group_study",'*',$condition);
		
		// $result['upcomming_classes'] = $this->all_group_studymodel->upcommingClasses();

		$condition_cus = "language_master_id= $language_id ";
		$result['language_home'] = $this->common->getData("tbl_language_master",'*',$condition_cus);
		// echo "<pre>";print_r($result['upcomming_classes']);exit;
		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	public function getClassesList(){
		$language_id = $_SESSION['language_id'];
		// echo "<pre>";print_r($_POST);exit;
		// $condition = " s.starts_date >= '".date('Y-m-d')."' AND languages LIKE '%$language_id%' AND c.status = 'Active' ";
		$condition = " s.ends_date >= '".date('Y-m-d')."' AND languages LIKE '%$language_id%' AND c.status = 'Active' ";
		
		// start this is for filter
		if(isset($_POST['value']) &&  !empty($_POST['value'])){
			if($_POST['value'] == 'Adult'){
				$condition .= " && c.category = 'Adult' ";
			}
			if($_POST['value'] == 'Children'){
				$condition .= " && c.category = 'Children' ";
			}
			if($_POST['value'] == 'Online'){
				$condition .= " && c.class_type = 'Online' ";
			}
			if($_POST['value'] == 'In Person'){
				$condition .= " && c.class_type = 'In Person' ";
			}
			if($_POST['value'] == 'All'){
				$condition .= " && 1=1";
			}
		}
		$colArray = array('c.category','c.no_of_hours','c.duration_in_week','c.headline','s.duration');
		if(isset($_POST['search_box']) &&  !empty($_POST['search_box'])){
			for($i=0;$i<5;$i++)
		 	{	
				$condition .= " OR $colArray[$i] like '%".$_POST['search_box']."%'";
			}
		}
		// echo $condition;
		
		$result = array();
		$main_table = array("tbl_classes as c", array("c.classes_id,c.category,c.no_of_hours,c.duration_in_week,c.title,c.banner_image,c.headline,c.class_type"));
		$join_tables = array(
			array("", "tbl_schedule as  s", "c.classes_id  = s.class_drop_down", array("s.starts_date,s.repeat_on")),
		);

		$limit = array("offset"=>$_POST['start'],"rows"=>$_POST['limit']);

		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "DESC","c.classes_id",$limit);
		// fetch query
		$result['classes_listing'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result	
		// echo "<pre>";print_r($result['classes_listing']);exit;
		
		// echo $this->db->last_query();
		$totalCount = count($result['classes_listing']);
		// $result['hidden_language'] = $_POST['hidden_language'];
		$condition_cus = "language_master_id= $language_id ";
		$result['language_home'] = $this->common->getData("tbl_language_master",'*',$condition_cus);
		// echo "<pre>";print_r($result['language_home']);exit;
		$classesListingHtml = '';
		// $totalCount = 3;
		if($result['classes_listing']){
			$classesListingHtml = $this->load->view('group_study_listing',$result,true);
		}

		if($classesListingHtml){
			echo json_encode(array("success"=>true,"classesListingHtml"=>$classesListingHtml,"totalCount"=>$totalCount));
		}else{
			echo json_encode(array("success"=>false,"classesListingHtml"=>$classesListingHtml,"totalCount"=>$totalCount));
		}

	}
}

?>
