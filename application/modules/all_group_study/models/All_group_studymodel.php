<?PHP
class All_group_studymodel extends CI_Model
{
	
	function getdata($table, $select = "*", $condition = '',$obj = false) {
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){
			$this->db->where("($condition)");
		}
		$query = $this->db->get();
		if($query->num_rows() >= 1){
			if($obj){
				return $query->result();
			}else{
				return $query->result_array();
			}
		}else{
			return false;
		}
	}
	function upcommingClasses(){
        $this -> db -> select('c.classes_id,c.no_of_hours,c.duration_in_week,c.title,c.banner_image,c.headline,s.starts_date');
        $this -> db -> from('tbl_classes as c');
        $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this->db->where('s.starts_date >=', date('Y-m-d'));
		$this->db->where('c.status','Active');
		$this->db->group_by('c.classes_id');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }


}
?>
