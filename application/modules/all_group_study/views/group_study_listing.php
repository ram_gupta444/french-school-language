<?php 
$delay = 100;
foreach ($classes_listing as $key => $value) {?>
<div class="gsl-single flex-wrapper flex-align-start flex-justify-sb" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
    <img src="<?= (!empty($value['banner_image'])?base_url('images/class/').$value['banner_image']:"") ?>" alt="" class="gsl-img">
    <div class="gsl-single-right">
        <div class="study-label-wrapper">
            <div class="all-group-label"><?= (!empty($value['class_type'])?$value['class_type']:"") ?></div>
            <div class="all-group-label"><?= (!empty($value['category'])?$value['category']:"") ?></div>
        </div>
        <div class="group-info-wrapper flex-wrapper flex-align-center flex-justify-start mb16">
            <div class="lang-name txts semibold">
                <img src="<?= (!empty($language_home[0]['language_logo'])?base_url('images/language_logo/').$language_home[0]['language_logo']:"") ?>" alt="">
                <?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?>
            </div>
            <div class="category-name txts semibold">
                Category Name
            </div>
        </div>
        <h4 class="txtl bold txt-black"><?= (!empty($value['headline'])?$value['headline']:"") ?></h4>
        <div class="course-details-wrapper flex-wrapper flex-align-center flex-justify-sb mt16">
            <div class="cd-left">
                <div class="txts bold">Course duration: <?= (!empty($value['duration_in_week'])?$value['duration_in_week']:"") ?> weeks<br />
                Frequency: <?= (!empty($value['repeat_on']))? count(explode(',',$value['repeat_on'])):"" ?> times a week,<br />
                <?= (!empty($value['no_of_hours'])?$value['no_of_hours']:"") ?> hour per class
                </div>
            </div>
            <div class="cd-right">
                <a href="<?= base_url('all_group_study_detail/index?text='.rtrim(strtr(base64_encode("id=".$value['classes_id']), '+/', '-_'), '=').'')?>" class="btn-fls btn-medium btn-fls-secondary">See options</a>
            </div>
        </div>
    </div>
</div>
<?php 
$delay += 100;
}?>
