<section class="sec-internal-intro">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="sec-top-wrapper">
            <ol class="breadcrumb breadcrumb-chevron">
              <li class="breadcrumb-item"><a href="<?php echo base_url();?>home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url().$_SESSION['language_slug'].'/'.'language_home'?>"><?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?></a></li>
              <li class="breadcrumb-item active"><?= (!empty($adult_group_study[0]['section1_heading'])?$adult_group_study[0]['section1_heading']:"") ?></li>
            </ol>
            <div class="sec-top-title mb16" data-aos="fade-up">
              <h1 class="title-1 bold txt-orange"><?= (!empty($adult_group_study[0]['section1_heading'])?$adult_group_study[0]['section1_heading']:"") ?></h1>
              <img src="<?PHP echo base_url();?>assets/images/private-tuitions-border.svg" alt="" class="stt-border img-fluid">
            </div>
            <div class="txtm" data-aos="fade-up" data-aos-delay="200" >
            <?= (!empty($adult_group_study[0]['section1_description'])?$adult_group_study[0]['section1_description']:"") ?>
            </div>
          </div>
        </div>
        <span id="classes-list"></span>
      </div>
    </div>
  </div>
</section>

<section class="sec-single adult-gs-sec-1" >
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="filter-wrapper flex-wrapper flex-align-center flex-justify-sb">
          <h2 class="title-2 bold txt-orange group-study-title mb32" data-aos="fade-up" data-aos-delay="200">
            Upcoming Classes
            <img src="<?PHP echo base_url();?>assets/images/upcoming-classes-title.svg" alt="" class="uc-img">
          </h2>
          <a href="#/" class="btn-fls btn-medium btn-fls-secondary btn-filter"><img src="<?PHP echo base_url();?>assets/images/filter.svg" alt=""> Filter</a>
        </div>
        <div class="filter-content-wrapper">
          <form>
            <div class="filter-content">
              <div class="fc-sec-single">
                <a href="#/" class="fcs-single active" onclick="getfilteredClassesList('All')">View All</a>
                <a href="#/" class="fcs-single" onclick="getfilteredClassesList('Adult')">Adult</a>
                <a href="#/" class="fcs-single" onclick="getfilteredClassesList('Children')">Children</a>
              </div>
              <div class="fc-sec-single">
                <a href="#/" class="fcs-single active" onclick="getfilteredClassesList('All')">View All</a>
                <a href="#/" class="fcs-single" onclick="getfilteredClassesList('Online')">Online</a>
                <a href="#/" class="fcs-single" onclick="getfilteredClassesList('In Person')">In Person</a>
              </div>
              <input type="text" name="search_box" id="search_box" placeholder="Search" class="filter-search">
            </div>
          </form>
        </div>
        <!-- loader part start here -->
        <div class="loader-wrapper center" id="loader_load"  style="display:none">		
            <img src="<?= base_url('assets/images/loader.gif')?>"  alt="">
          <p class="loader-text">Loading...</p>
        </div>
        <!-- loader part end here -->	
        <!-- <div class="row">
          <div class="col-12">
            <div class="show-count">
              <p>Showing <span id="recordFound"></span> of <span id="totalRecords"></span> </p>
            </div>
          </div>
        </div> -->
        <div class="group-study-list-wrapper flex-wrapper flex-align-start flex-justify-sb" id="classes_listing_div">
         
        </div>
        <div class="row" id="no_data_founf_div">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial sec-testimonial-above-footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="<?PHP echo base_url();?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24"><?= (!empty($adult_group_study[0]['section2_heading'])?$adult_group_study[0]['section2_heading']:"") ?></h3>
            <h4 class="title-4 bold txt-white tq-name"><?= (!empty($adult_group_study[0]['section2_sub_heading'])?$adult_group_study[0]['section2_sub_heading']:"") ?></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

</html>
<script>
var limit = 10;
var start = 0;
$(window).on( "load", function() { 
  getClassesList(limit,start);
});

// for product listing 
function getClassesList(limit,start,value=null,search_box=null){
 
  $("#loader_load").show();
    $.ajax({
				url: "<?= base_url('all_group_study/getClassesList')?>",
				type: "POST",
				data:{limit,start,value,search_box},
				dataType: "json",
				success: function(response){
          // console.log(response);
					if(response.success){
            if(start == 0 ){
              $("#classes_listing_div").html(response.classesListingHtml);	
            }else{
              $("#classes_listing_div").append(response.classesListingHtml);
            }
            $("#totalRecords").html(response.totalCount);	
            $("#loader_load").hide();
					}else{
            $("#classes_listing_div").html('');
            $("#no_data_founf_div").html('No data found');
            $("#loader_load").hide();
					}

          // to count the number of  result found so far 
          // $("#recordFound").html($("#classes_listing_div > div").length);
          
          if($("#classes_listing_div > div").length == response.totalCount ){
              $(".btn-secondary-itara").hide();
          }
				}
			});
}

function getfilteredClassesList(value){
  // alert(value);
  var limit = 10;
  var start = 0;
  getClassesList(limit,start,value);
}

$('#search_box').on("input", function() {
    var search_box = this.value;
    var limit = 10;
    var start = 0;
    getClassesList(limit,start,'',search_box);
});
</script>