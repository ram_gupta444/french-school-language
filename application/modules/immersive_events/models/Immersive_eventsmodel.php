<?PHP
class Immersive_eventsmodel extends CI_Model
{
	
	function upcommingEvents(){
        $this -> db -> select('e.*');
        $this -> db -> from('tbl_events as e');
        // $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this->db->where('e.day_and_date >=', date('Y-m-d'));
        // $this->db->where('e.category','Adult');
		    // $this->db->group_by('c.classes_id');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function getCustomList($event_id){
        $this -> db -> select('count(registration_id) as reg_count');
        $this -> db -> from('tbl_registration');
        // $this -> db -> join("tbl_language_master as l","l.language_master_id IN explode(',',c.event_id)");
        $this -> db -> where('type ', 'Event');
        $this -> db -> where('event_id ', $event_id);
        $this -> db -> where('payment_status ', 'Completed');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1){
          return $query->result_array();
        }
        else{
          return false;
        }
     }

}
?>
