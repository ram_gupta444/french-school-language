<?php 
$delay = 100;
foreach ($upcoming_event as $key => $value) {?>
    <div class="il-single" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
    <img src="<?= (!empty($value['banner_image'])?base_url('images/event/').$value['banner_image']:"") ?>" alt="" class="ils-img">
        <div class="ils-location-wrapper ip-label-1 flex-wrapper flex-align-center flex-justify-sb mt16">
            <div class="location-name txts">
                <div class="all-group-label"><?= (!empty($value['event_type'])? $value['event_type']:"") ?></div>
                <div class="all-group-label"><?= (!empty($value['category'])? $value['category']:"") ?></div>
              </div>
            <?php if(!empty($value['no_of_seet'] - $available_events[$key][0]['reg_count']) && ($value['no_of_seet'] - $available_events[$key][0]['reg_count']) != 0){ ?>
                <div class="availability txts bold"><span class="seat-status available"><?= ($value['no_of_seet'] - $available_events[$key][0]['reg_count']) ?> Seats Available</span></div>
            <?php }else{ ?>
                <div class="availability txts bold"><span class="seat-status soldout">Sold out</span></div>
           <?php }?>
          
            <div class="meet-location txts mt8 medium"><?= (!empty($value['event_type']) && $value['event_type'] == 'Online') ? "Zoom": (!empty($value['location'])? $value['location']:"") ?></div>
            <div class="flex-full txt-gold txts semibold"><span>Starts <?= (!empty($value['day_and_date'])? date(" jS F Y", strtotime($value['day_and_date'])):"") ?></span></div>
              <div class="flex-full txt-gold txts semibold"><span><?= (!empty($value['event_timeline'])? $value['event_timeline']:"") ?></span></div>
            <h4 class="flex-full txtl bold txt-blue mt8 mb8"><?= (!empty($value['headline'])? $value['headline']:"") ?></h4>
            <div class="txtm ils-desc">
            <?= (!empty($value['intro_text'])? $value['intro_text']:"") ?>
            </div>
            <div class="ils-cta mt16 w100">
            <a href="<?= base_url('immersive_event_detail/index?text='.rtrim(strtr(base64_encode("id=".$value['events_id']), '+/', '-_'), '=').'')?>" class="btn-fls btn-large btn-fls-tertiary w100">View Information</a>
            </div>
        </div>
    </div>
<?php 
$delay += 100;
}?>
