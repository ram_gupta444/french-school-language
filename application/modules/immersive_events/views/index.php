<section class="sec-internal-intro">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="sec-top-wrapper">
            <div class="sec-top-title mb16" data-aos="fade-up">
              <h1 class="title-1 bold txt-orange"><?= (!empty($immersive_event[0]['section1_heading'])? $immersive_event[0]['section1_heading']:"") ?></h1>
              <img src="assets/images/private-tuitions-border.svg" alt="" class="stt-border img-fluid">
            </div>
            <div class="txtm" data-aos="fade-up" data-aos-delay="200">
            <?= (!empty($immersive_event[0]['section1_description'])? $immersive_event[0]['section1_description']:"") ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single adult-gs-sec-1">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="filter-wrapper flex-wrapper flex-align-center flex-justify-sb">
          <h2 class="title-2 bold txt-orange group-study-title mb32" data-aos="fade-up" data-aos-delay="200">
          <?= (!empty($immersive_event[0]['dynamic_data_title'])? $immersive_event[0]['dynamic_data_title']:"") ?>
            <img src="assets/images/upcoming-events-title.svg" alt="" class="ue-img">
          </h2>
          <a href="#/" class="btn-fls btn-medium btn-fls-secondary btn-filter"><img src="assets/images/filter.svg" alt=""> Filter</a>
        </div>
        <div class="filter-content-wrapper">
          <form>
            <div class="filter-content">
              <select name="language_drop" id="language_drop" class="filter-select country" onchange="GetSelectedTextValue(this)">
                <option value="">Select language</option>
                <?php foreach ($Languages as $key => $value) { ?>
                  <option value="<?= $value['language_master_id']?>" ><?= $value['language_name']?></option>
                <?php } ?>
              </select>
              <div class="fc-sec-single">
                <a href="#/" class="fcs-single active" onclick="getfilteredImmersiveList('All')">View All</a>
                <a href="#/" class="fcs-single" onclick="getfilteredImmersiveList('Adult')">Adult</a>
                <a href="#/" class="fcs-single" onclick="getfilteredImmersiveList('Children')">Children</a>
              </div>
              <div class="fc-sec-single">
                <a href="#/" class="fcs-single active" onclick="getfilteredImmersiveList('All')">View All</a>
                <a href="#/" class="fcs-single" onclick="getfilteredImmersiveList('Online')">Online</a>
                <a href="#/" class="fcs-single" onclick="getfilteredImmersiveList('In Person')">In Person</a>
              </div>
              <input type="text" name="search_box" id="search_box" placeholder="Search" class="filter-search">
            </div>
          </form>
        </div>
        <!-- loader part start here -->
        <div class="loader-wrapper center" id="loader_load"  style="display:none">		
            <img src="<?= base_url('assets/images/loader.gif')?>"  alt="">
          <p class="loader-text">Loading...</p>
        </div>
        <!-- loader part end here -->	
        <!-- <div class="row">
          <div class="col-12">
            <div class="show-count">
              <p>Showing <span id="recordFound"></span> of <span id="totalRecords"></span> </p>
            </div>
          </div>
        </div> -->
        <div class="immersive-list-wrapper flex-wrapper flex-align-start flex-justify-start" id="event_listing_div">

        </div>
        <div id="no_data_founf_div" class="title-4 semibold txt-black"></div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial sec-testimonial-above-footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24"><?= (!empty($immersive_event[0]['section2_heading'])?$immersive_event[0]['section2_heading']:"") ?></h3>
            <h4 class="title-4 bold txt-white tq-name"><?= (!empty($immersive_event[0]['section2_sub_heading'])?$immersive_event[0]['section2_sub_heading']:"") ?></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

</html>
<script>
var limit = 10;
var start = 0;
$(window).on( "load", function() { 
  getImmersiveList(limit,start);
});

// for product listing 
function getImmersiveList(limit,start,value=null,search_box=null,language=null){
 
  $("#loader_load").show();
    $.ajax({
				url: "<?= base_url('immersive_events/getImmersiveList')?>",
				type: "POST",
				data:{limit,start,value,search_box,language},
				dataType: "json",
				success: function(response){
          // console.log(response);
					if(response.success){
            if(start == 0 ){
              $("#event_listing_div").html(response.eventsListingHtml);	
              $("#no_data_founf_div").html('');
            }else{
              $("#event_listing_div").append(response.eventsListingHtml);
              $("#no_data_founf_div").html('');
            }
            $("#totalRecords").html(response.totalCount);	
            $("#loader_load").hide();
					}else{
            $("#event_listing_div").html('');
            $("#no_data_founf_div").html('No data found');
            $("#loader_load").hide();
					}

          // to count the number of  result found so far 
          // $("#recordFound").html($("#event_listing_div > div").length);
          
          if($("#event_listing_div > div").length == response.totalCount ){
              $(".btn-secondary-itara").hide();
          }
				}
			});
}

function getfilteredImmersiveList(value){
  // alert(value);
  var limit = 10;
  var start = 0;
  getImmersiveList(limit,start,value);
}

$('#search_box').on("input", function() {
    var search_box = this.value;
    var limit = 10;
    var start = 0;
    getImmersiveList(limit,start,'',search_box);
});

function GetSelectedTextValue(language) {
  var selectedValue = language.value;
  var limit = 10;
  var start = 0;
  getImmersiveList(limit,start,'','',selectedValue);
}
</script>