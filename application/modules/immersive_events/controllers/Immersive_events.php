<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Immersive_events extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('immersive_eventsmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		unset($_SESSION['language_id']);
		$_SESSION['last_stage'] = "immersive_events";
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
		
		$result = array();
		$condition = "1=1";
	
		$result['immersive_event'] = $this->common->getData("tbl_adult_program",'adult_program_id,section1_description,section1_heading,dynamic_data_title,section2_heading,section2_sub_heading',$condition);

		$condition = "status = 'Active'  ";
		$result['Languages'] = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition);
		// echo "<pre>";print_r($result['immersive_event']);exit;
		// $result['upcomming_classes'] = $this->immersive_eventsmodel->upcommingClasses();
		// $result['upcoming_event'] = $this->immersive_eventsmodel->upcommingEvents();

		// echo "<pre>";print_r($result['adult_group_study']);exit;
		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	public function getImmersiveList(){
		
		// $condition = " day_and_date >= '".date('Y-m-d')."' ";
		$condition = " status = 'Active' ";
		
		// start this is for filter
		if(isset($_POST['value']) &&  !empty($_POST['value'])){
			if($_POST['value'] == 'Adult'){
				$condition .= " && category = 'Adult' ";
			}
			if($_POST['value'] == 'Children'){
				$condition .= " && category = 'Children' ";
			}
			if($_POST['value'] == 'Online'){
				$condition .= " && event_type = 'Online' ";
			}
			if($_POST['value'] == 'In Person'){
				$condition .= " && event_type = 'In Person' ";
			}
			if($_POST['value'] == 'All'){
				$condition .= " && 1=1";
			}
		}

		if(isset($_POST['language']) &&  !empty($_POST['language'])){
			// $condition .= " && languages IN (".$_POST['language'].")";
			$condition .= " && languages LIKE '%".$_POST['language']."%' ";
		}

		$colArray = array('category','location','headline','intro_text','day_and_date');
		if(isset($_POST['search_box']) &&  !empty($_POST['search_box'])){
			for($i=0;$i<5;$i++)
		 	{	
				$condition .= " OR $colArray[$i] like '%".$_POST['search_box']."%'";
			}
		}
		// echo $condition;
		$result = array();
		$result['upcoming_event'] = $this->common->getDataLimit("tbl_events",'*',$condition,"events_id","DESC",$_POST['start'],$_POST['limit']);

		// echo "<pre>";print_r($result['upcoming_event']);exit;
		if(!empty($result['upcoming_event'])){
			foreach ($result['upcoming_event'] as $key => $value) {
				$result['available_events'][] = $this->immersive_eventsmodel->getCustomList($value['events_id']);
			}
		}
		
		// echo "<pre>";print_r($result['available_events']);exit;
		// echo $this->db->last_query();exit;
		if(!empty($result['upcoming_event'])){
			$totalCount = count($result['upcoming_event']);
		}else{
			$totalCount = 0;
		}
		
		$eventsListingHtml = '';
		// $totalCount = 3;
		if($result['upcoming_event']){
			$eventsListingHtml = $this->load->view('event_listing',$result,true);
		}

		if($eventsListingHtml){
			echo json_encode(array("success"=>true,"eventsListingHtml"=>$eventsListingHtml,"totalCount"=>$totalCount));
		}else{
			echo json_encode(array("success"=>false,"eventsListingHtml"=>$eventsListingHtml,"totalCount"=>$totalCount));
		}

	}
}

?>
