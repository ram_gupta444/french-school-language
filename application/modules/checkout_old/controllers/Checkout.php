<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Checkout extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('checkoutmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->library('paypal_lib');
		// checklogin();
	}

	function index(){
		$_SESSION['last_stage'] = "checkout";
		$result = array();
		// echo "<pre>";print_r($_SESSION['fls_user'][0]['user_id']);exit;
		if($_POST['type'] == 'Event'){
			$event_id = $_POST['hidden_event_id'];
			$condition = "events_id = $event_id ";
		
			$result['event_detail'] = $this->common->getData("tbl_events",'event_fees,no_of_seet,headline,intro_text,events_id',$condition);
		}
		if($_POST['type'] == 'Class'){
			$result['schedule_id'] = $_POST['hidden_schedule_id'];
			$class_id = $_POST['hidden_class_id'];
			$condition = "classes_id = $class_id ";
		
			$result['class_detail'] = $this->common->getData("tbl_classes",'class_fees,no_of_seet,headline,intro_text,classes_id',$condition);
		}
		
		$result['reg_count'] = (!empty($_POST['reg_count'])) ? $_POST['reg_count'] : 1;
		$result['type'] = $_POST['type'];

		if(!empty($_SESSION['fls_user'][0]['user_id']) ){
			$condition = " user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
			$result['login_user'] = $this->common->getData('tbl_users','*',$condition);

			$condition = " user_id = '".$_SESSION['fls_user'][0]['user_id']."' ORDER BY registration_id  DESC limit 1";
			$result['register_user'] = $this->common->getData('tbl_registration','*',$condition);
			// echo $this->db->last_query();exit;
		}
		// echo "<pre>";print_r($result['login_user']);exit;
		$this->load->view('template/header.php',$result);
		if($_POST['type'] == 'Event'){
			$this->load->view('index',$result);
		}
		if($_POST['type'] == 'Class'){
			$this->load->view('classIndex',$result);
		}
		$this->load->view('template/footer.php');
	}
	
	function buy()
	{
		// echo "<pre>";print_r($_POST);exit;
		$_SESSION["registration_id"] = md5(date("m-d-Y H:i:s.u"));

		if(!empty($_SESSION['fls_user'][0]['user_id']) ){
			$user_id = $_SESSION['fls_user'][0]['user_id'];
		}else{
			$data_user = array(); 
			$data_user['email_id'] = (!empty($_POST['email_id'][0])) ? $_POST['email_id'][0] : '';
			$data_user['full_name'] = (!empty($_POST['first_name'][0])).' '.(!empty($_POST['last_name'][0]));
			$data_user['role_id'] = '2';
			$data_user['dob'] = $_POST['dob'][0];
			$data_user['password'] = md5($_POST['password'][0]);
			$data_user['is_email_verified'] = 'Yes';
			$data_user['status'] = 'Active';
			$data_user['updated_on'] = date("Y-m-d H:i:s");
			$data_user['created_on'] = date("Y-m-d H:i:s");
			$result_user = $this->common->insertData('tbl_users',$data_user,'1');
			$user_id = $result_user;
		}
		
		$data = array();
		foreach($_POST['first_name'] as $key=>$value){
			$data['user_id'] = $user_id;
			$data['type'] = (!empty($_POST['type'])) ? $_POST['type'] : '';
			$data['first_name'] = (!empty($_POST['first_name'])) ? $_POST['first_name'][$key] : '';
			$data['last_name'] = (!empty($_POST['last_name'][$key])) ? $_POST['last_name'][$key] : '';
			$data['email_id'] = (!empty($_POST['email_id'][$key])) ? $_POST['email_id'][$key] : '';
			$data['phone_no'] = (!empty($_POST['phone_no'][$key])) ? $_POST['phone_no'][$key] : '';
			$data['address'] = (!empty($_POST['address'][$key])) ? $_POST['address'][$key] : '';
			$data['country'] = (!empty($_POST['country'][$key])) ? $_POST['country'][$key] : '';
			$data['state'] = (!empty($_POST['state'][$key])) ? $_POST['state'][$key] : '';
			$data['city'] = (!empty($_POST['city'][$key])) ? $_POST['city'][$key] : '';
			$data['zip_code'] = (!empty($_POST['zip_code'][$key])) ? $_POST['zip_code'][$key] : '';
			$data['company_name'] = (!empty($_POST['company_name'][$key])) ? $_POST['company_name'][$key] : '';
			$data['event_id'] = (!empty($_POST['hidden_event_id'])) ? $_POST['hidden_event_id'] : '';
			$data['schedule_id'] = (!empty($_POST['hidden_schedule_id'])) ? $_POST['hidden_schedule_id'] : '';
			$data['status'] = 'Active';
			$data['session_id'] = $_SESSION["registration_id"];
			$data['created_on'] = date("Y-m-d H:i:s");
			
			$result = $this->checkoutmodel->insertData('tbl_registration', $data,'1');
		}
		
		// Set variables for paypal form 
		// echo base_url().'paypal/success';exit;
        $returnURL = base_url().'checkout/success'; //payment success url 
        $cancelURL = base_url().'checkout/cancel'; //payment cancel url 
        $notifyURL = base_url().'checkout/ipn'; //ipn url 
         
        // Get product data from the database 
		if($_POST['type'] == 'Event'){
			$event = $this->checkoutmodel->getRows($_POST['hidden_event_id']);
			$this->paypal_lib->add_field('item_name',$event['headline']); 
			$this->paypal_lib->add_field('item_number', $event['events_id']); 
			$this->paypal_lib->add_field('amount',  ($_POST['totaluser'] * $event['event_fees'])); 
			// $this->paypal_lib->add_field('user_id', $user_id);
		}
		if($_POST['type'] == 'Class'){
			$class = $this->checkoutmodel->getRowsClass($_POST['hidden_class_id']);
			$this->paypal_lib->add_field('item_name',$class['headline']); 
			$this->paypal_lib->add_field('item_number', $class['classes_id']); 
			$this->paypal_lib->add_field('amount',  ($_POST['totaluser'] * $class['class_fees'])); 
			// $this->paypal_lib->add_field('user_id', $user_id);
		}
        // Get current user ID from the session (optional) 
         
        // Add fields to paypal form 
		$custom = array('type'=>$_POST['type'],'user_id'=>$user_id,'session_id'=>$_SESSION['registration_id']);
        $this->paypal_lib->add_field('return', $returnURL); 
        $this->paypal_lib->add_field('cancel_return', $cancelURL); 
        $this->paypal_lib->add_field('notify_url', $notifyURL); 
		$this->paypal_lib->add_field('custom', json_encode($custom)); 
        // Render paypal form 
        $this->paypal_lib->paypal_auto_form(); 
	}

	function success(){ 
        // Get the transaction data 
        $paypalInfo = $this->input->get(); 
        //  echo "<pre>";print_r($paypalInfo);exit;
        $productData = $paymentData = array(); 
        if(!empty($paypalInfo['item_number']) && !empty($paypalInfo['tx']) && !empty($paypalInfo['amt']) && !empty($paypalInfo['cc']) && !empty($paypalInfo['st'])){ 
            $item_name = $paypalInfo['item_name']; 
            $item_number = $paypalInfo['item_number']; 
            $txn_id = $paypalInfo["tx"]; 
            $payment_amt = $paypalInfo["amt"]; 
            $currency_code = $paypalInfo["cc"]; 
            $status = $paypalInfo["st"]; 
			// because of ipn not working
			$data =array();
			// Insert the transaction data in the database 
			$custom_values = json_decode($paypalInfo["custom"]);
			// echo $custom_values->user_id ;exit;
			// echo "<pre>";print_r($custom_values);exit;
			$data['user_id']    = $custom_values->user_id; 
			$data['session_id']    = $custom_values->session_id; 
			if($custom_values->type == 'Event'){
				$data['event_id']    = $paypalInfo["item_number"];
			}
			if($custom_values->type == 'Class'){
				$data['class_id']    = $paypalInfo["item_number"];
			}
			$data['class_id']    = $paypalInfo["item_number"];  
			$data['txn_id']    = $paypalInfo["txn_id"]; 
			$data['payment_gross']    = $paypalInfo["mc_gross"]; 
			$data['currency_code']    = $paypalInfo["mc_currency"]; 
			$data['payer_name']    = trim($paypalInfo["first_name"].' '.$paypalInfo["last_name"], ' '); 
			$data['payer_email']    = $paypalInfo["payer_email"]; 
			$data['status'] = $paypalInfo["payment_status"]; 
			$this->checkoutmodel->insertTransaction($data);
			$update_data = array();
			$update_data['updated_on'] = date("Y-m-d H:i:s");
			$update_data['payment_status'] = $paypalInfo["payment_status"];
			
			$condition = array('user_id' => $custom_values->user_id, 'type' => $custom_values->type, 'session_id' => $custom_values->session_id);

			$result = $this->checkoutmodel->updateRecord('tbl_registration', $update_data,$condition);


			// because of ipn not working end

            // Get product info from the database 
			if($custom_values->type == 'Event'){
            $productData = $this->checkoutmodel->getRows($item_number); 
			}
			if($custom_values->type == 'Class'){
			$productData = $this->checkoutmodel->getRowsClass($item_number); 
			}
            // Check if transaction data exists with the same TXN ID 
            $paymentData = $this->checkoutmodel->getPayment(array('txn_id' => $txn_id)); 
        } 
         
        // Pass the transaction data to view 
        $data['product'] = $productData; 
        $data['payment'] = $paymentData; 
        $this->load->view('success', $data); 
    } 
      
     function cancel(){ 
        // Load payment failed view 
        $this->load->view('cancel'); 
     } 
      
	 // ipn not working
	 function ipn(){ 
        // Retrieve transaction data from PayPal IPN POST 
        $paypalInfo = $this->input->post(); 
		$data =array();
		$data['test'] = $paypalInfo;
		// $this->checkoutmodel->insertTransaction($data); 
        //  print_r($paypalInfo)."ipn";exit;
        if(!empty($paypalInfo)){ 
            // Validate and get the ipn response 
            $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo); 
            // Check whether the transaction is valid 
            if($ipnCheck){ 
                // Check whether the transaction data is exists 
                $prevPayment = $this->checkoutmodel->getPayment(array('txn_id' => $paypalInfo["txn_id"])); 
                if(!$prevPayment){ 
					$data =array();
                    // Insert the transaction data in the database 
                    $data['session_id']    = $paypalInfo["custom"]; 
                    $data['event_id']    = $paypalInfo["item_number"]; 
                    $data['txn_id']    = $paypalInfo["txn_id"]; 
                    $data['payment_gross']    = $paypalInfo["mc_gross"]; 
                    $data['currency_code']    = $paypalInfo["mc_currency"]; 
                    $data['payer_name']    = trim($paypalInfo["first_name"].' '.$paypalInfo["last_name"], ' '); 
                    $data['payer_email']    = $paypalInfo["payer_email"]; 
                    $data['status'] = $paypalInfo["payment_status"]; 
     
                    $this->checkoutmodel->insertTransaction($data); 
                } 
            } 
        } 
    }
}

?>
