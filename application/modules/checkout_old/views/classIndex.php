<section class="sec-checkout sec-bg">
  <div class="container">
    <div class="row">
      <div class="col-12">
      <form class="form-horizontal" action="<?php echo base_url();?>checkout/buy" id="formRegister" method="POST" enctype="multipart/form-data">
        <div class="checkout-wrapper flex-wrapper flex-align-start flex-justify-sb">
          <div class="cw-left">
            <div class="cwl-content">
              <a href="javascript:history.go(-1)" class="checkout-back"><img src="assets/images/back-arrow-orange.svg" alt=""> <span>Event / Class name</span></a>
            </div>
            <div class="contact-info-wrapper">
              <div class="ci-title flex-wrapper flex-align-center flex-justify-sb mb32">
                <h3 class="title-3 bold txt-blue">Contact Information</h3>
                <a href="#/" class="txtm add-reg" onclick="addSection();">Add <img src="assets/images/read-more-icon.svg" alt=""></a>
              </div>
              <?php $user = (!empty($login_user)) ? explode(' ',$login_user[0]['full_name']) : '';?>
              <div class="checkout-registration-list" id="section_main_div">
                <?php for ($i=1; $i <= $reg_count; $i++) { ?>
                  <div class="reg-single" id="reg-single_<?= $i?>">
                  <div class="rs-title mb16">
                    <a href="#/" class="remove-reg" onclick="removeDiv(<?= $i?>);">
                      <img src="assets/images/remove-icon-orange.svg" alt="Delete" title="Delete">
                    </a>
                    <span class="txtm bold">Registration <?= $i ?></span>
                    <img src="assets/images/arrow-down-grey.svg" alt="" class="arrow-reg">
                  </div>
                  <div class="reg-form flex-wrapper flex-align-center flex-justify-sb">
                    <div class="form-group">
                      <input type="text" class="input-fls" name="first_name[]" id="first_name" value="<?= (!empty($login_user ) && $i == 1) ? $user[0] : '' ?>" required>
                      <label for="">First Name<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="last_name[]" id="last_name" value="<?= (!empty($login_user ) && $i == 1) ? $user[1] : '' ?>"  required>
                      <label for="">Last Name<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="email_id[]" id="email_id" value="<?= (!empty($login_user ) && $i == 1) ? $login_user[0]['email_id'] : '' ?>" required> 
                      <label for="">Email address<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="phone_no[]" id="phone_no" oninput="this.value=this.value.replace(/[^0-9]/g,'')" maxlength="10" minlength="10" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['phone_no'] : '' ?>" required>
                      <label for="">Phone number<sup>*</sup></label>
                    </div>
                    <div class="form-group f100">
                      <textarea class="input-fls" row="10" name="address[]" id="address" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['address'] : '' ?>"></textarea>
                      <label for="first_name">Address</label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="country[]" id="country" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['country'] : '' ?>" required>
                      <label for="">Country<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="state[]" id="state" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['state'] : '' ?>" required>
                      <label for="">State<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="city[]" id="city" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['city'] : '' ?>" required>
                      <label for="">City<sup>*</sup></label>
                    </div>
                    <div class="form-group">
                      <input type="text" class="input-fls" name="zip_code[]" id="zip_code" value="<?= (!empty($register_user ) && $i == 1) ? $register_user[0]['zip_code'] : '' ?>" required>
                      <label for="">Zip Code<sup>*</sup></label>
                    </div>
                    <?php if(empty($login_user) && $i == 1){?>
                    <div class="form-group f100">
                      <input type="password" class="input-fls" name="password[]" id="password" required>
                      <label for="">Password<sup>*</sup></label>
                    </div>
                    <?php }?>
                    <p class="txts semibold">If you are corporate, please fill your company name</p>
                    <div class="form-group f100">
                      <textarea class="input-fls" row="10" placeholder="Enter company name" name="company_name[]" id="company_name"><?= (!empty($register_user ) && $i == 1) ? $register_user[0]['company_name'] : '' ?></textarea>
                    </div>
                  </div>
                </div>
                <?php }?>
              
              </div>
            </div>
          </div>
          <div class="cw-right">
            <div class="order-summary-wrapper">
              <h3 class="title-3 bold txt-blue mb24">Order Summary</h3>
              <div class="order-items-wrapper">
                <?php for ($k=1; $k <= $reg_count; $k++) { ?>
                  <div id="summary_main_div">
                  <div class="order-items-single flex-wrapper flex-align-start flex-justify-sb mb16" id="summary_row_<?= $k?>">
                    <div class="oi-left">
                      <div class="txtl txt-black"><?= (!empty($class_detail[0]['headline'])) ? $class_detail[0]['headline'] : '' ?></div>
                    </div>
                    <div class="oi-right">
                      <div class="txtl bold txt-black">$<?= (!empty($class_detail[0]['class_fees'])) ? $class_detail[0]['class_fees'] : '' ?></div>
                    </div>
                  </div>
                </div>
               <?php 
              }?>
                
               
                <div class="order-items-single order-items-total  flex-wrapper flex-align-start flex-justify-sb mb16">
                  <div class="oi-left">
                    <div class="txtl bold txt-black">Total</div>
                  </div>
                  <div class="oi-right">
                    <div class="txtl bold txt-black">$<span id="totalPrice"></span></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="order-summary-cta mt32">
              <!-- <a href="#/" class="btn-fls btn-large btn-fls-primary">Proceed with PayPal</a> -->
              <button type="submit" class="btn-fls btn-large btn-fls-primary">Proceed with PayPal</button>
            </div>
            <input type="hidden" name="totaluser" id="totaluser">
            <input type="hidden" name="type" id="type" value="<?= (!empty($type)) ? $type : '' ?>">
            <input type="hidden" name="hidden_schedule_id" id="hidden_schedule_id" value="<?= (!empty($schedule_id)) ? $schedule_id : '' ?>">
            <input type="hidden" name="hidden_class_id" id="hidden_class_id" value="<?= (!empty($class_detail[0]['classes_id'])) ? $class_detail[0]['classes_id'] : '' ?>">
            
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</section>
</body>

</html>
<script>

  $(document).ready(function() {
      total_price()
  });

function addSection() {

var section_cnt = $('#section_main_div').children('.reg-single').length;
var i = section_cnt;
var section_cnt_view = ++i;
if (section_cnt_view) {
  // console.log(section_cnt);
  var section_html = '<div class="reg-single" id="reg-single_' + section_cnt_view + '">' +
                  '<div class="rs-title mb16">'+
                    '<a href="#/" class="remove-reg" onclick="removeDiv(' + section_cnt_view + ');">'+
                      '<img src="assets/images/remove-icon-orange.svg" alt="Delete" title="Delete">'+
                    '</a>'+
                    '<span class="txtm bold">Registration ' + section_cnt_view + '</span>'+
                    '<img src="assets/images/arrow-down-grey.svg" alt="" class="arrow-reg" onclick="arrow(' + section_cnt_view + ');" id="reg_form_arrow' + section_cnt_view + '">'+
                  '</div>'+
                  '<div class="reg-form flex-wrapper flex-align-center flex-justify-sb" id="reg-form_' + section_cnt_view + '">'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="first_name[]" id="first_name" required>'+
                      '<label for="">First Name<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="last_name[]" id="last_name" required>'+
                      '<label for="">Last Name<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="email_id[]" id="email_id" required>'+
                      '<label for="">Email address<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="phone_no[]" id="phone_no" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="10" minlength="10" required>'+
                      '<label for="">Phone number<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group f100">'+
                      '<textarea class="input-fls" row="10" name="address[]" id="address"></textarea>'+
                      '<label for="first_name">Address</label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="country[]" id="country" required>'+
                      '<label for="">Country<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="state[]" id="state" required>'+
                      '<label for="">State<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="city[]" id="city" required>'+
                      '<label for="">City<sup>*</sup></label>'+
                    '</div>'+
                    '<div class="form-group">'+
                      '<input type="text" class="input-fls" name="zip_code[]" id="zip_code" required>'+
                      '<label for="">Zip Code<sup>*</sup></label>'+
                    '</div>'+
                    '<p class="txts semibold">If you are corporate, please fill your company name</p>'+
                    '<div class="form-group f100">'+
                      '<textarea class="input-fls" row="10" placeholder="Enter company name" name="company_name[]" id="company_name"></textarea>'+
                    '</div>'+
                  '</div>'+
    
    '</div>';
  $("#section_main_div").append(section_html);
  

  var section_summery = 
                        '<div class="order-items-single flex-wrapper flex-align-start flex-justify-sb mb16"       id="summary_row_'+section_cnt_view+'">'+
                          '<div class="oi-left">'+
                            '<div class="txtl txt-black"><?= (!empty($class_detail[0]['headline'])) ? trim($class_detail[0]['headline']) : "" ?></div>'+
                          '</div>'+
                          '<div class="oi-right">'+
                            '<div class="txtl bold txt-black">$<?= (!empty($class_detail[0]['class_fees'])) ? $class_detail[0]['class_fees'] : '' ?></div>'+
                          '</div>'+
                      '</div>';
$("#summary_main_div").append(section_summery);  
total_price()
} else {
  alert("OOPS!! something went wrong");
}
}

function removeDiv(section_cnt) {
    // console.log(section_cnt);
    var cnt = $('#section_main_div').children('.reg-single').length;
    console.log(section_cnt);
    if(cnt === 1){
      alert("Atleast one div require");
      return false;
    }else{
      $("#reg-single_" + section_cnt).remove();
      $("#summary_row_" + section_cnt).remove();
      total_price()
    }
    
  }
function arrow(section_cnt){
  $("#reg-form_" + section_cnt).toggle();
    $("#reg_form_arrow" + section_cnt).toggleClass('arrow-reg arrow-reg rotate-icon')
}

function total_price(){
  var total_price = 0;
  var new_cnt = $('#section_main_div').children('.reg-single').length;
  total_price = new_cnt * <?= (!empty($class_detail[0]['class_fees'])) ? $class_detail[0]['class_fees'] : '' ?>;
  $("#totalPrice").html(total_price);

  $("#totaluser").val(new_cnt);

}
</script>