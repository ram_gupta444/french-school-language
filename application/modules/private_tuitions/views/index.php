<section class="sec-internal-intro">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="sec-top-wrapper">
            <ol class="breadcrumb breadcrumb-chevron">
              <li class="breadcrumb-item"><a href="<?php echo base_url();?>home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url().$_SESSION['language_slug'].'/'.'language_home'?>"><?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?></a></li>
              <li class="breadcrumb-item active"><?= (!empty($private_tuitions[0]['heading'])?$private_tuitions[0]['heading']:"") ?></li>
            </ol>
            <div class="sec-top-title mb16" data-aos="fade-up">
              <h1 class="title-1 bold txt-orange"><?= (!empty($private_tuitions[0]['heading'])) ? $private_tuitions[0]['heading'] : '' ?></h1>
              <img src="<?php echo base_url();?>assets/images/private-tuitions-border.svg" alt="" class="stt-border img-fluid">
            </div>
            <div class="txtm txt-black" data-aos="fade-up" data-aos-delay="200">
            <?= (!empty($private_tuitions[0]['description'])) ? $private_tuitions[0]['description'] : '' ?>
              <div class="pt-banner-cta mt32">
                <a href="javascript:history.go(-1)" class="btn-fls btn-large btn-fls-tertiary"><?= (!empty($private_tuitions[0]['button_text'])) ? $private_tuitions[0]['button_text'] : '' ?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single pvt-tuitions-sec-1">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="pvt-tuition-wrapper">
            <div class="pt-list-single flex-wrapper flex-align-center flex-justify-sb">
              <div class="ptl-left" data-aos="fade-right" data-aos-delay="200">
                <img src="<?= (!empty($private_tuitions[0]['section1_image'])?base_url('images/private_tuition/').$private_tuitions[0]['section1_image']:"") ?>" alt="" class="pt-list-img">
                <span>01</span>
              </div>
              <div class="ptl-right" data-aos="fade-left" data-aos-delay="200">
                <h2 class="title-2 bold txt-blue mb16">
                <?= (!empty($private_tuitions[0]['section1_heading'])) ? $private_tuitions[0]['section1_heading'] : '' ?>
                  <img src="<?php echo base_url();?>assets/images/private-tuition-title.svg" alt="" class="ptl-title-img">
                </h2>
                <h4 class="title-4 bold txt-black mb24"><?= (!empty($private_tuitions[0]['section1_sub_heading'])) ? $private_tuitions[0]['section1_sub_heading'] : '' ?></h4>
                <div class="txtm">
                  <div class="ptl-content">
                  <?= (!empty($private_tuitions[0]['section1_description'])) ? $private_tuitions[0]['section1_description'] : '' ?>
                  </div>
                  <a href="#/" class="link-bold ptl-read-more-link">Read More <!-- <img src="<?php echo base_url();?>assets/images/read-more-icon.svg" alt=""> --></a>
                </div>
              </div>
            </div>
            <div class="pt-list-single flex-wrapper flex-align-center flex-justify-sb">
              <div class="ptl-left" data-aos="fade-left" data-aos-delay="200">
                <img src="<?= (!empty($private_tuitions[0]['section2_image'])?base_url('images/private_tuition/').$private_tuitions[0]['section2_image']:"") ?>" alt="" class="pt-list-img">
                <span>02</span>
              </div>
              <div class="ptl-right" data-aos="fade-right" data-aos-delay="200">
                <h2 class="title-2 bold txt-blue mb16">
                <?= (!empty($private_tuitions[0]['section2_heading'])) ? $private_tuitions[0]['section2_heading'] : '' ?>
                  <img src="<?php echo base_url();?>assets/images/private-tuition-title-2.svg" alt="" class="ptl-title-img">
                </h2>
                <h4 class="title-4 bold txt-black mb24"><?= (!empty($private_tuitions[0]['section2_sub_heading'])) ? $private_tuitions[0]['section2_sub_heading'] : '' ?></h4>
                <div class="txtm">
                  <div class="ptl-content">
                  <?= (!empty($private_tuitions[0]['section2_description'])) ? $private_tuitions[0]['section2_description'] : '' ?>
                  </div>
                  <a href="#/" class="link-bold ptl-read-more-link">Read More <!-- <img src="<?php echo base_url();?>assets/images/read-more-icon.svg" alt=""> --></a> 
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
</body>

</html>