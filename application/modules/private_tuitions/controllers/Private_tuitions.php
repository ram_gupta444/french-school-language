<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Private_tuitions extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('private_tuitionsmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		$_SESSION['last_stage'] = 'private_tuitions';
		$language_id = $_SESSION['language_id'];
		$result = array();
		$condition = "language = '".$_SESSION['language_id']."' ";
		$result['private_tuitions'] = $this->common->getData("tbl_private_tuition",'*',$condition);
		// echo "<pre>";print_r($result['private_tuitions']);exit;
		$condition_cus = "language_master_id= $language_id ";
		$result['language_home'] = $this->common->getData("tbl_language_master",'*',$condition_cus);
		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
}

?>
