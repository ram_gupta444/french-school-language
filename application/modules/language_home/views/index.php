<section class="sec-homepage-banner">
  <div class="container">
    <div class="row">
      <div class="col-12 plm0 prm0">
        <img src="<?PHP echo base_url();?>assets/images/language-banner-bg.svg" alt="" class="language-banner-bg">
        <div class="language-page-banner-wrapper mxw-1060">
          <ol class="breadcrumb breadcrumb-chevron">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>home">Home</a></li>
            <li class="breadcrumb-item active"><?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?></li>
          </ol>
          <div class="language-banner-content flex-wrapper flex-align-start flex-justify-sb">
            <div class="lbc-left">
              <div class="language-banner-title mb32">
                <h2 class="title-2 bold txt-orange"><?= (!empty($language_home[0]['heading'])?$language_home[0]['heading']:"") ?></h2>
                <img src="<?PHP echo base_url();?>assets/images/language-banner-title-border.svg" alt="" class="lb-border img-fluid">
                
              </div>
              <?= (!empty($language_home[0]['description'])?$language_home[0]['description']:"") ?>
              <a href="<?= (!empty($language_home[0]['button_link'])?$language_home[0]['button_link']:"") ?>" class="btn-fls btn-large btn-fls-primary"><?= (!empty($language_home[0]['button_text'])?$language_home[0]['button_text']:"") ?></a>
            </div>
            <div class="lbc-right">
              <img src="<?= (!empty($language_home[0]['image'])?base_url('images/language_home/').$language_home[0]['image']:"") ?>" alt="" class="lb-img img-fluid">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single sec-home-whyus">
  <div class="sec-home-whyus-bg"></div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060 home-whyus-container">
          <div class="home-whyus-wrapper-top flex-wrapper flex-align-top flex-justify-sb mb24">
            <div class="hwwt-left" data-aos="fade-right">
              <h2 class="title-2 bold txt-orange">
                <!-- Why Learn French<br /> With Us? -->
                <?= (!empty($language_home[0]['section2_heading'])?$language_home[0]['section2_heading']:"") ?>
                <img src="<?PHP echo base_url();?>assets/images/learn-french-title.svg" alt="" class="hwwt-title-img hwwt-title-french">
              </h2>
            </div>
            <div class="hwwt-right txtm" data-aos="fade-left">
              <p> <?= (!empty($language_home[0]['section2_sub_heading'])?$language_home[0]['section2_sub_heading']:"") ?></p>
            </div>
          </div>
          <h4 class="title-4 bold txt-blue title-reasons mb24" data-aos="fade-up"><?= (!empty($language_home[0]['section2_description'])?$language_home[0]['section2_description']:"") ?></h4>
          <div class="whyus-steps-wrapper flex-wrapper flex-align-top flex-justify">
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="200">
              <h4>01</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($language_home[0]['section2_heading1'])?$language_home[0]['section2_heading1']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($language_home[0]['section2_description1'])?$language_home[0]['section2_description1']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="400">
              <h4>02</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($language_home[0]['section2_heading2'])?$language_home[0]['section2_heading2']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($language_home[0]['section2_description2'])?$language_home[0]['section2_description2']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="600">
              <h4>03</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($language_home[0]['section2_heading3'])?$language_home[0]['section2_heading3']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($language_home[0]['section2_description3'])?$language_home[0]['section2_description3']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="800">
              <h4>04</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($language_home[0]['section2_heading4'])?$language_home[0]['section2_heading4']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($language_home[0]['section2_description4'])?$language_home[0]['section2_description4']:"") ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="sec-single sec-home-programs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-2 bold txt-orange mb24 text-center" data-aos="fade-up">Our Programs <img src="<?PHP echo base_url();?>assets/images/home-programs-arrow.png" alt="" class="hp-arrow"></h2>
        <div class="home-programs-wrapper flex-wrapper flex-align-top flex-justify-sb">
        <?php if(!empty($our_programs)){
            foreach ($our_programs as $key => $value) { ?>
          <div class="hp-single lp-single flex-wrapper" data-aos="fade-up" data-aos-delay="200">
            <img src="<?= (!empty($value['image'])?base_url('images/our_program/').$value['image']:"") ?>" alt="" class="hp-single-img">
            <div class="lp-right">
              <h3 class="title-3 bold txt-blue mb8"><?= (!empty($value['heading'])?$value['heading']:"") ?></h3>
              <div class="txtm gl-short-desc">
                <?= (!empty($value['description'])?$value['description']:"") ?>               
              </div>
              <div class="hp-cta flex-wrapper flex-align-center flex-justify-sb mt16">
                <a href="<?= base_url(!empty($value['button_link1']) ? $_SESSION['language_slug'].'/'.$value['button_link1']:"")?>" class="btn-fls btn-medium btn-fls-primary w100">Discover</a>
              </div>
            </div>
          </div>
          <?php }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial sec-testimonial-language-home">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="<?PHP echo base_url();?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24">
            <?= (!empty($language_home[0]['section3_heading'])?$language_home[0]['section3_heading']:"") ?>
            </h3>
            <h4 class="title-4 bold txt-white tq-name">
            <?= (!empty($language_home[0]['section3_sub_heading'])?$language_home[0]['section3_sub_heading']:"") ?>
            </h4>
          </div>
        </div>
        <h2 class="testimonial-title" data-aos="fade-up" data-aos-delay="100">Testimonials</h2>
      </div>
    </div>
  </div>
</section>
<section class="sec-home-testimonial lang-home-testimonial">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="home-testimonial-wrapper">
          <div class="ht-left">
            <div class="ht-carousel-wrapper" data-aos="fade-up">
              <div class="testimonial-slider owl-carousel">
                <?php if(!empty($testimonial_details)){
                  foreach ($testimonial_details as $key => $value) { ?>
                    <div class="testimonial-single">
                    <div class="testimonial-text txtm">
                      <p><?= $value['testimonial_description']; ?></p>
                    </div>
                    <div class="testimonial-name">
                      <h4 class="title-4 bold"><?= $value['testimonialname']; ?></h4>
                    </div>
                  </div>
                 <?php }
                  }?>
              </div>
            </div>
          </div>
          <div class="ht-right">
            <div class="video-testimonial-wrapper owl-carousel flex-wrapper flex-align-start flex-justify-sb">
            <?php if(!empty($testimonial_videos)){
              foreach ($testimonial_videos as $key => $value) { ?>
                <div class="vt-single" data-aos="fade-down-right">
                  <iframe src="<?= $value['url']?>" width="100%" height="187" frameborder="0" allowfullscreen></iframe>
                  <div class="vt-info-wrapper flex-wrapper flex-align-center flex-justify-sb mt8">
                    <h4 class="title-4 bold txt-black"><?= (!empty($value['title'])? $value['title']:"") ?></h4> 
                   <!--  <img src="<?php echo base_url("assets/images/play-icon-outline-blue.svg")?>" alt="" onclick="playVido('<?= $value['url']?>')" title="Play Video" data-toggle="tooltip"> -->
                  </div>
                </div>
             <?php }
            }?>

            </div>

            <div class="counter-wrapper flex-wrapper flex-align-center flex-justify-center">
              <?php if(!empty($numbers)){
                $delay = 100;
                foreach ($numbers as $key => $value) { ?>
                  <div class="cw-single txt-white" data-aos="fade-up" data-aos-delay="<?= $delay?>">
                  <h2><?= (!empty($value['numbers'])?$value['numbers']:"") ?>+</h2>
                  <h4 class="title-4 bold"><?= (!empty($value['title'])?$value['title']:"") ?></h4>
                </div>
              <?php $delay +=100; }
              }
                ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Modal -->
<div class="modal fade" id="bsModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
          </div>
            <iframe  scrolling="no" class="embed-responsive-item" id="modulevideodiv" width="100%"  height="450px" frameborder="2" allowfullscreen ></iframe>
            <div class="clear-fix"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</body>

</html>

<script>

$(document).ready(function(){
  $(document).on("click",".modal-backdrop, #bsModal3 .close, #bsModal3 .btn", function() {
          $("#bsModal3 iframe").attr("src", $("#bsModal3 iframe").attr("src"));
  });
  $("#bsModal3").on('hidden.bs.modal', function (e) {
    console.log($("#bsModal3 iframe").attr("src"));
      $("#bsModal3 iframe").attr("src", $("#bsModal3 iframe").attr("src"));
  });
})

function playVido(video_link){
	if(video_link != "" && video_link != "undefined" && video_link != "null"){
    $("#modulevideodiv").prop("src",video_link);
    $("#bsModal3").modal('show');
	}
}
</script>