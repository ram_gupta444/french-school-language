<?PHP
class Language_homemodel extends CI_Model
{
	
	function getdata($table, $select = "*", $condition = '',$obj = false) {
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){
			$this->db->where("($condition)");
		}
		$query = $this->db->get();
		if($query->num_rows() >= 1){
			if($obj){
				return $query->result();
			}else{
				return $query->result_array();
			}
		}else{
			return false;
		}
	}

	function getdata_custom($table, $select = "*", $condition = '',$language_id) {
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){
			// $this->db->where_in('languages',$language_id);
			$this->db->where("languages LIKE '%$language_id%' ");
			$this->db->where($condition);
		}
		$query = $this->db->get();
		// echo $this->db->last_query();exit;
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
	  $this->db->insert($tbl_name,$data_array);
	  //print_r($this->db->last_query());
		//exit;
	  $result_id = $this->db->insert_id();
	  if($sendid == 1)
	  {
		return $result_id;
	  }
	}

}
?>
