<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Language_home extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('language_homemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$result = array();
		$language_id = "";
		// if(!empty($_GET['text']) && isset($_GET['text'])){
		// 	$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
		// 	parse_str($varr,$url_prams);
		// 	$language_id = $url_prams['id'];
		// 	$_SESSION['language_id'] = $language_id;
		// }
		// echo $this->uri->segment(1);exit;
		$condition_lag = "slug = '".$this->uri->segment(1)."' ";
		$language = $this->common->getData("tbl_language_master",'language_master_id',$condition_lag);
		// echo "<pre>"; print_r($language[0]['language_master_id']);exit;
		$language_id = $language[0]['language_master_id'];
		$_SESSION['language_id'] = $language_id;
		$_SESSION['language_slug'] = $this->uri->segment(1);
		// get home content data 
		$condition = "1=1";
		$condition_cus = "language_master_id= $language_id";
		$result['language_home'] = $this->common->getData("tbl_language_master",'*',$condition_cus);

		$result['home'] = $this->common->getData("tbl_home",'*',$condition);
		$result['gallery_images'] = $this->common->getData("tbl_gallery_image",'*',$condition);
		$result['numbers'] = $this->common->getData("tbl_counts",'*',$condition);
		// $result['our_programs'] = $this->common->getData("tbl_our_program",'our_program_id,image,heading,description,button_text1,button_link1,button_text2,button_link2',$condition);
		if(!empty($_SESSION['language_id'])){
			$language_id =  $_SESSION['language_id'];
			$langcondition = "languages LIKE '%$language_id%' ";
		}else{
			$langcondition = "1 = 1";
		}
		$result['testimonial_videos'] = $this->common->getData("tbl_testimonial_video",'*',$langcondition);
		
		$condition = "status = 'Active' ";
		// $result['testimonial_details'] = $this->common->getData("tbl_testimonial",'*',$condition);

		$result['testimonial_details'] = $this->language_homemodel->getdata_custom("tbl_testimonial",'*',$condition,$language_id);

		$result['our_programs'] = $this->language_homemodel->getdata_custom("tbl_our_program",'*',$condition,$language_id);

		// echo $this->db->last_query();exit;
		// echo "<pre>";
		// print_r($result['testimonial_details']);
		// exit;
		

		$result['meta_description'] = $result['home'][0]['meta_description'];
		$result['meta_keywords'] = $result['home'][0]['meta_title'];

		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer_without_img.php');
	}

}

?>
