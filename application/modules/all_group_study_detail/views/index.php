<section class="sec-gs-detail-top">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="event-detail-breabcrumb">
          <ol class="breadcrumb breadcrumb-chevron">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= base_url('language_home/index?text='.rtrim(strtr(base64_encode("id=".$language_home[0]['language_master_id']), '+/', '-_'), '=').'')?>"><?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?></a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>all_group_study">Group Learning</a></li>
            <li class="breadcrumb-item active">Class Details</li>
          </ol>
        </div>
        <div class="gs-detail-wrapper flex-wrapper flex-align-top flex-justify-sb">
          <div class="gsd-left">
            <!-- <a href="<?= base_url()?>adult_group_study" class="gs-back" ><img src="<?= base_url()?>assets/images/back-icon-blue.svg" alt=""> <span>Adult Group Study</span></a> -->
            <h4 class="title-4 bold gs-detail-coursename mb8" data-aos="fade-up"><?= (!empty($adult_group_study_detail[0]['title'])? $adult_group_study_detail[0]['title']:"") ?></h4>
            <h3 class="title-3 bold txt-orange mb32" data-aos="fade-up"><?= (!empty($adult_group_study_detail[0]['headline'])? $adult_group_study_detail[0]['headline']:"") ?></h3>
            <div class="txtm gs-detail-intro" data-aos="fade-up">
            <?= (!empty($adult_group_study_detail[0]['intro_text'])? $adult_group_study_detail[0]['intro_text']:"") ?>
            </div>
            <div class="gsd-learnto gsd-single" data-aos="fade-up" data-aos-delay="300">
              <h4 class="title-4 bold txt-blue gs-detail-subtitle mb16"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> <span><?= (!empty($adult_group_study_detail[0]['section2_heading'])? $adult_group_study_detail[0]['section2_heading']:"") ?></span></h4>
              <div class="txtl">
              <?= (!empty($adult_group_study_detail[0]['section2_paragraph'])? $adult_group_study_detail[0]['section2_paragraph']:"") ?>
              </div>
            </div>
            <div class="gsd-single" data-aos="fade-up" data-aos-delay="300">
              <h4 class="title-4 bold txt-blue gs-detail-subtitle mb16"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> <span><?= (!empty($adult_group_study_detail[0]['section3_heading'])? $adult_group_study_detail[0]['section3_heading']:"") ?></span></h4>
              <div class="txtl">
              <?= (!empty($adult_group_study_detail[0]['section3_paragraph'])? $adult_group_study_detail[0]['section3_paragraph']:"") ?>
              </div>
            </div>
            <div class="gsd-schedule gsd-single" data-aos="fade-up">
              <h4 class="title-4 bold txt-blue gs-detail-subtitle mb24"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> <span>Schedule</span></h4>
              <div class="gsd-schedule-wrapper flex-wrapper flex-align-start flex-justify-start">
                <?php 
                  if(!empty($schedules)){
                    $delay = 200;
                    foreach ($schedules as $key => $value) {
                      $days = $value['repeat_on'];
                      $explodedays = explode(',',$days);
                      $repeat_on = implode(', ',$explodedays);
                      ?>
                     <div class="gsd-schedule-single" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
                        <p class="txtm bold txt-orange mb16">Option <?= $key+1?></p>
                        <h4 class="title-4 medium txt-black mb8"><?= (!empty($value['repeat_on'])? $repeat_on:"") ?></h4>
                        <p class="txtm gsd-timing mb0">Begins: <?= (!empty($value['starts_date'])? date(" jS F Y", strtotime($value['starts_date'])):"") ?></p>
                        <p class="txtm gsd-timing mb0">Ends: <?= (!empty($value['ends_date'])? date(" jS F Y", strtotime($value['ends_date'])):"") ?></p>
                        <p class="txtm gsd-timing"><?= (!empty($value['start_time'])? $value['start_time']:"") ?>-<?= (!empty($value['end_time'])? $value['end_time']:"") ?></p>
                        <form action="<?= base_url('checkout') ?>" method ="POST">
                          <div class="gsd-schedule-cta flex-wrapper flex-align-center flex-justify-sb mt24">
                            <!-- <a href="#/" class="btn-fls btn-medium btn-fls-secondary">Enroll Now</a> -->
                            <input type="hidden" name="hidden_schedule_id" id="hidden_schedule_id" value="<?= $value['schedule_id'] ?>">
                            <input type="hidden" name="hidden_class_id" id="hidden_class_id" value="<?= $value['class_drop_down'] ?>">
                            <input type="hidden" name="type" id="type" value="Class">
                            <button type="submit" class="btn-fls btn-large btn-fls-tertiary">Enroll Now</button>
                            <select name="reg_count" id="reg_count" class="gsd-count">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                            </select>
                          </div>
                        </form>
                      </div>
                   <?php 
                   $delay += 200;
                   }
                  }
                ?>
               
              </div>
            </div>
            <div class="gsd-single" data-aos="fade-up">
              <h4 class="title-4 bold txt-blue gs-detail-subtitle mb16"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> <span><?= (!empty($adult_group_study_detail[0]['section4_heading'])? $adult_group_study_detail[0]['section4_heading']:"") ?></span></h4>
              <div class="txtl">
              <?= (!empty($adult_group_study_detail[0]['section4_paragraph'])? $adult_group_study_detail[0]['section4_paragraph']:"") ?>
              </div>
            </div>
          </div>
          <!-- <div class="gsd-right" data-aos="fade-up" data-aos-delay="500"> -->
          <div class="gsd-right">
            <div class="gsd-cost-wrapper mb16">
              <h2 class="gsd-title-cost title-2 bold flex-wrapper flex-align-center mb24">SGD<?= (!empty($adult_group_study_detail[0]['class_fees'])? $adult_group_study_detail[0]['class_fees']:"") ?><span></h2>
              <h4 class="title-4 bold txt-white gs-detail-subtitle mb24"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> Level: <span>&nbsp; <?= (!empty($adult_group_study_detail[0]['level_name'])? $adult_group_study_detail[0]['level_name']:"") ?></span></h4>
              <h4 class="title-4 bold txt-white gs-detail-subtitle mb24"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> Hours: <span>&nbsp;  <?= (!empty($adult_group_study_detail[0]['total_hours'])? $adult_group_study_detail[0]['total_hours'].' Hours':"") ?> </span></h4>
              <h4 class="title-4 bold txt-white gs-detail-subtitle mb24"><img src="<?= base_url()?>assets/images/gs-subtitle-icon.svg" alt=""> Duration: <span>&nbsp;  <?= (!empty($adult_group_study_detail[0]['duration_in_week'])? $adult_group_study_detail[0]['duration_in_week']:"") ?> Weeks</span></h4>
            </div>
            <img src="<?= (!empty($adult_group_study_detail[0]['banner_image'])?base_url('images/class/').$adult_group_study_detail[0]['banner_image']:"") ?>" alt="" class="gsd-cost-banner">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single sec-gs-detail-2">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-2 bold txt-orange other-courses-title mb40" data-aos="fade-up">Other Courses <img src="<?= base_url()?>assets/images/home-programs-arrow.png" alt="" class="other-courses-icon"></h2>
        <div class="group-study-list-wrapper flex-wrapper flex-align-start flex-justify-sb">

          <?php 
          
          if(!empty($other_courcess)){
            $otherdelay = 100;
            foreach ($other_courcess as $key => $value) {?>
            <div class="gsl-single flex-wrapper flex-align-start flex-justify-sb" data-aos="fade-up" data-aos-delay="<?= $otherdelay;?>">
                <img src="<?= (!empty($value['banner_image'])?base_url('images/class/').$value['banner_image']:"") ?>" alt="" class="gsl-img">
                <div class="gsl-single-right">
                    <div class="study-label-wrapper">
                        <div class="all-group-label"><?= (!empty($value['class_type'])?$value['class_type']:"") ?></div>
                        <div class="all-group-label"><?= (!empty($value['category'])?$value['category']:"") ?></div>
                    </div>
                    <div class="group-info-wrapper flex-wrapper flex-align-center flex-justify-start mb16">
                        <div class="lang-name txts semibold">
                            <img src="<?= (!empty($language_home[0]['language_logo'])?base_url('images/language_logo/').$language_home[0]['language_logo']:"") ?>" alt="">
                            <?= (!empty($language_home[0]['language_name'])?$language_home[0]['language_name']:"") ?>
                        </div>
                        <div class="category-name txts semibold">
                            Category Name
                        </div>
                    </div>
                    <h4 class="txtl bold txt-black"><?= (!empty($value['headline'])?$value['headline']:"") ?></h4>
                    <div class="course-details-wrapper flex-wrapper flex-align-center flex-justify-sb mt16">
                        <div class="cd-left">
                            <div class="txts bold">Course duration: <?= (!empty($value['duration_in_week'])?$value['duration_in_week']:"") ?> weeks<br />
                            Frequency: <?= (!empty($value['repeat_on']))? count(explode(',',$value['repeat_on'])):"" ?> times a week,<br />
                            <?= (!empty($value['duration_in_week'])?$value['duration_in_week']:"") ?> hour per class
                            </div>
                        </div>
                        <div class="cd-right">
                            <a href="<?= base_url('all_group_study_detail/index?text='.rtrim(strtr(base64_encode("id=".$value['classes_id']), '+/', '-_'), '=').'')?>" class="btn-fls btn-medium btn-fls-secondary">See options</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            $otherdelay += 100;
            }
          }?>
         
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial sec-testimonial-above-footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="<?= base_url()?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24"><?= (!empty($adult_group_study[0]['section2_heading'])?$adult_group_study[0]['section2_heading']:"") ?></h3>
            <h4 class="title-4 bold txt-white tq-name"><?= (!empty($adult_group_study[0]['section2_sub_heading'])?$adult_group_study[0]['section2_sub_heading']:"") ?></h4>
          </div>
        </div>        
      </div>
    </div>
  </div>
</section>
</body>

</html>