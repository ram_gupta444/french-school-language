<?PHP
class All_group_study_detailmodel extends CI_Model
{

	function upcommingSchedule($classes_id){
        $this -> db -> select('c.classes_id,s.*');
        $this -> db -> from('tbl_classes as c');
        $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this->db->where('s.ends_date >=', date('Y-m-d'));
        $this->db->where('s.class_drop_down',$classes_id);
		    // $this->db->group_by('c.classes_id');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function other_courcess($classes_id){
        $this -> db -> select('c.classes_id,c.no_of_hours,c.duration_in_week,c.title,c.banner_image,c.headline,c.class_type,c.category,s.starts_date,s.repeat_on,sc.similar_classes_id');
        $this -> db -> from('tbl_classes as c');
        $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this -> db -> join('tbl_similar_classes as sc', 'sc.class_id  = c.classes_id');
        $this->db->where('s.ends_date >=', date('Y-m-d'));
        $this->db->where('sc.classes_id ',$classes_id );
        $this->db->where('c.status ','Active');
		    $this->db->group_by('s.class_drop_down');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function getDataCustom($condition){
        $this -> db -> select('c.*,l.level_name');
        $this -> db -> from('tbl_classes as c');
        $this -> db -> join('tbl_level as l', 'c.level  = l.level_id');
        $this->db->where($condition );
		   
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }


}
?>
