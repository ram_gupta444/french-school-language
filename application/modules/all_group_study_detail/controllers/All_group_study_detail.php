<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class All_group_study_detail extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('all_group_study_detailmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		if(empty($_SESSION['language_id'])){
			redirect(base_url());
		}
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;

		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$language_id = $_SESSION['language_id'];
		$result = array();
		$adult_group_id = "";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$adult_group_id = $url_prams['id'];

		$condition = "classes_id = '".$adult_group_id."' ";
		// $result['adult_group_study_detail'] = $this->common->getData("tbl_classes",'*',$condition);
		$result['adult_group_study_detail'] = $this->all_group_study_detailmodel->getDataCustom($condition);
		}
		$result['adult_group_study'] = $this->common->getData("tbl_adult_group_study",'section2_heading,section2_sub_heading','1=1');
		// schedule section 
		$result['schedules'] = $this->all_group_study_detailmodel->upcommingSchedule($adult_group_id);
		// echo $adult_group_id;exit;
		// other courcess
		$result['other_courcess'] = $this->all_group_study_detailmodel->other_courcess($adult_group_id);
		// $result['upcomming_classes'] = $this->adult_group_studymodel->upcommingClasses();
		// echo "<pre>";print_r($result['other_courcess']);exit;
		$condition_cus = "language_master_id= $language_id ";
		$result['language_home'] = $this->common->getData("tbl_language_master",'*',$condition_cus);
		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}

}

?>
