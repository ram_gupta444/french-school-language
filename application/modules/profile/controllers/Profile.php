<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Profile extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('profilemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		checklogin();
	}

	function index(){

		$result = array();
		$_SESSION['last_stage'] = 'profile';
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$event_type = $this->uri->segment(2);
		// echo $event_type;exit;
		if($event_type =='bookedEvents'){
			$result['type'] = 'booked_events';
		}else if($event_type =='bookedClass'){
			$result['type'] = 'booked_class';
		}
		else{
			$result['type'] = 'profile';
		}
		// echo "<pre>";print_r($_SESSION['fls_user'][0]['user_id']);exit;
		// $condition = "user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
		// $result['user_detail'] = $this->common->getData("tbl_users",'full_name,email_id,phone',$condition);
		// echo "<pre>";print_r($result['user_detail']);exit;
		
		// $classesListingHtml = $this->load->view('my_account',$result,true);
		// if($classesListingHtml){
		// 	echo json_encode(array("success"=>true,"classesListingHtml"=>$classesListingHtml));
		// }else{
		// 	echo json_encode(array("success"=>false,"classesListingHtml"=>$classesListingHtml));
		// }

		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}
	function getProfile()
	{
		// print_r($_POST);exit;
		if(empty($_POST['section']) || $_POST['section'] == 'profile' ){
			$result = array();
			$condition = "user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
			$result['user_detail'] = $this->common->getData("tbl_users",'user_id,full_name,email_id,phone',$condition);
			$Htmlpage = $this->load->view('my_account',$result,true);
			if($Htmlpage){
				echo json_encode(array("success"=>true,"Htmlpage"=>$Htmlpage,"section"=>'profile'));
			}else{
				echo json_encode(array("success"=>false,"Htmlpage"=>$Htmlpage));
			}
		}
		if(!empty($_POST['section']) && $_POST['section'] == 'booked_class' ){
			$result = array();
			$condition = "user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
			$result['bookClass_detail'] = $this->profilemodel->getClassdata($_SESSION['fls_user'][0]['user_id']);
			// echo "<pre>";print_r($result['bookClass_detail']);exit;
			
			$Htmlpage = $this->load->view('my_booked_class',$result,true);
			if($Htmlpage){
				echo json_encode(array("success"=>true,"Htmlpage"=>$Htmlpage,"section"=>'booked_class'));
			}else{
				echo json_encode(array("success"=>false,"Htmlpage"=>$Htmlpage));
			}
		}
		if(!empty($_POST['section']) && $_POST['section'] == 'booked_events' ){
			$result = array();
			$condition = "user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
			$result['bookEvent_detail'] = $this->profilemodel->getEventdata($_SESSION['fls_user'][0]['user_id']);
			// echo $this->db->last_query();exit;
			$Htmlpage = $this->load->view('my_booked_events',$result,true);
			// echo "<pre>";print_r($Htmlpage);exit;
			if($Htmlpage){
				echo json_encode(array("success"=>true,"Htmlpage"=>$Htmlpage,"section"=>'booked_events'));
			}else{
				echo json_encode(array("success"=>false,"Htmlpage"=>$Htmlpage));
			}
		}
	}

	function submitForm()
	{
		$condition = "email_id = '".$_POST['email_id']."' ";
		if(isset($_POST['user_id']) && (!empty($_POST['user_id'])))
		{
			$condition .= " &&  user_id !='".$_POST['user_id']."' ";
		}
		$check_email = $this->profilemodel->getdata("tbl_users",'email_id',$condition);
		if(!empty($check_email)){
			echo json_encode(array("success"=>"0",'msg'=>'Email Already Present!'));
			exit;
		}
		$condition = "user_id = '".$_POST['user_id']."' ";
		$result = $this->common->getData('tbl_users','*',$condition);
		if($result){
			$data_array =array();
			if(!empty($_POST['password'])){
				$data_array['password'] = MD5($_POST['password']);
			}
			$data_array['full_name'] = $_POST['full_name'];
			$data_array['email_id'] = $_POST['email_id'];
			$data_array['phone'] = $_POST['phone_no'];
			$result_update = $this->profilemodel->updateRecord('tbl_users', $data_array,'user_id',$_POST['user_id']);

			$_SESSION['last_state'] = 'profile';
			echo json_encode(array('success'=>true, 'msg'=>'Update Successful.'));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'code'=>'201', 'msg'=>'Something went wrong, Please try again later!'));
			exit; 
		}
	}

	function cancelEventModal()
	{
		if(!empty($_SESSION['fls_user'][0]['user_id'])){
			$result = array();
			// print_r($_POST);exit;
			$user_id = $_SESSION['fls_user'][0]['user_id'];
			$event_id = $this->input->post("event_id");
			$session_id = $this->input->post("session_id");
			$result['userlisting'] = $this->profilemodel->getUsers($user_id,$event_id,$session_id);
			
			$ListingHtml = '';
			if($result['userlisting']){
				$ListingHtml = $this->load->view('cancelEventModal',$result,true);
			}
			// echo "<pre>";print_r($ListingHtml);exit;
			if($ListingHtml){
				echo json_encode(array("success"=>true,"ListingHtml"=>$ListingHtml));
			}else{
				echo json_encode(array("success"=>false,"ListingHtml"=>$ListingHtml));
			}
		}
	}

	function eventCancel()
	{
		// echo "<pre>";print_r($_POST['cancel_event_id'][0]);exit;
		if(!empty($_SESSION['fls_user'][0]['user_id'])){
			if(empty($_POST['cancel_event_id'][0])){
				echo json_encode(array('success'=>false, 'msg'=>'please select check box'));
				exit;
			}
			
			$result['payment'] = $this->profilemodel->getRegisteredDetail($_POST['cancel_event_id'][0]);
			// echo "<pre>";print_r($result['payment'][0]['event_fees']);exit;
			if(!empty($result['payment'])){
			$result['getpayment'] = $this->profilemodel->getPaymentDetail($_SESSION['fls_user'][0]['user_id'],$result['payment'][0]['event_id'],$result['payment'][0]['session_id']);
			// echo "<pre>";print_r($result['getpayment']);exit;

			$amt = count($_POST['cancel_event_id']) * $result['payment'][0]['event_fees'];
			
			if(!empty($result['getpayment'])){

			  $curl = curl_init();
              curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/oauth2/token',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Basic QWRIVjlYRlV5TTNjUU1IRHk5eTdXM1gxM19ZSUxtRm9jWHVWQUk2Q1I4UElZZU9sQTU1MDFIdk15aWwwNGZsZXI2SkdwZld4SUFybTRxUnQ6RUZfRF8zaUNVWmRTdnBiaGtwNVFhVVFzUjVFY2x0MmR2YlRBQS1kakRMVUIwTDZ4Vk1pcE1JRXNJNzhUQmNNOFpyNlQ3X0txRnpCYWJxRHA=',
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));
            
            $auth_response = json_decode(curl_exec($curl));
            $access_token = $auth_response->access_token;
            curl_close($curl);
			// echo "<pre>";print_r($access_token);exit;

			if($access_token){
				$curl = curl_init();
				$transaction_id = $result['getpayment'][0]['txn_id'];
				curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://api.sandbox.paypal.com/v2/payments/captures/'.$transaction_id.'/refund',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS =>'{
					"amount": {
						"value": "'.$amt.'",
						"currency_code": "SGD"
					}

				}',
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json',
					'Authorization: Bearer ' .$access_token,
					'Prefer: return=representation',
					'Cookie: cookie_check=yes; d_id=c326a3da33154878b59a47f177dc6a121635334746143; enforce_policy=gdpr_v2.1; ts=vreXpYrS%3D1730029145%26vteXpYrS%3D1635336545%26vr%3Dc18bedf517c0a602122d27a8ff68b4ab%26vt%3Dc18bedf517c0a602122d27a8ff68b4aa%26vtyp%3Dnew; ts_c=vr%3Dc18bedf517c0a602122d27a8ff68b4ab%26vt%3Dc18bedf517c0a602122d27a8ff68b4aa'
				),
				));

				// $response = curl_exec($curl);
				$final_response = json_decode(curl_exec($curl));
				curl_close($curl);
				// echo "<pre>";
				// print_r($final_response);
				// exit;
				if($final_response->status == 'COMPLETED'){
                    $refund_amt = $final_response->seller_payable_breakdown->net_amount->value; 
                    $currency_code = $final_response->amount->currency_code;  
                    $status = $final_response->status;  
					
					$result['refund_amt'] = $this->profilemodel->getPaymentRefund($transaction_id);

					$data_array = array();
					$data_array['refund_amt'] = ($result['refund_amt'][0]['refund_amt'] + $refund_amt);
					$result_update = $this->profilemodel->updateRecord('tbl_payments', $data_array,'txn_id',$transaction_id);

					$reg_array = array();
					foreach ($_POST['cancel_event_id'] as $key => $value) {
						$reg_array['payment_status'] = 'REFUNDED';
						$result_update = $this->profilemodel->updateRecord('tbl_registration', $reg_array,'registration_id',$value);
					}
					$admin_result['admindata'] = $this->profilemodel->getdata1("tbl_users",'email_id',"role_id = 1");
					foreach ($_POST['cancel_event_id'] as $ckey => $values) {
						$register_data = array();
						$register_data['maildata'] = $this->profilemodel->getRegData($values);
						$this->email->from(FROM_MAIL, 'The Global Language Center');
						$this->email->to($register_data['maildata'][0]['email_id']);
						$this->email->cc($admin_result['admindata'][0]['email_id']);
						$this->email->subject('The Global Language Center event cancellation confirmation');
						$content = $this->load->view("bookingCancelation",$register_data,true);
						$this->email->message($content);
						$this->email->send();
					}

					echo json_encode(array('success'=>true, 'msg'=>'Event cancelled.'));
                }else{
                    echo json_encode(array('success'=>false, 'msg'=>'Payment not refunded.'));
                }
			}
				
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Payment details not found.'));
			}
				
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while cancel event.'));
			}
		}
	}

}

?>
