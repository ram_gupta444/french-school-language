<?PHP
class Profilemodel extends CI_Model
{
    function getData($table,$fields,$condition){
        $this -> db -> select($fields);
        $this -> db -> from($table);
        $this->db->where($condition);
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
          return $query->result_array();
        }else{
          return false;
        }
      }
      function updateRecord($tableName, $data, $column, $value)
      {
          $this->db->where("$column", $value);
          $this->db->update($tableName, $data);
          if ($this->db->affected_rows() > 0) {
              return true;
          }
          else {
              return true;
          }
      }
      function getClassdata($user_id){
        $this -> db -> select('r.registration_id,r.type,r.language_id,c.category,c.category,c.location,c.class_fees,c.no_of_hours,c.duration_in_week,c.banner_image,c.title,c.headline,c.intro_text,c.class_type,s.schedule_id,s.title as schedule_title,s.duration,s.repeat_on,s.starts_date,s.start_time,s.ends_date,s.end_time,l.language_name,l.language_logo');
        $this -> db -> from('tbl_registration as r');
        $this -> db -> join('tbl_schedule as s', 's.schedule_id  = r.schedule_id', 'inner');
        $this -> db -> join('tbl_classes as c', 'c.classes_id  = s.class_drop_down', 'inner');
        $this -> db -> join('tbl_language_master as l', 'l.language_master_id  = r.language_id', 'left');
        $this->db->where('r.user_id', $user_id);
        $this->db->where('r.type', 'Class');
        $this->db->where('r.payment_status', 'Completed');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function getEventdata($user_id){
        $this -> db -> select('r.registration_id,r.session_id,r.event_id,r.type,count(r.session_id) as user_count,e.*');
        $this -> db -> from('tbl_registration as r');
        $this -> db -> join('tbl_events as e', 'e.events_id  = r.event_id', 'inner');
        $this->db->where('r.user_id', $user_id);
        $this->db->where('r.type', 'Event');
        $this->db->where('r.payment_status', 'Completed');
        $this->db->group_by('r.session_id'); 
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function getUsers($user_id,$event_id,$session_id)
      {
        $this -> db -> select('r.registration_id,r.session_id,r.event_id,r.type,r.first_name,r.last_name,r.email_id,r.phone_no,r.address,e.*');
        $this -> db -> from('tbl_registration as r');
        $this -> db -> join('tbl_events as e', 'e.events_id  = r.event_id', 'inner');
        $this->db->where('r.type', 'Event');
        $this->db->where('r.user_id', $user_id);
        $this->db->where('r.event_id', $event_id);
        $this->db->where('r.session_id', $session_id);
        $this->db->where('r.payment_status', 'Completed');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function getRegisteredDetail($registration_id)
      {
        $this -> db -> select('r.registration_id,r.session_id,r.event_id,r.type,r.first_name,r.last_name,r.email_id,r.phone_no,e.event_fees');
        $this -> db -> from('tbl_registration as r');
        $this -> db -> join('tbl_events as e', 'e.events_id  = r.event_id', 'inner');
        $this->db->where('r.type', 'Event');
        $this->db->where('r.registration_id', $registration_id);
        $this->db->where('r.payment_status', 'Completed');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function getPaymentDetail($user_id,$event_id,$session_id)
      {
        $this -> db -> select('*');
        $this -> db -> from('tbl_payments');
        $this->db->where('user_id', $user_id);
        $this->db->where('event_id', $event_id);
        $this->db->where('session_id', $session_id);
        // $this->db->where('status', 'Completed');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }

      function getPaymentRefund($transaction_id)
      {
        $this -> db -> select('refund_amt');
        $this -> db -> from('tbl_payments');
        $this->db->where('txn_id', $transaction_id);
        
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function getRegData($registration_id)
      {
        $this -> db -> select('r.first_name,r.last_name,r.email_id,e.headline,e.day_and_date,e.time');
        $this -> db -> from('tbl_registration as r');
        $this -> db -> join('tbl_events as e', 'e.events_id  = r.event_id', 'inner');
        $this->db->where('r.registration_id', $registration_id);
        
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
        if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function getdata1($table, $fields, $condition){
        //echo "Select $fields from $table where $condition";exit;
        $sql = $this->db->query("Select $fields from $table where $condition");
        if($sql->num_rows() > 0){
          return $sql->result_array();
        }else{
          return false;
        }
      }
      
}
?>
