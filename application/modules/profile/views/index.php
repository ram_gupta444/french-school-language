<section class="sec-myaccount sec-bg">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="title-3 bold txt-blue mb40 person-name">Hi, Paul Adams</h3>
        <h1 class="title-1 bold txt-orange mb24">Your Account</h1>
        <div class="tabs-wrapper">
          <ul class="tabs-list">
            <li><a href="#/" onclick="getProfile('booked_events')" data-tab="booked-events" id="booked_events">Booked Events</a></li>
            <li><a href="#/" onclick="getProfile('booked_class')" data-tab="booked-classes" id="booked_class">Booked Classes</a></li>
            <li><a href="#/" onclick="getProfile('profile')" data-tab="account-details" id="profile">Account Details</a></li>
          </ul>
        </div>
        <div class="tabs-content-wrapper" id="profile_listing_div">

      </div>
    </div>
  </div>
</section>
</body>
</html>

<script>
//   $(window).on( "load", function() { 
//   getProfile(value =null);
// });
$( document ).ready(function() {
  getProfile('<?= $type ?>');
});
// for product listing 
function getProfile(value =null){
    var tab_value = value;
    // console.log(tab_value);
    $('.tabs-list li a').removeClass('active');
    if(tab_value === 'booked_events'){
      $("#booked_events").attr("class", "active");
    }
    if(tab_value === 'booked_class'){
      $("#booked_class").attr("class", "active");
    }
    if(tab_value === 'profile'){
      $("#profile").attr("class", "active");
    }

  $.ajax({
      url: "<?= base_url('profile/getProfile')?>",
      type: "POST",
      data:{section:value},
      dataType: "json",
      success: function(response){
        // console.log(response.section);
        if(response.success){
          if(response.section === 'profile'){
            $("#profile").attr("class", "active");
          }
          if(response.section === 'booked_class'){
            $("#booked_class").attr("class", "active");
          }
          if(response.section === 'booked_events'){
            $("#booked_events").attr("class", "active");
          }
            $("#profile_listing_div").html(response.Htmlpage);
        }
      }
    });
}

</script>