<div class="tabs-content-wrapper" style="padding: 40px 0 0 0;">
    <h3 class="title-3 bold txt-blue mb24" data-aos="fade-up">Account Details</h3>    
    <form id="editUser" method="post" enctype="multipart/form-data" class="no-float-label">
        <div class="account-form-wrapper flex-wrapper flex-align-center flex-justify-sb">
        <div class="af-single" data-aos="fade-up" data-aos-delay="100">
            <input type="hidden" name="user_id" id="user_id" value="<?= (!empty($user_detail[0]['user_id'])) ? $user_detail[0]['user_id'] : '' ?>">
            <label>Full name</label>
            <input type="text" name="full_name" id="full_name" class="input-fls" value="<?= (!empty($user_detail[0]['full_name'])) ? $user_detail[0]['full_name'] : ''?>" placeholder="Enter full name">
            <a href="#/" class="edit-af"><img src="<?= base_url()?>assets/images/edit-account-icon.svg" alt=""></a>
            <label for="full_name" generated="true" class="error"></label>
        </div>
        <div class="af-single" data-aos="fade-up" data-aos-delay="200">
            <label>Username</label>
            <input type="text" name="email_id" id="email_id" class="input-fls" value="<?= (!empty($user_detail[0]['email_id'])) ? $user_detail[0]['email_id'] : ''?>" placeholder="Enter email id">
            <a href="#/" class="edit-af"><img src="<?= base_url()?>assets/images/edit-account-icon.svg" alt=""></a>
            <label for="email_id" generated="true" class="error"></label>
        </div>
        <div class="af-single" data-aos="fade-up" data-aos-delay="300">
            <label>Phone number</label>
            <input type="text" name="phone_no" id="phone_no" class="input-fls" value="<?= (!empty($user_detail[0]['phone'])) ? $user_detail[0]['phone'] : ''?>" oninput="this.value=this.value.replace(/[^0-9]/g,'')" maxlength="10" minlength="10" placeholder="Enter phone number">
            <a href="#/" class="edit-af"><img src="<?= base_url()?>assets/images/edit-account-icon.svg" alt=""></a>
            <label for="phone_no" generated="true" class="error"></label> 
        </div>
        <!-- <div class="af-single" data-aos="fade-up" data-aos-delay="400">
            <input type="password" name="password" id="password" class="input-fls" value="">
            <a href="#/" class="edit-af"><img src="<?= base_url()?>assets/images/edit-account-icon.svg" alt=""></a>
        </div> -->
        </div>
        <div class="af-cta">
        <button type="submit" class="btn-fls btn-large btn-fls-primary" data-aos="fade-up" data-aos-delay="600">Update</button>
        </div>
    </form>
</div>

<script>
    var vRules = {

      full_name: {
          required: true
      },
      email_id: {
          required: true
      },
      /*   phone_no: {
          required: true
      }, */
  };

  var vMessages = {

      full_name: {
          required: "Please enter full name"
      },
      email_id: {
          required: "Please enter email id"
      },
     /*  phone_no: {
          required: "enter enter phone number"
      }, */
  };

  $("#editUser").validate({
      rules: vRules,
      messages: vMessages,
      submitHandler: function (form) {
          var act = "<?php echo base_url();?>profile/submitForm";
          console.log(act);
          $("#editUser").ajaxSubmit({
              url: act,
              type: 'POST',
              dataType: 'json',
              cache: false,
              clearForm: false,
              beforeSubmit: function (arr, $form, options) {
                  //return false;
              },
              success: function (response) {
                  if (response.success) {
                        $.toast({
                            heading: 'success',
                            text: response.msg,
                            icon: 'success',
                            loader: false,        // Change it to false to disable loader
                            loaderBg: '#002291',  // To change the background
                            position: 'top-right',
                            stack: false
                        })
                      setTimeout(function () {
                          window.location = "<?php echo base_url();?><?= (!empty($_SESSION['last_state'])?$_SESSION['last_state']:"home")?>";
                      }, 2000);
                  } else {
                        $.toast({
                            heading: 'Error!',
                            text: response.msg,
                            icon: 'error',
                            loader: false,        // Change it to false to disable loader
                            loaderBg: '#bd0202',  // To change the background
                            position: 'top-right',
                            stack: false
                        })
                  }
              }
          });
      }
  });
</script>