<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
  <tr>
    <td valign="top" align="center">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
        <tr>
          <td valign="top" align="center">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <a href="#/" target="_blank" style="outline:none;">
                      <img src="<?= base_url()?>assets/images/tglc-logo.jpg" border="0" alt="" title="" style="width:70px;height:70px;"/>
                    </a>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:16px 0;"> 
                      Dear <?= $maildata[0]['first_name'].' '.$maildata[0]['last_name'] ?>, </p>

                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 16px 0;">We have received your request to cancel your booking for <?= strip_tags($maildata[0]['headline'])?> on <?= (!empty($maildata[0]['day_and_date'])? date(" jS F Y", strtotime($maildata[0]['day_and_date'])):"") ?>, <?= $maildata[0]['time']?>. A refund has been initiated and will be credited to your account within 72 hours.
                    </p>
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 16px 0;">Thank you.</p>
                    
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 8px 0;">Warm Regards,</p>
                    
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 16px 0;">The Global Language Center </p>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="width:100%;background:#002290;margin:30px 0 0 0;padding:50px 30px;color:#fff;">
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      The <strong>Global French Language Centre</strong> is a borderless language learning institute in
                      Singapore that allows you to enjoy a unique and authentic French experience as we provide a
                      holistic experience for all levels and ages.
                    </p>
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      If you have any questions or concerns, we’re here to help.<br />
                      <strong>Contact us <a style="color:#fff;" href="mailto:info@thegloballanguagecenter.org" target="_blank">info@thegloballanguagecenter.<wbr>org</a> or +65 89506085</strong>
                    </p>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>