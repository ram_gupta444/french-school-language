<div class="tabs-content-wrapper" style="padding: 40px 0 0 0;">
    <h3 class="title-3 bold txt-blue mb24">Booked Classes</h3>
    <div class="group-study-list-wrapper flex-wrapper flex-align-start flex-justify-sb">
        <?php 
        $delay = 100;
        if(!empty($bookClass_detail)){
            foreach ($bookClass_detail as $key => $value) { ?>
            <div class="gsl-single flex-wrapper flex-align-start flex-justify-sb" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
                <img src="<?= (!empty($value['banner_image'])?base_url('images/class/').$value['banner_image']:"") ?>" alt="" class="gsl-img">
                <div class="gsl-single-right">
                    <div class="study-label-wrapper">
                        <div class="all-group-label"><?= (!empty($value['class_type'])?$value['class_type']:"") ?></div>
                        <div class="all-group-label"><?= (!empty($value['category'])?$value['category']:"") ?></div>
                    </div>
                    <div class="group-info-wrapper flex-wrapper flex-align-center flex-justify-start mb16">
                        <div class="lang-name txts semibold">
                            <img src="<?= (!empty($value['language_logo'])?base_url('images/language_logo/').$value['language_logo']:"") ?>" alt="">
                            <?= (!empty($value['language_name'])?$value['language_name']:"") ?>
                        </div>
                        <div class="category-name txts semibold">
                            Category Name
                        </div>
                    </div>
                    <h4 class="txtl bold txt-black"><?= (!empty($value['headline'])?$value['headline']:"") ?></h4>
                    <div class="course-details-wrapper flex-wrapper flex-align-center flex-justify-sb mt16">
                        <div class="cd-left">
                            <div class="txts bold">Course duration: <?= (!empty($value['duration_in_week'])?$value['duration_in_week']:"") ?> weeks<br />
                            Frequency: <?= (!empty($value['repeat_on']))? count(explode(',',$value['repeat_on'])):"" ?> times a week,<br />
                            <?= (!empty($value['no_of_hours'])?$value['no_of_hours']:"") ?> hour per class
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }
            }else{ ?>
            <h1 class="title-4 bold txt-blue mb24">You have no booked classes</h1>
          <?php  } ?>
           
    </div>
</div>