<div class="tabs-content-wrapper" style="padding: 40px 0 0 0;">
    <h3 class="title-3 bold txt-blue mb24">Booked Events</h3>
    <div class="immersive-list-wrapper flex-wrapper flex-align-start flex-justify-start">
        <?php
        $delay = 100;
        if(!empty($bookEvent_detail)){
            foreach ($bookEvent_detail as $key => $value) { ?>
                <div class="il-single" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
                <img src="<?= (!empty($value['banner_image'])?base_url('images/event/').$value['banner_image']:"") ?>" alt="" class="ils-img">
                    <div class="ils-location-wrapper ip-label-1 flex-wrapper flex-align-center flex-justify-sb mt16">
                        <div class="location-name txts" style="flex:0 0 100%;">
                            <div class="all-group-label"><?= (!empty($value['event_type'])? $value['event_type']:"") ?></div>
                            <div class="all-group-label"><?= (!empty($value['category'])? $value['category']:"") ?></div>

                            <div class="all-group-label"><?= (!empty($value['user_count'])? $value['user_count']:"") ?> <?= (!empty($value['user_count']) && $value['user_count'] >1) ? "seats":"seat" ?></div>
                            
                        </div>
                     
                        <div class="meet-location txts mt8 medium"><?= (!empty($value['event_type']) && $value['event_type'] == 'Online') ? "Zoom": (!empty($value['location'])? $value['location']:"") ?></div>
                        <div class="flex-full txt-gold txts semibold"><span>Starts <?= (!empty($value['day_and_date'])? date(" jS F Y", strtotime($value['day_and_date'])):"") ?></span></div>
                        <div class="flex-full txt-gold txts semibold"><span><?= (!empty($value['event_timeline'])? $value['event_timeline']:"") ?></span></div>
                        <h4 class="flex-full txtl bold txt-blue mt8 mb8"><?= (!empty($value['headline'])? $value['headline']:"") ?></h4>
                        <div class="txtm ils-desc">
                        <?= (!empty($value['intro_text'])? $value['intro_text']:"") ?>
                        </div>
                        <?php 
                        // 48 hours before means 2 days
                        $preDate = date('Y-m-d',strtotime("-2 day", strtotime(date("Y-m-d"))));
                        if($value['day_and_date'] >= $preDate){
                            $disabled = '';
                        }else{
                            $disabled = 'disabled';
                        }
                        ?>
                        <div class="ils-cta mt16 w100">
                            <!-- <a href="#" class="btn-fls btn-large btn-fls-primary <?= $disabled?> w100 " onclick="cancelEventfunction('<?= $value['event_id']?>','<?= $value['session_id'] ?>')" <?= $disabled?> >Cancel Event</a> -->

                            <button type="button" class="btn-fls btn-large btn-fls-primary <?= $disabled?> w100" onclick="cancelEventfunction('<?= $value['event_id']?>','<?= $value['session_id'] ?>')" <?= $disabled?>>Cancel Event</button>
                        </div>
                    </div>
                </div>
                <div id="PrintContentdiv_body"></div>
            <?php }
          }else{ ?>
            <h1 class="title-4 bold txt-blue mb24">You have no booked events</h1>
         <?php } ?>
    </div>
</div>

<script>
    function cancelEventfunction(event_id,session_id){
		$.ajax({
			url: "<?php echo base_url($this->router->fetch_module());?>/cancelEventModal",
			dataType: 'json',
			type: "post",
			data: {event_id:event_id,session_id:session_id },
			success: function (response) {
				console.log(response);
				if(response.success){
					$('#PrintContentdiv_body').html(response.ListingHtml);
					$('#exampleModalLong').modal('show');
				}
			}
		});
    }
</script>