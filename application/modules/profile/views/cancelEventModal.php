<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog" role="document" style="width:70%;max-width:1024px;"> 
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Canel Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  id="form-cancel" method="post">
      <div class="modal-body">
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Cancel Event</th>
                <th>Name</th>
                <th>Mobile No.</th>
                <th>E-mail ID</th>
                <th>Event name</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            foreach ($userlisting as $key => $value) { ?>
            <tr>
                <td><input type="checkbox" id="cancel_event_id" name="cancel_event_id[]" value="<?= $value['registration_id']?>"></td>
                <td><?= $value['first_name'].' '.$value['last_name'] ?></td>
                <td><?= $value['phone_no']?></td>
                <td><?= $value['email_id']?></td>
                <td><?= (!empty($value['headline'])) ? trim(preg_replace('/\s+/', ' ', $value['headline'])) : "" ?></td>
                <td><?= $value['event_fees']?></td>
            </tr>
            <?php }
            ?>
            </tbody>
        </table>
        </div>
      </div>
       <!-- loader part start here -->
        <div class="loader-wrapper center" id="loader_load"  style="display:none">		
            <img src="<?= base_url('assets/images/loader.gif')?>"  alt="" style="padding-left: 351px;margin-top: -195px;">
            <p class="loader-text">Loading...</p>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="cancelEventBtn">Cancel Event</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>

  $("#form-cancel").validate({
    // rules: vRules,
    // messages: vMessages,
    submitHandler: function(form) {
      var act = "<?php echo base_url(); ?>profile/eventCancel";
      $("#loader_load").show();
      $("#form-cancel").ajaxSubmit({
        url: act,
        type: 'post',
        dataType: 'json',
        cache: false,
        clearForm: false,
        success: function(response) {
          if (response.success) {
            $("#loader_load").hide();
            $.toast({
              heading: 'success',
              text: response.msg,
              icon: 'success',
              loader: false, // Change it to false to disable loader
              loaderBg: '#002291', // To change the background
              position: 'top-right',
              stack: false
            })
            // alert(response.msg);
            setTimeout(function() {
              window.location = "<?php echo base_url(); ?>profile/bookedEvents";
            }, 2000);
          } else {
            $("#loader_load").hide();
            $.toast({
              heading: 'Error!',
              text: response.msg,
              icon: 'error',
              loader: false, // Change it to false to disable loader
              loaderBg: '#bd0202', // To change the background
              position: 'top-right',
              stack: false
            })
            return false;
          }
        }
      });
    }
  });
</script>