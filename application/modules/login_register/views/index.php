<section class="sec-login sec-bg">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="login-wrapper">
          <form id="form-signin" method="post" enctype="multipart/form-data">
            <div class="login-form">
              <h3 class="title-3 bold txt-orange mb8">Sign in to Your Account</h3>
              <p class="txtl mb40">Enter your email address and password to login.</p>
              <div class="form-group">
                <input type="text" class="input-fls" id="loginemail" name="loginemail">
                <label for="">Email address<sup>*</sup></label>
                <label for="loginemail" generated="true" class="error error-label"></label>
              </div>
              <div class="form-group">
                <input type="password" class="input-fls password" id="loginpassword" name="loginpassword">
                <label for="">Password<sup>*</sup></label>
                <label for="loginpassword" generated="true" class="error error-label"></label>
                <a href="#/" class="showpassword"><img src="<?= base_url()?>assets/images/view-password.svg" alt=""></a>
              </div>
              <div class="form-group ta-left">
                <a href="#/" class="fp-link txts">Forgot Password</a>
              </div>
              <div class="form-group mt40 login-cta">
                <button type="submit" class="btn-fls btn-large btn-fls-primary w100">Sign in</button>
              </div>
              <div class="form-group">
                <div class="txts">
                  Dont have an account? <a href="#/" class="txts link-bold create-account-link">Create an account</a>
                </div>
              </div>
            </div>
          </form>
          <form id="fp_form" method="post">
            <div class="forgot-password-form">
              <h3 class="title-3 bold txt-orange mb8">Forgot Password</h3>
              <p class="txtl mb40">Enter your email address to reset password.</p>
              <div class="form-group">
                <input type="email" class="input-fls" name="forgot_email_id">
                <label for="">Email address<sup>*</sup></label>
              </div>
              <div class="form-group mt40 login-cta">
                <button type="submit" class="btn-fls btn-large btn-fls-primary w100">Reset Password</button>
              </div>
              <div class="form-group">
                <a href="#/" class="txts link-bold login-link">Back to login</a>
              </div>
            </div>
          </form>
          <form id="form-signup" method="post" enctype="multipart/form-data">
            <div class="register-form">
              <h3 class="title-3 bold txt-orange mb40">Create an Account</h3>            
              <div class="form-group">
                <input type="text" class="input-fls" name="first_name" id="first_name">
                <label for="">First Name<sup>*</sup></label>
                <label for="first_name" generated="true" class="error error-label"></label>
              </div>
              <div class="form-group">
                <input type="text" class="input-fls" name="last_name" id="last_name">
                <label for="">Last Name<sup>*</sup></label>
                <label for="last_name" generated="true" class="error error-label"></label>
              </div>
              <div class="form-group">
                <input type="email" class="input-fls" name="useremail" id="useremail">
                <label for="useremail">Email<sup>*</sup></label>
                <label for="useremail" generated="true" class="error error-label"></label>
              </div>
              <div class="form-group dob-wrapper">
                <input type="date" class="input-fls" name="dob" id="dob" onfocus="(this.type='date')" onfocusout="(this.type='text')">
                <label for="">Date of Birth<sup>*</sup></label>
                <label for="dob" generated="true" class="error error-label error-label-dob" style="display:none !important;"></label>
              </div>
              <div class="form-group">
                <input type="password" class="input-fls password" name="password" id="password">  
                <label for="">Set Password<sup>*</sup></label>
                <a href="#/" class="showpassword"><img src="<?= base_url()?>assets/images/view-password.svg" alt=""></a>
                <label for="password" generated="true" class="error error-label"></label>
              </div>            
              <div class="form-group mt40 login-cta">
                <button type="submit" class="btn-fls btn-large btn-fls-primary w100">Sign up</button>
              </div>
              <div class="form-group">
                <div class="txts">
                  Have an account? <a href="#/" class="txts link-bold login-link">Sign in</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

</html>
<script>
    // for register form submit 
    var vRules = {
      useremail: {
          required: true,
          email: true,
          remote: {
              url:"<?php echo base_url('login_register/dataEmailExist')?>" ,
              type: "post",
              data: {email_id: function() {return $( "#useremail" ).val();}}
              }
      },
      first_name: {
          required: true
      },
      last_name: {
          required: true
      },
        dob: {
          required: true
      },
      password: {
          required: true
      }
  };

  var vMessages = {

    useremail: {
        required: "Please enter email.",
          email: "Please enter valid email.",
          remote : 'Email id is already exist'
      },
      first_name: {
          required: "Please enter first name"
      },
      last_name: {
          required: "Please enter last name"
      },
      dob: {
          required: "Please select date of birth"
      },
      password: {
          required: "Please enter password."
      }

  };

  $("#form-signup").validate({
      rules: vRules,
      messages: vMessages,
      submitHandler: function (form) {
          var act = "<?php echo base_url();?>login_register/submitForm";
          console.log(act);
          $("#form-signup").ajaxSubmit({
              url: act,
              type: 'POST',
              dataType: 'json',
              cache: false,
              clearForm: false,
              beforeSubmit: function (arr, $form, options) {
                  //return false;
              },
              success: function (response) {
                  if (response.success) {
                      // alert(response.msg);
                      $.toast({
                        heading: 'success',
                        text: response.msg,
                        icon: 'success',
                        loader: false,        // Change it to false to disable loader
                        loaderBg: '#002291',  // To change the background
                        position: 'top-right',
                        stack: false
                      })
                      setTimeout(function () {
                          window.location = "<?php echo base_url();?><?= (!empty($_SESSION['last_state'])?$_SESSION['last_state']:"home")?>";
                      }, 2000);
                  } else {
                      // alert(response.msg);
                      $.toast({
                        heading: 'Error!',
                        text: response.msg,
                        icon: 'error',
                        loader: false,        // Change it to false to disable loader
                        loaderBg: '#bd0202',  // To change the background
                        position: 'top-right',
                        stack: false
                      })
                  }
              }
          });
      }
  });

  var vRules = {
    forgot_email_id:{required:true},
};
var vMessages = {
	forgot_email_id:{required:"Please enter email id."},
};

$("#fp_form").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login_register/forgot_password";
		$("#fp_form").ajaxSubmit({
			url: act, 
			type: 'post',
      dataType: 'json',
			cache: false,
			clearForm: false,
			success: function (response) {
				if (response.success) {
            // alert(response.msg);
            $.toast({
                heading: 'success',
                text: response.msg,
                icon: 'success',
                loader: false,        // Change it to false to disable loader
                loaderBg: '#002291',  // To change the background
                position: 'top-right',
                stack: false
              })
					  setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);
				}
				else
				{	
            $.toast({
              heading: 'Error!',
              text: response.msg,
              icon: 'error',
              loader: false,        // Change it to false to disable loader
              loaderBg: '#bd0202',  // To change the background
              position: 'top-right',
              stack: false
            })
					return false;
				}
			}
		});
	}
});

$("#form-signin").validate({
    rules: {
      loginemail:{required:true},
      loginpassword:{required:true},
      },
    messages: {
      loginemail:{required:"Please Enter Email id."},
      loginpassword:{required:"Please Enter Password."},
      },
    submitHandler: function(form) 
    { 
      
      var act = "<?= base_url('login_register/form_validate')?>";
      $("#form-signin").ajaxSubmit({
        url: act, 
        type: 'post',
        dataType: 'json',
        cache: false,
        clearForm: false,
        async:false,
        beforeSubmit : function(arr, $form, options){
          $(".btn btn-black").hide();
        },
        success: function(response) 
        {
          if(response.success){
            // alert(response.msg);
              $.toast({
                heading: 'success',
                text: response.msg,
                icon: 'success',
                loader: false,        // Change it to false to disable loader
                loaderBg: '#002291',  // To change the background
                position: 'top-right',
                stack: false
              })
            setTimeout(function () {
                window.location = "<?= (!empty($_COOKIE['last_stage'])?$_COOKIE['last_stage']:base_url('home'))?>";
               
            }, 2000);
          }else{
            $.toast({
              heading: 'Error!',
              text: response.msg,
              icon: 'error',
              loader: false,        // Change it to false to disable loader
              loaderBg: '#bd0202',  // To change the background
              position: 'top-right',
              stack: false
            })
            // alert(response.msg);
          }
        }
      });

    }
  });
</script>