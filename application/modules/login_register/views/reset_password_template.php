<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
  <tr>
    <td valign="top" align="center">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
        <tr>
          <td valign="top" align="center">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <a href="#/" target="_blank" style="outline:none;">
                      <img src="<?= base_url()?>assets/images/tglc-logo.jpg" border="0" alt="" title="" style="width:70px;height:70px;"/>
                    </a>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <h1
                      style="font-family: Helvetica, Arial, sans-serif;margin:40px 0 40px 0;padding:0 0 40px 0 ;border-bottom:1px solid #cdcdcd;color:#001866;font-weight: bold;font-size:52px;line-height:62px;">
                      Reset your password</h1>
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#666D78;margin:0 0 16px 0;">
                      You’re receiving this e-mail because you requested a password reset for your account.</p>
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#666D78;margin:0 0 16px 0;">
                      Please tap the button below to choose a new password.
                    </p>
                    <p style="margin:40px 0 0 ;"><a href="<?= $link; ?>"
                        style="display:inline-block;font-family: Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;text-transform:uppercase;padding:16px 42px;background:#EF4135;color:#ffffff;border-radius: 8px 0px;text-decoration:none;">Reset
                        Password</a></p>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="width:100%;background:#002290;margin:70px 0 0 0;padding:50px 30px;color:#fff;">
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      The <strong>Global French Language Centre</strong> is a borderless language learning institute in
                      Singapore that allows you to enjoy a unique and authentic French experience as we provide a
                      holistic experience for all levels and ages.
                    </p>
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      If you have any questions or concerns, we’re here to help.<br />
                      <strong>Contact us info@thegloballanguagecenter.org or +65 89506085</strong>
                    </p>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>