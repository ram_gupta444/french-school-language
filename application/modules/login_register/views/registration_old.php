<style>
	@font-face {
	font-family: 'Barlow';
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.eot');
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.eot?#iefix') format('embedded-opentype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.woff2') format('woff2'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.woff') format('woff'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.ttf') format('truetype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Regular.png#Barlow-Regular') format('svg');
	font-weight: normal;
	font-style: normal;
	font-display: swap;
}


@font-face {
	font-family: 'Barlow';
	src: url('http://footage.webshowcase-india.com/assets/fonts/-Medium.eot');
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Medium.eot?#iefix') format('embedded-opentype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Medium.woff2') format('woff2'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Medium.woff') format('woff'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Medium.ttf') format('truetype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Medium.png#Barlow-Medium') format('svg');
	font-weight: 500;
	font-style: normal;
	font-display: swap;
}

@font-face {
	font-family: 'Barlow';
	src: url('Barlow-SemiBold.eot');
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-SemiBold.eot?#iefix') format('embedded-opentype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-SemiBold.woff2') format('woff2'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-SemiBold.woff') format('woff'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-SemiBold.ttf') format('truetype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-SemiBold.png#Barlow-SemiBold') format('svg');
	font-weight: 600;
	font-style: normal;
	font-display: swap;
}

@font-face {
	font-family: 'Barlow';
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.eot');
	src: url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.eot?#iefix') format('embedded-opentype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.woff2') format('woff2'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.woff') format('woff'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.ttf') format('truetype'), url('http://footage.webshowcase-india.com/assets/fonts/Barlow-Bold.png#Barlow-Bold') format('svg');
	font-weight: bold;
	font-style: normal;
	font-display: swap;
}	
</style>
<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
	<tr>
		<td valign="top" align="center">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
				<tr>
					<td valign="top" align="left">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
							<tr>
								<td valign="top" align="left" colspan="2">
									<img src="<?= base_url()?>assets/images/multicolored-line.jpg" style="width:100%;">
								</td>
							</tr>
							<tr>
								<td valign="top" align="left" colspan="2">
									<img src="<?= base_url()?>assets/images/ff-color-logo-email.png" border="0" alt="" title=""
										style="display:block;width:170px;margin:30px 0;padding:0 0 0 30px;" />
								</td>
							</tr>
							<tr>
								<td valign="top" align="left" colspan="2" >									
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Hi <?= $full_name ?>,</p>
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">Thank you for creating a Fls account. </p>
									
									<p style="margin:40px 30px;">
										<a href="<?= $verification_link ?>" style="padding:14px 15px;color:#fff;background-color: #214D7F;font-size: 16px;letter-spacing: 0;line-height: 24px;font-family:'Barlow',sans-serif;text-decoration:none;text-transform: uppercase;">Verify my email address</a>
									</p>

									
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">Regards,</p>	
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">Fls Team
									</p>									
									
									<a href="#/" style="font-size:16px;color: #214D7F;letter-spacing: 0;line-height: 24px;text-align: center;margin:0 0 0 30px;text-decoration:underline;font-family:'Barlow',sans-serif"><?= $verification_link ?></a>

								</td>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>