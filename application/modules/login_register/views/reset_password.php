<html>

<body class="login">
  <section class="sec-login sec-bg">
    <div class="wrapper wrapper-login">
      <div class="container container-login animated fadeIn">
        <div class="row">
          <div class="col-12">
            <div class="login-wrapper">
              <h3 class="title-3 bold txt-orange mb8">Reset Password</h3>
              <?php if ($id) { ?>
                <div class="login-form">
                  <form class="login-form" id="form-validate" method="post">
                    <input type="hidden" id="hidden_user_id" name="hidden_user_id" value="<?php echo (!empty($id) ? $id : '') ?>">
                    <div class="form-group" style="text-align:left;">
                      <label for="password" class="placeholder">New Password</label>
                      <div class="position-relative">
                        <input id="password" name="password" type="password" class="form-control" required placeholder="Enter new password">
                      </div>
                    </div>
                    <div class="form-group" style="text-align:left;">
                      <label for="cpassword" class="placeholder">Confirm New Password</label>
                      <div class="position-relative">
                        <input id="cpassword" name="cpassword" type="password" class="form-control" required placeholder="Enter new password again">
                      </div>
                    </div>
                    <div class="form-group form-action-d-flex mb-3">
                      <button type="submit" class="btn-fls btn-large btn-fls-primary w100">Change Password</button>
                    </div>
                  </form>
                </div>
              <?php  } else { ?>
                <div class="heading_s1 text-center">
                  <h2>Link Expired!</h2>
                  <h3 style="font-weight:normal;">Please try again if you have not recovered your password!</h3>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>

</html>
<script>
  var vRules = {
    password: {
      required: true
    },
    cpassword: {
      required: true
    },
  };
  var vMessages = {
    password: {
      required: "Please enter new password."
    },
    cpassword: {
      required: "Please enter confirm password."
    }
  };
  $("#form-validate").validate({
    rules: vRules,
    messages: vMessages,
    submitHandler: function(form) {
      var act = "<?php echo base_url(); ?>login_register/change_password";
      $("#form-validate").ajaxSubmit({
        url: act,
        type: 'post',
        dataType: 'json',
        cache: false,
        clearForm: false,
        success: function(response) {
          if (response.success) {
            $.toast({
              heading: 'success',
              text: response.msg,
              icon: 'success',
              loader: false, // Change it to false to disable loader
              loaderBg: '#002291', // To change the background
              position: 'top-right',
              stack: false
            })
            // alert(response.msg);
            setTimeout(function() {
              window.location = "<?php echo base_url(); ?>home";
            }, 2000);
          } else {
            $.toast({
              heading: 'Error!',
              text: response.msg,
              icon: 'error',
              loader: false, // Change it to false to disable loader
              loaderBg: '#bd0202', // To change the background
              position: 'top-right',
              stack: false
            })
            return false;
          }
        }
      });
    }
  });
</script>