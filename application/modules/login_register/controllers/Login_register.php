<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_register extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		 $this->load->library('user_agent');
		$this->load->model('login_registermodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
	}

    public function index(){
		unset($_SESSION['language_id']);
        $this->load->view('template/header.php');
        $this->load->view('index.php');
        $this->load->view('template/footer.php');
    }

	
	public function form_validate(){
		$username = $this->input->post('loginemail'); 
		$password = $this->input->post('loginpassword'); 
		$condition = "(email_id = '".$username."') AND password = md5('".$password."')  AND is_email_verified ='Yes' AND role_id = 2 ";
		$result = $this->common->getData('tbl_users','*',$condition);
		if($result){

				$session_data = array('fls_user'=>$result);  
				$this->session->set_userdata($session_data);
				$_SESSION['last_state'] = 'profile';
			echo json_encode(array('success'=>true, 'msg'=>'Login Successful.'));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'code'=>'201', 'msg'=>'Invalid Email ID/ Password.'));
			exit;  
			 
		} 
	}


	public function logout(){
		$this->session->unset_userdata('fls_user');
// 		redirect(base_url('login_register'));
        redirect((!empty($_COOKIE['last_stage'])?$_COOKIE['last_stage']:base_url('home')));
	}
	public function verify() {
	    $data = array();
		$data['is_email_verified'] = 'Yes';
		$data['status'] = 'Active';
	    $condition = "email_id = '".$_GET['email']."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
		    $_SESSION['toast'] = "yes";
			redirect(base_url());
		}
	}

	public function dataEmailExist(){

		$condition = " email_id = '".$_POST['email_id']."'  ";
		$result=$this->login_registermodel->getdata("tbl_users","email_id",$condition);
		if($result>0){
			echo  json_encode(FALSE);
		}else{
			echo  json_encode(TRUE);
		} 
	}
    public function submitForm(){

		$condition = " email_id = '".$this->input->post('useremail')."'  ";
		$result=$this->login_registermodel->getdata("tbl_users","email_id",$condition);

		if($result){
			echo json_encode(array('success'=>false, 'msg'=>'User already exist.'));
			exit;
		}

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		
            $data = array(); 
			$data['email_id'] = $this->input->post('useremail');
			$data['full_name'] = $this->input->post('first_name').' '.$this->input->post('last_name');
			$data['role_id'] = '2';
			$data['dob'] = $this->input->post('dob');
			$data['password'] = md5($this->input->post('password'));
			$data['is_email_verified'] = 'No';
			$data['status'] = 'In-active';
			$data['updated_on'] = date("Y-m-d H:i:s");
            $data['created_on'] = date("Y-m-d H:i:s");

            $this->email->from(FROM_MAIL, 'The Global Language Center');
            $this->email->to($this->input->post('useremail'));
            $this->email->subject('Welcome to The Global Language Center');
            $data['verification_link'] = base_url()."login_register/verify?email=".$data['email_id'];	
            $content = $this->load->view("registration",$data,true);
            $this->email->message($content);
            $this->email->send();
				unset($data['verification_link']);
				$result = $this->common->insertData('tbl_users',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'User Registration verification mail sent. Please verify email.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'There was some error. Please try again.'));
					exit;
				}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
			exit;
		}
    }
	function forgot_password()
	{
		$result = $this->login_registermodel->getDataCustom("tbl_users", "*","status = 'Active' AND email_id ='".$_POST['forgot_email_id']."' ");
		// echo "<pre>";print_r($result[0]['user_id']);exit;
        if($result){
			$code = md5(rand());
			$data_array =array();
			$data_array['verification_code'] = $code;
			$data_array['is_email_verified'] = 'No';
			$result_update = $this->login_registermodel->updateRecord('tbl_users', $data_array,'user_id',$result[0]['user_id']);
			$link = base_url().'login_register/reset_password/'.$code;
		
			if($result_update){
				$this->email->from(FROM_MAIL, 'The Global Language Center');
				$this->email->to($_POST['forgot_email_id']);
				$this->email->subject('Reset account password');
				$data = array();
				$data['link'] = $link;
				$content = $this->load->view("reset_password_template",$data,true);
				// $content = "To reset your fls account password click the link <a href='$link'>Here</a>.";
				$this->email->message($content);
				$this->email->send();
			}
			echo json_encode(array("success"=>true, "msg"=>'A link to reset password is sent to your mail.'));
			exit;	
		}else{
			echo json_encode(array("success"=>false, "msg"=>'Please enter a valid email!'));
			exit;
		}
	}
	function reset_password()
	{
		$code = $this->uri->segment(3);
		$result = $this->login_registermodel->getDataCustom("tbl_users", "user_id","status = 'Active' AND verification_code ='".$code."' ");
		// print_r($result[0]['user_id']);exit;
		$user_id['id'] = $result[0]['user_id'];
		$this->load->view('template/header.php');
		$this->load->view('reset_password.php',$user_id);
		$this->load->view('template/footer.php');
	}
	function change_password()
	{
		$result = $this->login_registermodel->getDataCustom("tbl_users", "*","status = 'Active' AND user_id ='".$_POST['hidden_user_id']."' ");
		if($result){
			if(!isset($_POST['password'])){
				echo json_encode(array("success"=>false,'msg'=>'Please enter new password.'));
				exit;
			}
			if(!isset($_POST['cpassword'])){
				echo json_encode(array("success"=>false,'msg'=>'Please enter confirm new password.'));
				exit;
			}
			if($_POST['password'] != $_POST['cpassword']){
				echo json_encode(array("success"=>false,'msg'=>'Passwords do not match!.'));
				exit;
			}
			$data_array =array();
			$data_array['verification_code'] = '';
			$data_array['password'] = MD5($_POST['password']);
			$data_array['is_email_verified'] = 'Yes';
			$result_update = $this->login_registermodel->updateRecord('tbl_users', $data_array,'user_id',$result[0]['user_id']);
			if($result_update){
				echo json_encode(array("success"=>true, "msg"=>'Password changed successfully!'));
				exit;
			}else{
				echo json_encode(array("success"=>false, "msg"=>'Something went wrong, Please try again later!'));
				exit;
			}
		}else{
			echo json_encode(array("success"=>false, "msg"=>'User not found, Please try again later!'));
			exit;
		}
	}
}
