<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Aboutus extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('aboutusmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
	}

	function index(){
		$_SESSION['last_stage'] = "aboutus";
		unset($_SESSION['language_id']);
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$result = array();
		$condition = "1=1";
		$result['about_us'] = $this->common->getData("tbl_about_us",'*',$condition);
		// echo "<pre>";print_r($result['about_us']);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}

}

?>
