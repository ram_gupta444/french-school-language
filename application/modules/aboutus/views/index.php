<section class="sec-internal-intro">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="sec-top-wrapper about-top-wrapper">
            <div class="sec-top-title border-right mb16" data-aos="fade-up">
              <h1 class="title-1 bold txt-orange" ><?= (!empty($about_us[0]['heading'])?$about_us[0]['heading']:"") ?></h1>
              <img src="<?php echo base_url();?>assets/images/sec-top-aboutus.svg" alt="" class="stt-border img-fluid">
            </div>
            <div class="txtm" data-aos="fade-up" data-aos-delay="200">
              <?= (!empty($about_us[0]['description'])?$about_us[0]['description']:"") ?>
            </div>
          </div>
          <div class="about-intro-wrapper flex-wrapper flex-align-center flex-justify-sb mt40">
            <div class="ai-left" data-aos="fade-right" data-aos-delay="100">
              <img src="<?= (!empty($about_us[0]['section1_image'])?base_url('images/about_us/').$about_us[0]['section1_image']:"") ?>" alt="" class="ai-img">
            </div>
            <div class="ai-right" data-aos="fade-left" data-aos-delay="100">
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($about_us[0]['section1_heading'])?$about_us[0]['section1_heading']:"") ?></h3>
              <div class="txtm">
                <?= (!empty($about_us[0]['section1_description'])?$about_us[0]['section1_description']:"") ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single about-sec-1">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="core-values-wrapper flex-wrapper flex-align-start flex-justify-sb mxw-1060">
          <div class="cv-left">
            <div class="cv-single cv-vision" data-aos="fade-right" data-aos-delay="200">
              <img src="<?= (!empty($about_us[0]['section2_image'])?base_url('images/about_us/').$about_us[0]['section2_image']:"") ?>" alt="" class="value-icon">
              <h3 class="title-3 bold txt-blue mt16 mb16"><?= (!empty($about_us[0]['section2_heading'])?$about_us[0]['section2_heading']:"") ?></h3>
              <div class="txtm">
              <?= (!empty($about_us[0]['section2_description'])?$about_us[0]['section2_description']:"") ?>
              </div>
            </div>
            <div class="cv-single cv-mission" data-aos="fade-right" data-aos-delay="200">
              <img src="<?= (!empty($about_us[0]['section3_image'])?base_url('images/about_us/').$about_us[0]['section3_image']:"") ?>" alt="" class="value-icon">
              <h3 class="title-3 bold txt-blue mt16 mb16"><?= (!empty($about_us[0]['section3_heading'])?$about_us[0]['section3_heading']:"") ?></h3>
              <div class="txtm">
              <?= (!empty($about_us[0]['section3_description'])?$about_us[0]['section3_description']:"") ?>
              </div>
            </div>
          </div>
          <div class="cv-right">
            <div class="cv-single cv-values" data-aos="fade-left" data-aos-delay="200">
              <img src="<?= (!empty($about_us[0]['section4_image'])?base_url('images/about_us/').$about_us[0]['section4_image']:"") ?>" alt="" class="value-icon">
              <h3 class="title-3 bold mt16 mb16"><?= (!empty($about_us[0]['section4_heading'])?$about_us[0]['section4_heading']:"") ?></h3>
              <div class="txtm">
              <?= (!empty($about_us[0]['section4_description'])?$about_us[0]['section4_description']:"") ?>
              </div>
              <!-- <ul class="core-values-list">
                <li>Excellence in our endeavours</li>
                <li>Diversity in our beliefs</li>
                <li>Dignity, integrity, civility in our conduct</li>
                <li>Fostering personal, professional growth</li>
              </ul> -->
              <?= (!empty($about_us[0]['section4_sub_description'])?$about_us[0]['section4_sub_description']:"") ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<section class="sec-testimonial sec-testimonial-about">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start " data-aos="fade-right">
            <img src="<?php echo base_url();?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
            <div class="tq-text">
              <h3 class="title-1 bold txt-white mb24"><?= (!empty($about_us[0]['section5_heading'])?$about_us[0]['section5_heading']:"") ?></h3>
              <h4 class="title-4 bold txt-white tq-name"><?= (!empty($about_us[0]['section5_description'])?$about_us[0]['section5_description']:"") ?></h4>
            </div>
          </div>
          <h2 class="testimonial-title" data-aos="fade-up" data-aos-delay="100"><?= (!empty($about_us[0]['section6_trans_heading'])?$about_us[0]['section6_trans_heading']:"") ?></h2>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-about-3">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <div class="why-choose-us-wrapper">
            <img src="<?= (!empty($about_us[0]['section6_image'])?base_url('images/about_us/').$about_us[0]['section6_image']:"") ?>" alt="" class="why-us-img" data-aos="fade-up" data-aos-delay="100">
            <div class="why-us-bottom flex-wrapper flex-align-start flex-justify-sb mt32">
              <div class="wub-left" data-aos="fade-right" data-aos-delay="100">
                <h2 class="title-2 bold txt-blue"><?= (!empty($about_us[0]['section6_heading'])?$about_us[0]['section6_heading']:"") ?> <img src="<?php echo base_url();?>assets/images/about-why-us.svg" alt="" class="about-why-us-img"></h2>
              </div>
              <div class="wub-right" data-aos="fade-left" data-aos-delay="100">
                <div class="txtl">
                <?= (!empty($about_us[0]['section6_description'])?$about_us[0]['section6_description']:"") ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

</html>