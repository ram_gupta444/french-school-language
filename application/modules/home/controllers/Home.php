<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('homemodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		unset($_SESSION['language_id']);
		$_SESSION['last_stage'] = 'home';

		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$result = array();
		// get home content data 
		$condition = "1=1";
		$result['home'] = $this->common->getData("tbl_home",'*',$condition);
		$result['gallery_images'] = $this->common->getData("tbl_gallery_image",'*',$condition);
		$result['numbers'] = $this->common->getData("tbl_counts",'*',$condition);
		// $result['our_programs'] = $this->common->getData("tbl_our_program",'our_program_id,image,heading,description,button_text1,button_link1,button_text2,button_link2',$condition);

		if(!empty($_SESSION['language_id'])){
			$language_id =  $_SESSION['language_id'];
			$langcondition = "languages LIKE '%$language_id%' ";
		}else{
			$langcondition = "1 = 1";
		}
		$result['testimonial_videos'] = $this->common->getData("tbl_testimonial_video",'*',$langcondition);
		// echo "<pre>";
		// print_r($result['testimonial_videos']);
		// exit;
		$condition = "status = 'Active' ";
		$result['testimonial_details'] = $this->common->getData("tbl_testimonial",'*',$condition);
		
		$result['meta_description'] = $result['home'][0]['meta_description'];
		$result['meta_keywords'] = $result['home'][0]['meta_title'];

		$result['language_home'] = $this->common->getData("tbl_language_master",'*');

		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}
	function submitForm(Type $var = null)
	{
		// echo "<pre>";
		// print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['message'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter message'));
				exit;
			}
			$data = array();
			$data['message'] = (!empty($_POST['message'])) ? $_POST['message'] : '';
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = (!empty($_SESSION["fls_user"][0]->user_id)) ? $_SESSION["fls_user"][0]->user_id : '0';

			$result = $this->homemodel->insertData('tbl_messages', $data,'1');
			// admin mail body
			$admin_result['admindata'] = $this->homemodel->getdata1("tbl_users",'*',"role_id = 1");
			if (!empty($result)) {
				$this->email->from(FROM_MAIL, 'The Global Language School');
				$this->email->to($admin_result['admindata'][0]['email_id']);
				$this->email->subject('Stay connected form request');
				$content = $this->load->view("footer_mail",$data,true);
				$this->email->message($content);
				$this->email->send();
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Mail sent successfully..'
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in sending mail. Please try again..'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	public function cookies(){
		$cookie_name = "fls_cookie";
		$cookie_value = "fls_cookie";
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30 + 360), "/");
	}

	function termsCondition()
	{
		$result = array();
		// get home content data 
		$condition = "1=1";
		$result['terms_condition'] = $this->common->getData("tbl_terms_conditions",'*',$condition);
		// echo "<pre>";print_r($result['terms_condition']);exit;
		$this->load->view('template/header.php');
		$this->load->view('termsCondition',$result);
		$this->load->view('template/footer.php');
	}

}

?>
