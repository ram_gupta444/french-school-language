<section class="sec-homepage-banner">
  <div class="container">
    <div class="row">
      <div class="col-12 plm0 prm0">
        <div class="homepage-banner-wrapper" >
          <img src="<?= (!empty($home[0]['banner_image'])?base_url('images/home/').$home[0]['banner_image']:"") ?>" alt="" class="img-fluid" data-aos="fade-up">
          <div class="homepage-banner-text">
            <h1 class="title-1 bold txt-orange mb32"><?= (!empty($home[0]['banner_heading'])?$home[0]['banner_heading']:"") ?></h1>
            <a href="<?= (!empty($home[0]['button_link'])?$home[0]['button_link']:"") ?>" class="btn-fls btn-large btn-fls-secondary"><?= (!empty($home[0]['button_text'])?$home[0]['button_text']:"") ?></a>
          </div>
        </div>
      </div>
      <div class="col-12">
        <div class="homepage-about-wrapper" data-aos="fade-up">
          <h2 class="title-2 bold txt-orange"><?= (!empty($home[0]['section2_heading'])?$home[0]['section2_heading']:"") ?></h2>
          <img src="<?PHP echo base_url();?>assets/images/about-title-border.svg" alt="" class="img-fluid">
          <div class="ha-text txtm">
            <p><?= (!empty($home[0]['section2_description'])?$home[0]['section2_description']:"") ?></p>
            <a href="<?= (!empty($home[0]['knowMorebutton_link'])?$home[0]['knowMorebutton_link']:"") ?>" class="link-bold"><?= (!empty($home[0]['knowMorebutton_text'])?$home[0]['knowMorebutton_text']:"") ?><img src="<?PHP echo base_url();?>assets/images/know-more-icon.svg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single sec-home-whyus">
  <div class="sec-home-whyus-bg"></div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060 home-whyus-container">
          <div class="home-whyus-wrapper-top flex-wrapper flex-align-top flex-justify-sb mb24">
            <div class="hwwt-left" data-aos="fade-right">
              <h2 class="title-2 bold txt-orange" ><?= (!empty($home[0]['section3_heading'])?$home[0]['section3_heading']:"") ?>
              <img src="<?= base_url()?>assets/images/learn-languages-title.svg" alt="" class="hwwt-title-img">
            </h2>
            </div>
            <div class="hwwt-right txtm" data-aos="fade-left">
              <p><?= (!empty($home[0]['section3_description'])?$home[0]['section3_description']:"") ?></p>
            </div>
          </div>
          <h4 class="title-4 bold txt-blue title-reasons mb24" data-aos="fade-up"><?= (!empty($home[0]['section3_sub_heading'])?$home[0]['section3_sub_heading']:"") ?></h4>
          <div class="whyus-steps-wrapper flex-wrapper flex-align-top flex-justify">
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="200">
              <h4>01</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($home[0]['section3_heading1'])?$home[0]['section3_heading1']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($home[0]['section3_description1'])?$home[0]['section3_description1']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="400">
              <h4>02</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($home[0]['section3_heading2'])?$home[0]['section3_heading2']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($home[0]['section3_description2'])?$home[0]['section3_description2']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="600">
              <h4>03</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($home[0]['section3_heading3'])?$home[0]['section3_heading3']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($home[0]['section3_description3'])?$home[0]['section3_description3']:"") ?></p>
              </div>
            </div>
            <div class="whyus-step-single" data-aos="fade-up" data-aos-delay="800">
              <h4>04</h4>
              <h3 class="title-3 bold txt-blue mb16"><?= (!empty($home[0]['section3_heading4'])?$home[0]['section3_heading4']:"") ?></h3>
              <div class="txtm">
                <p><?= (!empty($home[0]['section3_description4'])?$home[0]['section3_description4']:"") ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="sec-single sec-home-programs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-2 bold txt-orange mb24" data-aos="fade-up">Our Programs <img src="<?PHP echo base_url();?>assets/images/home-programs-arrow.png" alt="" class="hp-arrow"></h2>
        <div class="home-programs-wrapper flex-wrapper flex-align-top flex-justify-sb">
          <?php if(!empty($our_programs)){
            foreach ($our_programs as $key => $value) { ?>
              <div class="hp-single" data-aos="fade-up" data-aos-delay="200">
                <img src="<?= (!empty($value['image'])?base_url('images/our_program/').$value['image']:"") ?>" alt="" class="hp-single-img">
                <h3 class="title-3 bold txt-blue mt16 mb8"><?= (!empty($value['heading'])?$value['heading']:"") ?></h3>
                <div class="txtm">
                  <p><?= (!empty($value['description'])?$value['description']:"") ?></p>
                </div>
                <div class="hp-cta flex-wrapper flex-align-center flex-justify-sb mt16">
                  <?php if(!empty($value['button_text1'])){ ?>
                    <a href="<?= (!empty($value['button_link1'])?$value['button_link1']:"") ?>" class="btn-fls btn-large btn-fls-primary <?= (empty($value['button_text2'])?"w100":"") ?>"><?= (!empty($value['button_text1'])?$value['button_text1']:"") ?></a>
                 <?php  }?>
                  
                 <?php if(!empty($value['button_text2'])){ ?>
                  <a href="<?= (!empty($value['button_link2'])?$value['button_link2']:"") ?>" class="btn-fls btn-large btn-fls-secondary"><?= (!empty($value['button_text2'])?$value['button_text2']:"") ?></a>
                  <?php  }?>
                </div>
              </div>
           <?php }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section> -->
<section class="sec-single sec-home-languages">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="languages-wrapper">
          <h2 class="title-2 bold txt-orange text-center mb16" data-aos="fade-in">Languages you can learn with us</h2>
          <h4 class="title-4 bold text-center mb32" data-aos="fade-in" data-aos-delay="200">Let's find the perfect classes for you</h4>
          <div class="language-list flex-wrapper flex-align-center flex-justify-sa">
          <?php if(!empty($language_home)){
            foreach ($language_home as $key => $value) { 
              $sel='';
              if(!empty($_SESSION['language_id'])){
                $sel= ($value['language_master_id'] == $_SESSION['language_id'] )?'selected':'';
              }
            ?>
            <a href="<?= base_url().trim($value['slug']).'/'.trim($value['slug']).'_home'?>" <?=$sel?> class="language-single" data-aos="fade-right">
            <img src="<?= (!empty($value['language_logo'])?base_url('images/language_logo/').$value['language_logo']:"") ?>" alt="">
              <span class="title-4 bold"><?= (!empty($value['language_name'])?$value['language_name']:"") ?></span></a>
            <?php }
          }
          ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="<?PHP echo base_url();?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24"><?= (!empty($home[0]['section4_heading'])?$home[0]['section4_heading']:"") ?></h3>
            <h4 class="title-4 bold txt-white tq-name"><?= (!empty($home[0]['section4_sub_heading'])?$home[0]['section4_sub_heading']:"") ?></h4>
          </div>
        </div>
        <h2 class="testimonial-title" data-aos="fade-up" data-aos-delay="100">Testimonials</h2>
      </div>
    </div>
  </div>
</section>
<section class="sec-home-testimonial">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="home-testimonial-wrapper">
          <div class="ht-left">
            <div class="ht-carousel-wrapper" data-aos="fade-up">
              <div class="testimonial-slider owl-carousel">
                <?php if(!empty($testimonial_details)){
                  foreach ($testimonial_details as $key => $value) { ?>
                    <div class="testimonial-single">
                    <div class="testimonial-text txtm">
                      <p><?= $value['testimonial_description']; ?></p>
                    </div>
                    <div class="testimonial-name">
                      <h4 class="title-4 bold"><?= $value['testimonialname']; ?></h4>
                    </div>
                  </div>
                 <?php }
                  }?>
              </div>
            </div>
          </div>
          <div class="ht-right">
          <div class="video-testimonial-wrapper owl-carousel flex-wrapper flex-align-start flex-justify-sb">
          <?php if(!empty($testimonial_videos)){
              foreach ($testimonial_videos as $key => $value) { ?>
                <div class="vt-single" data-aos="fade-down-right">
                  <!-- <img src="<?PHP echo base_url();?>assets/images/vt-img.png" alt="" class="vt-img"> -->
                  
                  <iframe src="<?= $value['url']?>" width="100%" height="187" frameborder="0" allowfullscreen></iframe>
                  <div class="vt-info-wrapper flex-wrapper flex-align-center flex-justify-sb mt8">
                    <h4 class="title-4 bold txt-black"><?= (!empty($value['title'])? $value['title']:"") ?></h4>
                    <!-- <a href="#/"><img src="<?PHP echo base_url();?>assets/images/play-icon-outline-blue.svg" alt=""></a> -->
                    <!-- <img src="<?php echo base_url("assets/images/play-icon-outline-blue.svg")?>" alt="" onclick="playVido('<?= $value['url']?>')" title="Play Video" data-toggle="tooltip"> -->
                  </div>
                </div>
             <?php }
            }?>
    
          </div>
          <div class="counter-wrapper flex-wrapper flex-align-center flex-justify-center">
            <?php if(!empty($numbers)){
              $delay = 100;
              foreach ($numbers as $key => $value) { ?>
                <div class="cw-single txt-white" data-aos="fade-up" data-aos-delay="<?= $delay?>">
                <h2><?= (!empty($value['numbers'])?$value['numbers']:"") ?>+</h2>
                <h4 class="title-4 bold"><?= (!empty($value['title'])?$value['title']:"") ?></h4>
              </div>
             <?php $delay +=100; }
             }
              ?>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Modal -->
<div class="modal fade" id="bsModal3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
          </div>
            <iframe  scrolling="no" class="embed-responsive-item" id="modulevideodiv" width="100%"  height="450px" frameborder="2" allowfullscreen ></iframe>
            <div class="clear-fix"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>

</div>
</body>
</html>

<script>
$(document).ready(function(){
  $(document).on("click",".modal-backdrop, #bsModal3 .close, #bsModal3 .btn", function() {
          $("#bsModal3 iframe").attr("src", $("#bsModal3 iframe").attr("src"));
  });
  $("#bsModal3").on('hidden.bs.modal', function (e) {
    console.log($("#bsModal3 iframe").attr("src"));
      $("#bsModal3 iframe").attr("src", $("#bsModal3 iframe").attr("src"));
  });
})

function playVido(video_link){
	if(video_link != "" && video_link != "undefined" && video_link != "null"){
    $("#modulevideodiv").prop("src",video_link);
    $("#bsModal3").modal('show');
	}
}


</script>

<?php 
if(!empty($_SESSION['toast'])){
  if($_SESSION['toast'] == 'yes'){ ?>
    <script>
     $.toast({
     heading: 'Thank you!',
     text: 'Email address verified.',
     icon: 'success',
     loader: false,        // Change it to false to disable loader
     loaderBg: '#002291',  // To change the background
     position: 'top-right',
     stack: false
     })
 </script>
 <?php 
 unset($_SESSION["toast"]); 
     
 }
}

?>