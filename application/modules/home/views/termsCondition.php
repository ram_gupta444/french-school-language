<section class="page-container">
	<section class="page-section pt-50 pb-50 ptmob-25 pbmob-25" id="terms-conditions">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
					<div class="tnc-wrapper">
						<?= $terms_condition[0]['terms_conditions']; ?>
						<div class="info-msg-wrapper">
							<img src="images/informartion.svg" alt="">
							<span>This policy was last modified on <?= date("M, d Y",strtotime($terms_condition[0]['created_on']))?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
