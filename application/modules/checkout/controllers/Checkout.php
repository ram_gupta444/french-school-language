<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Checkout extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('checkoutmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->library('paypal_lib');
		// checklogin();
		define('url_sandbox','api-m.sandbox.paypal.com');
		//define('url_live','paypal.com');
	}

	function index(){
		if(empty($_POST['hidden_event_id']) && empty($_POST['hidden_class_id'])){
			redirect(base_url());
		}
		$_SESSION['last_stage'] = "checkout";
    
		$result = array();
		// echo "<pre>";print_r($_SESSION['fls_user'][0]['user_id']);exit;
		if($_POST['type'] == 'Event'){
			$event_id = $_POST['hidden_event_id'];
			$condition = "events_id = $event_id ";
		
			$result['event_detail'] = $this->common->getData("tbl_events",'event_fees,no_of_seet,headline,intro_text,events_id',$condition);
		}
		if($_POST['type'] == 'Class'){
			$result['schedule_id'] = $_POST['hidden_schedule_id'];
			$class_id = $_POST['hidden_class_id'];
			$condition = "classes_id = $class_id ";
		
			$result['class_detail'] = $this->common->getData("tbl_classes",'class_fees,no_of_seet,headline,intro_text,classes_id',$condition);
		}
		
		$result['reg_count'] = $_POST['reg_count'];
		$result['type'] = $_POST['type'];

		if(!empty($_SESSION['fls_user'][0]['user_id']) ){
			$condition = " user_id = '".$_SESSION['fls_user'][0]['user_id']."' ";
			$result['login_user'] = $this->common->getData('tbl_users','*',$condition);

			$condition = " user_id = '".$_SESSION['fls_user'][0]['user_id']."' ORDER BY registration_id  DESC limit 1";
			$result['register_user'] = $this->common->getData('tbl_registration','*',$condition);
			// echo $this->db->last_query();exit;
		}
		// echo "<pre>";print_r($result['login_user']);exit;
    $result['language_home'] = $this->common->getData("tbl_language_master",'*');
    $result['counties'] = $this->common->getData("countries",'*');
		$this->load->view('template/header.php',$result);
		if($_POST['type'] == 'Event'){
			$this->load->view('index',$result);
		}
		if($_POST['type'] == 'Class'){
			$this->load->view('classIndex',$result);
		}
		$this->load->view('template/footer.php');
	}
	
//

	function buy()
	{
    // echo "<pre>";print_r($_POST);exit;
		$_SESSION["registration_id"] = md5(date("m-d-Y H:i:s.u"));
		if(!empty($_SESSION['fls_user'][0]['user_id']) ){
			$user_id = $_SESSION['fls_user'][0]['user_id'];
		}else{
			$data_user = array(); 
			$data_user['email_id'] = (!empty($_POST['email_id'][0])) ? $_POST['email_id'][0] : '';
			$data_user['full_name'] = $_POST['first_name'][0].' '.$_POST['last_name'][0];
			$data_user['role_id'] = '2';
			$data_user['password'] = md5($_POST['password'][0]);
			$data_user['is_email_verified'] = 'Yes';
			$data_user['status'] = 'Active';
			$data_user['updated_on'] = date("Y-m-d H:i:s");
			$data_user['created_on'] = date("Y-m-d H:i:s");
			$result_user = $this->common->insertData('tbl_users',$data_user,'1');
			$user_id = $result_user;
		}
		
		$data = array();
		foreach($_POST['first_name'] as $key=>$value){
			$data['user_id'] = $user_id;
			$data['type'] = (!empty($_POST['type'])) ? $_POST['type'] : '';
			$data['first_name'] = (!empty($_POST['first_name'])) ? $_POST['first_name'][$key] : '';
			$data['last_name'] = (!empty($_POST['last_name'][$key])) ? $_POST['last_name'][$key] : '';
			$data['email_id'] = (!empty($_POST['email_id'][$key])) ? $_POST['email_id'][$key] : '';
			$data['phone_no'] = (!empty($_POST['phone_no'][$key])) ? $_POST['phone_no'][$key] : '';
			$data['address'] = (!empty($_POST['address'][$key])) ? $_POST['address'][$key] : '';
			$data['country'] = (!empty($_POST['country'][$key])) ? $_POST['country'][$key] : '';
			$data['state'] = (!empty($_POST['state'][$key])) ? $_POST['state'][$key] : '';
			$data['city'] = (!empty($_POST['city'][$key])) ? $_POST['city'][$key] : '';
			$data['zip_code'] = (!empty($_POST['zip_code'][$key])) ? $_POST['zip_code'][$key] : '';
			$data['company_name'] = (!empty($_POST['company_name'][$key])) ? $_POST['company_name'][$key] : '';
			$data['event_id'] = (!empty($_POST['hidden_event_id'])) ? $_POST['hidden_event_id'] : '';
			$data['schedule_id'] = (!empty($_POST['hidden_schedule_id'])) ? $_POST['hidden_schedule_id'] : '';
			$data['status'] = 'Active';
			$data['payment_status'] = 'ABANDONED';
			$data['session_id'] = $_SESSION["registration_id"];
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['language_id'] = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : '';
			
			$result = $this->checkoutmodel->insertData('tbl_registration', $data,'1');
		}
		
        // Get product data from the database 
		if($_POST['type'] == 'Event'){
			$event = $this->checkoutmodel->getRows($_POST['hidden_event_id']);
			$event_class_id = $event['events_id'];
			$amt = ($_POST['totaluser'] * $event['event_fees']);
			$_SESSION['event_id'] = $event['events_id'];
			$_SESSION['headline'] = $event['headline'];
		}
		
		if($_POST['type'] == 'Class'){
			$class = $this->checkoutmodel->getRowsClass($_POST['hidden_class_id']);
			$event_class_id = $class['classes_id'];
			$amt = ($_POST['totaluser'] * $class['class_fees']);
			$_SESSION['class_id'] = $class['classes_id'];
			$_SESSION['headline'] = $class['headline'];
		}
		$_SESSION['checkOutUserId'] = $user_id;
        // Get current user ID from the session (optional) 
        $type =     $_POST['type'];
        $_SESSION['type'] = $_POST['type'];
        $returnURL = base_url().'checkout/success'; //payment success url 
        $cancelURL = base_url().'checkout/cancel'; //payment cancel url 
        // $notifyURL = base_url().'checkout/ipn'; //ipn url 
        // check authentication 
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/oauth2/token',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Basic QWRIVjlYRlV5TTNjUU1IRHk5eTdXM1gxM19ZSUxtRm9jWHVWQUk2Q1I4UElZZU9sQTU1MDFIdk15aWwwNGZsZXI2SkdwZld4SUFybTRxUnQ6RUZfRF8zaUNVWmRTdnBiaGtwNVFhVVFzUjVFY2x0MmR2YlRBQS1kakRMVUIwTDZ4Vk1pcE1JRXNJNzhUQmNNOFpyNlQ3X0txRnpCYWJxRHA=',
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        
        $auth_response = json_decode(curl_exec($curl));
        
        $access_token = $auth_response->access_token;
        curl_close($curl);
        if($access_token){
        // for create order 
         $postfield = '';
         $postfield ='{
                      "intent": "CAPTURE",
                      "purchase_units": [
                        {
                       "reference_id":"'.$event_class_id.'",
                        "description":"shiv description",
                          "amount": {
                            "currency_code": "SGD",
                            "value":"'.$amt.'"
                          },
                         "description":"Order description here",
                        "soft_descriptor":"Brand Name"
                        }
                      ],
                       "application_context": {
                        "brand_name": "fls",
                        "locale": "en-IN",
                        "shipping_preference": "NO_SHIPPING",
                        "user_action": "PAY_NOW",
                        "return_url": "'.$returnURL.'",
                        "cancel_url": "'.$cancelURL.'"
                      }
                    }';
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v2/checkout/orders',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$postfield,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer ' .$access_token,
          ),
        ));
        
        $response = curl_exec($curl);

        $CreateResponse = curl_exec($curl);
        $CreateResponse = json_decode($CreateResponse);
        curl_close($curl);
        // print_r($CreateResponse);
        // exit;
        redirect($CreateResponse->links[1]->href);
        exit;
        }else{
            // echo redirect the page to previous screen 
        }
	}

	function success(){ 
        // Get the transaction data 
        $paypalInfo = $this->input->get(); 
        if($paypalInfo['token']){
            $curl = curl_init();
              curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/oauth2/token',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Basic QWRIVjlYRlV5TTNjUU1IRHk5eTdXM1gxM19ZSUxtRm9jWHVWQUk2Q1I4UElZZU9sQTU1MDFIdk15aWwwNGZsZXI2SkdwZld4SUFybTRxUnQ6RUZfRF8zaUNVWmRTdnBiaGtwNVFhVVFzUjVFY2x0MmR2YlRBQS1kakRMVUIwTDZ4Vk1pcE1JRXNJNzhUQmNNOFpyNlQ3X0txRnpCYWJxRHA=',
                'Content-Type: application/x-www-form-urlencoded'
              ),
            ));
            
            $auth_response = json_decode(curl_exec($curl));
            $access_token = $auth_response->access_token;
            curl_close($curl);
            if($access_token){
                 $curl = curl_init();
            
                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v2/checkout/orders/'.$paypalInfo['token'].'/capture',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                     'Authorization: Bearer ' .$access_token,
                  ),
                ));
                
                $final_response = json_decode(curl_exec($curl));
                curl_close($curl);
                if($final_response->status == 'COMPLETED'){
                    $item_name = ''; 
                    $item_number = ''; 
                    $txn_id = $final_response->purchase_units[0]->payments->captures[0]->id; 
                    $payment_amt = $final_response->purchase_units[0]->payments->captures[0]->amount->value; 
                    $currency_code = $final_response->purchase_units[0]->payments->captures[0]->amount->currency_code;  
                    $status = $final_response->status;     
                    $payment_id = $final_response->id;     
                    $type = $_SESSION['type'];     
                    $payer_name = $final_response->payer->name->given_name.' '.$final_response->payer->name->surname;     
                    $payer_email = $final_response->payer->email_address;     
                }else{
                    $this->cancel();
                }
            // echo "<pre>";
            // echo "$final_response";
            // print_r($final_response);
            // exit;
            }
        }
        //  echo "<pre>";print_r($paypalInfo);exit;
        $productData = $paymentData = array(); 
			$data =array();
			// Insert the transaction data in the database 
			$data['user_id']    = $_SESSION['checkOutUserId']; 
			$data['session_id']    = $_SESSION["registration_id"]; 
			if($type == 'Event'){
				$data['event_id']    = $_SESSION['event_id'];
			}
			if($type == 'Class'){
				$data['class_id']    = $_SESSION['class_id'];
			}
			 
			$data['txn_id']    = $txn_id; 
			$data['payment_gross']    = $payment_amt; 
			$data['currency_code']    = $currency_code; 
			$data['payer_name']    = $payer_name; 
			$data['payer_email']    = $payer_email; 
			$data['status'] = $status; 
			$this->checkoutmodel->insertTransaction($data);
			$update_data = array();
			$update_data['updated_on'] = date("Y-m-d H:i:s");
			$update_data['payment_status'] = $status;
			
			$condition = array('user_id' => $_SESSION['checkOutUserId'], 'type' => $type, 'session_id' => $_SESSION["registration_id"]);

			$result = $this->checkoutmodel->updateRecord('tbl_registration', $update_data,$condition);
			// booking confirmation mail
			
			$registeredData = $this->checkoutmodel->getdata('tbl_registration','email_id,first_name,last_name,type,phone_no,address',$condition);
			
            // Get product info from the database 
			if($type == 'Event'){
        $productData = $this->checkoutmodel->getRows($_SESSION['event_id']); 
			}
			if($type == 'Class'){
			  $productData = $this->checkoutmodel->getRowsClass($_SESSION['class_id']); 
			}
        // Check if transaction data exists with the same TXN ID 
        $paymentData = $this->checkoutmodel->getPayment(array('txn_id' => $txn_id)); 
         
         
        // Pass the transaction data to view 
        $data['product'] = $productData; 
        $data['payment'] = $paymentData; 
        // admin mail 
        $admin_result['admindata'] = $this->checkoutmodel->getdata1("tbl_users",'email_id',"role_id = 1");
        $register_data = array();
        foreach ($registeredData as $key => $value) {
          $register_data['first_name'] = $value['first_name'];
          $register_data['last_name'] = $value['last_name'];
          // $register_data['mobile_no'] = $value['phone_no'];
          // $register_data['email'] = $value['email_id'];
          // $register_data['address'] = $value['address'];
          $register_data['registeredData'] = $registeredData;
          $register_data['type'] = $value['type'];
          $register_data['product'] = $productData; 
          $register_data['payment'] = $paymentData; 
          $this->email->from(FROM_MAIL, 'The Global Language Center');
          $this->email->to($value['email_id']);
          $this->email->cc($admin_result['admindata'][0]['email_id']);
          $this->email->subject('booking confirmation');
          $content = $this->load->view("booking",$register_data,true);
          $this->email->message($content);
          $this->email->send();
        }
        $data['language_home'] = $this->common->getData("tbl_language_master",'*');
        $this->load->view('template/header.php',$data);
        $this->load->view('success', $data); 
        $this->load->view('template/footer.php');
    } 
     function cancel(){ 
        // Load payment failed view 
        $data =array();
        $data['language_home'] = $this->common->getData("tbl_language_master",'*');
        $this->load->view('template/header.php',$data);
        $this->load->view('cancel'); 
        $this->load->view('template/footer.php');
     } 
     public function checkEmailId()
     {
      $condition = " email_id = '".$_POST['email_id']."'  ";

      if(!empty($_SESSION['fls_user'][0]['user_id']))
      {
        $condition .= " &&  user_id !='".$_SESSION['fls_user'][0]['user_id']."' ";
      }
      $result=$this->checkoutmodel->getdata1("tbl_users","email_id",$condition);
      // echo $this->db->last_query();exit;
      if($result>0){
        echo json_encode(array('success'=>false, 'msg'=>'1'));
      }else{
        echo json_encode(array('success'=>true, 'msg'=>'done'));
        // echo  json_encode(TRUE);
      } 
     }

     public function getState(){

      $condition =" country_id = '".$_POST['country_id']."' ";
      $result = $this->common->getData("states","*",$condition);
      
      $option = '<option value=""> Select State </option>';
      $state_id = '';
      
      // this is used from profile section code by shiv 
      if(isset($_REQUEST['state_id']) && !empty($_REQUEST['state_id'])){
        $state_id = $_REQUEST['state_id'];
      }
  
      if(!empty($result)){
        for($i=0;$i<sizeof($result);$i++){
          $sel = ($result[$i]['id'] == $state_id) ? 'selected' : '';
          $option .= '<option value="'.$result[$i]['id'].'" '.$sel.' >'.$result[$i]['name'].'</option>';
        }
      }
      echo json_encode(array("status"=>"success","option"=>$option));
      exit;
    }
   
}

?>
