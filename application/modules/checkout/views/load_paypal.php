<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->
  </head>

  <body>
    <!-- Include the PayPal JavaScript SDK; replace "test" with your own sandbox Business account app client ID -->
    <script src="https://www.paypal.com/sdk/js?client-id=AdHV9XFUyM3cQMHDy9y7W3X13_YILmFocXuVAI6CR8PIYeOlA5501HvMyil04fler6JGpfWxIArm4qRt&currency=USD"></script>

    <!-- Set up a container element for the button -->
    <div id="paypal-button-container"></div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        
      paypal.Buttons({

        // Sets up the transaction when a payment button is clicked
        createOrder: function(data, actions) {
          return actions.order.create({
            purchase_units: [{
              

                reference_id: "<?= !empty($item_number) ? $item_number : "" ?>",
                description: "<?= (!empty($item_name)) ? preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $item_name) : '' ?>",

                custom_id: "<?= !empty($type) ? $type : "" ?>",
                soft_descriptor: "<?= !empty($user_id) ? $user_id : "1" ?>",
              amount: {
                value: '<?= !empty($amount) ? $amount : "0" ?>', // Can reference variables or functions. Example: `value: document.getElementById('...').value`
              },
            
            }]
          });
        },

        // Finalize the transaction after payer approval
        onApprove: function(data, actions) {
          return actions.order.capture().then(function(orderData) {
            // Successful capture! For dev/demo purposes:
                console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                // var transaction = orderData.purchase_units[0].payments.captures[0];
                // alert('Transaction '+ transaction.status + ': ' + transaction.id + '\n\nSee console for all available details');
                $.ajax({
                    url:"<?php echo base_url();?>checkout/paypalResponse",
                    type:"POST",
                    data:{orderData:JSON.stringify(orderData, null, 2)},
                    success: function (response) {
                            var res = eval('('+response+')');
                            if(res['success'] == "1")
                            {
                                alert(res['msg']);
                                location.reload();
                            }
                            else
                            {	
                                alert(res['msg']);
                                location.reload();
                                return false;
                            }
                        }
                    })

            // When ready to go live, remove the alert and show a success message within this page. For example:
            // var element = document.getElementById('paypal-button-container');
            // element.innerHTML = '';
            // element.innerHTML = '<h3>Thank you for your payment!</h3>';
            // Or go to another URL:  actions.redirect('thank_you.html');
          });
        }
      }).render('#paypal-button-container');

    </script>
  </body>
</html>