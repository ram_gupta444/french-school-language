<?php 
$delay = 100;
foreach ($upcoming_event as $key => $value) {?>
    <div class="il-single" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
    <img src="<?= (!empty($value['banner_image'])?base_url('images/event/').$value['banner_image']:"") ?>" alt="" class="ils-img">
        <div class="ils-location-wrapper flex-wrapper flex-align-center flex-justify-sb mt16">
            <div class="location-name txts"><?= (!empty($value['location'])? $value['location']:"") ?></div>
            <div class="availability txts bold"><span class="seat-status available">6 Seats Available</span></div>
            <div class="date-time mt16 w100">
            <div class="class-label"><?= (!empty($value['day_and_date'])? date("l ,F j", strtotime($value['day_and_date'])):"") ?></div>
            <div class="class-label"><?= (!empty($value['time'])? $value['time']:"") ?></div>
            </div>
            <h4 class="txtl bold txt-blue mb8">Spin a yarn: The original storytelling</h4>
            <div class="txtm ils-desc">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </div>
            <div class="ils-cta mt16 w100">
            <a href="<?= base_url('immersive_event_detail/index?text='.rtrim(strtr(base64_encode("id=".$value['events_id']), '+/', '-_'), '=').'')?>" class="btn-fls btn-large btn-fls-tertiary w100">View Information</a>
            </div>
        </div>
    </div>
<?php 
$delay += 100;
}?>
