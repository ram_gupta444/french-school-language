<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
  <tr>
    <td valign="top" align="center">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
        <tr>
          <td valign="top" align="center">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <a href="#/" target="_blank" style="outline:none;">
                      <img src="<?= base_url()?>assets/images/tglc-logo.jpg" border="0" alt="" title="" style="width:70px;height:70px;"/>
                    </a>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="padding:0 30px 0 30px">
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:16px 0;">
                      Dear <?= $first_name.' '.$last_name ?>, </p>
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 16px 0;">Thank you for registering with The Global Language Center (TGLC). Please find below the information submitted by you. </p>
                    <table cellspacing="0" cellpadding="0" border="1" style="background:#fff;width:100%;">
                        <tr>
                            <th style="padding:0 10px">Name</th>
                            <th style="padding:0 10px">Mobile No.</th>
                            <th style="padding:0 10px">E-mail ID</th>
                            <th style="padding:0 10px">Programme/Event name</th>
                            <!-- <th style="padding:0 10px">Address</th> -->
                        </tr>
                        <?php foreach ($registeredData as $key => $value) { ?>
                          <tr>
                            <td style="padding:0 10px"><?= $value['first_name'].' '.$value['last_name'] ?></td>
                            <td style="padding:0 10px"><?= $value['phone_no']?></td>
                            <td style="padding:0 10px"><?= $value['email_id']?></td>
                            <td style="padding:0 10px"><?= (!empty($product['headline'])) ? trim(preg_replace('/\s+/', ' ', $product['headline'])) : "" ?></td>
                        </tr>
                        <?php }?>
                       
                    </table>
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:16px 0;">Our education advisor would get in touch with you in the next 48 hours. In case you may have other queries, please feel free to reach out to us on +65 89 50 60 85. </p>
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 8px 0;">Warm Regards,</p>
                    <p style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#333333;margin:0 0 16px 0;">The Global Language Center </p>
                  </div>
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  <div style="width:100%;background:#002290;margin:30px 0 0 0;padding:50px 30px;color:#fff;">
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      The <strong>Global French Language Centre</strong> is a borderless language learning institute in
                      Singapore that allows you to enjoy a unique and authentic French experience as we provide a
                      holistic experience for all levels and ages.
                    </p>
                    <p
                      style="font-family: Helvetica, Arial, sans-serif;font-size:14px;line-height:24px;color:#ffffff;margin:0 0 16px 0">
                      If you have any questions or concerns, we’re here to help.<br />
                      <strong>Contact us <a style="color:#fff;" href="mailto:info@thegloballanguagecenter.org" target="_blank">info@thegloballanguagecenter.<wbr>org</a> or +65 89506085</strong>
                    </p>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>