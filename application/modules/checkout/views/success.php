<?php if (!empty($payment)) { ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="title-1 bold txt-blue mt40 mb24 success">Your payment is successful!</h1>
      </div>
      <div class="col-12 col-md-6 col-lg-4">        
        <h4 class="title-4 bold txt-blue mb16">Payment Information</h4>
        <p class="txtm mb8"><strong>Reference Number:</strong> #<?php echo $payment['payments_id']; ?></p>
        <p class="txtm mb8"><strong>Transaction ID:</strong> <?php echo $payment['txn_id']; ?></p>
        <p class="txtm mb8"><strong>Paid Amount:</strong> <?php echo $payment['payment_gross'] . ' ' . $payment['currency_code']; ?></p>
        <p class="txtm mb16"><b>Payment Status:</b> <?php echo $payment['status']; ?></p>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <h4 class="title-4 bold txt-blue mb16">Payer Information</h4>
        <p class="txtm mb8"><strong>Name:</strong> <?php echo $payment['payer_name']; ?></p>
        <p class="txtm mb16"><strong>Email:</strong> <?php echo $payment['payer_email']; ?></p>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <h4 class="title-4 bold txt-blue mb16">Course Information</h4>
        <p class="txtm mb16"><strong>Course Name:</strong> <?php echo $product['headline']; ?></p>       
        <p class="txtm mb16"><strong>Price:</strong> <?php echo $payment['payment_gross'] . ' ' . $payment['currency_code']; ?></p>
      </div>
    </div>

  </div>

<?php } else { ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="error">Transaction failed. Please try again!</h1>
      </div>
    </div>
  </div>
<?php } ?>
<div class="container">
  <div class="row">
    <div class="col-12">
      <div id="center_button" class="mt24">
        <a class="btn-fls btn-large btn-fls-primary" href="<?php echo base_url(); ?>home">Go to Homepage</a>        
      </div>
    </div>
  </div>
</div> 