<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
	<tr>
		<td valign="top" align="center">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
				<tr>
					<td valign="top" align="left">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
							<tr>
								<td valign="top" align="left" colspan="2" >									
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Hi <?= $first_name.' '.$last_name ?>,</p>
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">congratulations <?= $type ?>  booked successfully</p>

									<p style="font-size:14px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;"><b>Payment Information</b></p>

									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Reference Number: #<?php echo $payment['payments_id']; ?></p>
									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Transaction ID: <?php echo $payment['txn_id']; ?></p>
									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Paid Amount: <?php echo $payment['payment_gross'].' '.$payment['currency_code']; ?></p>
									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Payment Status: <?php echo $payment['status']; ?></p>
									
									<p style="font-size:14px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;"><b>Payer Information</b></p>

									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Name: <?php echo $payment['payer_name']; ?></p>
									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Email: <?php echo $payment['payer_email']; ?></p>
									
									<p style="font-size:14px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;"><b>Product Information</b></p>

									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Name: <?= (!empty($product['headline'])) ? trim(preg_replace('/\s+/', ' ', $product['headline'])) : "" ?></p>
									<p style="font-size:12px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 12px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;text-transform: capitalize;">Price: <?php echo $payment['payment_gross'].' '.$payment['currency_code']; ?></p>

									
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">Regards,</p>	
									<p style="font-size:16px;color: #1A1A1A;letter-spacing: 0;line-height: 24px;margin:0 0 15px 0;font-family:'Barlow',sans-serif;padding:0 0 0 30px;">Fls Team
									</p>									
									
								</td>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>