<?PHP
class Checkoutmodel extends CI_Model
{
  function __construct() { 
      $this->transTable = 'tbl_payments'; 
  } 
	function upcommingEvents(){
        $this -> db -> select('e.*');
        $this -> db -> from('tbl_events as e');
        // $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this->db->where('e.day_and_date >=', date('Y-m-d'));
        $this->db->where('e.category','Adult');
		    // $this->db->group_by('c.classes_id');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function insertData($tbl_name,$data_array,$sendid = NULL)
      {
        $this->db->insert($tbl_name,$data_array);
        //print_r($this->db->last_query());
          //exit;
        $result_id = $this->db->insert_id();
        
        if($sendid == 1)
        {
          return $result_id;
        }
      }

      public function getRows($id = ''){ 
        $this->db->select('*'); 
        $this->db->from('tbl_events'); 
        $this->db->where('status', 'Active'); 
        if($id){ 
            $this->db->where('events_id', $id); 
            $query = $this->db->get(); 
            $result = $query->row_array(); 
        }else{ 
            $this->db->order_by('events_id', 'asc'); 
            $query = $this->db->get(); 
            $result = $query->result_array(); 
        } 
        // return fetched data 
        return !empty($result)?$result:false; 
    } 
    public function getRowsClass($id = ''){ 
      $this->db->select('*'); 
      $this->db->from('tbl_classes'); 
      $this->db->where('status', 'Active'); 
      if($id){ 
          $this->db->where('classes_id', $id); 
          $query = $this->db->get(); 
          $result = $query->row_array(); 
      }else{ 
          $this->db->order_by('classes_id', 'asc'); 
          $query = $this->db->get(); 
          $result = $query->result_array(); 
      } 
      // return fetched data 
      return !empty($result)?$result:false; 
  } 
  /* 
     * Fetch payment data from the database 
     * @param id returns a single record if specified, otherwise all records 
     */ 
    public function getPayment($conditions = array()){ 
      $this->db->select('*'); 
      $this->db->from('tbl_payments'); 
       
      if(!empty($conditions)){ 
          foreach($conditions as $key=>$val){ 
              $this->db->where($key, $val); 
          } 
      } 
       
      $result = $this->db->get(); 
      return ($result->num_rows() > 0)?$result->row_array():false; 
  } 
   
  /* 
   * Insert payment data in the database 
   * @param data array 
   */ 
  public function insertTransaction($data){ 
      $insert = $this->db->insert('tbl_payments',$data); 
      return $insert?true:false; 
  } 
  function updateRecord($tableName, $data, $condition)
  {
    $this->db->where($condition); 
    $this->db->update($tableName, $data);
    if ($this->db->affected_rows() > 0) {
      return true;
    }
    else {
      return true;
    }
  }

  function getdata($table, $select, $condition) {
		$this->db->select($select);
		$this->db->from($table);
    $this->db->where($condition);
		$query = $this->db->get();
		if($query->num_rows() >= 1){
      return $query->result_array();
		}else{
			return false;
		}
	}
  function getdata1($table, $fields, $condition){
    //echo "Select $fields from $table where $condition";exit;
    $sql = $this->db->query("Select $fields from $table where $condition");
    if($sql->num_rows() > 0){
      return $sql->result_array();
    }else{
      return false;
    }
  }

}
?>
