<?PHP
class Immersive_event_detailmodel extends CI_Model
{
	
	
      function other_event($event_id){
        $this -> db -> select('e.*');
        $this -> db -> from('tbl_events as e');
        // $this -> db -> join('tbl_schedule as s', 'c.classes_id  = s.class_drop_down');
        $this -> db -> join('tbl_similar_events as se', 'se.event_id  = e.events_id');
        // $this->db->where('s.starts_date >=', date('Y-m-d'));
        $this->db->where('se.events_id ',$event_id );
        $this->db->where('e.status ','Active');
		    // $this->db->group_by('s.class_drop_down');
        $query = $this -> db -> get();
        // print_r($this->db->last_query());exit;
          if($query -> num_rows() >= 1)
        {
          return $query->result_array();
        }
        else
        {
          return false;
        }
      }
      function getCustomList($event_id){
        $this -> db -> select('count(registration_id) as reg_count');
        $this -> db -> from('tbl_registration');
        $this -> db -> where('type ', 'Event');
        $this -> db -> where('event_id ', $event_id);
        $this -> db -> where('payment_status ', 'Completed');
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
          return $query->result_array();
        }
        else{
          return false;
        }
     }


}
?>
