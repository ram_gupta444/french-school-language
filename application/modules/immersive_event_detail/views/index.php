<section class="sec-ip-detail-top">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="event-detail-breabcrumb">
          <ol class="breadcrumb breadcrumb-chevron">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>immersive_events">Immersive Events</a></li>
            <li class="breadcrumb-item active">Event Details</li>
          </ol>
        </div>
        <div class="ip-detail-top-wrapper flex-wrapper flex-align-start flex-justify-start">
          <div class="ipd-left">
            <img src="<?= (!empty($adult_program_detail[0]['banner_image'])?base_url('images/event/').$adult_program_detail[0]['banner_image']:"") ?>" alt="" class="ipd-banner">
          </div>
          <div class="ipd-right">
            <div class="ipd-right-content">
              <div class="study-label-wrapper">
                <div class="all-group-label"><?= (!empty($adult_program_detail[0]['event_type'])? $adult_program_detail[0]['event_type']:"") ?></div>
                <div class="all-group-label"><?= (!empty($adult_program_detail[0]['category'])? $adult_program_detail[0]['category']:"") ?></div>
              </div>

              <div class="txtl txt-white mb16 location-address"><?= (!empty($adult_program_detail[0]['event_type']) && $adult_program_detail[0]['event_type'] == 'Online') ? "Zoom": (!empty($adult_program_detail[0]['location'])? $adult_program_detail[0]['location']:"") ?></div>
              <div class="flex-full txts txt-white"><span>Starts <?= (!empty($adult_program_detail[0]['day_and_date'])? date(" jS F Y", strtotime($adult_program_detail[0]['day_and_date'])):"") ?></span></div>
              <div class="flex-full txts txt-white mb16"><span><?= (!empty($adult_program_detail[0]['event_timeline'])? $adult_program_detail[0]['event_timeline']:"") ?></span></div>

              <!-- <div class="txts txt-white mb8">
                <span class="ie-date"><?= (!empty($adult_program_detail[0]['day_and_date'])? date("l ,F j", strtotime($adult_program_detail[0]['day_and_date'])):"") ?></span> | <span class="ie-time"><?= (!empty($adult_program_detail[0]['time'])? $adult_program_detail[0]['time']:"") ?></span>
              </div>
              <h3 class="title-3 bold txt-white" data-aos="fade-up" data-aos-delay="700"><?= (!empty($adult_program_detail[0]['location'])? $adult_program_detail[0]['location']:"") ?></h3>
              <div class="txtl txt-white mb40 location-address" data-aos="fade-up" data-aos-delay="700">
                <p>
                  Name of Venue 000, Name of Street
                  <br />(<a href="#/" class="txt-white">View Map</a>)
                </p>
              </div> -->
              
              <!-- <div class="availability txts bold"><span class="seat-status available"><?= ($adult_program_detail[0]['no_of_seet'] - $available_events[0]['reg_count'])?> Seats Available</span></div> -->

              <?php if(!empty($adult_program_detail[0]['no_of_seet'] - $available_events[0]['reg_count']) && ($adult_program_detail[0]['no_of_seet'] - $available_events[0]['reg_count']) != 0){ 
                $disabled = ''?>
                <div class="availability txts bold mb8"><span class="seat-status seat-status-detail available"><?= ($adult_program_detail[0]['no_of_seet'] - $available_events[0]['reg_count']) ?> Seats Available</span></div>
                <?php }else{ 
                  $disabled = 'disabled'?>
                    <div class="availability txts bold mb8"><span class="seat-status seat-status-detail  soldout">Sold out</span></div>
              <?php }?>
           
              <h2 class="gsd-title-cost title-2 bold flex-wrapper flex-align-center" data-aos="fade-up" data-aos-delay="900">S$<?= (!empty($adult_program_detail[0]['event_fees'])? $adult_program_detail[0]['event_fees']:"") ?><!--<span> (GST Included)</span>--></h2>

              <div class="ipd-cta">
                <form action="<?= base_url('checkout') ?>" method ="POST">
                  <div class="gsd-schedule-cta flex-wrapper flex-align-center flex-justify-start mt24" data-aos="fade-up" data-aos-delay="1200">
                      <input type="hidden" name="hidden_event_id" id="hidden_event_id" value="<?= $adult_program_detail[0]['events_id'] ?>">
                      <input type="hidden" name="type" id="type" value="Event">
                      <button type="submit" class="btn-fls btn-large btn-fls-tertiary <?= $disabled?> mr16" <?= $disabled?>>Register</button>
                      <?php if(empty($disabled)){ ?>
                        <select name="reg_count" id="reg_count" class="gsd-count">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                      </select>
                     <?php } ?>
                      
                  </div>				 
                </form>	
              </div>
            </div>
          </div>
        </div>
        <div class="ip-detail-bottom-wrapper flex-wrapper flex-align-start flex-justify-sb mt40">
          <div class="ipdb-left" data-aos="fade-right">
            <h3 class="title-3 bold txt-orange mb24"><?= (!empty($adult_program_detail[0]['headline'])? $adult_program_detail[0]['headline']:"") ?></h3>
            <div class="ipd-social">
              <a href="#/"><img src="<?= base_url()?>assets/images/fb-blue.svg" alt=""></a>
              <a href="#/"><img src="<?= base_url()?>assets/images/tw-blue.svg" alt=""></a>
              <a href="#/"><img src="<?= base_url()?>assets/images/li-blue.svg" alt=""></a>
              <a href="#/"><img src="<?= base_url()?>assets/images/ig-blue.svg" alt=""></a>
            </div>
          </div>
          <div class="ipdb-right" data-aos="fade-left">
            <div class="txtm">
            <?= (!empty($adult_program_detail[0]['intro_text'])? $adult_program_detail[0]['intro_text']:"") ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-single sec-ip-detail-2">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="title-2 bold txt-orange other-courses-title mb40" data-aos="fade-up">Other Events <img src="<?= base_url()?>assets/images/home-programs-arrow.png" alt="" class="other-courses-icon"></h2>
        <div class="immersive-list-wrapper flex-wrapper flex-align-start flex-justify-start">


            <?php 
            $delay = 100;
            foreach ($other_event as $key => $value) {?>
                <div class="il-single" data-aos="fade-up" data-aos-delay="<?= $delay;?>">
                <img src="<?= (!empty($value['banner_image'])?base_url('images/event/').$value['banner_image']:"") ?>" alt="" class="ils-img">
                    <div class="ils-location-wrapper ip-label-1 flex-wrapper flex-align-center flex-justify-sb mt16">
                        <div class="location-name txts">
                            <div class="all-group-label"><?= (!empty($value['event_type'])? $value['event_type']:"") ?></div>
                            <div class="all-group-label"><?= (!empty($value['category'])? $value['category']:"") ?></div>
                          </div>
                        <?php if(!empty($value['no_of_seet'] - $available_events_similar[$key][0]['reg_count']) && ($value['no_of_seet'] - $available_events_similar[$key][0]['reg_count']) != 0){ 
                          $disabled = ''?>
                            <div class="availability txts bold"><span class="seat-status available"><?= ($value['no_of_seet'] - $available_events_similar[$key][0]['reg_count']) ?> Seats Available</span></div>
                        <?php }else{ 
                          $disabled = 'disabled'?>
                            <div class="availability txts bold"><span class="seat-status soldout">Sold out</span></div>
                      <?php }?>
                      
                        <div class="meet-location txts mt8 medium"><?= (!empty($value['event_type']) && $value['event_type'] == 'Online') ? "Zoom": (!empty($value['location'])? $value['location']:"") ?></div>
                        <div class="flex-full txt-gold txts semibold"><span>Starts <?= (!empty($value['day_and_date'])? date(" jS F Y", strtotime($value['day_and_date'])):"") ?></span></div>
                          <div class="flex-full txt-gold txts semibold"><span><?= (!empty($value['event_timeline'])? $value['event_timeline']:"") ?></span></div>
                        <h4 class="flex-full txtl bold txt-blue mt8 mb8"><?= (!empty($value['headline'])? $value['headline']:"") ?></h4>
                        <div class="txtm ils-desc">
                        <?= (!empty($value['intro_text'])? $value['intro_text']:"") ?>
                        </div>
                        <div class="ils-cta mt16 w100">
                        <a href="<?= base_url('immersive_event_detail/index?text='.rtrim(strtr(base64_encode("id=".$value['events_id']), '+/', '-_'), '=').'')?>" class="btn-fls btn-large btn-fls-tertiary w100">View Information</a>
                        </div>
                    </div>
                </div>
            <?php 
            $delay += 100;
            }?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="sec-testimonial sec-testimonial-above-footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="testimonial-quote-wrapper flex-wrapper flex-align-start flex-justify-start" data-aos="fade-right">
          <img src="<?= base_url()?>assets/images/testimonial-comma.svg" alt="" class="tq-img">
          <div class="tq-text">
            <h3 class="title-1 bold txt-white mb24"><?= (!empty($adult_program[0]['section2_heading'])? $adult_program[0]['section2_heading']:"") ?></h3>
            <h4 class="title-4 bold txt-white tq-name"><?= (!empty($adult_program[0]['section2_sub_heading'])? $adult_program[0]['section2_sub_heading']:"") ?></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>