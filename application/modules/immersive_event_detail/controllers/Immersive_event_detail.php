<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Immersive_event_detail extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('immersive_event_detailmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		// checklogin();
	}

	function index(){
		unset($_SESSION['language_id']);
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;

		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
		
		$result = array();
		$event_id = "";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$event_id = $url_prams['id'];

			$condition = "events_id = '".$event_id."' ";
			$result['adult_program_detail'] = $this->common->getData("tbl_events",'*',$condition);
			foreach ($result['adult_program_detail'] as $key => $value) {
				$result['available_events'] = $this->immersive_event_detailmodel->getCustomList($value['events_id']);
			}
			// echo "<pre>";print_r($result['available_events']);exit;
		}
		$result['adult_program'] = $this->common->getData("tbl_adult_program",'adult_program_id,section2_heading,section2_sub_heading','1=1');
		$result['other_event'] = $this->immersive_event_detailmodel->other_event($event_id);

		if(!empty($result['other_event'])){
			foreach ($result['other_event'] as $key => $value) {
				$result['available_events_similar'][] = $this->immersive_event_detailmodel->getCustomList($value['events_id']);
			}
		}
		
// 		echo "<pre>";print_r($result['available_events']);exit;
		
		$this->load->view('template/header.php',$result);
		$this->load->view('index',$result);
		$this->load->view('template/footer.php');
	}

}

?>
