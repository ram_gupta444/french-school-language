<section class="sec-contact-us sec-bg">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="mxw-1060">
          <h1 class="title-1 bold txt-orange contact-us-title mb24" data-aos="fade-up">
          <?= (!empty($contact_us[0]['heading'])?$contact_us[0]['heading']:"") ?>
            <img src="<?php echo base_url();?>assets/images/contact-us-title.svg" alt="" class="cu-img">
          </h1>
          <div class="contact-us-wrapper flex-wrapper flex-align-start flex-justify-sb ">
            <div class="cu-left">
              <div class="txtm" data-aos="fade-up" data-aos-delay="300">
              <?= (!empty($contact_us[0]['description'])?$contact_us[0]['description']:"") ?>
              </div>
              <div class="contact-details-wrapper">
                <div class="cd-single flex-wrapper flex-align-start flex-justify-start" data-aos="fade-up" data-aos-delay="300">
                  <div class="cds-left">
                  <img src="<?= (!empty($contact_us[0]['section1_image'])?base_url('images/contact_us/').$contact_us[0]['section1_image']:"") ?>" alt="">
                  </div>
                  <div class="cds-right">
                    <div class="txtm"><?= (!empty($contact_us[0]['section1_phone'])?$contact_us[0]['section1_phone']:"") ?></div>
                  </div>
                </div>
                <div class="cd-single flex-wrapper flex-align-start flex-justify-start" data-aos="fade-up" data-aos-delay="600">
                  <div class="cds-left">
                    <img src="<?= (!empty($contact_us[0]['section2_image'])?base_url('images/contact_us/').$contact_us[0]['section2_image']:"") ?>" alt="">
                  </div>
                  <div class="cds-right">
                    <div class="txtm cds-email"><?= (!empty($contact_us[0]['section2_email'])?$contact_us[0]['section2_email']:"") ?></div>
                  </div>
                </div>
                <div class="cd-single flex-wrapper flex-align-start flex-justify-start" data-aos="fade-up" data-aos-delay="900">
                  <div class="cds-left">
                    <img src="<?= (!empty($contact_us[0]['section3_image'])?base_url('images/contact_us/').$contact_us[0]['section3_image']:"") ?>" alt="">
                  </div>
                  <div class="cds-right">
                    <div class="txtm"><?= (!empty($contact_us[0]['section3_description'])?$contact_us[0]['section3_description']:"") ?></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="cu-right">
              <div class="contact-form-wrapper" data-aos="fade-up" data-aos-delay="300">
                <form id="form-contact-us" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                  <input type="text" class="input-fls" id="name" name="name">
                    <label for="first_name">Name <sup>*</sup></label>
                    <label for="name" generated="true" class="error error-label"></label>
                  </div>
                  <div class="form-group">
                  <input type="text" class="input-fls" id="email" name="email">
                    <label for="email">Email <sup>*</sup></label>
                    <label for="email" generated="true" class="error error-label"></label>
                  </div>
                  <div class="form-group">
                  <input type="text" class="input-fls" id="phone_number" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="phone_number" maxlength="10" minlength="10">
                    <label for="phone">Phone <sup>*</sup></label>
                    <label for="phone_number" generated="true" class="error error-label"></label>
                  </div>
                  <div class="">
                    <select name="language" id="language" class="select-contact">
                    <option value="">Select language to learn</option>
                    <?php foreach ($laguages as $key => $value) { ?>
                      <option value="<?= $value['language_master_id']?>"><?= $value['language_name']?></option>
                    <?php } ?>
                    </select>
                  </div>
                  <div class="">
                    <select name="course" id="course" class="select-contact">
                      <option value="">Enquire About</option>
                      <option value="Request for a Free Evaluation">Request for a Free Evaluation</option>
                      <option value="Private Tuitions">Private Tuitions</option>
                      <option value="Group Classes">Group Classes</option>
                      <option value="Translation Services">Translation Services</option>
                      <option value="Other">Other</option>
                    </select>
                  </div>
                  <div style="clear:both;"></div> 
                  <div class="form-group">
                  <textarea class="input-fls" row="10" id="message" name="message" placeholder="Enter your message"></textarea>
                 <!--  <label for="message">Message</label> -->
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn-fls btn-large btn-fls-primary w100">Book a FREE Evaluation</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>
<script>
  // to store bespoke response  
  var vRules = {
        name: {
          required: true
      },
        email: {
          required: true
      },
      phone_number: {
          required: true
      },
      language: {
          required: true
      },
  };

  var vMessages = {

      name: {
          required: "Please enter name"
      },
      email: {
          required: "Please enter email"
      },
      phone_number: {
        required: "Please enter phone number"
      },
      language: {
        required: "Please select language"
      },

  };
 
  $("#form-contact-us").validate({
      rules: vRules,
      messages: vMessages,
      submitHandler: function (form) {
          var act = "<?php echo base_url();?>contact_us/submitResponse";
          // console.log(act);
          $("#form-contact-us").ajaxSubmit({
              url: act,
              type: 'POST',
              dataType: 'json',
              cache: false,
              clearForm: false,
             
              success: function (response) {
                  if (response.success) {
                      // alert(response.msg);
                      $.toast({
                        //heading: 'success',
                        text: response.msg,
                        icon: 'success',
                        loader: false,        // Change it to false to disable loader
                        loaderBg: '#002291',  // To change the background
                        position: 'top-right',
                        stack: false
                      })
                      setTimeout(function () {
                          window.location = "<?php echo base_url();?>contact_us";
                      }, 5000);
                  } else {
                      // alert(response.msg);
                      $.toast({
                        //heading: 'Error!',
                        text: response.msg,
                        icon: 'error',
                        loader: false,        // Change it to false to disable loader
                        loaderBg: '#bd0202',  // To change the background
                        position: 'top-right',
                        stack: false
                      })
                  }
              }
          });
      }
  });

</script>