<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Contact_us extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('contact_usmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
	}

	function index(){
		$_SESSION['last_stage'] = 'contact_us';
		unset($_SESSION['language_id']);
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
			$url = "https://";   
		}else{
			$url = "http://";   
		}	  
		// Append the host(domain name, ip) to the URL.   
		$url.= $_SERVER['HTTP_HOST'];   
		// Append the requested resource location to the URL   
		$url.= $_SERVER['REQUEST_URI'];
		$_SESSION['last_stage'] = $url;
		
		$cookie_name = "last_stage";
		$cookie_value = $url;
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

		$result = array();
		$condition = "1=1";
		$result['contact_us'] = $this->common->getData("tbl_contact",'*',$condition);

		$condition = "status = 'Active'  ";
		$result['laguages'] = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 
		// echo "<pre>";print_r($result);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$result);
		$this->load->view('template/footer_without_img.php');
	}

	public function submitResponse(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			if(!isset($_POST['language'])){
				echo json_encode(array('success'=>false, 'msg'=>'Please select language.'));
				exit;
			}
            $data = array(); 
			$data['name'] = ($this->input->post('name'));
			$data['email'] = $this->input->post('email');
			$data['phone_number'] = $this->input->post('phone_number');
			$data['message'] = $this->input->post('message');
			$data['created_on'] = date("Y-m-d H:i:s");
			// $condition = " email_template_id = 1 ";
			// $chekexist_result['existdata'] = $this->contact_usmodel->getdata1("tbl_email_template",'*',$condition);
			// echo "<pre>";print_r($chekexist_result['existdata'][0]['email_title']);exit;
			// user mail body
            $this->email->from(FROM_MAIL, 'The Global Language Center');
            $this->email->to($this->input->post('email'));
            $this->email->subject('Contact us enquiry');
           
            // $content = ('<p><strong>Thank you for getting in touch!&nbsp;</strong></p>
			// <p>We appreciate you contacting us. We will get back to you shortly!</p>');
			$content = $this->load->view("auto_generated_email",$data,true);
            $this->email->message($content);
			$this->email->send();

			// admin mail body
			$admin_result['admindata'] = $this->contact_usmodel->getdata1("tbl_users",'*',"role_id = 1");
			$this->email->from(FROM_MAIL, 'The Global Language Center'); 

			$get_language = $this->contact_usmodel->getdata1("tbl_language_master",'language_name',"language_master_id = '".$_POST['language']."' "); 
            $this->email->to($admin_result['admindata'][0]['email_id']);
            // $this->email->to('vinayak.joshi@attoinfotech.com');
            $this->email->subject('Contact Form Enquiry');
            $content1 = " '".$admin_result['admindata'][0]['full_name']."' you have recieved contact us request";
			$content1 = "Hello Admin, <br>";
			$content1 .= "Please find details of contact us enquiry below <br/>";
			$content1 .= "Name: ".$_POST['name']."<br>";
			$content1 .= "Email: ".$_POST['email']."<br>";
			$content1 .= "Phone: ".$_POST['phone_number']."<br>";
			$content1 .= "Language: ".$get_language[0]['language_name']."<br>";
			$content1 .= "Course: ".$_POST['course']."<br>";
			$content1 .= "Message: ".$_POST['message']."<br>";
			

            $this->email->message($content1);
			$this->email->send();
		
			$result = $this->common->insertData('tbl_contactus_response',$data,'1');
			
			if(!empty($result)){
				echo json_encode(array('success'=>true, 'msg'=>'Thank you for getting in touch with us'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

}

?>
