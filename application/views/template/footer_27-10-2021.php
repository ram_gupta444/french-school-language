
<footer>
  <div class="footer-top-img-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="fti-wrapper flex-wrapper flex-align-center flex-justify-sb">
            <?php 
            $condition ="1=1";
            $getgallery = $this->common->getData("tbl_gallery_image",'*',$condition); 
            if(!empty($getgallery)){ 
              $delay = 100;
              foreach ($getgallery as $key => $value) { 
                ?>
               <img src="<?= (!empty($value['image_name'])?base_url('images/gallery_images/').$value['image_name']:"") ?>" alt="" data-aos="fade-in" data-aos-delay="<?= $delay?>" >
             <?php $delay +=100;}
               }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-top-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="get-started-wrapper flex-wrapper flex-align-center flex-justify-sb" >
            <div class="gs-left" data-aos="fade-up">
              <h2 class="title-2 bold txt-orange">Let’s Get Started</h2>
            </div>
              <div class="gs-right">
                <div class="gs-form" data-aos="fade-up">
                  <!-- <form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data"> -->
                  <form>
                    <input type="text" class="gs-text" placeholder="Type your message here" name="message" id="message" required>
                    <button type="button" class="gs-btn" id="save-message">Next</button>
                  </form>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-links-container flex-wrapper flex-align-start flex-justify-sb" data-aos="fade-up">
            <div class="footer-single footer-logo-fls">
              <div class="footer-about-wrapper flex-wrapper flex-align-start flex-justify-sb">
                <div class="fa-left">
                  <img src="<?= base_url()?>assets/images/logo.svg" alt="" class="logo-footer">
                </div>
                <div class="fa-right">
                  <div class="about-footer-text txtm txt-white">
                    <p>Follow us on our social media channels to stay updated with our immersive events and more.</p>
                  </div>
                  <div class="footer-social mt16">
                    <a href="#/"><img src="<?= base_url()?>assets/images/fb-footer.svg" alt=""></a>
                    <a href="#/"><img src="<?= base_url()?>assets/images/tw-footer.svg" alt=""></a>
                    <a href="#/"><img src="<?= base_url()?>assets/images/ln-footer.svg" alt=""></a>
                    <a href="#/"><img src="<?= base_url()?>assets/images/in-footer.svg" alt=""></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-single footer-about-fls">
              <ul class="footer-links-list">
                <li><a href="<?php echo base_url();?>home">Home</a></li>
                <li><a href="<?php echo base_url();?>aboutus">About Us</a></li>
                <li><a href="<?php echo base_url();?>immersive_events">Immersive Events</a></li>
                <li><a href="<?= base_url()?>contact_us">Contact Us</a></li>
              </ul>
            </div>
            <div class="footer-single footer-adult-fls">
              <h4 class="txtm txt-white">Languages</h4>
              <ul class="footer-links-list">
                <?php 
                    $condition = "status = 'Active'  ";
                    $getLanguagefooter = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 
                ?>
                <?php foreach ($getLanguagefooter as $key => $value) { 
              ?>
              <li><a href="<?= base_url('language_home/index?text='.rtrim(strtr(base64_encode("id=".$value['language_master_id']), '+/', '-_'), '=').'')?>"><img src="assets/images/arrow-right-white.svg" alt="" class="footer-arrow-icon"> <span><?= $value['language_name']?></span></a></li>
               
              <?php } ?>
              </ul>
            </div>
            <div class="footer-single footer-contact-fls">
              <a href="<?= base_url()?>contact_us" class="btn-fls btn-large btn-fls-primary">Get in Touch</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-legal-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-legal flex-wrapper flex-align-center flex-justify-sb">
            <div class="fl-left txtm txt-white">
              <p>&copy; French Language School</p>
            </div>
            <div class="fl-right txtm txt-white">
              <a href="#/" class="txt-white">Privacy Policy</a> | <a href="#/" class="txt-white">Terms and Conditions</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</footer>



<script>
  $('#save-message').on('click', function() {
		var message = $('#message').val();
    if(message != ''){ 
		$.ajax({
			url:"<?php echo base_url();?>home/submitForm",
			type:"POST",
			data:{message:message},
			success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						alert(res['msg']);
            location.reload();
					}
					else
					{	
						alert(res['msg']);
            location.reload();
						return false;
					}
				}
			})
    }else{
      alert("Please enter message.");
    }
	});
</script>


