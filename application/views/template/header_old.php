<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no">
	<title>The French Lan</title>
	<link rel="icon" href="<?= base_url()?>assets/images/favicon.png" type="images/png" sizes="32x32">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/owl.carousel.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/animate.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/aos.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/main.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/responsive.css" />
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">

  <!-- <script src="<?php echo base_url();?>assets/js/core/jquery-2.1.4.min.js"></script> -->
  <script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
  <!-- <script type="text/javascript" src="<?PHP echo base_url();?>assets/js/jquery-ui.min.js"></script> -->
</head>
<body data-scroll-container>
<!-- Navigation -->
<header class="fls-nav-container">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="fls-nav-wrapper">
          <div class="nav-logo">
            <a href="index.php"><img src="<?= base_url()?>assets/images/logo.svg" alt=""></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarfls" aria-controls="navbargepl" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="nav-menu">
            <nav class="navbar navbar-expand-lg navbar-fls">
              <div class="collapse navbar-collapse" id="navbarfls">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url();?>home">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url();?>aboutus">About Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url();?>private_tuitions">Private Tuitions</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="adultlearning" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Adult Group Learning <img src="<?= base_url()?>assets/images/down-arrow-dark.svg" alt="">
                    </a>
                    <div class="dropdown-menu nav-fls-dropdown" aria-labelledby="adultlearning">
                      <a class="dropdown-item" href="<?php echo base_url();?>adult_group_study">Adult Group Study</a>
                      <a class="dropdown-item" href="<?php echo base_url();?>adult_immersive_program">Adult Immersive Program</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="childrenlearning" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Children Group Learning <img src="<?= base_url()?>assets/images/down-arrow-dark.svg" alt="">
                    </a>
                    <div class="dropdown-menu nav-fls-dropdown" aria-labelledby="childrenlearning">
                      <a class="dropdown-item" href="<?= base_url()?>children_group_study">Children Group Study</a>
                      <a class="dropdown-item" href="<?= base_url()?>children_immersive_program">Children Immersive Program</a>
                    </div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url()?>contact_us">Contact Us</a>
                  </li>
                  <li class="nav-item my-account-mobile">
                    <a class="nav-link" href="#/">My Account</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
          <div class="nav-account">
            <a href="#/" class="account-link">My Account <img src="<?= base_url()?>assets/images/user.svg" alt="" class="user-img"></a>
            <div class="my-account-popup nav-fls-dropdown">
              <a href="myaccount-details.php">My Profile</a>
              <a href="myaccount-booked-events.php">Booked Events</a>
              <a href="myaccount-booked-classes.php">Booked Classes</a>
              <a href="login.php">Log Out</a>
              <a href="<?= base_url()?>login_register">Login</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="popup-overlay"></div>
<!-- My Account Mobile-->
<div class="my-account-popup-mobile nav-fls-dropdown">
  <a href="myaccount-details.php">My Profile</a>
  <a href="myaccount-booked-events.php">Booked Events</a>
  <a href="myaccount-booked-classes.php">Booked Classes</a>
  <a href="login.php">Log Out</a>
</div>
<!-- My Account Mobile-->
<div class="content-push"></div>