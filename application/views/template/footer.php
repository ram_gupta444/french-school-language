
<footer>
  <div class="footer-top-img-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="fti-wrapper flex-wrapper flex-align-center flex-justify-sb">
            <?php 
            $condition ="1=1";
            $getgallery = $this->common->getData("tbl_gallery_image",'*',$condition); 
            if(!empty($getgallery)){ 
              $delay = 100;
              foreach ($getgallery as $key => $value) { 
                ?>
               <img src="<?= (!empty($value['image_name'])?base_url('images/gallery_images/').$value['image_name']:"") ?>" alt="" data-aos="fade-in" data-aos-delay="<?= $delay?>" >
             <?php $delay +=100;}
               }
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-top-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="get-started-wrapper flex-wrapper flex-align-center flex-justify-sb" >
            <div class="gs-left" data-aos="fade-up">
              <h3 class="title-3 bold txt-orange">Stay Connected for Future Events and Classes</h3>
            </div>
              <div class="gs-right">
                <div class="gs-form" data-aos="fade-up">
                  <form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                  <!-- <form> -->
                    <input type="email" class="gs-text" placeholder="Enter Email Address" name="message" id="message" required>
                    <button type="submit" class="gs-btn">Submit</button>
                  </form>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-links-container flex-wrapper flex-align-start flex-justify-sb" data-aos="fade-up">
            <div class="footer-single footer-logo-fls">
              <div class="footer-about-wrapper flex-wrapper flex-align-start flex-justify-sb">
                <div class="fa-left">
                  <img src="<?= base_url()?>assets/images/logo-footer.png" alt="" class="logo-footer">
                </div>
                <div class="fa-right">
                  <div class="about-footer-text txtm txt-white">
                    <p>Follow us on our social media channels to stay updated with our immersive events and more.</p>
                  </div>
                  <div class="footer-social mt16">
                    <a href="https://www.facebook.com/thegloballanguagecenter/" target="_blank"><img src="<?= base_url()?>assets/images/fb-footer.svg" alt=""></a>
                    <a href="https://www.linkedin.com/company/the-global-language-center" target="_blank"><img src="<?= base_url()?>assets/images/ln-footer.svg" alt=""></a>
                    <a href="https://www.instagram.com/thegloballanguagecenter/" target="_blank"><img src="<?= base_url()?>assets/images/in-footer.svg" alt=""></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-single footer-about-fls">
              <ul class="footer-links-list">
                <li><a href="<?php echo base_url();?>home">Home</a></li>
                <li><a href="<?php echo base_url();?>aboutus">About Us</a></li>
                <li><a href="<?php echo base_url();?>immersive_events">Immersive Events</a></li>
                <li><a href="<?= base_url()?>contact_us">Contact Us</a></li>
              </ul>
            </div>
            <div class="footer-single footer-adult-fls">
              <h4 class="txtm txt-white">Languages</h4>
              <ul class="footer-links-list">
                <?php 
                    $condition = "status = 'Active'  ";
                    $getLanguagefooter = $this->common->getData("tbl_language_master",'language_master_id,language_name,slug',$condition); 
                ?>
                <?php foreach ($getLanguagefooter as $key => $value) { 
              ?>
              <!-- <li><a href="<?= base_url('language_home/index?text='.rtrim(strtr(base64_encode("id=".$value['language_master_id']), '+/', '-_'), '=').'')?>"><img src="<?php echo base_url();?>assets/images/arrow-right-white.svg" alt="" class="footer-arrow-icon"> <span><?= $value['language_name']?></span></a></li> -->
              <li><a href="<?= base_url().trim($value['slug']).'/'.trim($value['slug']).'_home'?>"><img src="<?php echo base_url();?>assets/images/arrow-right-white.svg" alt="" class="footer-arrow-icon"><span><?= $value['language_name']?></span></a></li>
              <?php } ?>
              </ul>
            </div>
            <div class="footer-single footer-contact-fls">
              <a href="<?= base_url()?>contact_us" class="btn-fls btn-large btn-fls-primary">Get in Touch</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-legal-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-legal flex-wrapper flex-align-center flex-justify-sb">
            <div class="fl-left txtm txt-white">
              <p>&copy; The Global Language Center</p>
            </div>
            <div class="fl-right txtm txt-white">
              <a href="<?= base_url()?>home/termsCondition" class="txt-white">Terms, Conditions, and Policies</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</footer>
<?php if(!isset($_COOKIE['fls_cookie'])) { ?>
<div class="cookie-container">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="cookie-content">
						<div class="cookie-text">	
							<p>We use cookies to provide you with better experience. By using our website you agree to the TGLC's <br>
								<a href="<?= base_url()?>home/termsCondition">Terms, Conditions and Policies</a> which includes TGLC's practices regarding personal data and cookies.
							</p>	
						</div>
						<div class="cookie-link">
						<a href="#/" class="btn-fls btn-large btn-fls-secondary mr16 reject-cookie">No, Thanks!</a>
						<a href="#/" class="btn-fls btn-large btn-fls-primary accept-cookie">I Agree</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>


<script>
  var vRules = {
      message: {
          required: true
      }
  };
  var vMessages = {
      message: {
          required: "Please enter message"
      }
  };

  $("#form-validate").validate({
      rules: vRules,
      messages: vMessages,
      submitHandler: function (form) {
          var act = "<?php echo base_url();?>home/submitForm";
          // console.log(act);
          $("#form-validate").ajaxSubmit({
              url: act,
              type: 'POST',
              dataType: 'json',
              cache: false,
              clearForm: false,
              beforeSubmit: function (arr, $form, options) {
                  //return false;
              },
              success: function (response) {
                  if (response.success) {
                      $.toast({
                        heading: 'Thank you!',
                        text: response.msg,
                        icon: 'success',
                        loader: false,        // Change it to false to disable loader
                        loaderBg: '#002291',  // To change the background
                        position: 'top-right',
                        stack: false
                      })
                      // alert(response.msg);
                      setTimeout(function () {
                          window.location = "<?php echo base_url();?><?= (!empty($_SESSION['last_state'])?$_SESSION['last_state']:"home")?>";
                      }, 2000);
                  } else {
                        $.toast({
                          heading: 'Error!',
                          text: response.msg,
                          icon: 'error',
                          loader: false,        // Change it to false to disable loader
                          loaderBg: '#bd0202',  // To change the background
                          position: 'top-right',
                          stack: false
                        })
                      // alert(response.msg);
                  }
              }
          });
      }
  });

  $(document).on("click",".accept-cookie",function () {
		$.ajax({
		url: "<?= base_url('home/cookies')?>",
		type: "POST",
		// data:{ [csrfName]: csrfHash },
		dataType: "json",
			success: function(response){
				// location.reload();
			}
		}); 
	});

  $('.accept-cookie,.reject-cookie').click(function(){
			$('.cookie-container').slideToggle();
			/*$('.cookie-overlay').fadeOut();*/
		})	
</script>


