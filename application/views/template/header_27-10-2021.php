<!DOCTYPE html>
<html>
<head>
  <style>
    .error{
      color:red;
    }
  </style>
  <meta charset="UTF-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no">
	<title>The French Lan</title>
	<link rel="icon" href="<?= base_url()?>assets/images/favicon.png" type="images/png" sizes="32x32">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/owl.carousel.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/animate.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/aos.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/main.css" />
	<link rel="stylesheet" href="<?= base_url()?>assets/css/responsive.css" />
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">


  
  <script src="<?php echo base_url();?>assets/js/core/jquery-2.1.4.min.js"></script>
  <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
  <script src="<?= base_url()?>assets/js/owl.carousel.min.js"></script> 
  <script src="<?= base_url()?>assets/js/jquery.fancybox.min.js"></script>
  <script src="<?= base_url()?>assets/js/jquery.nice-select.min.js"></script>
  <script src="<?= base_url()?>assets/js/aos.js"></script>
  <script src="<?= base_url()?>assets/js/script.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
</head>

<header class="fls-nav-container">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="fls-nav-wrapper">
          <div class="nav-logo">
            <a href="index.php"><img src="<?= base_url()?>assets/images/logo.svg" alt=""></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarfls" aria-controls="navbargepl" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="nav-menu">
            <nav class="navbar navbar-expand-lg navbar-fls">
              <div class="collapse navbar-collapse" id="navbarfls">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link <?= ($this->uri->segment(1) == "home")?"active":"";?>" href="<?php echo base_url();?>home">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?= ($this->uri->segment(1) == "aboutus")?"active":"";?>" href="<?php echo base_url();?>aboutus">About Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?= ($this->uri->segment(1) == "immersive_events")?"active":"";?>" href="<?php echo base_url();?>immersive_events">Immersive Events</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?= ($this->uri->segment(1) == "contact_us")?"active":"";?>" href="<?= base_url()?>contact_us">Contact Us</a>
                  </li>
                  <li class="nav-item language-mobile">
                    <a class="nav-link" href="#/">Select Language to Learn</a>
                  </li>
                  <li class="nav-item my-account-mobile">
                    <a class="nav-link" href="#/">My Account</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
          <div class="nav-language">
            <?php 
                $condition = "status = 'Active'  ";
                $getLanguage = $this->common->getData("tbl_language_master",'language_master_id,language_name',$condition); 
            ?>	
            <select class="nav-language-select" id="dynamic_select">
              <option value="">Select language to learn</option>
              <?php foreach ($getLanguage as $key => $value) { 
                $sel='';
                if(!empty($_SESSION['language_id'])){
                  $sel= ($value['language_master_id'] == $_SESSION['language_id'] )?'selected':'';
                }
              ?>
              ?>
                <option value="<?= base_url('language_home/index?text='.rtrim(strtr(base64_encode("id=".$value['language_master_id']), '+/', '-_'), '=').'')?>" <?=$sel?> ><?= $value['language_name']?></option>
              <?php } ?>
            </select>
          </div>
          <script>
                $(function(){
                  // bind change event to select
                  $('#dynamic_select').on('change', function () {
                      var url = $(this).val(); // get selected value
                      if (url) { // require a URL
                          window.location = url; // redirect
                      }
                      return false;
                  });
                });
          </script>
          <div class="nav-account">
            <a href="#/" class="account-link">My Account <img src="<?= base_url()?>assets/images/user.svg" alt="" class="user-img"></a>
           <?php if(!empty($_SESSION['fls_user'][0]['user_id']) ){ ?>
            <div class="my-account-popup nav-fls-dropdown">
              <a href="<?= base_url()?>profile/myAccount">My Profile</a>
              <a href="<?= base_url()?>profile/bookedEvents">Booked Events</a>
              <a href="<?= base_url()?>profile/bookedClass">Booked Classes</a>
              <a href="<?= base_url()?>login_register/logout">Log Out</a>
            </div>
            <?php }else{ ?>
              <div class="my-account-popup nav-fls-dropdown">
                <a href="<?= base_url()?>login_register">Sign in</a>
              </div>
           <?php }?>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<div class="popup-overlay"></div>
<!-- My Account Mobile-->
<?php if(!empty($_SESSION['fls_user'][0]['user_id']) ){ ?>
<div class="my-account-popup-mobile nav-fls-dropdown">
  <a href="<?= base_url()?>profile/myAccount">My Profile</a>
  <a href="<?= base_url()?>profile/bookedEvents">Booked Events</a>
  <a href="<?= base_url()?>profile/bookedClass">Booked Classes</a>
  <a href="<?php echo base_url();?>login_register">Log Out</a>
</div>
<?php 
}else{ ?>
    <div class="my-account-popup-mobile nav-fls-dropdown">
      <a href="<?= base_url()?>login_register">Sign in</a>
    </div>
  <?php }?>
<!-- My Account Mobile-->
<!-- Language Mobile-->
<div class="language-popup-mobile nav-fls-dropdown">
  <a href="#/">English</a>
  <a href="#/">French</a>
  <a href="#/">Spanish</a>
  
</div>
<!-- Language Mobile-->
<div class="content-push"></div>

