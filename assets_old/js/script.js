$(document).ready(function () {
  /*Toggle contact popup*/
  const $menu = $('.nav-account');
  $(document).mouseup(e => {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
      && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu.children('.my-account-popup').removeClass('is-active');
    }
  });
  $('.account-link').on('click', () => {
    $menu.children('.my-account-popup').toggleClass('is-active');
  });
  /*Toggle contact popup*/
  /*Toggle between hamburger and close icon*/
  $('.navbar-toggler').click(function () {
    $(this).toggleClass('open');
  })
  /*Toggle between hamburger and close icon*/
  /*My account popup*/
  $('.my-account-mobile a').click(function () {
    $('.popup-overlay,.my-account-popup-mobile').fadeIn();
  })
  $('.popup-overlay').click(function () {
    $(this).fadeOut();
    $('.tls-popup-contact,.my-account-popup-mobile').fadeOut()
  })
  /*My account popup*/
  /*Homepage programs equal height*/
  if ($(window).width() > 767) {
    var maxHeight = 0;
    $(".hp-single div.txtm").each(function () {
      if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
    });
    $(".hp-single div.txtm").outerHeight(maxHeight);

    var maxHeight2 = 0;
    $(".hp-single h3").each(function () {
      if ($(this).outerHeight() > maxHeight2) { maxHeight2 = $(this).outerHeight(); }
    });
    $(".hp-single h3").outerHeight(maxHeight2);
  }
  /*Homepage programs equal height*/
  /*Testimonial carousel*/
  $('.testimonial-slider').owlCarousel({
    animateOut: 'animate__fadeOutLeft',
    animateIn: 'animate__fadeInRight',
    items: 1,
    margin: 70,
    stagePadding: 0,
    loop: true,
    nav: true,
    navText: ["<img src='assets/images/arrow-left-outline-blue.svg'>", "<img src='assets/images/arrow-right-filled-blue.svg'>"],
    dots: false,
  });

  /*Testimonial carousel*/
  /*Private tuitions read more*/
  $('.ptl-read-more-link').click(function () {
    $(this).siblings('.ptl-content').toggleClass('read-more');
  })
  /*Private tuitions read more*/
  $('.gsd-count').niceSelect();
  /*Floating labels*/
  var formFields = $('.form-group');

  formFields.each(function () {
    var field = $(this);
    var input = field.find('input');
    var textarea = field.find('textarea');
    var label = field.find('label');

    function checkInput() {
      var valueLength = input.val().length;

      if (valueLength > 0) {
        label.addClass('freeze')
      } else {
        label.removeClass('freeze')
      }
    }
    function checkTextarea() {
      var valueLength2 = textarea.val().length;

      if (valueLength2 > 0) {
        label.addClass('freeze')
      } else {
        label.removeClass('freeze')
      }
    }

    input.change(function () {
      checkInput()
    })
    textarea.change(function () {
      checkTextarea()
    })
  });
  /*Floating labels*/
  /*Login toggle*/
  $('.fp-link').click(function(){
    $('.login-form,.register-form').hide();
    $('.forgot-password-form').show();

  })
  $('.login-link').click(function(){
    $('.forgot-password-form,.register-form').hide();
    $('.login-form').show();    
  })
  $('.create-account-link').click(function(){
    $('.login-form,.forgot-password-form').hide();  
    $('.register-form').show();      
  })
  /*Login toggle*/
  /*Tabs toggle*/
  $('.tabs-list li a').on('click', function () {   
    $('.tabs-list li a').removeClass('active');
    $(this).addClass('active');
    var tab_value = $(this).attr('data-tab');
    $('.tabs-single').hide();
    $('.tabs-single.' + tab_value).show();  
  })
  /*Tabs toggle*/
  /*Checkout registration*/
  $('.rs-title a.remove-reg').on('click',function(){
    $(this).parent('.rs-title').parent('.reg-single').remove()
  })
  $('.arrow-reg').on('click',function(){
    $(this).parent('.rs-title').siblings('.reg-form').toggle();
    $(this).toggleClass('rotate-icon')
  });
  /*Checkout registration*/
  AOS.init({
    delay: 100,
    duration: 500,
  });
});
